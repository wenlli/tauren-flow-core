﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.Extensions.DependencyInjection;

namespace Tauren.Flow.Core.ConfigExtension
{
    /// <summary>
    /// 版本配置类
    /// </summary>
    public class ApiVersionConfig
    {
        /// <summary>
        /// 配置api的版本
        /// </summary>
        /// <param name="services"></param>
        public static void AddApiVersioning(IServiceCollection services)
        {

            services.AddApiVersioning(opt =>
            {
                opt.ReportApiVersions = true;
                opt.AssumeDefaultVersionWhenUnspecified = true;
                opt.DefaultApiVersion = ApiVersion.Default;
                //opt.ApiVersionReader = new HeaderApiVersionReader("x-api-version");///只在header 中添加版本
                //opt.ApiVersionReader = ApiVersionReader.Combine(new QueryStringApiVersionReader(), new HeaderApiVersionReader() { HeaderNames = { "api-version" } });///header和QueryString中都添加版本
            });
        }
    }
}
