﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Tauren.Flow.Entity.Config;

namespace Tauren.Flow.Core.ConfigExtension
{
    public class RegisterConfigure
    {
        public static void RegisterConfigures(IApplicationBuilder app, IHostEnvironment env, IConfiguration configuration, Microsoft.AspNetCore.Hosting.IApplicationLifetime lifetime)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            var ApiConfig = configuration.Get<ApiVersionsConfig>();
            FilePath path = ApiConfig.FilePath;

            // app.RegisterConsul(ApiConfig.Consul);
            //app.RegisterZipKinTrace(new LoggerFactory(), lifetime);
            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors(builder =>
            {
                builder.AllowAnyHeader();
                builder.AllowAnyMethod();
                //允许所有的来源地址跨域访问
                builder.AllowAnyOrigin();
            });
            app.UseAuthentication();//配置授权
            app.UseAuthorization();
            app.UseSwagger();


            ///使用静态文件目录
            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(path.PhysicalFilePath),
                RequestPath = path.ApiFilePath
            });

            ///使用swagger UI
            app.UseSwaggerUI(c =>
            {
                ApiConfig.ApiVersions.ForEach(a =>
                {
                    c.SwaggerEndpoint(string.Format("/swagger/v{0}/swagger.json", a.version), string.Format("Bullhead management system  interface document--flow v{0}", a.version));
                });
                c.RoutePrefix = string.Empty;
            });
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            app.UseHealthChecks("/health");
            app.UseApiVersioning();
        }
    }
}
