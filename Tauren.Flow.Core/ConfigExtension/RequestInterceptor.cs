﻿using Castle.DynamicProxy;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Tauren.Flow.DLL.System_Log;
using Tauren.Flow.DLL.TenantInformation;
using Tauren.Flow.Entity.Const;
using Tauren.Flow.Entity.Enum;
using Tauren.Flow.Entity.Global;
using Tauren.Flow.Entity.Input;
using Tauren.Flow.Entity.Model;
using Tauren.Flow.Infrastructure.Common;
using Tauren.Flow.Service.System_Log;

namespace Tauren.Flow.Core.ConfigExtension
{
    /// <summary>
    /// AOP拦截
    /// </summary>
    public class RequestInterceptor : IInterceptor
    {
        private readonly IHttpContextAccessor _accessor;
        private ISystemLogService logService;
        private ITenantInformationBusiness tenantBusinese;
        private readonly string[] ExceptMethod = new string[] {
            "Tauren.Flow.service.system_log.systemlogservice"
            , "Tauren.Flow.dll.system_log.isystemlogbusiness",
            "Tauren.Flow.service.system_log.isystemlogservice"
            , "Tauren.Flow.dll.system_log.systemlogbusiness" ,
            "Tauren.Flow.service.fileversionbll.trackfile",
            "Tauren.Flow.service.fileversionbll.trackfileservice",
            "Tauren.Flow.service.filebll.downloadservice"
        };
        private readonly string[] ExceptMethod2 = new string[] {
            "Tauren.Flow.dll.tenantinformation.itenantinformationbusiness"
            ,"Tauren.Flow.dll.tenantinformation.tenantinformationbusiness"  };

        private readonly string[] LogInMethod = new string[] { };
        /// <summary>
        /// 全局会话
        /// </summary>
        public GlobalModel Global { get; set; } = new GlobalModel();
        /// <summary>
        /// 构造哈数
        /// </summary>
        /// <param name="accessor"></param>
        public RequestInterceptor(IHttpContextAccessor accessor, IDbConnection _dbConnection)
        {
            _accessor = accessor ?? throw new ArgumentNullException(nameof(accessor));
            if (_accessor.HttpContext != null)
            {
                if (_accessor.HttpContext.Items.ContainsKey(GlobalConst.GolbalData) && !string.IsNullOrEmpty(_accessor.HttpContext.Items[GlobalConst.GolbalData] + ""))
                {
                    Global = JsonConvert.DeserializeObject<GlobalModel>(_accessor.HttpContext.Items[GlobalConst.GolbalData] + "");
                }
                Global.Language = (_accessor == null) ? "zh-cn" : string.IsNullOrEmpty(_accessor.HttpContext.Request.Headers["language"] + "") ? "zh-cn" : _accessor.HttpContext.Request.Headers["language"] + "";
            }
            logService = new SystemLogService(new SystemLogBusiness(_dbConnection));
            tenantBusinese = new TenantInformationBusiness(_dbConnection);

        }
        /// <summary>
        /// 进行拦截
        /// </summary>
        /// <param name="invocation"></param>
        public void Intercept(IInvocation invocation)
        {


            SystemLog model = GetSystemLog(invocation);
            try
            {
                //调用下一个拦截器直到目标方法
                invocation.Proceed();

                if (ExceptMethod.Any(o => o == invocation.TargetType.FullName.ToLower()) || (ExceptMethod2.Any(o => o == invocation.TargetType.FullName.ToLower())) && invocation.Method.Name.ToLower() == "getbycode") return;

                //判断是否为异步方法
                if (IsAsyncMethod(invocation.Method))
                {
                    var type = invocation.Method.ReturnType;
                    var resultProperty = type.GetProperty("Result");
                    if (resultProperty == null) return;
                    var result = resultProperty.GetValue(invocation.ReturnValue);
                    DistinguishLogModel(model, JsonConvert.SerializeObject(result), invocation);
                }
                else//同步方法
                {
                    DistinguishLogModel(model, JsonConvert.SerializeObject(invocation.ReturnValue), invocation);
                }


            }
            catch (Exception ex)
            {
                DistinguishLogModel(model, $"【出现异常】：{ex.Message + ex.InnerException}", invocation, LogType.Error);
                throw ex;
            }
        }

        //判断是否为异步方法
        private bool IsAsyncMethod(System.Reflection.MethodInfo method)
        {
            return method.ReturnType == typeof(Task) ||
                   method.ReturnType.IsGenericType && method.ReturnType.GetGenericTypeDefinition() == typeof(Task<>);
        }

        /// <summary>
        /// 初始化类
        /// </summary>
        /// <param name="invocation"></param>
        /// <returns></returns>
        private SystemLog GetSystemLog(IInvocation invocation)
        {
            SystemLog model = new SystemLog();
            model.IPAddress = $"{_accessor.HttpContext.Connection.RemoteIpAddress + ""}:{_accessor.HttpContext.Connection.RemotePort + ""}";
            model.ServerAddress = $"{_accessor.HttpContext.Connection.LocalIpAddress + ""}:{_accessor.HttpContext.Connection.LocalPort + ""}";
            model.InterfacePath = $"{_accessor.HttpContext.Connection.LocalIpAddress + ""}:{_accessor.HttpContext.Connection.LocalPort + ""}{_accessor.HttpContext.Request.Path.ToString()}";
            model.ClientPath = _accessor.HttpContext.Request.Headers.ContainsKey("Referer") ? _accessor.HttpContext.Request.Headers["Referer"].ToString() : _accessor.HttpContext.Request.Headers.ContainsKey("Origin") ? _accessor.HttpContext.Request.Headers["Origin"].ToString() : "";
            model.UserAgent = _accessor.HttpContext.Request.Headers.ContainsKey("User-Agent") ? _accessor.HttpContext.Request.Headers["User-Agent"].ToString() : "";
            model.RequestParamtetr = JsonConvert.SerializeObject(invocation.Arguments);
            model.Creatdate = DateTime.Now;
            model.Modifdate = DateTime.Now;
            model.ExecuteClass = invocation.TargetType.FullName + "";
            model.ExecuteMethod = invocation.Method.Name + "";
            model.ExecuteParameter = JsonConvert.SerializeObject(invocation.Arguments);
            model.ExecuteDate = DateTime.Now;
            return model;
        }

        /// <summary>
        /// 判断是否是登录信息
        /// </summary>
        /// <param name="MethodName"></param>
        /// <returns></returns>
        private bool IsLogInMethod(string MethodName)
        {
            return LogInMethod.Any(o => o == MethodName.ToLower());
        }

        /// <summary>
        /// 进行日志添加
        /// </summary>
        /// <param name="model"></param>
        /// <param name="result"></param>
        /// <param name="invocation"></param>
        /// <param name="type"></param>
        private void DistinguishLogModel(SystemLog model, string result, IInvocation invocation, LogType type = LogType.Info)
        {
            if (IsLogInMethod(invocation.TargetType.FullName))
            {
                if (type == LogType.Info) type = LogType.LogInInfo;
                else type = LogType.LogInError;
                Parallel.For(0, 1, e =>
                {
                    GenerateInLoginModel(model, invocation, JsonConvert.SerializeObject(result), (int)type);
                });
            }
            else
            {
                Parallel.For(0, 1, e =>
                {
                    GenerateNotInLoginModel(model, JsonConvert.SerializeObject(result), (int)type);
                });
            }
        }
        /// <summary>
        /// 不是登录时的日志
        /// </summary>
        /// <param name="model"></param>
        /// <param name="content"></param>
        /// <param name="type"></param>
        private void GenerateNotInLoginModel(SystemLog model, string content, int type)
        {
            model.Account = Global.Account;
            model.CNName = Global.EmpName;
            model.Code = Global.TenementCode;
            model.Creator = Global.EmpId;
            model.EmpId = Global.EmpId;
            model.ENName = Global.EmpEnName;
            model.ExecuteCompletiontime = DateTime.Now;
            model.ExecuteResult = content;
            model.Language = Global.Language;
            model.LogType = type;
            model.Modifier = Global.EmpId;
            model.OrgId = Global.OrgId;
            model.OrgName = Global.OrgName;
            model.Phone = Global.TenementPhone;
            model.PositionId = Global.PositionId;
            model.PositionName = Global.PostionName;
            model.TCnname = Global.TenementName;
            model.UserId = Global.UserId;
            model.EmpCode = Global.EmpCode;
            logService.Add(model);
        }
        /// <summary>
        /// 不是登录时的日志
        /// </summary>
        /// <param name="model"></param>
        /// <param name="content"></param>
        /// <param name="type"></param>
        private void GenerateInLoginModel(SystemLog model, IInvocation invocation, string content, int type)
        {
            var body = _accessor.HttpContext.Request.Body;
            string code = "";
            LogInUser logInUser = GetLogInModel(invocation, out code);
            Entity.Model.TenantInformation tenantInformation = tenantBusinese.GetByCode(code);

            model.Account = logInUser.Account;
            model.Code = code;
            model.ExecuteCompletiontime = DateTime.Now;
            model.ExecuteResult = content;
            model.Language = Global.Language;
            model.LogType = type;
            if (tenantInformation != null)
            {
                model.CNName = "";
                model.Creator = -1;
                model.EmpId = -1;
                model.ENName = "";
                model.Modifier = -1;
                model.OrgId = -1;
                model.OrgName = "";
                model.Phone = tenantInformation.Phone;
                model.PositionId = -1;
                model.PositionName = "";
                model.TCnname = tenantInformation.CNName;
                model.UserId = -1;
                model.EmpCode = "";
            }
            logService.Add(model);
        }

        private LogInUser GetLogInModel(IInvocation invocation, out string code)
        {
            List<LogInModel> models = JsonConvert.DeserializeObject<List<LogInModel>>(JsonConvert.SerializeObject(invocation.Arguments));
            LogInModel logInModel = models.FirstOrDefault();
            string UserInfo = logInModel.Content.AESDecrypt(logInModel.Key);
            code = logInModel.TenantCode;
            return UserInfo.ToEntity<LogInUser>();
        }
    }
}
