﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tauren.Flow.Entity.Global;
using Tauren.Flow.Entity.Model;
using Tauren.Flow.Infrastructure.Common;
using Tauren.Flow.Service.FlowCenterBLL;
using Tauren.Flow.Service.FlowSortBLL;

namespace Tauren.Flow.Core.Controllers
{
    /// <summary>
    /// 流程中心
    /// </summary>
    [Route("api/flowcenter")]
    [ApiController]
    [ApiVersion("1.0")]
    [ApiVersion("2.0")]
    public class FlowCenterController : ControllerBase
    {
        private IFlowSortService sortService;
        private IFlowFromService centerService;
        private IFlowFormEventServices eventServices;
        private IFlowCenterService flowCenterService;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="_sortService"></param>
        /// <param name="_centerService"></param>
        /// <param name="_eventServices"></param>
        /// <param name="_flowCenterService"></param>
        public FlowCenterController(IFlowSortService _sortService, IFlowFromService _centerService, IFlowFormEventServices _eventServices, IFlowCenterService _flowCenterService)
        {
            this.sortService = _sortService;
            this.centerService = _centerService;
            this.eventServices = _eventServices;
            this.flowCenterService = _flowCenterService;
        }
        /// <summary>
        /// 新增数据
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost, Route("v{version:apiVersion}/save")]
        public IActionResult Save([FromBody] List<WorkFlowInfoSort> input) => sortService.Add(input).ToJsonResult();

        /// <summary>
        /// 获取所有流程工作台
        /// </summary> 
        /// <returns></returns>
        [HttpPost, Route("v{version:apiVersion}/flows")]
        public IActionResult GetFlows() => sortService.GetFlows().ToJsonResult();

        /// <summary>
        /// 获取表单字段配置
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost, Route("v{version:apiVersion}/formconfig")]
        public IActionResult GetFormConfig([FromBody] FlowParameter input) => centerService.FlowFormFieldConfig(input).ToJsonResult();
        /// <summary>
        /// 获取表单字段配置
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost, Route("v{version:apiVersion}/formdata")]
        public IActionResult GetFormData([FromBody] FlowParameter input) => centerService.FlowFromData(input).ToJsonResult();

        /// <summary>
        /// 获取表单字段配置
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost, Route("v{version:apiVersion}/formsave")]
        public IActionResult FormSaveData([FromBody] FlowRquestParameter input) => eventServices.FormSave(input).ToJsonResult();

        /// <summary>
        /// 清除重复提交key
        /// </summary>
        /// <param name="key"></param>
        /// <param name="taskId"></param>
        /// <returns></returns>
        [HttpPost, Route("v{version:apiVersion}/clearrepeatkey/{key}/{taskId}")]
        public IActionResult ClearRepeat([FromRoute] string key, [FromRoute] string taskId) => eventServices.ClearRepeatKey(key, taskId).ToJsonResult();

        /// <summary>
        /// 分页获取流程信息列表
        /// </summary>
        /// <param name="queryModel"></param>
        /// <returns></returns>
        [HttpPost, Route("v{version:apiVersion}/pages")]
        public IActionResult Pages([FromBody] Entity.Global.QueryModel queryModel) => flowCenterService.Pages(queryModel).ToJsonResult();

        /// <summary>
        /// 获取流程图
        /// </summary>
        /// <param name="flowId"></param>
        /// <param name="flowversionId"></param>
        /// <param name="taskId"></param>
        /// <returns></returns>
        [HttpPost, Route("v{version:apiVersion}/diagram/{flowId}/{flowversionId}/{taskId}")]
        public IActionResult Diagram([FromRoute] long flowId, [FromRoute] long flowversionId, [FromRoute] long taskId) => flowCenterService.Diagram(flowId, flowversionId, taskId).ToJsonResult();

        /// <summary>
        /// 分页获取流程信息列表
        /// </summary>
        /// <param name="queryModel"></param>
        /// <returns></returns>
        [HttpPost, Route("v{version:apiVersion}/hpages")]
        public IActionResult IndexPages([FromBody] Entity.Global.QueryModel queryModel) => flowCenterService.GetIndexPages(queryModel).ToJsonResult();
        /// <summary>
        /// 获取流历史
        /// </summary>
        /// <param name="flowId"></param>
        /// <param name="flowversionId"></param>
        /// <param name="taskId"></param>
        /// <returns></returns>
        [HttpPost, Route("v{version:apiVersion}/history/{flowId}/{flowversionId}/{taskId}")]
        public IActionResult FlowHistory([FromRoute] long flowId, [FromRoute] long flowversionId, [FromRoute] long taskId) => flowCenterService.FlowHistory(flowId, flowversionId, taskId).ToJsonResult();
    }
}
