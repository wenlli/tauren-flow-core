﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Tauren.Flow.Entity.Global;
using Tauren.Flow.Infrastructure.Common;
using Tauren.Flow.Service.FlowCenterBLL;
using Tauren.Flow.Service.FlowEngineBLL;

namespace Tauren.Flow.Core.Controllers
{
    /// <summary>
    /// 引擎执行接口
    /// </summary>
    [Route("api/flowengine")]
    [ApiController]
    [ApiVersion("1.0")]
    [ApiVersion("2.0")]
    public class FlowEngineController : ControllerBase
    {
        private IFlowNextTaskInfoEventService infoEventService;
        private IFlowEngineService flowEngineService;
        /// <summary>
        /// 引擎执行接口
        /// </summary>
        public FlowEngineController(IFlowNextTaskInfoEventService _infoEventService, IFlowEngineService _flowEngineService)
        {
            infoEventService = _infoEventService;
            flowEngineService = _flowEngineService;
        }
        /// <summary>
        /// 获取下一节点信息
        /// </summary>
        [HttpPost, Route("v{version:apiVersion}/nextnode")]
        public IActionResult NextNodeInfo([FromBody] FlowRquestParameter Paramter) => infoEventService.NextNodeInfos(Paramter).ToJsonResult();

        /// <summary>
        /// 进行待办任务处理
        /// </summary>
        [HttpPost, Route("v{version:apiVersion}/approval")]
        public IActionResult ProcessApproval([FromBody] FlowParameter Paramter) => flowEngineService.FlowEngineRunEvent(Paramter).ToJsonResult();

        /// <summary>
        /// 任务进行回收
        /// </summary>
        /// <param name="taskId">当前任务ID</param>
        /// <returns></returns>
        [HttpPost, Route("v{version:apiVersion}/recycling/{taskId}")]
        public IActionResult FlowRecycling([FromRoute] long taskId) => flowEngineService.FlowRecycling(taskId).ToJsonResult();

        /// <summary>
        /// 终止流程
        /// </summary>
        /// <param name="taskId">当前任务ID</param>
        /// <returns></returns>
        [HttpPost, Route("v{version:apiVersion}/termination")]
        public IActionResult FlowTermination([FromBody] RequestParameter Parameter) => flowEngineService.FlowTermination(Parameter).ToJsonResult();

        /// <summary>
        /// 传阅和抄送任务读办
        /// </summary>
        /// <param name="taskId">当前任务ID</param>
        /// <returns></returns>
        [HttpPost, Route("v{version:apiVersion}/read")]
        public IActionResult FlowRead([FromBody] RequestParameter Parameter) => flowEngineService.FlowRead(Parameter).ToJsonResult();

        /// <summary>
        /// 获取传阅下一节点信息
        /// </summary>
        [HttpPost, Route("v{version:apiVersion}/circulatednode")]
        public IActionResult CirculatedNextNodeInfo([FromBody] FlowRquestParameter Paramter) => infoEventService.CirculatedNextNodeInfo(Paramter).ToJsonResult();
        /// <summary>
        /// 进行传阅任务处理
        /// </summary>
        [HttpPost, Route("v{version:apiVersion}/circulated")]
        public IActionResult ProcessCirculated([FromBody] FlowParameter Paramter) => flowEngineService.FlowEngineCirculatedEvent(Paramter).ToJsonResult();

        /// <summary>
        /// 退回任务
        /// </summary>
        /// <param name="taskId">当前任务ID</param>
        /// <returns></returns>
        [HttpPost, Route("v{version:apiVersion}/back")]
        public IActionResult FlowTaskBack([FromBody] RequestParameter Paramter) => flowEngineService.FlowTaskBack(Paramter).ToJsonResult();

        /// <summary>
        /// 退回任务
        /// </summary>
        /// <param name="taskId">当前任务ID</param>
        /// <returns></returns>
        [HttpPost, Route("v{version:apiVersion}/delete/{taskId}")]
        public IActionResult FlowTaskDelete([FromRoute] long taskId) => flowEngineService.FlowTaskDelete(taskId).ToJsonResult();

        /// <summary>
        /// 获取退回到的节点
        /// </summary>
        /// <param name="taskId">当前任务ID</param>
        /// <returns></returns>
        [HttpPost, Route("v{version:apiVersion}/backnodeinfo/{taskId}")]
        public IActionResult FlowGetNodeInfo([FromRoute] long taskId) => infoEventService.BackStepNodeInfo(taskId).ToJsonResult();
        /// <summary>
        /// 获取退回到的节点
        /// </summary>
        /// <param name="Paramter">任务信息</param>
        /// <returns></returns>
        [HttpPost, Route("v{version:apiVersion}/skipnodeinfo/")]
        public IActionResult SkipNodeInfo([FromBody] FlowRquestParameter Paramter) => infoEventService.SkipNodeInfo(Paramter).ToJsonResult();

        /// <summary>
        /// 退回至具体的节点
        /// </summary>
        /// <param name="Paramter">任务信息</param>
        /// <returns></returns>
        [HttpPost, Route("v{version:apiVersion}/backstep/")]
        public IActionResult BackStep([FromBody] FlowParameter Paramter) => flowEngineService.FlowTaskBackStep(Paramter).ToJsonResult();


    }
}
