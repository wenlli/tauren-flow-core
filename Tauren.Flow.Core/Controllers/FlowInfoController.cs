﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tauren.Flow.Entity.Model;
using Tauren.Flow.Entity.Output;
using Tauren.Flow.Infrastructure.Common;
using Tauren.Flow.Service.FlowInfoBLL;

namespace Tauren.Flow.Core.Controllers
{
    /// <summary>
    /// 流程基本信息
    /// </summary>
    [Route("api/flowinfo")]
    [ApiController]
    [ApiVersion("1.0")]
    [ApiVersion("2.0")]
    public class FlowInfoController : ControllerBase
    {
        private IFlowInfoService service;
        private IFlowInstanceService flowInstanceService;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="_service"></param>
        public FlowInfoController(IFlowInfoService _service, IFlowInstanceService _flowInstanceService)
        {
            this.service = _service;
            this.flowInstanceService = _flowInstanceService;
        }

        /// <summary>
        /// 分页获取流程信息列表
        /// </summary>
        /// <param name="queryModel"></param>
        /// <returns></returns>
        [HttpPost, Route("v{version:apiVersion}/pages")]
        public IActionResult Pages([FromBody] Entity.Global.QueryModel queryModel) => service.Pages(queryModel).ToJsonResult();

        /// <summary>
        /// 新增数据
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost, Route("v{version:apiVersion}/add")]
        public IActionResult Add([FromBody] Entity.Input.FlowInfo_InPut input) => service.Add(input).ToJsonResult();

        /// <summary>
        /// 删除信息
        /// </summary>
        /// <param name="id">实体id</param>
        /// <returns></returns>
        [HttpPost, Route("v{version:apiVersion}/del/{id}")]
        public IActionResult Delete([FromRoute] int id) => service.Delete(id).ToJsonResult();

        /// <summary>
        /// 卸载
        /// </summary>
        /// <param name="id">ID</param> 
        /// <returns></returns>
        [HttpPost, Route("v{version:apiVersion}/uninstall/{id}")]
        public IActionResult Uninstall(int id) => service.Uninstall(id).ToJsonResult();
        /// <summary>
        /// 卸载
        /// </summary>
        /// <param name="id">ID</param> 
        /// <returns></returns>
        [HttpPost, Route("v{version:apiVersion}/get/{id}")]
        public IActionResult FirstOrDefault(int id, int versionId = 0) => service.FirstOrDefault(id, versionId).ToJsonResult();

        /// <summary>
        /// 保存流程数据
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost, Route("v{version:apiVersion}/save")]
        public IActionResult Save([FromBody] Entity.Output.Flow_OutPut input) => service.SaveFlow(input).ToJsonResult();

        /// <summary>
        /// /获取表单最新的字段
        /// </summary>
        /// <param name="FormVersionId">表单版本ID</param>
        /// <param name="FlowVersionId">流程版本ID</param>
        /// <param name="StepId">步骤ID</param>
        /// <returns></returns>
        [HttpPost, Route("v{version:apiVersion}/stepfields/{FormVersionId}/{FlowVersionId}/{StepId}")]
        public IActionResult FlowStepFields([FromRoute] long FormVersionId, [FromRoute] long FlowVersionId, [FromRoute] string StepId) => service.FlowStepFields(FormVersionId, FlowVersionId, StepId).ToJsonResult();

        /// <summary>
        /// 保存流程数据
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost, Route("v{version:apiVersion}/publish")]
        public IActionResult Publish([FromBody] Entity.Output.Flow_OutPut input) => service.PublishFlow(input).ToJsonResult();
        /// <summary>
        /// 流程人员
        /// </summary>
        /// <param name="EmpId"></param>
        /// <returns></returns>
        [HttpPost, Route("v{version:apiVersion}/flowemps/{EmpId}")]
        public IActionResult FlowEmps([FromRoute] string EmpId) => service.GetFlowEmployees(EmpId).ToJsonResult();
        /// <summary>
        /// 流程人员范围
        /// </summary>
        /// <param name="PositionId"></param>
        /// <returns></returns>
        [HttpPost, Route("v{version:apiVersion}/flowpos/{PositionId}")]
        public IActionResult FlowPos([FromRoute] string PositionId) => service.GetFlowPositions(PositionId).ToJsonResult();

        /// <summary> 
        /// /获取快捷显示字段
        /// </summary>
        /// <param name="FlowId">流程ID</param>
        /// <param name="versionId">流程版本ID</param>
        /// <param name="type">字段显示类型</param>
        /// <returns></returns>
        [HttpPost, Route("v{version:apiVersion}/quickfield")]
        public IActionResult QuickDisplayField([FromQuery] long FlowId = 0, [FromQuery] long versionId = 0, [FromQuery] string type = "") => service.QuickDisplayField(FlowId, versionId, type).ToJsonResult();

        /// <summary> 
        /// /获取实例显示字段
        /// </summary> 
        /// <param name="type">显示类型</param>
        /// <returns></returns>
        [HttpPost, Route("v{version:apiVersion}/savedisplyfield/{type}")]
        public IActionResult SaveDisplayField([FromRoute] string type, [FromBody] List<Entity.Model.Flow_Display_Field> Models, [FromQuery] long FlowId = 0, [FromQuery] long versionId = 0) => service.SaveDisplayField(type, Models, FlowId, versionId).ToJsonResult();

        /// <summary> 
        /// 获取表单最新的字段
        /// </summary>
        /// <param name="FormVersionId">表单版本ID</param>
        /// <param name="FlowVersionId">流程版本ID</param> 
        /// <param name="complateField">节点上已有的字段配置</param> 
        /// <returns></returns>
        [HttpPost, Route("v{version:apiVersion}/endfields/{FormVersionId}/{FlowVersionId}")]
        public IActionResult EndFlowStepFields([FromRoute] long FormVersionId, [FromRoute] long FlowVersionId, [FromBody] List<Flow_Complated_Info_OutPut> complateField) => service.EndFlowStepFields(FormVersionId, FlowVersionId, complateField).ToJsonResult();
        /// <summary> 
        /// 获取流程树形结构
        /// </summary> 
        /// <returns></returns>
        [HttpPost, Route("v{version:apiVersion}/flowtree")]
        public IActionResult FlowTree() => flowInstanceService.FlowTree().ToJsonResult();
        /// <summary>
        /// 获取流程实例数据
        /// </summary>
        /// <param name="queryModel"></param>
        /// <returns></returns>
        [HttpPost, Route("v{version:apiVersion}/instance")]
        public IActionResult Instances([FromBody] Entity.Global.QueryModel queryModel) => flowInstanceService.Instance(queryModel).ToJsonResult();

    }
}
