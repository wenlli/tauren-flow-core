﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Tauren.Flow.Core.Controllers
{

    /// <summary>
    /// 心跳服务
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    public class HealthController : ControllerBase
    {
        [HttpGet, Route("get")]
        public IActionResult Get() => Ok("ok");
    }
}
