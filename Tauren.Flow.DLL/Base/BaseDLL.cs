﻿using Dapper;
using Dapper.Contrib.Extensions;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Tauren.Flow.Entity.Global;
using Tauren.Flow.Entity.Model;
using Tauren.Flow.Infrastructure.Globals;

namespace Tauren.Flow.DLL.Base
{
    public class BaseDLL : IBaseDLL
    {
        public BaseDLL(IConnectionDLLBase _dLLBase)
        {
            if (!(_dLLBase is null))
            {
                BusinessConnection = _dLLBase.BusinessConnection;
                ResConnection = _dLLBase.ResConnection;
                Global = _dLLBase.Global;
                BasicDataConnection = _dLLBase.BasicDataConnection;
                MainConnection = _dLLBase.MainConnection;
                FormConnection = _dLLBase.FormConnection;
                FileConnection = _dLLBase.FileConnection;
            }
        }
        /// <summary>
        /// 业务数据链接
        /// </summary>
        protected IDbConnection BusinessConnection { get; set; }
        /// <summary>
        /// 资源数据库链接
        /// </summary>
        protected IDbConnection ResConnection { get; set; }

        /// <summary>
        /// 基础数据库链接
        /// </summary>
        protected IDbConnection BasicDataConnection { get; set; }

        /// <summary>
        /// 租户表链接
        /// </summary>
        protected IDbConnection MainConnection { get; set; }

        /// <summary>
        /// 表单数据库
        /// </summary>
        protected IDbConnection FormConnection { get; set; }
        /// <summary>
        /// 文件链接
        /// </summary>
        protected IDbConnection FileConnection { get; set; }

        /// <summary>
        /// 全局变量
        /// </summary>
        protected GlobalModel Global { get; set; }
        /// <summary>
        /// 添加信息
        /// </summary>
        /// </summary>
        /// <param name="model">实体</param>
        /// <returns></returns>
        public long Add<T>(T model, IDbTransaction transaction = null) where T : class, new()
        {
            return BusinessConnection.Insert<T>(entityToInsert: model, transaction: transaction);
        }
        /// <summary>
        /// 修改信息
        /// </summary>
        /// </summary>
        /// <param name="model">实体</param>
        /// <returns></returns>
        public bool Update<T>(T model, IDbTransaction transaction = null) where T : class, new()
        {
            return BusinessConnection.Update<T>(entityToUpdate: model, transaction: transaction);
        }

        /// <summary>
        /// 删除信息
        /// </summary>
        /// </summary>
        /// <param name="model">实体</param>
        /// <returns></returns>
        public bool Delete<T>(T model, IDbTransaction transaction = null) where T : class, new()
        {
            return BusinessConnection.Delete<T>(entityToDelete: model, transaction: transaction);
        }

        /// <summary>
        /// 获取信息
        /// </summary>
        /// </summary>
        /// <param name="model">实体</param>
        /// <returns></returns>
        public T Get<T>(object id, IDbTransaction transaction = null) where T : class, new()
        {
            return BusinessConnection.Get<T>(id: id, transaction: transaction);
        }

        /// <summary>
        /// 获取单个信息--获取基础数据
        /// </summary>
        /// </summary>
        /// <param name="model">实体</param>
        /// <returns></returns>
        public T Single<T>(object id, IDbTransaction transaction = null) where T : class, new()
        {
            return BusinessConnection.Get<T>(id: id, transaction: transaction);
        }
        /// <summary>
        /// 获取所有数据
        /// </summary>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public List<T> All<T>(IDbTransaction transaction = null) where T : class, new()
        {
            return BusinessConnection.GetAll<T>(transaction: transaction).AsList();
        }

        /// <summary>
        /// 获取所有数据--获取基础数据
        /// </summary>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public List<T> GetAll<T>(IDbTransaction transaction = null) where T : class, new()
        {
            return BasicDataConnection.GetAll<T>(transaction: transaction).AsList();
        }

        public List<ExtButtons> JudgePermissions(string MenuCode)
        {
            if (!Global.IsAdmin)
                return BasicDataConnection.Query<ExtButtons>(@$"select b.*,br.value as label  from menus m 
                                        inner join menus_roles mr on m.menuid = mr.menuid
                                        inner join rolepermission re on re.roleid = mr.roleid
                                        inner join rolepersoninfo rp on rp.roleid = mr.roleid
                                        inner join buttons_permission bp on bp.permissionid = re.permissionid
                                        left join  buttons b on b.btnid = bp.btnid
                                        left join buttonres br on b.btnid = br.btnid and br.`language` = '{Global.Language}'
                                        where m.code = '{MenuCode}' and rp.empid = {Global.EmpId}").AsList();
            else
            {
                return BasicDataConnection.Query<ExtButtons>(@$"select b.*,br.value as label from buttons b  
                                        left join buttonres br on b.btnid = br.btnid and br.`language` = '{Global.Language}'
                                        where b.isuseflow =0").AsList();
            }
        }
        /// <summary>
        /// 判断是否是系统管理员
        /// </summary>
        /// <param name="EmpId"></param>
        /// <returns></returns>
        public bool IsCurrentRoleByAdmin(int EmpId)
        {
            bool b = false;
            var roles = BasicDataConnection.Query<Entity.Model.Roleinfo>($"select r.* from roleinfo r inner join rolepersoninfo p on p.roleid =r.autoid where p.empid={EmpId} and r.code='admin'");
            if (roles.Any())
            {
                b = true;
            }
            return b;
        }
        public void BuliderDbConnection(string Connections)
        {
            this.BusinessConnection = new MySqlConnection(Connections);
        }

        public T GetByCode<T>(string code) where T : class, new() => MainConnection.QueryFirstOrDefault<T>($"SELECT * FROM {typeof(T).Name} WHERE code='{code}'");

        /// <summary>
        /// 根据条件和条件值获取基础库中的数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="queryField"></param>
        /// <param name="queryFieldValue"></param>
        /// <returns></returns>
        public List<T> GetBasicListByQuery<T>(string queryField, string queryFieldValue) where T : class, new() => BasicDataConnection.Query<T>($"SELECT * FROM {typeof(T).Name} WHERE {queryField} in ({queryFieldValue})").AsList();
        /// <summary>
        /// 根据表名动态获取数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="queryField"></param>
        /// <param name="queryFieldValue"></param>
        /// <returns></returns>
        public List<dynamic> GetBasicListByQuery(string queryField, string queryFieldValue, string queryTableCode) => BasicDataConnection.Query<dynamic>($"SELECT * FROM {queryTableCode} WHERE {queryField} in ({queryFieldValue})").AsList();

        /// <summary>
        /// 根据条件和条件值获取数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="queryField"></param>
        /// <param name="queryFieldValue"></param>
        /// <returns></returns>
        public List<T> GetAllList<T>(string queryField, string queryFieldValue) where T : class, new() => BusinessConnection.Query<T>($"SELECT * FROM {typeof(T).Name} WHERE {queryField} in ({queryFieldValue})").AsList();
        /// <summary>
        /// 根据表名动态获取数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="queryField"></param>
        /// <param name="queryFieldValue"></param>
        /// <returns></returns>
        public List<dynamic> GetAllList(string queryField, string queryFieldValue, string queryTableCode) => BusinessConnection.Query<dynamic>($"SELECT * FROM {queryTableCode} WHERE {queryField} in ({queryFieldValue})").AsList();
        /// <summary>
        /// 添加信息
        /// </summary>
        /// </summary>
        /// <param name="model">实体</param>
        /// <returns></returns>
        public long AddBasicBusiness<T>(T model, IDbTransaction transaction = null) where T : class, new()
        {
            return BasicDataConnection.Insert<T>(entityToInsert: model, transaction: transaction);
        }
        /// <summary>
        /// 修改信息
        /// </summary>
        /// </summary>
        /// <param name="model">实体</param>
        /// <returns></returns>
        public bool UpdateBasicBusiness<T>(T model, IDbTransaction transaction = null) where T : class, new()
        {
            return BasicDataConnection.Update<T>(entityToUpdate: model, transaction: transaction);
        }
        /// <summary>
        /// 获取信息
        /// </summary>
        /// </summary>
        /// <param name="model">实体</param>
        /// <returns></returns>
        public T BasicGet<T>(object id, IDbTransaction transaction = null) where T : class, new()
        {
            return BasicDataConnection.Get<T>(id: id, transaction: transaction);
        }

        public List<T> GetBasicQuery<T>(string tableCode, string queryField, string queryFieldValue) where T : class, new() => BasicDataConnection.Query<T>($"SELECT * FROM {tableCode} WHERE {queryField} in ({queryFieldValue})").AsList();

        public T BasicGetValue<T>(string tableCode, string queryField, string queryFieldValue, string resultField) => BasicDataConnection.QueryFirstOrDefault<T>($"SELECT {resultField} FROM {tableCode} WHERE {queryField} in ({queryFieldValue})");
    }
}
