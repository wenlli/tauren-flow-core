﻿using System.Collections.Generic;
using System.Data;
using Tauren.Flow.Entity.Model;

namespace Tauren.Flow.DLL.Base
{
    public interface IBaseDLL
    {
        /// <summary>
        /// 添加信息
        /// </summary>
        /// </summary>
        /// <param name="model">实体</param>
        /// <returns></returns>
        long Add<T>(T model, IDbTransaction transaction = null) where T : class, new();
        /// <summary>
        /// 修改信息
        /// </summary>
        /// </summary>
        /// <param name="model">实体</param>
        /// <returns></returns>
        bool Update<T>(T model, IDbTransaction transaction = null) where T : class, new();

        /// <summary>
        /// 添加业务信息
        /// </summary>
        /// </summary>
        /// <param name="model">实体</param>
        /// <returns></returns>
        long AddBasicBusiness<T>(T model, IDbTransaction transaction = null) where T : class, new();
        /// <summary>
        /// 修改业务信息
        /// </summary>
        /// </summary>
        /// <param name="model">实体</param>
        /// <returns></returns>
        bool UpdateBasicBusiness<T>(T model, IDbTransaction transaction = null) where T : class, new();

        /// <summary>
        /// 删除信息
        /// </summary>
        /// </summary>
        /// <param name="model">实体</param>
        /// <returns></returns>
        bool Delete<T>(T model, IDbTransaction transaction = null) where T : class, new();

        /// <summary>
        /// 获取单个信息
        /// </summary>
        /// </summary>
        /// <param name="model">实体</param>
        /// <returns></returns>
        T Get<T>(object id, IDbTransaction transaction = null) where T : class, new();

        /// <summary>
        /// 获取单个信息--获取基础数据
        /// </summary>
        /// </summary>
        /// <param name="model">实体</param>
        /// <returns></returns>
        T Single<T>(object id, IDbTransaction transaction = null) where T : class, new();

        /// <summary>
        /// 获取所有数据
        /// </summary>
        /// <param name="transaction"></param>
        /// <returns></returns>
        List<T> All<T>(IDbTransaction transaction = null) where T : class, new();
        List<T> GetAll<T>(IDbTransaction transaction = null) where T : class, new();
        /// <summary>
        /// 判断权限
        /// </summary>
        /// <param name="menuId"></param>
        /// <returns></returns>
        List<ExtButtons> JudgePermissions(string MenuCode);
        /// <summary>
        /// 判断是否是系统管理员
        /// </summary>
        /// <param name="EmpId"></param>
        /// <returns></returns>
        bool IsCurrentRoleByAdmin(int EmpId);
        void BuliderDbConnection(string Connections);
        /// <summary>
        /// 根据code获取数据
        /// </summary>
        /// <param name="autoId"></param>
        /// <returns></returns>
        T GetByCode<T>(string code) where T : class, new();

        /// <summary>
        /// 根据条件和条件值获取基础库中的数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="queryField"></param>
        /// <param name="queryFieldValue"></param>
        /// <returns></returns>
        List<T> GetBasicListByQuery<T>(string queryField, string queryFieldValue) where T : class, new();
        /// <summary>
        /// 根据表名动态获取数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="queryField"></param>
        /// <param name="queryFieldValue"></param>
        /// <returns></returns>
        List<dynamic> GetBasicListByQuery(string queryField, string queryFieldValue, string queryTableCode);
        /// <summary>
        /// 根据条件和条件值获取数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="queryField"></param>
        /// <param name="queryFieldValue"></param>
        /// <returns></returns>
        List<T> GetAllList<T>(string queryField, string queryFieldValue) where T : class, new();
        /// <summary>
        /// 根据表名动态获取数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="queryField"></param>
        /// <param name="queryFieldValue"></param>
        /// <returns></returns>
        List<dynamic> GetAllList(string queryField, string queryFieldValue, string queryTableCode);
        /// <summary>
        /// 获取单个信息
        /// </summary>
        /// </summary>
        /// <param name="model">实体</param>
        /// <returns></returns>
        T BasicGet<T>(object id, IDbTransaction transaction = null) where T : class, new();
        /// <summary>
        /// 根据表名字段字段值获取数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tableCode"></param>
        /// <param name="queryField"></param>
        /// <param name="queryFieldValue"></param>
        /// <returns></returns>
        List<T> GetBasicQuery<T>(string tableCode, string queryField, string queryFieldValue) where T : class, new();

        /// <summary>
        /// 获取某个值
        /// </summary>
        /// </summary>
        /// <param name="model">实体</param>
        /// <returns></returns>
        T BasicGetValue<T>(string tableCode, string queryField, string queryFieldValue, string resultField);
    }
}
