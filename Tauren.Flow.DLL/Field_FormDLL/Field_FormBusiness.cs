﻿using Dapper;
using Dapper.Contrib.Extensions;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tauren.Flow.Entity.Global;
using Tauren.Flow.Entity.Model;
using Tauren.Flow.Entity.Output;
using Tauren.Flow.Infrastructure.Globals;

namespace Tauren.Flow.DLL.Field_FormDLL
{
    /// <summary>
    /// 设置表单需要的表信息
    /// </summary>
    internal class Field_FormBusiness : Base.BaseDLL, IField_FormBusiness
    {
        public Field_FormBusiness(IConnectionDLLBase _dLLBase) : base(_dLLBase)
        {
        }
        public List<Field_Form_VersionExt> GetVersionFields(long VersionId) => FormConnection.Query<Field_Form_VersionExt>(@"SELECT b.autoid versionid,   a.autoid AS formid, c.autoid AS areaid
	                                                                                                    ,c.cnname AS areacnname, c.enname AS areaenname, c.areacode, c.tablecode 
                                                                                                        ,c.`sequence` AS areaseq, d.autoid AS fieldid
	                                                                                                    , d.fieldcode, d.fieldkey, d.cnname, d.enname,d.required,d.`sequence`, d.`show`
	                                                                                                    , d.editor,1 areausable,1 fieldusable,c.maintable,controlstype,d.controlsouces,c.records,1 areaeditor,1 areashow
                                                                                                  FROM field_form a
	                                                                                                    INNER JOIN field_form_version b ON a.autoid = b.formid  
	                                                                                                    LEFT JOIN field_form_area c ON c.versionid = b.autoid
	                                                                                                    LEFT JOIN field_form_area_fields d ON d.areaid = c.autoid 
                                                                                                  WHERE b.autoid = @AutoId", new Entity.Model.Field_Form_Version { AutoId = VersionId }).AsList();
        public List<TableFields> GetTableFields(string TableCode) => BasicDataConnection.Query<TableFields>($@"select
	                                                                                                                t.tablecode,
	                                                                                                                t.enable as TableEnable,
                                                                                                                    t.records,
                                                                                                                    t.sequence,
	                                                                                                                t.havedelete TableHaveDelete,
                                                                                                                    t.cnname tablename,
                                                                                                                    t.enname tableenname,
	                                                                                                                f.fieldcode,
	                                                                                                                f.enable as FieldEnable,
                                                                                                                    f.sequence seq,
                                                                                                                    f.fieldenname,
                                                                                                                    f.fieldname,
	                                                                                                                f.havedelete FieldHaveDelete 
                                                                                                            from tableinfo t
                                                                                                            left join fields f on
	                                                                                                            f.tablecode = t.tablecode
                                                                                                            where
	                                                                                                            t.tablecode in({TableCode})").AsList();

        public List<Form_TableInfoExt> GetTableByCode(string TableCode) => FormConnection.Query<Form_TableInfoExt>($@"select autoid,enable ,tablecode ,havedelete from form_tableinfo ft where ft.tablecode in({TableCode});").AsList();

        public List<Flow_Quick_Field> GetFlowQuickFields(long FlowId, long VersionId) => BusinessConnection.Query<Flow_Quick_Field>("SELECT * FROM flow_quick_field WHERE flowid=@FlowId and flowversionid=@FlowVersionId", new Flow_Quick_Field() { FlowId = FlowId, FlowVersionId = VersionId }).AsList();

        public List<Flow_Instance_Field> GetFlowInstanceFields(long FlowId, long VersionId) => BusinessConnection.Query<Flow_Instance_Field>("SELECT * FROM flow_instance_field WHERE flowid=@FlowId and flowversionid=@FlowVersionId", new Flow_Quick_Field() { FlowId = FlowId, FlowVersionId = VersionId }).AsList();

        public bool DeleteDisplayFields(string tableCode, long FlowId, long VersionId)
        {
            return this.BusinessConnection.Execute($"DELETE FROM {tableCode} WHERE flowid=@FlowId and flowversionid=@FlowVersionId", new Flow_Quick_Field() { FlowId = FlowId, FlowVersionId = VersionId }) >= 0;
        }

        public List<FieldConfig> GetGlobalFields(long VersionId) => FormConnection.Query<FieldConfig>($@"SELECT a.cnname formcnname,a.enname formenname,
                                                                        a.useapplicantempid, b.autoid versionid, b.versionno, a.autoid AS formid, c.autoid AS areaid
	                                                                    , c.cnname AS areacnname, c.enname AS areaenname, c.areacode, c.tablecode, c.presentation
	                                                                    , c.rowes, c.columnes, c.records, c.`sequence` AS areaseq, d.autoid AS fieldid
	                                                                    , d.fieldcode, d.fieldkey, d.cnname, d.enname, d.controlstype
	                                                                    , d.controlsouces, d.`type`, d.defaultvalue, d.required, d.max
	                                                                    , d.min, d.precisions, d.selecttype, d.`sequence`, d.`show`
	                                                                    , d.editor, e.leftbracket, e.cascadefield, e.`condition`, e.cascadefieldvalue
	                                                                    , e.rightbracket, e.associated, f.regulars,c.maintable,d.flength,1 areausable,1 fieldusable
                                                                    FROM field_form a
	                                                                    LEFT JOIN  field_form_version b
	                                                                    ON a.autoid = b.formid
	                                                                    LEFT JOIN field_form_area c ON c.versionid = b.autoid
	                                                                    LEFT JOIN field_form_area_fields d ON d.areaid = c.autoid
	                                                                    LEFT JOIN field_form_area_fields_condition e ON d.fieldkey = e.fieldkey
	                                                                    LEFT JOIN field_form_area_fields_regular f ON d.autoid = f.fieldId 
                                                                    WHERE b.autoid = @AutoID   ", new { AutoID = VersionId }).AsList();

        /// <summary>
        /// 根据表单获取表信息
        /// </summary>
        /// <param name="formid"></param>
        /// <returns></returns>
        public List<FormTable> GetFormTables(long formid) => FormConnection.Query<FormTable>(@"SELECT TableCode,FlowTableCode,ForeignKey,FormId FROM form_field_primary_tableinfo WHERE formid=@FormId
                                                                                                UNION
                                                                                                SELECT b.TableCode, b.FlowTableCode, b.ForeignKey, a.FormId FROM form_field_primary_tableinfo a 
                                                                                                LEFT JOIN form_field_child_tableinfo b on a.autoid = b.primarytableid WHERE formid = @FormId",
                                                                                                new { FormId = formid }).AsList();

        public Dictionary<string, List<dynamic>> GetFlowFormData(Dictionary<string, string> RewSql)
        {
            Dictionary<string, List<dynamic>> results = new Dictionary<string, List<dynamic>>();
            if (RewSql.Any())
            {
                Dictionary<int, List<IDictionary<string, object>>> rd = new Dictionary<int, List<IDictionary<string, object>>>();
                StringBuilder sb = new StringBuilder();
                foreach (var item in RewSql)
                {
                    sb.Append(item.Value + ";     ");
                }
                var reads = this.BasicDataConnection.QueryMultiple(sb.ToString());
                //int i = 0;
                //while (!reads.IsConsumed)
                //{
                //    rd[i] = reads.Read() as List<IDictionary<string, object>>;
                //    i++;
                //}
                int d = 0;
                foreach (var item in RewSql)
                {
                    if (!results.ContainsKey(item.Key))
                        results[item.Key] = reads.Read<dynamic>().ToList();
                    d++;
                }
            }
            return results;
        }
        public ExPhotoInfo SingePhoto(int empid) => this.FileConnection.QueryFirstOrDefault<ExPhotoInfo>(sql: "SELECT p.*,f.fileuri FROM photoinfo p LEFT JOIN files f  ON p.fileid =f.autoid WHERE p.empid=@EmpId", param: new ExPhotoInfo() { EmpId = empid });

        /// <summary>
        /// 获取流程相关的表
        /// </summary>
        /// <param name="VersionId"></param>
        /// <returns></returns>
        public List<Field_Form_Area> GetFlowFormTables(long VersionId) => FormConnection.Query<Field_Form_Area>(@"SELECT
	                                                                                                                c.* 
                                                                                                                FROM
	                                                                                                                field_form a
	                                                                                                                INNER JOIN field_form_version b ON a.autoid = b.formid
	                                                                                                                LEFT JOIN field_form_area c ON c.versionid = b.autoid 
                                                                                                                WHERE
	                                                                                                                b.autoid= @AutoId", new Entity.Model.Field_Form_Version { AutoId = VersionId }).AsList();
    }
}
