﻿using System.Collections.Generic;
using Tauren.Flow.Entity.Global;
using Tauren.Flow.Entity.Model;
using Tauren.Flow.Entity.Output;

namespace Tauren.Flow.DLL.Field_FormDLL
{
    /// <summary>
    /// 设置表单需要的表信息
    /// </summary>
    public interface IField_FormBusiness : Base.IBaseDLL
    {
        /// <summary>
        /// 获取表单字段
        /// </summary>
        /// <param name="FormId"></param>
        /// <returns></returns>
        List<Field_Form_VersionExt> GetVersionFields(long VersionId);

        /// <summary>
        /// 获取表单需要表基础数据
        /// </summary>
        /// <param name="TableCode"></param>
        /// <returns></returns>
        List<TableFields> GetTableFields(string TableCode);

        /// <summary>
        /// 根据编码获取表
        /// </summary>
        /// <param name="TableCode"></param>
        /// <returns></returns>
        List<Form_TableInfoExt> GetTableByCode(string TableCode);

        /// <summary>
        /// 获取所有快捷字段
        /// </summary>
        /// <param name="FlowId"></param>
        /// <param name="VersionId"></param>
        /// <returns></returns>
        List<Flow_Quick_Field> GetFlowQuickFields(long FlowId, long VersionId);

        /// <summary>
        /// 获取所有实例显示字段
        /// </summary>
        /// <param name="FlowId"></param>
        /// <param name="VersionId"></param>
        /// <returns></returns>
        List<Flow_Instance_Field> GetFlowInstanceFields(long FlowId, long VersionId);

        /// <summary>
        /// 删除显示字段
        /// </summary>
        /// <param name="tableCode"></param>
        /// <param name="FlowId"></param>
        /// <param name="VersionId"></param>
        /// <returns></returns>
        bool DeleteDisplayFields(string tableCode, long FlowId, long VersionId);

        /// <summary>
        /// 获取全局表单字段
        /// </summary>
        /// <param name="FormId"></param>
        /// <returns></returns>
        List<FieldConfig> GetGlobalFields(long VersionId);

        /// <summary>
        /// 根据表单ID获取表单要使用的表
        /// </summary>
        /// <param name="formid"></param>
        /// <returns></returns>
        List<FormTable> GetFormTables(long formid);

        /// <summary>
        /// 组装表单sql
        /// </summary>
        /// <param name="RewSql"></param>
        /// <returns></returns>
        Dictionary<string, List<dynamic>> GetFlowFormData(Dictionary<string, string> RewSql);
        ExPhotoInfo SingePhoto(int empid);
        /// <summary>
        /// 获取流程相关的表
        /// </summary>
        /// <param name="VersionId"></param>
        /// <returns></returns>
        List<Field_Form_Area> GetFlowFormTables(long VersionId);
    }
}
