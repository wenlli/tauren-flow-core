﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tauren.Flow.Entity.Const;
using Tauren.Flow.Entity.Enum;
using Tauren.Flow.Entity.Global;
using Tauren.Flow.Entity.Model;
using Tauren.Flow.Entity.Output;
using Tauren.Flow.Infrastructure.Globals;

namespace Tauren.Flow.DLL.FlowCenterDLL
{
    public class FlowCenterBusiness : Base.BaseDLL, IFlowCenterBusiness
    {
        public FlowCenterBusiness(IConnectionDLLBase _dLLBase) : base(_dLLBase)
        {
        }

        /// <summary>
        /// 根据流程节点ID获取节点名称
        /// </summary>
        /// <param name="stepIds"></param>
        /// <returns></returns>
        public Dictionary<string, string> GetStepNames(string[] stepIds)
        {
            Dictionary<string, string> results = new Dictionary<string, string>();
            StringBuilder stringBuilder = new StringBuilder($" select flow_step_info.stepid , flow_step_info.versionid ,flow_step_info.cnname,flow_step_info.enname from flow_step_info flow_step_info where stepid in ('{string.Join("','", stepIds)}') ");
            List<Flow_Step_Info> flow_Step_Infos = this.BusinessConnection.Query<Flow_Step_Info>(stringBuilder.ToString()).AsList();
            flow_Step_Infos.ForEach(o =>
            {
                string key = $"{o.VersionId}_{o.StepId}";
                if (!results.ContainsKey(key))
                {
                    results[key] = Global.IsChinese ? o.CNName : o.ENName;
                }
            });
            return results;

        }

        /// <summary>
        /// 根据流程节点ID获取节点名称
        /// </summary>
        /// <param name="stepIds"></param>
        /// <returns></returns>
        public Dictionary<long, bool> GetTaskRecycleInfo(long[] TaskIds)
        {
            Dictionary<long, bool> results = new Dictionary<long, bool>();
            StringBuilder stringBuilder = new StringBuilder($" select * from flow_task flow_task where prevtaskid in ({string.Join(",", TaskIds)}) ");
            List<Flow_Task> _Tasks = this.BasicDataConnection.Query<Flow_Task>(stringBuilder.ToString()).AsList();
            var groupTask = _Tasks.Where(o => o.Status != (int)FlowTaskStatus.temp && o.Status != (int)FlowTaskStatus.otheraudit).GroupBy(o => o.PrevTaskId);
            foreach (var item in groupTask)
            {
                results[item.Key] = item.Any() && !item.Any(o => GlobalConst.TaskComplatedStatus.Any(p => p == o.Status));
            }
            return results;

        }

        /// <summary>
        /// 待办申请
        /// </summary>
        /// <param name="queryModel"></param>
        /// <param name="Total"></param>
        /// <returns></returns>
        public List<PageFlowCenterList> ToApplyPages(QueryModel queryModel, out long Total)
        {
            Total = 0;
            string where = "";
            if (!string.IsNullOrEmpty(queryModel.KeyWord))
            {
                where += $"   AND flow_task.instanceid LIKE '%{queryModel.KeyWord}%' or flow_task.title LIKE '%{queryModel.KeyWord}%' ";
            }
            if (string.IsNullOrEmpty(queryModel.SortField))
            {
                queryModel.SortField = "flow_task.receivingtime";
                queryModel.IsAsc = false;
            }
            else
            {
                queryModel.SortField = queryModel.SortField.ToLower().Replace("dic_", "").Replace("dic", "").Trim();
            }
            if (queryModel.ExceptDataField.Any())
            {
                queryModel.ExceptDataField.ForEach(o =>
                {
                    if (!string.IsNullOrEmpty(o.Value + ""))
                        where += $" AND flow_task.{o.Label}!='{o.Value}'";
                });
            }
            if (queryModel.paramters.Any())
            {
                queryModel.paramters.ForEach(o =>
                {
                    if (!string.IsNullOrEmpty(o.Value + ""))
                        where += $" AND flow_task.{o.Label}='{o.Value}'";
                });
            }
            string asc = queryModel.IsAsc ? "ASC" : "DESC";
            queryModel.Page = queryModel.Page == 0 ? 1 : queryModel.Page;
            StringBuilder sql = new StringBuilder();
            //where = string.IsNullOrEmpty(where) ? "" : $"where {where}";
            sql.Append(@$"SELECT count(flow_task.instanceid) as cnt FROM flow_task flow_task 
                        left join flow_instance flow_instance on
	                        flow_instance.groupid = flow_task.groupid
                        where
	                        flow_task.status = 0 and flow_task.`type`={(int)FlowTaskType.task} {where};");
            sql.Append(@$"select
	                        flow_task.instanceid,
	                        flow_task.taskid,
	                        flow_task.title,
	                        flow_task.groupid,
	                        flow_task.stepid,
	                        flow_task.reviewer,
	                        flow_task.sender,
	                        flow_task.receivingtime,
	                        flow_task.processtime,
	                        flow_task.`type`,
	                        flow_task.`read`,
	                        flow_task.urgency,
	                        flow_task.mandator,
	                        flow_task.status,
	                        flow_instance.flowid,
	                        flow_instance.flowversionid,
	                        flow_instance.formid,
	                        flow_instance.formversionid,
	                        flow_instance.flowstatus ,
	                        flow_instance.initiatorempid,
	                        flow_instance.applyempid,
	                        flow_instance.applydate,
	                        flow_instance.initiatordate,
                            flow_instance.complatedtime,
	                        flow_task.stepid currentstep,
	                        flow_task.taskid currenttaskid 
                        from
	                        flow_task flow_task 
                        left join flow_instance flow_instance on
	                        flow_instance.groupid = flow_task.groupid
                        where
	                        flow_task.status = 0 and flow_task.`type`={(int)FlowTaskType.task}
                            {where}  ORDER BY {queryModel.SortField} {asc} LIMIT {(queryModel.Page - 1) * queryModel.PageSize},{ queryModel.PageSize} ;");
            using (var reader = BasicDataConnection.QueryMultiple(sql.ToString()))
            {
                Total = reader.ReadFirstOrDefault<int>();
                List<PageFlowCenterList> pageModules = reader.Read<PageFlowCenterList>().ToList();
                long RowNo = ((queryModel.Page == 0 ? 1 : queryModel.Page) - 1) * queryModel.PageSize + 1;
                pageModules.ForEach(o =>
                {
                    o.RowNo = RowNo;
                    RowNo++;
                });
                return pageModules;
            }
        }

        /// <summary>
        /// 传阅事项
        /// </summary>
        /// <param name="queryModel"></param>
        /// <param name="Total"></param>
        /// <returns></returns>
        public List<PageFlowCenterList> ToCirculatedPages(QueryModel queryModel, out long Total)
        {
            Total = 0;
            string where = "";
            if (!string.IsNullOrEmpty(queryModel.KeyWord))
            {
                where += $"   AND flow_task.instanceid LIKE '%{queryModel.KeyWord}%' or flow_task.title LIKE '%{queryModel.KeyWord}%' ";
            }
            if (string.IsNullOrEmpty(queryModel.SortField))
            {
                queryModel.SortField = "flow_task.receivingtime";
                queryModel.IsAsc = false;
            }
            else
            {
                queryModel.SortField = queryModel.SortField.ToLower().Replace("dic_", "").Replace("dic", "").Trim();
            }
            if (queryModel.ExceptDataField.Any())
            {
                queryModel.ExceptDataField.ForEach(o =>
                {
                    if (!string.IsNullOrEmpty(o.Value + ""))
                        where += $" AND flow_task.{o.Label}!='{o.Value}'";
                });
            }
            if (queryModel.paramters.Any())
            {
                queryModel.paramters.ForEach(o =>
                {
                    if (!string.IsNullOrEmpty(o.Value + ""))
                        where += $" AND flow_task.{o.Label}='{o.Value}'";
                });
            }
            string asc = queryModel.IsAsc ? "ASC" : "DESC";
            queryModel.Page = queryModel.Page == 0 ? 1 : queryModel.Page;
            StringBuilder sql = new StringBuilder();
            //where = string.IsNullOrEmpty(where) ? "" : $"where {where}";
            sql.Append(@$"SELECT count(flow_task.instanceid) as cnt FROM flow_task flow_task 
                        left join flow_instance flow_instance on
	                        flow_instance.groupid = flow_task.groupid
                        where
	                        flow_task.`type`={(int)FlowTaskType.circulated} {where};");
            sql.Append(@$"select
	                        flow_task.instanceid,
	                        flow_task.taskid,
	                        flow_task.title,
	                        flow_task.groupid,
	                        flow_task.stepid,
	                        flow_task.reviewer,
	                        flow_task.sender,
	                        flow_task.receivingtime,
	                        flow_task.processtime,
	                        flow_task.`type`,
	                        flow_task.`read`,
	                        flow_task.urgency,
	                        flow_task.mandator,
	                        flow_task.status,
	                        flow_instance.flowid,
	                        flow_instance.flowversionid,
	                        flow_instance.formid,
	                        flow_instance.formversionid,
	                        flow_instance.flowstatus ,
	                        flow_instance.initiatorempid,
	                        flow_instance.applyempid,
	                        flow_instance.applydate,
	                        flow_instance.initiatordate,
	                        flow_task.stepid currentstep,
                            flow_instance.complatedtime,
	                        flow_task.taskid currenttaskid 
                        from
	                        flow_task flow_task 
                        left join flow_instance flow_instance on
	                        flow_instance.groupid = flow_task.groupid
                        where
	                        flow_task.`type`={(int)FlowTaskType.circulated}
                            {where}  ORDER BY {queryModel.SortField} {asc} LIMIT {(queryModel.Page - 1) * queryModel.PageSize},{ queryModel.PageSize} ;");
            using (var reader = BasicDataConnection.QueryMultiple(sql.ToString()))
            {
                Total = reader.ReadFirstOrDefault<int>();
                List<PageFlowCenterList> pageModules = reader.Read<PageFlowCenterList>().ToList();
                long RowNo = ((queryModel.Page == 0 ? 1 : queryModel.Page) - 1) * queryModel.PageSize + 1;
                pageModules.ForEach(o =>
                {
                    o.RowNo = RowNo;
                    RowNo++;
                });
                return pageModules;
            }
        }

        public List<PageFlowCenterList> ToDonePages(QueryModel queryModel, out long Total)
        {
            Total = 0;
            string where = "";
            if (!string.IsNullOrEmpty(queryModel.KeyWord))
            {
                where += $"   AND flow_task.instanceid LIKE '%{queryModel.KeyWord}%' or flow_task.title LIKE '%{queryModel.KeyWord}%' ";
            }
            if (string.IsNullOrEmpty(queryModel.SortField))
            {
                queryModel.SortField = "flow_task.receivingtime";
                queryModel.IsAsc = false;
            }
            else
            {
                queryModel.SortField = queryModel.SortField.ToLower().Replace("dic_", "").Replace("dic", "").Trim();
            }
            if (queryModel.ExceptDataField.Any())
            {
                queryModel.ExceptDataField.ForEach(o =>
                {
                    if (!string.IsNullOrEmpty(o.Value + ""))
                        where += $" AND flow_task.{o.Label}!='{o.Value}'";
                });
            }
            if (queryModel.paramters.Any())
            {
                queryModel.paramters.ForEach(o =>
                {
                    if (!string.IsNullOrEmpty(o.Value + ""))
                        where += $" AND flow_task.{o.Label}='{o.Value}'";
                });
            }
            string asc = queryModel.IsAsc ? "ASC" : "DESC";
            queryModel.Page = queryModel.Page == 0 ? 1 : queryModel.Page;
            StringBuilder sql = new StringBuilder();
            //where = string.IsNullOrEmpty(where) ? "" : $"where {where}";
            sql.Append(@$"SELECT count(flow_task.instanceid) as cnt FROM flow_task flow_task 
                        inner join flow_instance flow_instance on
	                        flow_instance.groupid = flow_task.groupid and flow_task.taskid != flow_instance.applytaskid
                        where
	                        flow_task.status in({(int)FlowTaskStatus.approved},{(int)FlowTaskStatus.otherapproved},{(int)FlowTaskStatus.assigned},{(int)FlowTaskStatus.otherreturned},{(int)FlowTaskStatus.returned},{(int)FlowTaskStatus.transfer},{(int)FlowTaskStatus.terminated},{(int)FlowTaskStatus.recycled}) and flow_task.`type`={(int)FlowTaskType.task}  {where};");
            sql.Append(@$"select
	                        flow_task.instanceid,
	                        flow_task.taskid,
	                        flow_task.title,
	                        flow_task.groupid,
	                        flow_task.stepid,
	                        flow_task.reviewer,
	                        flow_task.sender,
	                        flow_task.receivingtime,
	                        flow_task.processtime,
	                        flow_task.`type`,
	                        flow_task.`read`,
	                        flow_task.urgency,
	                        flow_task.mandator,
	                        flow_task.status,
	                        flow_instance.flowid,
	                        flow_instance.flowversionid,
	                        flow_instance.formid,
	                        flow_instance.formversionid,
	                        flow_instance.flowstatus ,
	                        flow_instance.initiatorempid,
	                        flow_instance.applyempid,
	                        flow_instance.applydate,
                            flow_instance.complatedtime,
	                        flow_instance.initiatordate,
	                        flow_task.stepid currentstep,
	                        flow_task.taskid currenttaskid 
                        from
	                        flow_task flow_task 
                        inner join flow_instance flow_instance on
	                        flow_instance.groupid = flow_task.groupid and flow_task.taskid != flow_instance.applytaskid
                        where
	                        flow_task.status in({(int)FlowTaskStatus.approved},{(int)FlowTaskStatus.otherapproved},{(int)FlowTaskStatus.assigned},{(int)FlowTaskStatus.otherreturned},{(int)FlowTaskStatus.returned},{(int)FlowTaskStatus.transfer},{(int)FlowTaskStatus.terminated},{(int)FlowTaskStatus.recycled}) and flow_task.`type`={(int)FlowTaskType.task}
                            {where}  ORDER BY {queryModel.SortField} {asc} LIMIT {(queryModel.Page - 1) * queryModel.PageSize},{ queryModel.PageSize} ;");
            using (var reader = BasicDataConnection.QueryMultiple(sql.ToString()))
            {
                Total = reader.ReadFirstOrDefault<int>();
                List<PageFlowCenterList> pageModules = reader.Read<PageFlowCenterList>().ToList();
                long RowNo = ((queryModel.Page == 0 ? 1 : queryModel.Page) - 1) * queryModel.PageSize + 1;
                pageModules.ForEach(o =>
                {
                    o.RowNo = RowNo;
                    RowNo++;
                });
                return pageModules;
            }
        }

        /// <summary>
        /// 待办审批
        /// </summary>
        /// <param name="queryModel"></param>
        /// <param name="Total"></param>
        /// <returns></returns>
        public List<PageFlowCenterList> ToDoPages(QueryModel queryModel, out long Total)
        {
            Total = 0;
            string where = "";
            if (!string.IsNullOrEmpty(queryModel.KeyWord))
            {
                where += $"   AND flow_task.instanceid LIKE '%{queryModel.KeyWord}%' or flow_task.title LIKE '%{queryModel.KeyWord}%' ";
            }
            if (string.IsNullOrEmpty(queryModel.SortField))
            {
                queryModel.SortField = "flow_task.receivingtime";
                queryModel.IsAsc = false;
            }
            else
            {
                queryModel.SortField = queryModel.SortField.ToLower().Replace("dic_", "").Replace("dic", "").Trim();
            }
            if (queryModel.ExceptDataField.Any())
            {
                queryModel.ExceptDataField.ForEach(o =>
                {
                    if (!string.IsNullOrEmpty(o.Value + ""))
                        where += $" AND flow_task.{o.Label}!='{o.Value}'";
                });
            }
            if (queryModel.paramters.Any())
            {
                queryModel.paramters.ForEach(o =>
                {
                    if (!string.IsNullOrEmpty(o.Value + ""))
                        where += $" AND flow_task.{o.Label}='{o.Value}'";
                });
            }
            string asc = queryModel.IsAsc ? "ASC" : "DESC";
            queryModel.Page = queryModel.Page == 0 ? 1 : queryModel.Page;
            StringBuilder sql = new StringBuilder();
            //where = string.IsNullOrEmpty(where) ? "" : $"where {where}";
            sql.Append(@$"SELECT count(flow_task.instanceid) as cnt FROM flow_task flow_task 
                        left join flow_instance flow_instance on
	                        flow_instance.groupid = flow_task.groupid
                        where
	                        flow_task.status in(1, 2) and flow_task.`type`={(int)FlowTaskType.task} {where};");
            sql.Append(@$"select
	                        flow_task.instanceid,
	                        flow_task.taskid,
	                        flow_task.title,
	                        flow_task.groupid,
	                        flow_task.stepid,
	                        flow_task.reviewer,
	                        flow_task.sender,
	                        flow_task.receivingtime,
	                        flow_task.processtime,
	                        flow_task.`type`,
	                        flow_task.`read`,
	                        flow_task.urgency,
	                        flow_task.mandator,
	                        flow_task.status,
	                        flow_instance.flowid,
	                        flow_instance.flowversionid,
	                        flow_instance.formid,
	                        flow_instance.formversionid,
	                        flow_instance.flowstatus ,
	                        flow_instance.initiatorempid,
	                        flow_instance.applyempid,
	                        flow_instance.applydate,
	                        flow_instance.initiatordate,
	                        flow_task.stepid currentstep,
                            flow_instance.complatedtime,
	                        flow_task.taskid currenttaskid 
                        from
	                        flow_task flow_task 
                        left join flow_instance flow_instance on
	                        flow_instance.groupid = flow_task.groupid
                        where
	                        flow_task.status in(1, 2) and flow_task.`type`={(int)FlowTaskType.task}
                            {where}  ORDER BY {queryModel.SortField} {asc} LIMIT {(queryModel.Page - 1) * queryModel.PageSize},{ queryModel.PageSize} ;");
            using (var reader = BasicDataConnection.QueryMultiple(sql.ToString()))
            {
                Total = reader.ReadFirstOrDefault<int>();
                List<PageFlowCenterList> pageModules = reader.Read<PageFlowCenterList>().ToList();
                long RowNo = ((queryModel.Page == 0 ? 1 : queryModel.Page) - 1) * queryModel.PageSize + 1;
                pageModules.ForEach(o =>
                {
                    o.RowNo = RowNo;
                    RowNo++;
                });
                return pageModules;
            }
        }

        /// <summary>
        /// 我的申请
        /// </summary>
        /// <param name="queryModel"></param>
        /// <param name="Total"></param>
        /// <returns></returns>
        public List<PageFlowCenterList> ToMyApplyPages(QueryModel queryModel, out long Total)
        {
            Total = 0;
            string where = "";
            if (!string.IsNullOrEmpty(queryModel.KeyWord))
            {
                where += $"   AND flow_task.instanceid LIKE '%{queryModel.KeyWord}%' or flow_task.title LIKE '%{queryModel.KeyWord}%' ";
            }
            if (string.IsNullOrEmpty(queryModel.SortField))
            {
                queryModel.SortField = "flow_task.receivingtime";
                queryModel.IsAsc = false;
            }
            else
            {
                queryModel.SortField = queryModel.SortField.ToLower().Replace("dic_", "").Replace("dic", "").Trim();
            }
            if (queryModel.ExceptDataField.Any())
            {
                queryModel.ExceptDataField.ForEach(o =>
                {
                    if (!string.IsNullOrEmpty(o.Value + ""))
                        where += $" AND flow_task.{o.Label}!='{o.Value}'";
                });
            }
            if (queryModel.paramters.Any())
            {
                queryModel.paramters.ForEach(o =>
                {
                    if (!string.IsNullOrEmpty(o.Value + ""))
                        where += $" AND flow_task.{o.Label}='{o.Value}'";
                });
            }
            string asc = queryModel.IsAsc ? "ASC" : "DESC";
            queryModel.Page = queryModel.Page == 0 ? 1 : queryModel.Page;
            StringBuilder sql = new StringBuilder();
            //where = string.IsNullOrEmpty(where) ? "" : $"where {where}";
            sql.Append(@$"SELECT count(flow_task.instanceid) as cnt FROM flow_task flow_task 
                        inner join flow_instance flow_instance on
	                        flow_instance.groupid = flow_task.groupid and flow_task.taskid=flow_instance.applytaskid
                        where
	                         flow_task.status in({(int)FlowTaskStatus.approved}) and flow_task.`type`={(int)FlowTaskType.task} {where};");
            sql.Append(@$"select
	                        flow_task.instanceid,
	                        flow_task.taskid,
	                        flow_task.title,
	                        flow_task.groupid,
	                        flow_task.stepid,
	                        flow_task.reviewer,
	                        flow_task.sender,
	                        flow_task.receivingtime,
	                        flow_task.processtime,
	                        flow_task.`type`,
	                        flow_task.`read`,
	                        flow_task.urgency,
	                        flow_task.mandator,
	                        flow_task.status,
	                        flow_instance.flowid,
	                        flow_instance.flowversionid,
	                        flow_instance.formid,
	                        flow_instance.formversionid,
	                        flow_instance.flowstatus ,
	                        flow_instance.initiatorempid,
	                        flow_instance.applyempid,
	                        flow_instance.applydate,
	                        flow_instance.initiatordate,
	                        flow_task.stepid currentstep,
                            flow_instance.complatedtime,
	                        flow_task.taskid currenttaskid 
                        from
	                        flow_task flow_task 
                        inner join flow_instance flow_instance on
	                        flow_instance.groupid = flow_task.groupid and flow_task.taskid=flow_instance.applytaskid 
                        where
	                        flow_task.status in({(int)FlowTaskStatus.approved}) and flow_task.`type`={(int)FlowTaskType.task}
                            {where}  ORDER BY {queryModel.SortField} {asc} LIMIT {(queryModel.Page - 1) * queryModel.PageSize},{ queryModel.PageSize} ;");
            using (var reader = BasicDataConnection.QueryMultiple(sql.ToString()))
            {
                Total = reader.ReadFirstOrDefault<int>();
                List<PageFlowCenterList> pageModules = reader.Read<PageFlowCenterList>().ToList();
                long RowNo = ((queryModel.Page == 0 ? 1 : queryModel.Page) - 1) * queryModel.PageSize + 1;
                pageModules.ForEach(o =>
                {
                    o.RowNo = RowNo;
                    RowNo++;
                });
                return pageModules;
            }
        }

        /// <summary>
        /// 我的抄送
        /// </summary>
        /// <param name="queryModel"></param>
        /// <param name="Total"></param>
        /// <returns></returns>
        public List<PageFlowCenterList> ToMyCCPages(QueryModel queryModel, out long Total)
        {
            Total = 0;
            string where = "";
            if (!string.IsNullOrEmpty(queryModel.KeyWord))
            {
                where += $"   AND flow_task.instanceid LIKE '%{queryModel.KeyWord}%' or flow_task.title LIKE '%{queryModel.KeyWord}%' ";
            }
            if (string.IsNullOrEmpty(queryModel.SortField))
            {
                queryModel.SortField = "flow_task.receivingtime";
                queryModel.IsAsc = false;
            }
            else
            {
                queryModel.SortField = queryModel.SortField.ToLower().Replace("dic_", "").Replace("dic", "").Trim();
            }
            if (queryModel.ExceptDataField.Any())
            {
                queryModel.ExceptDataField.ForEach(o =>
                {
                    if (!string.IsNullOrEmpty(o.Value + ""))
                        where += $" AND flow_task.{o.Label}!='{o.Value}'";
                });
            }
            if (queryModel.paramters.Any())
            {
                queryModel.paramters.ForEach(o =>
                {
                    if (!string.IsNullOrEmpty(o.Value + ""))
                        where += $" AND flow_task.{o.Label}='{o.Value}'";
                });
            }
            string asc = queryModel.IsAsc ? "ASC" : "DESC";
            queryModel.Page = queryModel.Page == 0 ? 1 : queryModel.Page;
            StringBuilder sql = new StringBuilder();
            //where = string.IsNullOrEmpty(where) ? "" : $"where {where}";
            sql.Append(@$"SELECT count(flow_task.instanceid) as cnt FROM flow_task flow_task 
                        left join flow_instance flow_instance on
	                        flow_instance.groupid = flow_task.groupid
                        where
	                          flow_task.`type`={(int)FlowTaskType.cc} {where};");
            sql.Append(@$"select
	                        flow_task.instanceid,
	                        flow_task.taskid,
	                        flow_task.title,
	                        flow_task.groupid,
	                        flow_task.stepid,
	                        flow_task.reviewer,
	                        flow_task.sender,
	                        flow_task.receivingtime,
	                        flow_task.processtime,
	                        flow_task.`type`,
	                        flow_task.`read`,
	                        flow_task.urgency,
	                        flow_task.mandator,
	                        flow_task.status,
	                        flow_instance.flowid,
	                        flow_instance.flowversionid,
	                        flow_instance.formid,
	                        flow_instance.formversionid,
	                        flow_instance.flowstatus ,
	                        flow_instance.initiatorempid,
	                        flow_instance.applyempid,
	                        flow_instance.applydate,
                            flow_instance.complatedtime,
	                        flow_instance.initiatordate,
	                        flow_task.stepid currentstep,
	                        flow_task.taskid currenttaskid 
                        from
	                        flow_task flow_task 
                        left join flow_instance flow_instance on
	                        flow_instance.groupid = flow_task.groupid
                        where
	                        flow_task.`type`={(int)FlowTaskType.cc}
                            {where}  ORDER BY {queryModel.SortField} {asc} LIMIT {(queryModel.Page - 1) * queryModel.PageSize},{ queryModel.PageSize} ;");
            using (var reader = BasicDataConnection.QueryMultiple(sql.ToString()))
            {
                Total = reader.ReadFirstOrDefault<int>();
                List<PageFlowCenterList> pageModules = reader.Read<PageFlowCenterList>().ToList();
                long RowNo = ((queryModel.Page == 0 ? 1 : queryModel.Page) - 1) * queryModel.PageSize + 1;
                pageModules.ForEach(o =>
                {
                    o.RowNo = RowNo;
                    RowNo++;
                });
                return pageModules;
            }
        }

        /// <summary>
        /// 获取流程当前所有未处理的任务信息
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        public Dictionary<long, List<ExtFlow_Instance_CurrentTask>> GetCurrentTaskInfos(long[] groupId)
        {
            Dictionary<long, List<ExtFlow_Instance_CurrentTask>> results = new Dictionary<long, List<ExtFlow_Instance_CurrentTask>>();
            StringBuilder stringBuilder = new StringBuilder($" select fic.*,ft.reviewer,ft.status from flow_instance_currenttask fic left join flow_task ft on ft.taskid =fic.currenttaskid  where fic.groupid in ({string.Join(",", groupId)}) ");
            List<ExtFlow_Instance_CurrentTask> _Tasks = this.BasicDataConnection.Query<ExtFlow_Instance_CurrentTask>(stringBuilder.ToString()).AsList();
            var groupTask = _Tasks.GroupBy(o => o.GroupId);
            foreach (var item in groupTask)
            {
                results[item.Key] = item.ToList();
            }
            return results;
        }

        /// <summary>
        /// 根据taskId获取所有流程
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        public List<Flow_Task> GetTasksByTaskId(long taskId) => this.BasicDataConnection.Query<Flow_Task>("SELECT a.* FROM flow_task a INNER JOIN flow_task b on b.groupid=a.groupid WHERE b.taskid=@taskId  order by sort asc ,processtime desc ", new { taskId = taskId }).AsList();
    }
}
