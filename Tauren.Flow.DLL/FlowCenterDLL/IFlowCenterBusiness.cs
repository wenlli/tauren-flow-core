﻿using System.Collections.Generic;
using Tauren.Flow.Entity.Global;
using Tauren.Flow.Entity.Model;
using Tauren.Flow.Entity.Output;

namespace Tauren.Flow.DLL.FlowCenterDLL
{
    /// <summary>
    /// 流程中心相关数据库操作底层类
    /// </summary>
    public interface IFlowCenterBusiness : Base.IBaseDLL
    {
        /// <summary>
        /// 分页获取待办事项信息
        /// </summary>
        /// <param name="queryModel"></param>
        /// <param name="Total"></param>
        /// <returns></returns>
        List<PageFlowCenterList> ToDoPages(QueryModel queryModel, out long Total);

        /// <summary>
        /// 分页获取待办申请信息
        /// </summary>
        /// <param name="queryModel"></param>
        /// <param name="Total"></param>
        /// <returns></returns>
        List<PageFlowCenterList> ToApplyPages(QueryModel queryModel, out long Total);

        /// <summary>
        /// 分页获取阅办信息
        /// </summary>
        /// <param name="queryModel"></param>
        /// <param name="Total"></param>
        /// <returns></returns>
        List<PageFlowCenterList> ToCirculatedPages(QueryModel queryModel, out long Total);

        /// <summary>
        /// 分页获取我的申请信息
        /// </summary>
        /// <param name="queryModel"></param>
        /// <param name="Total"></param>
        /// <returns></returns>
        List<PageFlowCenterList> ToMyApplyPages(QueryModel queryModel, out long Total);

        /// <summary>
        /// 分页获取我的抄送信息
        /// </summary>
        /// <param name="queryModel"></param>
        /// <param name="Total"></param>
        /// <returns></returns>
        List<PageFlowCenterList> ToMyCCPages(QueryModel queryModel, out long Total);
        /// <summary>
        /// 分页获取我的已办事项
        /// </summary>
        /// <param name="queryModel"></param>
        /// <param name="Total"></param>
        /// <returns></returns>
        List<PageFlowCenterList> ToDonePages(QueryModel queryModel, out long Total);

        /// <summary>
        /// 根据流程节点ID获取节点名称
        /// </summary>
        /// <param name="stepIds"></param>
        /// <returns></returns>
        Dictionary<string, string> GetStepNames(string[] stepIds);
        /// <summary>
        /// 根据流程节点ID获取节点名称
        /// </summary>
        /// <param name="stepIds"></param>
        /// <returns></returns>
        Dictionary<long, bool> GetTaskRecycleInfo(long[] TaskIds);
        /// <summary>
        /// 获取流程当前所有未处理的任务信息
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        Dictionary<long, List<ExtFlow_Instance_CurrentTask>> GetCurrentTaskInfos(long[] groupId);

        /// <summary>
        /// 根据taskid获取所有任务
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        List<Flow_Task> GetTasksByTaskId(long taskId);
    }
}
