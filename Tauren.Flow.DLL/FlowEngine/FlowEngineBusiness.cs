﻿using Dapper;
using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using Tauren.Flow.Entity.Enum;
using Tauren.Flow.Entity.Global;
using Tauren.Flow.Entity.Model;
using Tauren.Flow.Entity.Output;
using Tauren.Flow.Infrastructure.Globals;
using System.Linq;
using System.Data;
using System.Text;
using Tauren.Flow.Entity.Const;

namespace Tauren.Flow.DLL.FlowEngine
{
    /// <summary>
    ///  流程引擎执行数据库
    /// </summary>
    public class FlowEngineBusiness : Base.BaseDLL, IFlowEngineBusiness
    {
        public FlowEngineBusiness(IConnectionDLLBase _dLLBase) : base(_dLLBase)
        {

        }

        public bool DeleteBranchTask(List<long> deleteTaskIds)
            => this.BasicDataConnection.Execute($"delete from flow_branch_task where taskid in({string.Join(",", deleteTaskIds)})") >= 0;

        /// <summary>
        /// 删除当前任务数据
        /// </summary>
        /// <param name="currentTaskId"></param>
        /// <param name="prevTaskId"></param>
        /// <returns></returns>
        public bool DeleteCurrentTask(long currentTaskId, long prevTaskId) => this.BasicDataConnection.Execute("delete from flow_instance_currenttask where currenttaskid =@currentTaskId and prvetaskid = @prevTaskId", new { currentTaskId = currentTaskId, prevTaskId = prevTaskId }) >= 0;
        public bool DeleteCurrentTask(long prevTaskId) => this.BasicDataConnection.Execute("delete from flow_instance_currenttask where  prvetaskid = @prevTaskId", new { prevTaskId = prevTaskId }) >= 0;

        public bool DeleteCurrentTask(List<long> taskIds)
        {
            if (taskIds.Any())
            {
                return this.BasicDataConnection.Execute($"delete from flow_instance_currenttask where  currenttaskid  in({(string.Join(",", taskIds))})") >= 0;
            }
            return true;
        }

        /// <summary>
        /// 实例完成时处理业务数据
        /// </summary>
        /// <param name="writeDatas"></param>
        /// <returns></returns>
        public bool FlowComplatedDataProcess(List<WriteData> writeDatas)
        {
            bool b = true;
            WriteData empFields = writeDatas.FirstOrDefault(o => (o.TableCode + "").ToLower() == GlobalConst.FORM_EMPLOYEEBASICINFO_TABLENAME);
            if (empFields != null)
            {
                if (empFields.WriteFields.Any())
                {
                    List<WriteData> empChildFields = writeDatas.Where(o => (o.MainTableCode + "").ToLower() == GlobalConst.FORM_EMPLOYEEBASICINFO_TABLENAME).ToList();
                    ///是入职流程
                    if (empFields.IsAdd)
                    {
                        string fields = "," + string.Join(",", empFields.WriteFields.Select(o => o.FieldCode));
                        StringBuilder stringEmpBuilder = new StringBuilder($@"INSERT INTO employee  (sequence,instanceid,creator,modifier,creatdate,modifdate,havedelete,`enable`,`status` {fields})
SELECT (select ifnull(max(`sequence`),0)+1 from employee),{empFields.InstanceId},{Global.EmpId},{Global.EmpId},NOW(),NOW(),0,1,1 {fields} FROM employeeapply where autoId={empFields.BusinessId};");
                        b = BasicDataConnection.Execute(stringEmpBuilder.ToString()) > 0;
                        int id = 0;
                        if (b)
                            id = BasicDataConnection.QueryFirstOrDefault<int>("select @@IDENTITY;");
                        stringEmpBuilder = new StringBuilder();
                        ///子表数据操作
                        empChildFields.ForEach(o =>
                        {
                            if (o.WriteFields.Where(o => o.FieldCode != "empid").Any())
                            {
                                stringEmpBuilder.Append($"delete from {o.TableCode} where instanceid={o.InstanceId};   ");
                                string fields = "," + string.Join(",", o.WriteFields.Where(o => o.FieldCode != "empid").Select(p => p.FieldCode));
                                stringEmpBuilder.Append($@"INSERT INTO {o.TableCode}  (autoid,sequence,instanceid,creator,modifier,creatdate,modifdate,havedelete,`enable`,empid,{fields})
                                        SELECT autoId, (select ifnull(max(`sequence`), 0) + 1 from {o.TableCode}),{o.InstanceId},{Global.EmpId},{Global.EmpId},NOW(),NOW(),0,1,{id}, {fields} FROM {o.TableCode}apply where autoId = {o.BusinessId};  ");
                            }
                        });
                        if (!string.IsNullOrEmpty(stringEmpBuilder.ToString()))
                            b = BasicDataConnection.Execute(stringEmpBuilder.ToString()) >= 0;
                    }
                    ///重聘和员工吸怪
                    else
                    {
                        string fields = "," + string.Join(",", empFields.WriteFields.Select(o => $"a.{o.FieldCode} = b.{o.FieldCode}"));
                        StringBuilder stringEmpBuilder = new StringBuilder($@"update employee a join employeeapply b on a.empid=b.empid  set {fields},a.modifier={Global.EmpId},a.modifdate=now()  where b.autoid={empFields.BusinessId};  ");
                        ///子表数据操作
                        empChildFields.ForEach(o =>
                        {
                            if (o.WriteFields.Any())
                            {
                                stringEmpBuilder.Append($"delete from {o.TableCode} where empid in(select empid from employeeapply where autoid={empFields.BusinessId});   ");
                                string fields = "," + string.Join(",", o.WriteFields.Select(p => p.FieldCode));
                                stringEmpBuilder.Append($@"INSERT INTO {o.TableCode}  (autoid,sequence,instanceid,creator,modifier,creatdate,modifdate,havedelete,`enable`,{fields})
                                        SELECT autoId, (select ifnull(max(`sequence`), 0) + 1 from {o.TableCode}),{o.InstanceId},{Global.EmpId},{Global.EmpId},NOW(),NOW(),0,1, {fields} FROM {o.TableCode}apply where autoId = {o.BusinessId};  ");
                            }
                        });
                        if (!string.IsNullOrEmpty(stringEmpBuilder.ToString()))
                            b = BasicDataConnection.Execute(stringEmpBuilder.ToString()) >= 0;
                    }
                    ///去掉和员工相关表
                    writeDatas = writeDatas.Where(o => (o.MainTableCode + "").ToLower() != GlobalConst.FORM_EMPLOYEEBASICINFO_TABLENAME && (o.TableCode + "").ToLower() != GlobalConst.FORM_EMPLOYEEBASICINFO_TABLENAME).ToList();
                }
            }
            StringBuilder stringBuilder = new StringBuilder();
            writeDatas.ForEach(o =>
            {
                string foreignkey = "";
                if (!string.IsNullOrEmpty(o.MainTableCode))
                {
                    foreignkey = ",foreignkey";
                }
                if (o.WriteFields.Any())
                {
                    stringBuilder.Append($"delete from {o.TableCode} where instanceid={o.InstanceId};   ");
                    string fields = "," + string.Join(",", o.WriteFields.Select(o => o.FieldCode));
                    stringBuilder.Append($@"INSERT INTO {o.TableCode}  (autoid,sequence,instanceid,creator,modifier,creatdate,modifdate,havedelete,`enable` {foreignkey} {fields})
                                        SELECT autoId, (select ifnull(max(`sequence`), 0) + 1 from {o.TableCode}),{o.InstanceId},{Global.EmpId},{Global.EmpId},NOW(),NOW(),0,1 {foreignkey} {fields} FROM {o.TableCode}apply where autoId = {o.BusinessId};  ");
                }
            });
            if (!string.IsNullOrEmpty(stringBuilder.ToString()))
                b = BasicDataConnection.Execute(stringBuilder.ToString()) >= 0;
            return b;
        }

        /// <summary>
        /// 删除流程实例
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="areas"></param>
        /// <returns></returns>
        public bool FlowDeleteInstanceId(long groupId, List<FlowDataAreaParameter> areas)
        {
            StringBuilder stringBuilder = new StringBuilder(@"delete from flow_task where groupid=@GroupId; 
                                                            delete from flow_instance_currenttask where groupid=@GroupId; 
                                                            delete from flow_instance where groupid=@GroupId; ");
            if (areas.Any())
            {
                areas.ForEach(o =>
                {
                    stringBuilder.Append($"    delete from {o.FlowTableCode} where {o.PrimaryKey}={o.PrimaryValue};    ");
                });
            }
            return BasicDataConnection.Execute(stringBuilder.ToString(), new { GroupId = groupId }) > 0;
        }

        public List<Flow_Task> GetTasks(long groupId) => this.BasicDataConnection.Query<Flow_Task>("select * from flow_task  where groupId=@groupId", new { groupId = groupId }).AsList();

        public List<WriteField> GetWriteField(string tableCodes, long versionId)
        {
            List<WriteField> writeFields = this.FormConnection.Query<WriteField>("SELECT a.tablecode,b.fieldcode FROM field_form_area a left join field_form_area_fields b on b.areaid=a.autoid where a.versionid=@VersionId and a.areacode!='form_apply_employeebasicinfo'", new { VersionId = versionId }).ToList();
            writeFields.AddRange(this.BasicDataConnection.Query<WriteField>("SELECT tablecode,fieldcode FROM `fields` WHERE fieldcode='empid'").AsList());
            return writeFields;
        }

        /// <summary>
        /// 完成任务
        /// </summary>
        /// <param name="flowInfo"></param>
        /// <returns></returns>
        public bool ProcessComplete(FlowTaskInfo taskInfo)
        {
            bool b = this.BasicDataConnection.Execute("UPDATE flow_task SET `status`=@Status,processtime=@Processtime,modifier=@Modifier,modifdate=@Modifdate WHERE taskid=@TaskId", new { Status = (int)FlowTaskStatus.approved, Processtime = DateTime.Now, Modifier = Global.EmpId, Modifdate = DateTime.Now, TaskId = taskInfo.TaskId }) > 0;
            b = b && this.BasicDataConnection.Execute("UPDATE flow_instance SET flowstatus=@Status,ComplatedTime=@Modifdate  WHERE groupid=@GroupId", new { Status = (int)FlowInstanceStatus.completed, Modifier = Global.EmpId, Modifdate = DateTime.Now, GroupId = taskInfo.GroupId }) > 0;
            return b;
        }
        /// <summary>
        ///  撤回时需要删除未处理的任务
        /// </summary>
        /// <param name="deletedTask"></param>
        /// <returns></returns>
        public bool RecyclingDeleteTask(List<Flow_Task> deletedTask)
        {
            return this.BasicDataConnection.Delete(deletedTask);
        }
        /// <summary>
        /// 执行任务时更新流程申请时的业务数据
        /// </summary>
        /// <param name="complatedParameters"></param>
        /// <returns></returns>
        public bool RunTaskUpdateApplyBusinessStatus(List<ComplatedParameter> complatedParameters)
        {
            if (complatedParameters.Any())
            {
                var groups = complatedParameters.GroupBy(o => o.TableCode);
                StringBuilder stringBuilder = new StringBuilder();
                foreach (var item in groups)
                {
                    stringBuilder.Append($"update {item.Key} set status={(int)item.FirstOrDefault().InstanceStatus} where {item.FirstOrDefault().PrimaryKey} in({(string.Join(",", item.Select(o => o.PrimaryValue)))});");
                }
                return this.BasicDataConnection.Execute(stringBuilder.ToString()) >= 0;
            }
            else return true;
        }

    }
}
