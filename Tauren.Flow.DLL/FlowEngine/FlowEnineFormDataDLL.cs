﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Tauren.Flow.Entity.Enum;
using Tauren.Flow.Entity.Global;
using Tauren.Flow.Infrastructure.Globals;
using Dapper.Contrib.Extensions;
using Dapper;
using Tauren.Flow.Entity.Model;
using Tauren.Flow.Entity.Const;

namespace Tauren.Flow.DLL.FlowEngine
{
    public class FlowEnineFormDataDLL : Base.BaseDLL, IFlowEnineFormDataDLL
    {
        public FlowEnineFormDataDLL(IConnectionDLLBase _dLLBase) : base(_dLLBase)
        {
        }
        /// <summary>
        /// 保存表单数据
        /// </summary>
        /// <param name="areaParameters"></param>
        /// <returns></returns>
        public bool FlowFormDataSave(List<FlowDataAreaParameter> areaParameters)
        {
            bool b = true;
            StringBuilder sql = new StringBuilder();

            areaParameters.ForEach(are =>
           {
               sql.Append($"  insert into {are.FlowTableCode} ({string.Join(",", are.FieldParameters.Select(o => o.FieldCode))},creator,modifier,creatdate,modifdate,status) values({string.Join(",", are.FieldParameters.Select(o => BulidDynamicParameter(o).FieldValue))},{Global.EmpId},{Global.EmpId},'{DateTime.Now.ToString(GlobalConst.DATE_FORMAT_MINUTES)}','{DateTime.Now.ToString(GlobalConst.DATE_FORMAT_MINUTES)}',0)");

               sql.Append($"  ON DUPLICATE KEY UPDATE {string.Join(",", are.FieldParameters.Select(o => $"{o.FieldCode}={o.FieldValue}"))},creator={Global.EmpId},modifier={Global.EmpId},creatdate='{DateTime.Now}',modifdate='{DateTime.Now}';");
               b = this.BasicDataConnection.Execute(sql.ToString()) >= 0;
           });
            return b;
        }

        /// <summary>
        /// 组装参数化
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="fieldParameter"></param>
        private FlowFieldParameter BulidDynamicParameter(FlowFieldParameter fieldParameter)
        {
            FieldType fieldType = (FieldType)int.Parse(fieldParameter.FieldType);
            this.BuildDataValue(fieldParameter);
            switch (fieldType)
            {
                case FieldType.varchar:
                    if (!string.IsNullOrEmpty(fieldParameter.FieldValue + "")) fieldParameter.FieldValue = $"'{ fieldParameter.FieldValue}'";
                    else fieldParameter.FieldValue = "null";
                    break;
                case FieldType.date:
                    if (!string.IsNullOrEmpty(fieldParameter.FieldValue + "")) fieldParameter.FieldValue = $"'{ fieldParameter.FieldValue}'";
                    else fieldParameter.FieldValue = "null";
                    break;
                case FieldType.year:
                    if (!string.IsNullOrEmpty(fieldParameter.FieldValue + "")) fieldParameter.FieldValue = $"'{ fieldParameter.FieldValue}'";
                    else fieldParameter.FieldValue = "null";
                    break;
                case FieldType.month:
                    if (!string.IsNullOrEmpty(fieldParameter.FieldValue + "")) fieldParameter.FieldValue = $"'{ fieldParameter.FieldValue}'";
                    else fieldParameter.FieldValue = "null";
                    break;
                case FieldType.day:
                    if (!string.IsNullOrEmpty(fieldParameter.FieldValue + "")) fieldParameter.FieldValue = $"'{ fieldParameter.FieldValue}'";
                    else fieldParameter.FieldValue = "null";
                    break;
                case FieldType.datetime:
                    if (!string.IsNullOrEmpty(fieldParameter.FieldValue + "")) fieldParameter.FieldValue = $"'{ fieldParameter.FieldValue}'";
                    else fieldParameter.FieldValue = "null";
                    break;
                default:
                    if (fieldParameter.FieldValue == null) fieldParameter.FieldValue = "null";
                    break;
            }
            return fieldParameter;
        }
        private void BulidDictionaryParameter(Dictionary<string, object> parameters, FlowFieldParameter fieldParameter)
        {
            FieldType fieldType = (FieldType)int.Parse(fieldParameter.FieldType);
            this.BuildDataValue(fieldParameter);
            switch (fieldType)
            {
                case FieldType.varchar:
                    parameters.Add($"{fieldParameter.FieldCode}", fieldParameter.FieldValue);
                    break;
                case FieldType.bigint:
                    parameters.Add($"{fieldParameter.FieldCode}", fieldParameter.FieldValue);
                    break;
                case FieldType.doubled:
                    parameters.Add($"{fieldParameter.FieldCode}", fieldParameter.FieldValue);
                    break;
                case FieldType.date:
                    parameters.Add($"{fieldParameter.FieldCode}", string.IsNullOrEmpty(fieldParameter.FieldValue + "") ? fieldParameter.FieldValue : DateTime.Parse(fieldParameter.FieldValue + "").ToString(GlobalConst.DATE_FORMAT_DAY));
                    break;
                case FieldType.year:
                    parameters.Add($"{fieldParameter.FieldCode}", fieldParameter.FieldValue);
                    break;
                case FieldType.month:
                    parameters.Add($"{fieldParameter.FieldCode}", fieldParameter.FieldValue);
                    break;
                case FieldType.day:
                    parameters.Add($"{fieldParameter.FieldCode}", fieldParameter.FieldValue);
                    break;
                case FieldType.datetime:
                    parameters.Add($"{fieldParameter.FieldCode}", string.IsNullOrEmpty(fieldParameter.FieldValue + "") ? fieldParameter.FieldValue : DateTime.Parse(fieldParameter.FieldValue + "").ToString(GlobalConst.DATE_FORMAT_SECONDS));
                    break;
                case FieldType.bit:
                    parameters.Add($"{fieldParameter.FieldCode}", fieldParameter.FieldValue);
                    break;
                case FieldType.iint:
                    parameters.Add($"{fieldParameter.FieldCode}", fieldParameter.FieldValue);
                    break;
                default:
                    break;
            }
        }
        /// <summary>
        /// 转换值
        /// </summary>
        /// <param name="parameter"></param>
        public void BuildDataValue(FlowFieldParameter parameter)
        {
            switch (parameter.ControlsType)
            {
                case "employee":
                    if (parameter.FieldValues.Any())
                        parameter.FieldValue = string.Join(",", parameter.FieldValues.Select(o => o.Value));
                    else parameter.FieldValue = null;
                    break;
                case "organizational":
                    if (parameter.FieldValues.Any())
                        parameter.FieldValue = string.Join(",", parameter.FieldValues.Select(o => o.Value));
                    else parameter.FieldValue = null;
                    break;
                case "position":
                    {
                        if (parameter.FieldValues.Any())
                            parameter.FieldValue = string.Join(",", parameter.FieldValues.Select(o => o.Value));
                        else parameter.FieldValue = null;
                    }
                    break;
                case "parameter":
                    if (parameter.FieldValues.Any())
                        parameter.FieldValue = string.Join(",", parameter.FieldValues.Select(o => o.Value));
                    else parameter.FieldValue = null;
                    break;
                case "unit":
                    if (parameter.FieldValues.Any())
                        parameter.FieldValue = string.Join(",", parameter.FieldValues.Select(o => o.Value));
                    else parameter.FieldValue = null;
                    break;
                case "number":
                    if (string.IsNullOrEmpty(parameter.FieldValue + ""))
                        parameter.FieldValue = null;
                    break;
                default:
                    break;
            }
        }

        public bool DeleteFlowAndFromDataMaspping(long InstacneId) => this.BasicDataConnection.Execute("delete from flow_instance_business where instanceid=@InstacneId", new { InstacneId = InstacneId }) >= 0;

        public bool UpdateInstanceId(Flow_Instance _Instance)
        {
            StringBuilder stringBuilder = new StringBuilder(@"UPDATE flow_instance SET   initiatorempid=@InitiatorEmpId, applyempid=@ApplyEmpId, flowstatus=@FlowStatus, applydate=@ApplyDate, initiatordate=@InitiatorDate WHERE instanceid=@InstanceId;");
            return BasicDataConnection.Execute(stringBuilder.ToString(), _Instance) >= 0;
        }

        /// <summary>
        /// 查询条件
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public List<T> Query<T>(string conditions) where T : class, new()
        {
            return BasicDataConnection.Query<T>(conditions).AsList();
        }

        /// <summary>
        /// 添加当前任务
        /// </summary>
        /// <param name="currentTasks"></param>
        /// <param name="grouId"></param>
        /// <returns></returns>
        public bool AddCurrentTask(List<Flow_Instance_CurrentTask> currentTasks, long grouId)
        {
            if (currentTasks.Any())
                this.BasicDataConnection.Insert(currentTasks);
            return true;
        }
    }
}
