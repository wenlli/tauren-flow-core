﻿using Dapper;
using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using Tauren.Flow.Entity.Enum;
using Tauren.Flow.Entity.Global;
using Tauren.Flow.Entity.Model;
using Tauren.Flow.Entity.Output;
using Tauren.Flow.Infrastructure.Globals;
using System.Linq;
using System.Data;
using System.Text;
using Tauren.Flow.Entity.Const;

namespace Tauren.Flow.DLL.FlowEngine
{
    /// <summary>
    ///  流程实例执行数据库
    /// </summary>
    public class FlowInstanceBusiness : Base.BaseDLL, IFlowInstanceBusiness
    {
        public FlowInstanceBusiness(IConnectionDLLBase _dLLBase) : base(_dLLBase)
        {

        }
        /// <summary>
        /// 获取流程树
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public List<FlowInstanceTree> GetFlowTree(QueryModel query)
        {
            string where = "";
            if (query != null && query.paramters != null && query.paramters.Any())
            {
                query.paramters.ForEach(o =>
                {
                    if (!string.IsNullOrEmpty(o.Value + ""))
                        where += $" AND a.{o.Label}='{o.Value}'";
                });
            }
            return BusinessConnection.Query<FlowInstanceTree>($@"SELECT
                                                                        a.FlowId,
                                                                        a.CNName,
                                                                        a.ENName,
                                                                        a.sequence FSeq,
                                                                        a.view_instance,
                                                                        b.versionid verid,
                                                                        b.versionno verno,
                                                                        a.currentversion curver,
                                                                        b.`name` vername,
                                                                        b.sequence vseq
                                                                    FROM flowinfo a
                                                                    LEFT JOIN flow_version b ON a.flowid = b.flowid WHERE a.status !={(int)FlowStatus.delete}  {where}").AsList();
        }
        public List<PageFlowInstanceTable> Instances(QueryModel queryModel, out long Total)
        {
            Total = 0;
            string where = "";
            if (!string.IsNullOrEmpty(queryModel.KeyWord))
            {
                where += $"   AND flow_task.instanceid LIKE '%{queryModel.KeyWord}%' or flow_task.title LIKE '%{queryModel.KeyWord}%' ";
            }
            if (string.IsNullOrEmpty(queryModel.SortField))
            {
                queryModel.SortField = "flow_instance.initiatordate";
                queryModel.IsAsc = false;
            }
            else
            {
                queryModel.SortField = queryModel.SortField.ToLower().Replace("dic_", "").Replace("dic", "").Trim();
            }
            if (queryModel.ExceptDataField.Any())
            {
                queryModel.ExceptDataField.ForEach(o =>
                {
                    if (!string.IsNullOrEmpty(o.Value + ""))
                        where += $" AND flow_task.{o.Label}!='{o.Value}'";
                });
            }
            if (queryModel.paramters.Any())
            {
                queryModel.paramters.ForEach(o =>
                {
                    if (!string.IsNullOrEmpty(o.Value + ""))
                        where += $" AND flow_instance.{o.Label}='{o.Value}'";
                });
            }
            string asc = queryModel.IsAsc ? "ASC" : "DESC";
            queryModel.Page = queryModel.Page == 0 ? 1 : queryModel.Page;
            StringBuilder sql = new StringBuilder();
            //where = string.IsNullOrEmpty(where) ? "" : $"where {where}";
            sql.Append(@$"SELECT count(flow_task.instanceid) as cnt FROM flow_instance flow_instance
	                        LEFT JOIN flow_instance_currenttask flow_instance_currenttask ON flow_instance_currenttask.groupid = flow_instance_currenttask.groupid
	                        LEFT JOIN flow_task flow_task ON flow_task.taskid = flow_instance_currenttask.currenttaskid 
                        WHERE  flow_task.`type` ={(int)FlowTaskType.task} 
                            {where} ;");
            sql.Append(@$"  SELECT
	                        flow_task.instanceid,
	                        flow_task.taskid,
	                        flow_task.title,
	                        flow_task.groupid,
	                        flow_task.stepid,
	                        flow_task.reviewer,
	                        flow_task.sender,
	                        flow_task.receivingtime,
	                        flow_task.processtime,
	                        flow_task.`type`, 
	                        flow_task.STATUS,
	                        flow_instance.flowid,
	                        flow_instance.flowversionid,
	                        flow_instance.formid,
	                        flow_instance.formversionid,
	                        flow_instance.flowstatus,
	                        flow_instance.initiatorempid,
	                        flow_instance.applyempid,
	                        flow_instance.applydate,
	                        flow_instance.complatedtime,
	                        flow_instance.initiatordate	
                        FROM
	                        flow_instance flow_instance
	                        LEFT JOIN flow_instance_currenttask flow_instance_currenttask ON flow_instance_currenttask.groupid = flow_instance_currenttask.groupid
	                        LEFT JOIN flow_task flow_task ON flow_task.taskid = flow_instance_currenttask.currenttaskid 
                        WHERE  flow_task.`type` ={(int)FlowTaskType.task} 
                            {where}  ORDER BY {queryModel.SortField} {asc} LIMIT {(queryModel.Page - 1) * queryModel.PageSize},{ queryModel.PageSize} ;");
            using (var reader = BasicDataConnection.QueryMultiple(sql.ToString()))
            {
                Total = reader.ReadFirstOrDefault<int>();
                List<PageFlowInstanceTable> pageModules = reader.Read<PageFlowInstanceTable>().ToList();
                long RowNo = ((queryModel.Page == 0 ? 1 : queryModel.Page) - 1) * queryModel.PageSize + 1;
                pageModules.ForEach(o =>
                {
                    o.RowNo = RowNo;
                    RowNo++;
                });
                return pageModules;
            }
        }
    }
}
