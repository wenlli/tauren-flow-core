﻿using System;
using System.Collections.Generic;
using System.Text;
using Tauren.Flow.Infrastructure.Globals;
using Dapper;
using Dapper.Contrib.Extensions;
using System.Linq;
using Tauren.Flow.Entity.Enum;
using Tauren.Flow.Entity.Model;

namespace Tauren.Flow.DLL.FlowEngine
{
    public class Flow_SrategyDLL : Base.BaseDLL, IFlow_SrategyDLL
    {
        public Flow_SrategyDLL(IConnectionDLLBase _dLLBase) : base(_dLLBase)
        {
        }

        /// <summary>
        /// 获取当前用户所有的上级职位
        /// </summary>
        /// <param name="empId"></param>
        /// <returns></returns>
        public string GetCurrentUserAllParentPositionId(string empId, ApproveParttimePostion approveParttimePostion)
        {

            switch (approveParttimePostion)
            {
                case ApproveParttimePostion.Empty:
                case ApproveParttimePostion.Min:
                    var empIds = BasicDataConnection.Query<Employee>($@"WITH RECURSIVE pos
                                                                            AS(
                                                                                SELECT  p1.positionId, p1.parentId from `position`  p1 inner join `position`  p on p1.positionId=p.parentId inner join employee e on e.positionId = p.positionId where
                                                                                e.empid in ({empId}) and p.enable = 1 and p.havedelete != 1
                                                                                UNION ALL
                                                                                SELECT  p.positionId, p.parentId from `position`  p inner join pos po on p.positionId = po.parentId where
                                                                                  p.enable = 1 and p.havedelete != 1
                                                                            )
                                                                            SELECT * FROM pos  order by positionId   ; ");
                    return $"{(string.Join(",", empIds.Select(o => o.OrgId)))}'";
                case ApproveParttimePostion.PartTime:
                    var orgids = BasicDataConnection.Query<Parttimeinfo>($@"WITH RECURSIVE pos
                                                                            AS(
                                                                                SELECT  p1.positionId, p1.parentId from `position`  p1 inner join `position`  p on p1.positionId=p.parentId   inner join parttimeinfo e on e.positionId = p.positionId where
                                                                                e.empid in ({empId}) and p.enable = 1 and p.havedelete != 1
                                                                                UNION ALL
                                                                                SELECT  p.positionId, p.parentId from `position`  p inner join pos po on p.positionId = po.parentId where
                                                                                  p.enable = 1 and p.havedelete != 1
                                                                            )
                                                                            SELECT * FROM pos  order by positionId   ;");
                    return $"{(string.Join(",", orgids.Select(o => o.OrgId)))}";
                default: return "";
            }
        }



        /// <summary>
        /// 获取当前用户的部门或者兼职部门
        /// </summary>
        /// <param name="empId"></param>
        /// <returns></returns>
        public string GetCurrentUserOrgid(string empId, ApproveParttimePostion approveParttimePostion)
        {
            switch (approveParttimePostion)
            {
                case ApproveParttimePostion.Empty:
                case ApproveParttimePostion.Min:
                    var empIds = BasicDataConnection.Query<Employee>($"SELECT orgId FROM employee WHERE in ({empId});");
                    return $"{(string.Join(",", empIds.Select(o => o.OrgId)))}'";
                case ApproveParttimePostion.PartTime:
                    var orgids = BasicDataConnection.Query<Parttimeinfo>($"SELECT orgid FROM parttimeinfo where empid in ({empId});");
                    return $"{(string.Join(",", orgids.Select(o => o.OrgId)))}"; ;
                default: return "";
            }
        }

        public string GetCurrentUserParentOrgid(string empId, ApproveParttimePostion approveParttimePostion)
        {
            switch (approveParttimePostion)
            {
                case ApproveParttimePostion.Empty:
                case ApproveParttimePostion.Min:
                    var empIds = BasicDataConnection.Query<Organization>($"SELECT parentId  from organization where orgId in (SELECT orgId FROM employee WHERE empid in({empId}) AND orgid !='0');");
                    return $"{(string.Join(",", empIds.Select(o => o.ParentId)))}";
                case ApproveParttimePostion.PartTime:
                    var orgids = BasicDataConnection.Query<Organization>($"SELECT parentId  from organization where orgId in (SELECT orgid FROM parttimeinfo where empid in ({empId}) AND orgid !='0');");
                    return $"{(string.Join(",", orgids.Select(o => o.ParentId)))}"; ;
                default: return "";
            }
        }

        /// <summary>
        /// 获取当前用户的主岗职位或者兼职岗位的间接上级职位
        /// </summary>
        /// <param name="empId"></param>
        /// <returns></returns>
        public string GetCurrentUserParentPositionId(string empId, ApproveParttimePostion approveParttimePostion)
        {
            switch (approveParttimePostion)
            {
                case ApproveParttimePostion.Empty:
                case ApproveParttimePostion.Min:
                    var empIds = BasicDataConnection.Query<Position>($"select p1.parentId from `position` p1 inner join `position` p on p1.positionId =p.parentId inner join employee e on e.positionId =p.positionId  where e.empid in({empId}) and p.parentId !=0");
                    return $"'{(string.Join("','", empIds.Select(o => o.ParentId)))}'";
                case ApproveParttimePostion.PartTime:
                    var positionIds = BasicDataConnection.Query<Position>($"select p1.parentId from `position` p1 inner join `position` p on p1.positionId =p.parentId inner join parttimeinfo e on e.positionId =p.positionId  where e.empid in({empId}) and p.parentId !=0");
                    return $"'{(string.Join("','", positionIds.Select(o => o.ParentId)))}'"; ;
                default: return "";
            }
        }


        /// <summary>
        /// 获取当前用户的主岗职位或者兼职岗位的上级职位
        /// </summary>
        /// <param name="empId"></param>
        /// <returns></returns>
        public string GetCurrentUserPositionId(string empId, ApproveParttimePostion approveParttimePostion)
        {
            switch (approveParttimePostion)
            {
                case ApproveParttimePostion.Empty:
                case ApproveParttimePostion.Min:
                    var empIds = BasicDataConnection.Query<Position>($"SELECT parentId  from position where positionid in (SELECT positionid FROM employee WHERE empid in({empId}) AND positionid !='0');");
                    return $"'{(string.Join("','", empIds.Select(o => o.ParentId)))}'";
                case ApproveParttimePostion.PartTime:
                    var positionIds = BasicDataConnection.Query<Position>($"SELECT parentId  from position where positionid in (SELECT positionid FROM parttimeinfo where empid in ({empId}) AND positionid !='0');");
                    return $"'{(string.Join("','", positionIds.Select(o => o.ParentId)))}'"; ;
                default: return "";
            }
        }

        public List<int> GetOrganizationAllUser(string empId)
        {
            List<int> empIds = new List<int>();
            List<Employee> employees = BasicDataConnection.Query<Employee>($"SELECT eb2.empid FROM employee eb LEFT JOIN employee eb2 on eb2.orgId=eb.orgId WHERE eb.empid in({empId}) and eb2.`enable`=1 AND eb2.havedelete !=1 AND eb2.empid!=eb.empid").AsList();
            empIds = employees.Select(o => o.EmpId).ToList();
            return empIds;
        }

        public List<int> GetOrganizationAllUserByOrgid(string orgId)
        {
            List<int> empIds = new List<int>();
            List<Employee> employees = BasicDataConnection.Query<Employee>($"SELECT eb2.empid FROM  employee eb2  where eb2.orgId in({orgId}) and eb2.`enable`=1 AND eb2.havedelete !=1;").AsList();
            empIds = employees.Select(o => o.EmpId).ToList();
            return empIds;
        }

        /// <summary>
        /// 获取当前ID的部门层级
        /// </summary>
        /// <param name="orgIds"></param>
        /// <param name="level"></param>
        /// <returns></returns>
        public List<Vw_Organizationroute> GetOrgPrincipal(string orgId, string orgType)
        {
            StringBuilder sb = new StringBuilder($@"SELECT o.*,e.empid FROM vw_organizationroute o inner JOIN position p on p.orgId=o.id and p.orgType=o.orgType AND p.`enable`=1 AND p.havedelete!=1
                                                LEFT JOIN employee e on e.positionId = p.positionId
                                                WHERE o.id in({orgId}) AND p.positiontype = 0 AND o.currenttype = '{orgType}' AND e.`enable`= 1 and e.havedelete != 1");
            throw new NotImplementedException();
        }

        public List<int> GetOrgSupervisorPositionUsers(string orgIds, SendParttimePostion sendParttimePostion)
        {
            List<int> empIds = new List<int>();
            switch (sendParttimePostion)
            {
                case SendParttimePostion.Min:
                    {
                        List<Employee> employees = BasicDataConnection.Query<Employee>($"SELECT eb.* FROM position p INNER JOIN employee eb on p.positionId=eb.positionId WHERE eb.`enable`=1 AND eb.havedelete!=1 and  p.positiontype=1 AND p.orgId in({orgIds})").AsList();
                        empIds = employees.Select(o => o.EmpId).ToList();
                    }
                    break;
                case SendParttimePostion.PartTime:
                    {
                        List<Employee> employees = BasicDataConnection.Query<Employee>($"SELECT eb.empid FROM position p INNER JOIN parttimeinfo prt on p.positionId=prt.positionId INNER  JOIN employee eb on prt.empid=eb.empid WHERE eb.`enable`=1 AND eb.havedelete!=1 and  p.positiontype=1 AND p.orgId in({orgIds});").AsList();
                        empIds = employees.Select(o => o.EmpId).ToList();
                    }
                    break;
                case SendParttimePostion.MinPriority:
                    {
                        List<Employee> employees = BasicDataConnection.Query<Employee>($"SELECT eb.* FROM position p INNER JOIN employee eb on p.positionId=eb.positionId WHERE eb.`enable`=1 AND eb.havedelete!=1 and  p.positiontype=1 AND p.orgId in({orgIds})").AsList();
                        if (employees.Any())
                        {
                            empIds = employees.Select(o => o.EmpId).ToList();
                        }
                        else
                        {
                            List<Employee> employees1 = BasicDataConnection.Query<Employee>($"SELECT eb.empid FROM position p INNER JOIN parttimeinfo prt on p.positionId=prt.positionId INNER  JOIN employee eb on prt.empid=eb.empid WHERE eb.`enable`=1 AND eb.havedelete!=1 and  p.positiontype=1 AND p.orgId in({orgIds});").AsList();
                            empIds = employees1.Select(o => o.EmpId).ToList();
                        }
                    }
                    break;
                case SendParttimePostion.PartTimePriority:
                    {
                        List<Employee> employees1 = BasicDataConnection.Query<Employee>($"SELECT eb.empid FROM position p INNER JOIN parttimeinfo prt on p.positionId=prt.positionId INNER  JOIN employee eb on prt.empid=eb.empid WHERE eb.`enable`=1 AND eb.havedelete!=1 and  p.positiontype=1 AND p.orgId in({orgIds});").AsList();
                        if (employees1.Any())
                        {
                            empIds = employees1.Select(o => o.EmpId).ToList();
                        }
                        else
                        {
                            List<Employee> employees = BasicDataConnection.Query<Employee>($"SELECT eb.* FROM position p INNER JOIN employee eb on p.positionId=eb.positionId WHERE eb.`enable`=1 AND eb.havedelete!=1 and  p.positiontype=1 AND p.orgId in({orgIds})").AsList();
                            empIds = employees1.Select(o => o.EmpId).ToList();
                        }
                    }
                    break;
            }
            return empIds;
        }

        /// <summary>
        /// 获取人员的所有上级
        /// </summary>
        /// <param name="empId"></param>
        /// <returns></returns>
        public List<int> GetPersonalALLuperior(string empId)
        {
            List<int> res = new List<int>();
            List<dynamic> result = BasicDataConnection.Query<dynamic>($@"WITH RECURSIVE T
                                                                        AS( 
                                                                            SELECT eb.repoter  FROM employee eb WHERE empid in ({empId}) and repoter !=0
                                                                            UNION ALL 
                                                                            SELECT U.repoter  
                                                                            FROM employee U INNER JOIN T ON U.empid=T.repoter  
                                                                        ) 
                                                                        SELECT * FROM T WHERE repoter is not null").AsList();
            if (result.Any())
            {
                foreach (var item in result)
                {
                    var r = item as IDictionary<string, object>;
                    if (r.ContainsKey("repoter") && !string.IsNullOrEmpty(item["repoter"] + ""))
                    {
                        res.Add(int.Parse(item["repoter"] + ""));
                    }
                }
            }
            return res;
        }

        /// <summary>
        /// 获取人员的直接上级
        /// </summary>
        /// <param name="empId"></param>
        /// <returns></returns>
        public List<dynamic> GetPersonalImmediateSuperior(string empId)
        {
            return this.BasicDataConnection.Query<dynamic>($"select repoter from employee where empid   in ({empId}) and repoter !=0").AsList();
        }

        /// <summary>
        /// 获取人员的间接上级
        /// </summary>
        /// <param name="empId"></param>
        /// <returns></returns>
        public List<dynamic> GetPersonalIndirectSuperior(string empId)
        {
            return this.BasicDataConnection.Query<dynamic>($"select repoter from employee where empid in(select repoter from employee where empid in ({empId}))  and repoter !=0").AsList();
        }

        /// <summary>
        /// 获取职位下的员工
        /// </summary>
        /// <param name="orgIds"></param>
        /// <param name="sendParttimePostion"></param>
        /// <returns></returns>
        public List<int> GetPositionUsers(string positioIds, SendParttimePostion sendParttimePostion)
        {
            List<int> empIds = new List<int>();
            switch (sendParttimePostion)
            {
                case SendParttimePostion.Min:
                    {
                        List<Employee> employees = BasicDataConnection.Query<Employee>($"SELECT eb.* FROM   employee eb where  eb.positionId in({positioIds}) and  eb.`enable`=1 AND eb.havedelete!=1  ").AsList();
                        empIds = employees.Select(o => o.EmpId).ToList();
                    }
                    break;
                case SendParttimePostion.PartTime:
                    {
                        List<Employee> employees = BasicDataConnection.Query<Employee>($"SELECT eb.empid FROM  parttimeinfo prt INNER JOIN employee eb on prt.empid=eb.empid WHERE eb.`enable`=1 AND eb.havedelete!=1 and prt.positionId in({positioIds});").AsList();
                        empIds = employees.Select(o => o.EmpId).ToList();
                    }
                    break;
                case SendParttimePostion.MinPriority:
                    {
                        List<Employee> employees = BasicDataConnection.Query<Employee>($"SELECT eb.* FROM   employee eb where  eb.positionId in({positioIds}) and  eb.`enable`=1 AND eb.havedelete!=1  ").AsList();
                        if (employees.Any())
                        {
                            empIds = employees.Select(o => o.EmpId).ToList();
                        }
                        else
                        {
                            List<Employee> employees1 = BasicDataConnection.Query<Employee>($"SELECT eb.empid FROM  parttimeinfo prt INNER JOIN employee eb on prt.empid=eb.empid WHERE eb.`enable`=1 AND eb.havedelete!=1 and prt.positionId in({positioIds});").AsList();
                            empIds = employees1.Select(o => o.EmpId).ToList();
                        }
                    }
                    break;
                case SendParttimePostion.PartTimePriority:
                    {
                        List<Employee> employees1 = BasicDataConnection.Query<Employee>($"SELECT eb.empid FROM  parttimeinfo prt INNER JOIN employee eb on prt.empid=eb.empid WHERE eb.`enable`=1 AND eb.havedelete!=1 and prt.positionId in({positioIds});").AsList();
                        if (employees1.Any())
                        {
                            empIds = employees1.Select(o => o.EmpId).ToList();
                        }
                        else
                        {
                            BasicDataConnection.Query<Employee>($"SELECT eb.* FROM   employee eb where  eb.positionId in({positioIds}) and  eb.`enable`=1 AND eb.havedelete!=1  ").AsList();
                            empIds = employees1.Select(o => o.EmpId).ToList();
                        }
                    }
                    break;
            }
            return empIds;
        }

        /// <summary>
        /// 获取当前职位所有的上级职位
        /// </summary>
        /// <param name="empId"></param>
        /// <returns></returns>
        public string GetCurrentPositionAllParentPositionId(string currentPositionId)
        {
            var empIds = BasicDataConnection.Query<Employee>($@"WITH RECURSIVE pos
                                                                AS(
                                                                    SELECT  p1.positionId, p1.parentId from `position`  p1 inner join `position`  p on p1.positionId=p.parentId   where
                                                                    p.positionId in ({currentPositionId}) and p.enable = 1 and p.havedelete != 1
                                                                    UNION ALL
                                                                    SELECT  p.positionId, p.parentId from `position`  p inner join pos po on p.positionId = po.parentId where
                                                                        p.enable = 1 and p.havedelete != 1
                                                                )
                                                                SELECT * FROM pos  order by positionId   ; ");
            return $"{(string.Join(",", empIds.Select(o => o.OrgId)))}'";
        }
        /// <summary>
        /// 获取当前职位间接上级职位
        /// </summary>
        /// <param name="empId"></param>
        /// <returns></returns>
        public string GetCurrentPositionParentPositionId(string currentPositionId)
        {
            var empIds = BasicDataConnection.Query<Position>($"select p1.parentId from `position` p1 inner join `position` p on p1.positionId =p.parentId  where p.positionId in({currentPositionId}) and p.parentId !=0");
            return $"'{(string.Join("','", empIds.Select(o => o.ParentId)))}'";
        }


        /// <summary>
        /// 获取当前 职位 的上级职位
        /// </summary>
        /// <param name="empId"></param>
        /// <returns></returns>
        public string GetCurrentPositionPositionId(string currentPositionId)
        {
            var empIds = BasicDataConnection.Query<Position>($"SELECT parentId  from position where positionid in ({currentPositionId});");
            return $"'{(string.Join("','", empIds.Select(o => o.ParentId)))}'";

        }

    }
}
