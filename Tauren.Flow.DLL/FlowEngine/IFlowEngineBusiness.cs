﻿using System;
using System.Collections.Generic;
using System.Text;
using Tauren.Flow.Entity.Global;
using Tauren.Flow.Entity.Model;
using Tauren.Flow.Entity.Output;

namespace Tauren.Flow.DLL.FlowEngine
{
    /// <summary>
    /// 流程引擎执行数据库
    /// </summary>
    public interface IFlowEngineBusiness : Base.IBaseDLL
    {
        /// <summary>
        /// 完成任务
        /// </summary>
        /// <param name="taskInfo"></param>
        /// <returns></returns>
        bool ProcessComplete(FlowTaskInfo taskInfo);
        /// <summary>
        /// 获取所有的任务
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        List<Flow_Task> GetTasks(long groupId);

        /// <summary>
        /// 删除当前任务数据
        /// </summary>
        /// <param name="currentTaskId"></param>
        /// <param name="prevTaskId"></param>
        /// <returns></returns>
        bool DeleteCurrentTask(long currentTaskId, long prevTaskId);

        /// <summary>
        /// 删除当前任务数据
        /// </summary>
        /// <param name="currentTaskId"></param>
        /// <param name="prevTaskId"></param>
        /// <returns></returns>
        bool DeleteCurrentTask(long prevTaskId);

        bool DeleteCurrentTask(List<long>taskIds);

        /// <summary>
        /// 撤回时需要删除未处理的任务
        /// </summary>
        /// <param name="deletedTask"></param>
        /// <returns></returns>
        bool RecyclingDeleteTask(List<Flow_Task> deletedTask);

        /// <summary>
        /// 执行任务时更新流程申请时的业务数据
        /// </summary>
        /// <param name="complatedParameters"></param>
        /// <returns></returns>
        bool RunTaskUpdateApplyBusinessStatus(List<ComplatedParameter> complatedParameters);

        /// <summary>
        /// 删除流程实例
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        bool FlowDeleteInstanceId(long groupId, List<FlowDataAreaParameter> areas);

        /// <summary>
        /// 实例完成数据处理
        /// </summary>
        /// <param name="writeDatas"></param>
        /// <returns></returns>
        bool FlowComplatedDataProcess(List<WriteData> writeDatas);
        /// <summary>
        /// 获取要回写数据的字段
        /// </summary>
        /// <param name="tableCodes"></param>
        /// <returns></returns>
        List<WriteField> GetWriteField(string tableCodes, long versionId);
        /// <summary>
        /// 删除分支的当前任务
        /// </summary>
        /// <param name="deleteTaskIds"></param>
        /// <returns></returns>
        bool DeleteBranchTask(List<long> deleteTaskIds);
    }
}
