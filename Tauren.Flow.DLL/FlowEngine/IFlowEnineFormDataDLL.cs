﻿using System;
using System.Collections.Generic;
using System.Text;
using Tauren.Flow.Entity.Global;
using Tauren.Flow.Entity.Model;

namespace Tauren.Flow.DLL.FlowEngine
{
    /// <summary>
    /// 表单数据操作
    /// </summary>
    public interface IFlowEnineFormDataDLL : Base.IBaseDLL
    {
        /// <summary>
        /// 保存表单数据
        /// </summary>
        /// <param name="areaParameters"></param>
        /// <returns></returns>
        bool FlowFormDataSave(List<FlowDataAreaParameter> areaParameters);
        /// <summary>
        /// 删除流程和表单数据对应的映射表
        /// </summary>
        /// <param name="InstacneId"></param>
        /// <returns></returns>
        bool DeleteFlowAndFromDataMaspping(long InstacneId);
        /// <summary>
        /// 更新流程实例数据
        /// </summary>
        /// <param name="_Instance"></param>
        /// <returns></returns>
        bool UpdateInstanceId(Flow_Instance _Instance);
        /// <summary>
        /// 查询条件
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="conditions"></param>
        /// <returns></returns>
        List<T> Query<T>(string conditions) where T : class, new();

        /// <summary>
        /// 更新当前任务
        /// </summary>
        /// <param name="currentTasks"></param>
        /// <param name="grouId"></param>
        /// <returns></returns>
        bool AddCurrentTask(List<Flow_Instance_CurrentTask> currentTasks, long grouId);
    }
}
