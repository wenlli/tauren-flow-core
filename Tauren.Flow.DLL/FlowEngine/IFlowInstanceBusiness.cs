﻿using System;
using System.Collections.Generic;
using System.Text;
using Tauren.Flow.Entity.Global;
using Tauren.Flow.Entity.Model;
using Tauren.Flow.Entity.Output;

namespace Tauren.Flow.DLL.FlowEngine
{
    /// <summary>
    /// 流程引擎执行数据库
    /// </summary>
    public interface IFlowInstanceBusiness : Base.IBaseDLL
    {
        /// <summary>
        /// 获取流程树形结构
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        List<FlowInstanceTree> GetFlowTree(QueryModel query);

        /// <summary>
        /// 获取流程实列数据
        /// </summary>
        /// <param name="queryModel"></param>
        /// <param name="Total"></param>
        /// <returns></returns>
        List<PageFlowInstanceTable> Instances(QueryModel queryModel, out long Total);
    }
}
