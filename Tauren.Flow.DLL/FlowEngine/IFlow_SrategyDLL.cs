﻿using System;
using System.Collections.Generic;
using System.Text;
using Tauren.Flow.Entity.Enum;
using Tauren.Flow.Entity.Model;

namespace Tauren.Flow.DLL.FlowEngine
{
    public interface IFlow_SrategyDLL : Base.IBaseDLL
    {
        /// <summary>
        /// 获取人员的直接上级
        /// </summary>
        /// <param name="empId"></param>
        /// <returns></returns>
        List<dynamic> GetPersonalImmediateSuperior(string empId);

        /// <summary>
        /// 获取人员的间接上级
        /// </summary>
        /// <param name="empId"></param>
        /// <returns></returns>
        List<dynamic> GetPersonalIndirectSuperior(string empId);

        /// <summary>
        /// 获取人员的所有上级
        /// </summary>
        /// <param name="empId"></param>
        /// <returns></returns>
        List<int> GetPersonalALLuperior(string empId);

        /// <summary>
        /// 获取当前用户的部门或者兼职部门
        /// </summary>
        /// <param name="empId"></param>
        /// <returns></returns>
        string GetCurrentUserOrgid(string empId, ApproveParttimePostion approveParttimePostion);

        /// <summary>
        /// 获取当前用户的部门或者兼职部门上级部门
        /// </summary>
        /// <param name="empId"></param>
        /// <returns></returns>
        string GetCurrentUserParentOrgid(string empId, ApproveParttimePostion approveParttimePostion);

        /// <summary>
        /// 获取主管职位下员工
        /// </summary>
        /// <param name="orgIds"></param>
        /// <param name="sendParttimePostion"></param>
        /// <returns></returns>
        List<int> GetOrgSupervisorPositionUsers(string orgIds, SendParttimePostion sendParttimePostion);

        /// <summary>
        /// 获取当前员工所在部门下所有人
        /// </summary>
        /// <param name="empId"></param>
        /// <returns></returns>
        List<int> GetOrganizationAllUser(string empId);

        /// <summary>
        /// 获取当前ID的部门层级
        /// </summary>
        /// <param name="orgIds"></param>
        /// <param name="level"></param>
        /// <returns></returns>
        List<Vw_Organizationroute> GetOrgPrincipal(string orgId, string orgType);

        /// <summary>
        /// 获取当前员工所在部门下所有人
        /// </summary>
        /// <param name="empId"></param>
        /// <returns></returns>
        List<int> GetOrganizationAllUserByOrgid(string orgId);

        /// <summary>
        /// 获取当前用户的主岗职位或者兼职岗位的上级部门
        /// </summary>
        /// <param name="empId"></param>
        /// <returns></returns>
        string GetCurrentUserPositionId(string empId, ApproveParttimePostion approveParttimePostion);

        /// <summary>
        /// 获取当前用户的主岗职位或者兼职岗位的间接上级部门
        /// </summary>
        /// <param name="empId"></param>
        /// <returns></returns>
        string GetCurrentUserParentPositionId(string empId, ApproveParttimePostion approveParttimePostion);

        /// <summary>
        /// 获取职位下的员工
        /// </summary>
        /// <param name="orgIds"></param>
        /// <param name="sendParttimePostion"></param>
        /// <returns></returns>
        List<int> GetPositionUsers(string positioIds, SendParttimePostion sendParttimePostion);

        /// <summary>
        /// 获取当前用户所有的上级职位
        /// </summary>
        /// <param name="empId"></param>
        /// <returns></returns>
        string GetCurrentUserAllParentPositionId(string empId, ApproveParttimePostion approveParttimePostion);

        /// <summary>
        /// 获取当前职位所有的上级职位
        /// </summary>
        /// <param name="empId"></param>
        /// <returns></returns>
        string GetCurrentPositionAllParentPositionId(string currentPositionId);
        /// <summary>
        /// 获取当前职位间接上级职位
        /// </summary>
        /// <param name="empId"></param>
        /// <returns></returns>
        string GetCurrentPositionParentPositionId(string currentPositionId);


        /// <summary>
        /// 获取当前 职位 的上级职位
        /// </summary>
        /// <param name="empId"></param>
        /// <returns></returns>
        string GetCurrentPositionPositionId(string currentPositionId);

    }
}
