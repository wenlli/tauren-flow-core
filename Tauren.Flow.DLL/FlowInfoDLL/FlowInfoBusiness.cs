﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tauren.Flow.Entity.Global;
using Tauren.Flow.Entity.Model;
using Tauren.Flow.Entity.Output;
using Tauren.Flow.Infrastructure.Globals;

namespace Tauren.Flow.DLL.FlowInfoDLL
{
    /// <summary>
    /// 流程基本信息
    /// </summary>
    internal class FlowInfoBusiness : Base.BaseDLL, IFlowInfoBusiness
    {
        public FlowInfoBusiness(IConnectionDLLBase _dLLBase) : base(_dLLBase)
        {
        }

        /// <summary>
        /// 根据ID获取默认的单个数据
        /// </summary>
        /// <param name="FlowId"></param>
        /// <returns></returns>
        public FlowInfo FirstOrDefault(long FlowId) => BusinessConnection.QueryFirstOrDefault<FlowInfo>("select * from flowinfo where flowid=@FlowId", new FlowInfo() { FlowId = FlowId });

        /// <summary>
        /// 获取流程表单
        /// </summary>
        /// <param name="AutoId"></param>
        /// <returns></returns>
        public List<Form_OutPut> Forms(long AutoId) => FormConnection.Query<Form_OutPut>($@"select  ff.autoid, ff.cnname, ff.enname, ff.currentversion, ffv.autoid  currentversionid
                                                                                            from
                                                                                                field_form ff
                                                                                            left
                                                                                            join field_form_version ffv on
                                                                                            ff.autoid = ffv.formid

                                                                                            and ff.currentversion = ffv.versionno where ff.autoid ={AutoId}").ToList();
        /// <summary>
        /// 根据FLOWID获取设置时的数据
        /// </summary>
        /// <param name="FlowId"></param>
        /// <returns></returns>
        public ExtFlowInfo GetFlowSettingInfo(long FlowId, long VersionId)
        {
            StringBuilder stringBuilder = new StringBuilder();
            string where = "";
            if (VersionId > 0)
            {
                stringBuilder.Append($@"flow_version");
                where = $" where b.versionid={VersionId}";
            }
            else
            {
                stringBuilder.Append($@"(select fv.* from flow_version fv  where  fv.flowid = {FlowId}  order by fv.creatdate desc  limit 0, 1)");
                where = $" where  fsi.flowid={FlowId} ";
            }
            return BusinessConnection.QueryFirstOrDefault<ExtFlowInfo>($@"select fsi.*,b.versionid versionid,b.versionno versionno,b.paralleltask,b.sumbitprevstep,b.name versionname,b.formid,b.formversionid from flowinfo fsi left join {stringBuilder.ToString()} b on   fsi.flowid=b.flowid  {where}");
        }

        /// <summary>
        /// 判断数据是否存在
        /// </summary>
        /// <param name="FlowId"></param>
        /// <param name="CNName"></param>
        /// <param name="ENName"></param>
        /// <returns></returns>
        public bool IsRepeat(long FlowId, string CNName, string ENName)
        {
            string sql = $"SELECT top 1 *  FROM flowinfo WHERE enname='{CNName}' or enname='{ENName}'";
            if (FlowId > 0)
            {
                sql = $"{sql} AND flowid != {FlowId}";
            }
            return BusinessConnection.Query<Entity.Model.FlowInfo>(sql).Any();
        }

        /// <summary>
        /// 分页获取数据
        /// </summary>
        /// <param name="queryModel"></param>
        /// <param name="Total"></param>
        /// <returns></returns>
        public List<PageFlowInfoTable> Pages(QueryModel queryModel, out long Total)
        {
            Total = 0;
            string where = "";
            if (!string.IsNullOrEmpty(queryModel.KeyWord))
            {
                where += $"   AND r.cnname LIKE '%{queryModel.KeyWord}%' or r.enname LIKE '%{queryModel.KeyWord}%'";
            }
            if (string.IsNullOrEmpty(queryModel.SortField))
            {
                queryModel.SortField = "sequence";
                queryModel.IsAsc = false;
            }
            else
            {
                queryModel.SortField = queryModel.SortField.ToLower().Replace("dic_", "").Replace("dic", "").Trim();
            }
            if (queryModel.ExceptDataField.Any())
            {
                queryModel.ExceptDataField.ForEach(o =>
                {
                    if (!string.IsNullOrEmpty(o.Value + ""))
                        where += $" AND r.{o.Label}!='{o.Value}'";
                });
            }
            if (queryModel.paramters.Any())
            {
                queryModel.paramters.ForEach(o =>
                {
                    if (!string.IsNullOrEmpty(o.Value + ""))
                        where += $" AND r.{o.Label}='{o.Value}'";
                });
            }
            if (!queryModel.UseDisabledData)
            {
                where += $" AND r.status=3  ";
            }
            string asc = queryModel.IsAsc ? "ASC" : "DESC";
            queryModel.Page = queryModel.Page == 0 ? 1 : queryModel.Page;
            StringBuilder sql = new StringBuilder();
            where = string.IsNullOrEmpty(where) ? "" : $"where {where}";
            sql.Append(@$"SELECT count(r.flowid) as cnt FROM flowinfo r {where}  ;");
            sql.Append(@$"SELECT r.flowid, r.cnname, r.enname, r.type, r.flowicon,r.currentversion,r.status, r.`sequence`, r.creator, r.modifier, r.creatdate, r.modifdate,r.status,fv.name versionname,fv.versionid currentversionid FROM flowinfo r left join flow_version fv on fv.versionno=r.currentversion and fv.flowid=r.flowid
                            {where}  ORDER BY {queryModel.SortField} {asc} LIMIT {(queryModel.Page - 1) * queryModel.PageSize},{ queryModel.PageSize} ;");
            using (var reader = BusinessConnection.QueryMultiple(sql.ToString()))
            {
                Total = reader.ReadFirstOrDefault<int>();
                List<PageFlowInfoTable> pageModules = reader.Read<PageFlowInfoTable>().ToList();
                long RowNo = ((queryModel.Page == 0 ? 1 : queryModel.Page) - 1) * queryModel.PageSize + 1;
                pageModules.ForEach(o =>
                {
                    o.RowNo = RowNo;
                    RowNo++;
                });
                return pageModules;
            }
        }

        /// <summary>
        /// 获取步骤信息
        /// </summary>
        /// <param name="FlowId"></param>
        /// <param name="VersionId"></param>
        /// <returns></returns>
        public List<Flow_NodeInfos> StepInfo(long FlowId, long VersionId = 0)
        {
            string versionSql;
            if (VersionId > 0)
            {
                versionSql = $"where ffs.versionId ={VersionId}";
            }
            else
            {
                versionSql = $@"inner join (select
                                                versionid
                                            from

                                                flow_version fv
                                            where

                                                fv.flowid = {FlowId}
                                            order by

                                                fv.creatdate desc
                                            limit 0,
                                            1) b on fsi.versionid = b.versionid ";
            }
            StringBuilder stringBuilder = new StringBuilder(@"select 
	                                                            fsi.*,
                                                                fss.IsAutoSelectStep,
                                                                fss.ApprovalStrategy,
                                                                fss.SkipStrategy,
                                                                fss.StrategyType,
                                                                fss.ApproveParttimePostion,
                                                                fss.SendParttimePostion,
                                                                fss.ApproverType,
                                                                fss.FormField,
                                                                fss.FormFieldApproveType,
                                                                fss.AppoverRange,
                                                                fss.DefaultApprover,
                                                                fss.OrgLeve,
                                                                fss.CCStrategyType,
                                                                fss.CCApproveParttimePostion,
                                                                fss.CCSendParttimePostion,
                                                                fss.CCType,
                                                                fss.CCFormField,
                                                                fss.CCFormFieldApproveType,
                                                                fss.CCUser,
                                                                fss.CCOrgLeve 
                                                            from
	                                                            flow_step_info fsi
                                                            left join flow_step_strategy fss on
	                                                            fss.stepid = fsi.stepid and fss.versionid=fsi.versionid
	                                                        ");
            stringBuilder.Append(versionSql);
            return BusinessConnection.Query<Flow_NodeInfos>(stringBuilder.ToString()).AsList();
        }

        /// <summary>
        /// 获取流程连线配置
        /// </summary>
        /// <param name="FlowId"></param>
        /// <param name="VersionId"></param>
        /// <returns></returns>
        public List<Flow_Lines_OutPut> FlowLines(long FlowId, long VersionId = 0)
        {
            string versionSql;
            if (VersionId > 0)
            {
                versionSql = $"where fl.versionId ={VersionId}";
            }
            else
            {
                versionSql = $@"inner join (select
                                                versionid
                                            from

                                                flow_version fv
                                            where

                                                fv.flowid = {FlowId}
                                            order by

                                                fv.creatdate desc
                                            limit 0,
                                            1) b on fl.versionid = b.versionid ";
            }
            StringBuilder stringBuilder = new StringBuilder(@"select 
	                                                            fl.*                                                               
                                                            from
	                                                            flow_lines fl 
	                                                        ");
            stringBuilder.Append(versionSql);
            return BusinessConnection.Query<Flow_Lines_OutPut>(stringBuilder.ToString()).AsList();
        }

        /// <summary>
        /// 防止数据库添加重复数据
        /// </summary>
        /// <param name="Model">表单数据</param>
        /// <returns></returns>
        public long UnRepeatAdd(FlowInfo Model)
        {
            StringBuilder sb = new StringBuilder(@"INSERT INTO flowinfo (flowid, cnname, enname, `type`, manager, instancemanager, formid, selectapplyemp, flowicon, flowstyle, placeholder, skiprules, sendmessage, currentversion, status, `sequence`, creator, modifier, creatdate, modifdate)
                                                                 SELECT @FlowId, @CNName, @ENName, @Type, @Manager,@InstanceManager,@FormId,@SelectApplyEmp,@FlowIcon,@FlowStyle,@Placeholder,@SkipRules,@SendMessage,@CurrentVersion,@Status,
	                                                    (select ifnull(max(`sequence`),0)+1 from flowinfo), @Creator, @Modifier, now(), now()
                                                    FROM DUAL
                                                    WHERE NOT EXISTS (
	                                                    SELECT *
	                                                    FROM flowinfo
	                                                    WHERE cnname =@CNName
		                                                    AND enname =@ENName
                                                    );");
            return this.BusinessConnection.Execute(sb.ToString(), Model);
        }

        /// <summary>
        /// 获取按钮信息
        /// </summary>
        /// <param name="FlowId"></param>
        /// <param name="VersionId"></param>
        /// <returns></returns>
        public List<Flow_Buttons_Put> Buttons(long FlowId, long VersionId = 0)
        {
            string versionSql;
            if (VersionId > 0)
            {
                versionSql = $"where fb.versionId ={VersionId}";
            }
            else
            {
                versionSql = $@"inner join (select
                                                versionid
                                            from

                                                flow_version fv
                                            where

                                                fv.flowid = {FlowId}
                                            order by

                                                fv.creatdate desc
                                            limit 0,
                                            1) b on fb.versionid = b.versionid ";
            }
            StringBuilder stringBuilder = new StringBuilder(@"select 
	                                                            fb.*                                                               
                                                            from
	                                                            flow_buttons fb 
	                                                        ");
            stringBuilder.Append(versionSql);
            return BusinessConnection.Query<Flow_Buttons_Put>(stringBuilder.ToString()).AsList();
        }

        public List<Field_Form_VersionExt> StepFormInfo(long VersionId) => BusinessConnection.Query<Field_Form_VersionExt>(@"select
	                                                                                                                            a.versionid ,
	                                                                                                                            0 formid,
	                                                                                                                            a.autoid AreaId,
	                                                                                                                            1 AreaUsable,
	                                                                                                                            a.areacode AreaCode,
	                                                                                                                            a.TableCode,
	                                                                                                                            a.MainTable,
	                                                                                                                            b.autoid FieldId,
	                                                                                                                            b.FieldCode,
	                                                                                                                            b.FieldKey,
	                                                                                                                            b.required,
	                                                                                                                            b.`show` ,
	                                                                                                                            b.editor,
                                                                                                                                a.stepid,
                                                                                                                                a.editor as areaeditor,
                                                                                                                                a.show as areashow
                                                                                                                            from
	                                                                                                                            flow_step_field_area a
                                                                                                                            left join flow_step_area_fields b on
	                                                                                                                            b.areaid = a.autoid
                                                                                                                            where  a.versionid = @VersionId", new Entity.Model.Flow_Step_Info { VersionId = VersionId }).AsList();

        public FlowTaskInfo QueryCurrentFlowTaskInfo(long TaskId)
        {
            this.BasicDataConnection.Execute("update  flow_task set `read` =1 where  taskid =@TaskId ", new { TaskId = TaskId });
            return BasicDataConnection.QueryFirstOrDefault<FlowTaskInfo>("select a.*,b.flowid,b.flowversionid,b.formid,b.formversionid,b.initiatorempid,b.applyempid,a.taskid currenttaskid,a.stepid currentstep,b.flowstatus from flow_task a left join flow_instance b on b.groupid=a.groupid  where a.taskid=@TaskId", new { TaskId = TaskId });
        }

        public List<FlowTaskInfo> QueryALLFlowTasks(long GroupId) => BasicDataConnection.Query<FlowTaskInfo>("select a.*,b.flowid,b.flowversionid,b.formid,b.formversionid,b.initiatorempid,b.applyempid,a.taskid currenttaskid,a.stepid currentstep,b.flowstatus from flow_task a left join flow_instance b on b.groupid=a.groupid  where a.groupid=@GroupId", new { GroupId = GroupId }).AsList();

        /// <summary>
        /// 根据当前任务找到上游任务
        /// </summary>
        /// <param name="CurrentTaskId"></param>
        /// <returns></returns>
        public List<ExtFlow_Instance_CurrentTask> GetCurrentTaskPrevTask(long CurrentTaskId) => BasicDataConnection.Query<ExtFlow_Instance_CurrentTask>("SELECT * FROM flow_instance_currenttask WHERE currenttaskid=@CurrentTaskId", new { CurrentTaskId = CurrentTaskId }).AsList();

        public List<Flow_Complated_Info_OutPut> EndStepFields(long flowVersionId) => BusinessConnection.Query<Flow_Complated_Info_OutPut>("SELECT * FROM flow_complated_info where  flowversionid=@flowVersionId", new { flowVersionId = flowVersionId }).ToList();

        public bool DeleteFlowComplatedInfo(long flowVersionId) => BusinessConnection.Execute("DELETE FROM flow_complated_info where  flowversionid=@flowVersionId", new { flowVersionId = flowVersionId }) >= 0;
    }
}
