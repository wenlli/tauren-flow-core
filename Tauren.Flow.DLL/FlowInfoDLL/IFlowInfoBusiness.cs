﻿using System;
using System.Collections.Generic;
using System.Text;
using Tauren.Flow.DLL.Base;
using Tauren.Flow.Entity.Global;
using Tauren.Flow.Entity.Model;
using Tauren.Flow.Entity.Output;

namespace Tauren.Flow.DLL.FlowInfoDLL
{
    /// <summary>
    /// 流程基本信息
    /// </summary>
    public interface 
        IFlowInfoBusiness : IBaseDLL
    {
        /// <summary>
        /// 分页获取信息
        /// </summary>
        /// <param name="queryModel"></param>
        /// <param name="Total"></param>
        /// <returns></returns>
        List<PageFlowInfoTable> Pages(QueryModel queryModel, out long Total);

        /// <summary>
        /// 防止数据库添加重复数据
        /// </summary>
        /// <param name="Model">表单数据</param>
        /// <returns></returns>
        long UnRepeatAdd(FlowInfo Model);
        /// <summary>
        /// 判断数据库中是否已存在该名称
        /// </summary>
        /// <param name="autoId"></param>
        /// <param name="tableCode"></param>
        /// <returns></returns>
        bool IsRepeat(long FlowId, string CNName, string ENName);

        /// <summary>
        /// 根据ID获取默认的单个数据
        /// </summary>
        /// <param name="FlowId"></param>
        /// <returns></returns>
        FlowInfo FirstOrDefault(long FlowId);

        /// <summary>
        /// 获取流程表单
        /// </summary>
        /// <param name="AutoId"></param>
        /// <returns></returns>
        List<Form_OutPut> Forms(long AutoId);

        /// <summary>
        /// 获取步骤信息
        /// </summary>
        /// <param name="FlowId"></param>
        /// <param name="VersionId"></param>
        /// <returns></returns>
        List<Flow_NodeInfos> StepInfo(long FlowId, long VersionId = 0);

        /// <summary>
        /// 根据id获取流程设置数据
        /// </summary>
        /// <param name="FlowId"></param>
        /// <returns></returns>
        ExtFlowInfo GetFlowSettingInfo(long FlowId, long VersionId);

        /// <summary>
        /// 获取流程连线配置
        /// </summary>
        /// <param name="FlowId"></param>
        /// <param name="VersionId"></param>
        /// <returns></returns>
        List<Flow_Lines_OutPut> FlowLines(long FlowId, long VersionId = 0);

        /// <summary>
        /// 获取按钮信息
        /// </summary>
        /// <param name="FlowId"></param>
        /// <param name="VersionId"></param>
        /// <returns></returns>
        List<Flow_Buttons_Put> Buttons(long FlowId, long VersionId = 0);

        /// <summary>
        /// 流程版本和步骤获取节点字段
        /// </summary>
        /// <param name="VersionId"></param>
        /// <param name="StepId"></param>
        /// <returns></returns>
        List<Field_Form_VersionExt> StepFormInfo(long VersionId);

        /// <summary>
        /// 根据任务id获取任务
        /// </summary>
        /// <param name="TaskId"></param>
        /// <returns></returns>
        FlowTaskInfo QueryCurrentFlowTaskInfo(long TaskId);

        /// <summary>
        /// 根据groupId获取所有任务
        /// </summary>
        /// <param name="GroupId"></param>
        /// <returns></returns>
        List<FlowTaskInfo> QueryALLFlowTasks(long GroupId);

        /// <summary>
        /// 根据当前任务找到上游任务
        /// </summary>
        /// <param name="CurrentTaskId"></param>
        /// <returns></returns>
        List<ExtFlow_Instance_CurrentTask> GetCurrentTaskPrevTask(long CurrentTaskId);

        /// <summary>
        /// 根据流程版本和步骤ID获取字段
        /// </summary>
        /// <param name="FlowVersionId"></param>
        /// <param name="StepId"></param>
        /// <returns></returns>
        List<Flow_Complated_Info_OutPut> EndStepFields(long flowVersionId);

        /// <summary>
        /// 根据流程ID和流程版本ID删除回写配置
        /// </summary>
        /// <param name="flowId"></param>
        /// <param name="flowVersionId"></param>
        /// <returns></returns>
        bool DeleteFlowComplatedInfo(long flowVersionId);
    }
}
