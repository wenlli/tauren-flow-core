﻿using Dapper;
using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;
using Tauren.Flow.Entity.Enum;
using Tauren.Flow.Entity.Model;
using Tauren.Flow.Infrastructure.Globals;

namespace Tauren.Flow.DLL.FlowSortDLL
{
    public class FlowSortBusiness : Base.BaseDLL, IFlowSortBusiness
    {
        public FlowSortBusiness(IConnectionDLLBase _dLLBase) : base(_dLLBase)
        {
        }
        /// <summary>
        /// 添加排序
        /// </summary>
        /// <param name="flowSorts"></param>
        /// <returns></returns>
        public bool AddFlowSort(List<FlowSort> flowSorts)
        {
            this.BusinessConnection.Execute("delete from flowsort   where  empid =@EmpId", new { EmpId = Global.EmpId });
            return this.BusinessConnection.Insert(flowSorts) >= 0;
        }
        /// <summary>
        /// 获该员工所有流程
        /// </summary>
        /// <returns></returns>
        public List<WorkFlowInfoSort> GetFlows()
        {
            StringBuilder sb = new StringBuilder(@$"select f.flowid,
                                                        fv.versionid FlowVersionId,
                                                        fv.formid,
                                                        fv.formversionid FormVersionId,
                                                        f.cnname,
                                                        f.enname,
                                                        f.flowicon,
                                                        f.flowstyle,
                                                        f.placeholder,
                                                        ifnull(sort.sequence, 0) sequence
                                                    from
                                                        flowinfo f
                                                    inner
                                                    join flow_version fv on
                                                    fv.versionno = f.currentversion

                                                    and fv.flowid = f.flowid

                                                        left join flowsort sort on sort.flowid = f.flowid
                                                    where

                                                        f.status ={(int)FlowStatus.installed}");           
            return this.BusinessConnection.Query<WorkFlowInfoSort>(sb.ToString()).AsList();
        }
    }
}
