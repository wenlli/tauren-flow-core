﻿using System;
using System.Collections.Generic;
using System.Text;
using Tauren.Flow.DLL.Base;
using Tauren.Flow.Entity.Model;

namespace Tauren.Flow.DLL.FlowSortDLL
{
    /// <summary>
    /// 流程排序使用
    /// </summary>
    public interface IFlowSortBusiness : IBaseDLL
    {
        /// <summary>
        /// 新增工作台
        /// </summary>
        /// <param name="workBenches"></param>
        /// <returns></returns>
        bool AddFlowSort(List<FlowSort> flowSorts);

        /// <summary>
        /// 获该员工所有流程
        /// </summary>
        /// <param name="flowids"></param>
        /// <returns></returns>
        List<WorkFlowInfoSort> GetFlows();
    }
}
