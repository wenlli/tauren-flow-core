﻿using System.Collections.Generic;
using Tauren.Flow.DLL.Base;
using Tauren.Flow.Entity.Output;

namespace Tauren.Flow.DLL.Resouces
{
    /// <summary>
    /// 数据资源
    /// </summary>
    public interface IMenuResourcesBusiness : IBaseDLL
    {
        /// <summary>
        /// 根据模块编码获取指定资源
        /// </summary>
        /// <param name="ModuleCode"></param>
        /// <returns></returns>
        List<ModuleResouceTable> GetByModuleCode(string ModuleCode);
        /// <summary>
        /// 根据ModuleCode和ResCode获取资源
        /// </summary>
        /// <param name="ResourcesCode"></param>
        /// <returns></returns>
        List<ModuleResouceTable> GetByResCodeAndMoCoed(string ResourcesCode, string ModuleCode, long AutoID);

    }
}
