﻿using Dapper;
using System.Collections.Generic;
using System.Linq;
using Tauren.Flow.Entity.Model;
using Tauren.Flow.Entity.Output;
using Tauren.Flow.Infrastructure.Globals;

namespace Tauren.Flow.DLL.Resouces
{
    public class MenuResourcesBusiness : Base.BaseDLL, IMenuResourcesBusiness
    {
        public MenuResourcesBusiness(IConnectionDLLBase _dLLBase) : base(_dLLBase)
        {
        }
        /// <summary>
        /// 根据模块编码获取指定资源
        /// </summary>
        /// <param name="ModuleCode"></param>
        /// <returns></returns>
        public List<ModuleResouceTable> GetByModuleCode(string ModuleCode) => this.ResConnection.Query<ModuleResouceTable>($"select m.autoid,m.rescode,m.menucode,m.resoucestype ,mr.`language`,mr.value from moduleresouces m left join moduleresouces_res mr on m.resourceId =mr.resourceId where m.menucode =@MenuCode and m.enable =1 and m.isdelete=0", new ModuleResouces() { MenuCode = ModuleCode }).ToList();
        /// <summary>
        /// 根据ModuleCode和ResCode获取资源
        /// </summary>
        /// <param name="ResourcesCode"></param>
        /// <returns></returns>
        public List<ModuleResouceTable> GetByResCodeAndMoCoed(string ResourcesCode, string ModuleCode, long AutoID)
        {
            string where = "";
            if (AutoID > 0) where = "and m.autoid <> " + AutoID;
            return this.ResConnection.Query<ModuleResouceTable>($"select m.autoid,m.rescode,m.menucode,m.resoucestype ,mr.`language`,mr.value from moduleresouces m inner join moduleresouces_res mr on m.resourceId =mr.resourceId where m.rescode =@ResCode and m.enable =1  and m.isdelete=0 and m.menucode =@MenuCode {where}", new ModuleResouces() { ResCode = ResourcesCode, MenuCode = ModuleCode }).ToList();
        }
    }
}
