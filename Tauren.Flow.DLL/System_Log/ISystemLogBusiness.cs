﻿/// <summary>
/// 日志操作
/// </summary>
namespace Tauren.Flow.DLL.System_Log
{
    public interface ISystemLogBusiness
    {
        /// <summary>
        /// 新增 
        /// </summary>
        /// <param name="tenant"></param>
        /// <returns></returns>
        long Add(Entity.Model.SystemLog model); 
    }
}
