﻿using Dapper.Contrib.Extensions;
using System.Data;

namespace Tauren.Flow.DLL.System_Log
{
    /// <summary>
    /// 日志操作
    /// </summary>
    public class SystemLogBusiness : ISystemLogBusiness
    {
        public SystemLogBusiness(IDbConnection dbConnection)
        {
            this.dbConnection = dbConnection;
        }

        public IDbConnection dbConnection { get; set; }
        /// <summary>
        /// 新增租户
        /// </summary>
        /// <param name="tenant"></param>
        /// <returns></returns>
        public long Add(Entity.Model.SystemLog model) => dbConnection.Insert<Entity.Model.SystemLog>(model);         
    }
}
