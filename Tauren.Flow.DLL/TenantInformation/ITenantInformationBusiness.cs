﻿namespace Tauren.Flow.DLL.TenantInformation
{
    public interface ITenantInformationBusiness
    { 
        /// <summary>
        /// 根据code获取数据
        /// </summary>
        /// <param name="autoId"></param>
        /// <returns></returns>
        Entity.Model.TenantInformation GetByCode(string code); 
    }
}
