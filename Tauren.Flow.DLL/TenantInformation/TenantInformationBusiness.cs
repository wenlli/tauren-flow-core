﻿using Dapper;
using System.Data;
using System.Linq;

namespace Tauren.Flow.DLL.TenantInformation
{
    /// <summary>
    /// 租户操作数据库层
    /// </summary>
    public class TenantInformationBusiness : ITenantInformationBusiness
    {
        public TenantInformationBusiness(IDbConnection dbConnection)
        {
            this.dbConnection = dbConnection;
        }

        public IDbConnection dbConnection { get; set; } 
        /// <summary>
        /// 根据编码获取数据
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>

        public Entity.Model.TenantInformation GetByCode(string code) => dbConnection.Query<Entity.Model.TenantInformation>("SELECT * FROM TenantInformation WHERE code=@code", new Entity.Model.TenantInformation() { Code = code }).AsList().FirstOrDefault(); 
    }
}
