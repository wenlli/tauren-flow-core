﻿

using System.Collections.Generic;

namespace Tauren.Flow.Entity.Config
{
    public class ApiVersionsConfig
    {
        public virtual List<ApiVersion> ApiVersions { get; set; }
        public virtual FilePath FilePath { get; set; }
        /// <summary>
        /// redis 服务器
        /// </summary>
        public virtual string RedisAdress { get; set; }
        /// <summary>
        /// Redis 服务端口
        /// </summary>
        public virtual string RedisPort { get; set; }
        public virtual string RedisPwd { get; set; }
        public virtual Consul Consul { get; set; }

        /// <summary>
        /// 微服务集成接口
        /// </summary>
        public virtual string GetWay { get; set; }

        /// <summary>
        /// 表单服务
        /// </summary>
        public virtual string Form { get; set; }

        /// <summary>
        /// 基础服务
        /// </summary>
        public virtual string Business { get; set; }
    }
    public class ApiVersion
    {
        public virtual string version { get; set; }
    }
    public class FilePath
    {
        /// <summary>
        /// 物理路径
        /// </summary>
        public virtual string PhysicalFilePath { get; set; }
        /// <summary>
        /// api文件地址
        /// </summary>
        public virtual string ApiFilePath { get; set; }

        /// <summary>
        /// 文件的最大值
        /// </summary>
        public virtual int MaxFile { get; set; }
        /// <summary>
        /// OnlyOffice版本
        /// </summary>
        public virtual string ServerVersion { get; set; }
        /// <summary>
        /// 接口地址
        /// </summary>
        public virtual string ApiUrl { get; set; }
        public virtual string FielsUrl { get; set; }
        /// <summary>
        /// 网站地址
        /// </summary>
        public virtual string WebUrl { get; set; }
        /// <summary>
        /// 文件大小
        /// </summary>
        public virtual long FileMaxSize { get; set; }

        /// <summary>
        /// 文件类型
        /// </summary>
        public virtual string LimitFileType { get; set; }
        /// <summary>
        /// 文件类型
        /// </summary>
        public virtual string[] LimitTypes
        {
            get
            {
                if (string.IsNullOrEmpty(LimitFileType)) return new string[] { };
                else return LimitFileType.Split(new string[] { "," }, System.StringSplitOptions.RemoveEmptyEntries);
            }
        }
    }
}
