﻿namespace Tauren.Flow.Entity.Config
{
    public class Consul
    {
        public virtual ConsulChild Api { get; set; }
        public virtual ConsulChild Service { get; set; }
    }
   public class ConsulChild
    {
        public virtual string Name { get; set; }
        public virtual string Address { get; set; }
        public virtual int Port { get; set; }
    }
}
