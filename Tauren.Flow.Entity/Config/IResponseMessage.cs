﻿/*
 * 目的：返回请求结果
 * 创建人：Author
 * 创建时间：2019.10.08 10:00
 * 修改人;
 * 修改目的：
 * 修改时间
 * 修改结果：
 */
  
namespace Tauren.Flow.Entity.Config
{
    public interface IResponseMessage
    {
        /// <summary>
        /// 状态
        /// </summary>
        bool Status { get; set; }
       /// <summary>
       /// 返回编码
       /// </summary>
        int Code { get; set; }
        /// <summary>
        /// 返回信息
        /// </summary>
        string Message { get; set; }
        /// <summary>
        /// 返回体数据
        /// </summary>
        object Body { get; set; }
    }
}
