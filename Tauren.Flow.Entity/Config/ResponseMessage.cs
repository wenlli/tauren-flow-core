﻿using Tauren.Flow.Entity.Enum;

namespace Tauren.Flow.Entity.Config
{
   public class ResponseMessage : IResponseMessage
    {
        /// <summary>
        /// 返回请求是否成功-true/false
        /// </summary>
        public virtual bool Status { get; set; }
        /// <summary>
        /// 返回HTTP请求响应码
        /// </summary>
        public virtual int Code { get; set; }
        /// <summary>
        /// 返回响应后的错误信息
        /// </summary>
        public virtual string Message { get; set; }
        /// <summary>
        /// 返回请求成功后的响应内容
        /// </summary>
        public virtual object Body { get; set; }
    }
    public static class ResponseMessageExtension
    {

        /// <summary>
        /// 成功时返回数据类型
        /// </summary>
        /// <param name="data"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public static IResponseMessage Succeed(this object data, int Code = ErrorType.SUCCESS_CODE, string message = ErrorMessage.SUCCESS_CODE)
        {
            return new ResponseMessage()
            {
                Code = Code,
                Status = true,
                Message = message ?? ErrorMessage.SUCCESS_CODE,
                Body = data
            };

        }
        /// <summary>
        /// 成功时返回数据类型
        /// </summary>
        /// <param name="data"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public static IResponseMessage OnSucceed(this object data, string message = ErrorMessage.SUCCESS_CODE)
        {
            return new ResponseMessage()
            {
                Code = 200,
                Status = true,
                Message = message ?? ErrorMessage.SUCCESS_CODE,
                Body = data
            };

        }
        /// <summary>
        /// 错误数据返回信息
        /// </summary>
        /// <param name="Code"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static IResponseMessage Failure(this int Code, string message = ErrorMessage.SERVER_ERROR)
        {
            return new ResponseMessage()
            {
                Status = false,
                Message = message,
                Body = "",
                Code = Code
            };
        }
    }
}
