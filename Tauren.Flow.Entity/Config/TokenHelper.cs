﻿/****************************************************************************
* 类名：TokenHelper
* 描述：jwt token 处理类
* 创建人：Author
* 创建时间：2019.10.08 10:00
* 修改人;Author
* 修改时间：2019.10.08 10:00
* 修改描述：
* **************************************************************************
*/


using System.IO;
using System.Xml.Serialization;

namespace Tauren.Flow.Entity.Config
{
    [XmlRoot("TOKENHELPER")]
    public class TokenHelper
    {
        /// <summary>
        /// 私钥
        /// </summary>
        [XmlElement("TOKENSECRE_KEY")]
        public virtual string TokenSecreKey { get; set; }

        /// <summary>
        /// 发行人
        /// </summary>
        [XmlElement("ISSUER")]
        public virtual string Issuer { get; set; }

        /// <summary>
        /// 订阅者
        /// </summary>
        [XmlElement("AUDIENCE")]
        public virtual string Audience { get; set; }


        /// <summary>
        /// 过期时间
        /// </summary>
        [XmlElement("EXPIRATION")]
        public virtual string Expiration { get; set; }

        [XmlElement("AUTHENTICATE_SCHEME")]
        public virtual string AuthenticateScheme { get; set; }
        [XmlElement("IDENTITYURL")]
        public virtual string IdentityUrl { get; set; }

    }
    public class TokenHelperExtension
    {
        /// <summary>
        /// 生成Token帮助类
        /// </summary>
        public virtual TokenHelper tokenHelper { get; set; }
        public TokenHelperExtension()
        {
            tokenHelper = GetTokenHelper();
        }
        /// <summary>
        /// 获取生成Token的配置
        /// </summary>
        /// <returns></returns>
        private TokenHelper GetTokenHelper()
        {
            TokenHelper tokenHelper = new TokenHelper();
            IConfigFile con = new GeneralConfFileOperator();
            string path = Path.Combine(Directory.GetCurrentDirectory(), "Config", "TokenHelper.xml");
            tokenHelper = con.ReadConfFile<TokenHelper>(path, false);
            return tokenHelper;
        }
    }

}
