﻿using System.Collections.Generic;
using Tauren.Flow.Entity.Enum;

namespace Tauren.Flow.Entity.Const
{
    public class GlobalConst
    {
        /// <summary>
        /// 到分
        /// </summary>
        public const string DATE_FORMAT_MINUTES = "yyyy-MM-dd HH:mm";
        /// <summary>
        /// 到天
        /// </summary>
        public const string DATE_FORMAT_DAY = "yyyy-MM-dd";
        /// <summary>
        /// 到小时
        /// </summary>
        public const string DATE_FORMAT_HOURS = "yyyy-MM-dd HH";
        /// <summary>
        /// 到秒
        /// </summary>
        public const string DATE_FORMAT_SECONDS = "yyyy-MM-dd HH:mm:ss";

        #region 标识

        /// <summary>
        /// 人员缓存标识
        /// </summary>
        public const string EMPLOYEE_FLAG_REDIS_KEY = "EMPLOYEE_FLAG_REDIS_KEY";

        /// <summary>
        /// 组织缓存标识
        /// </summary>
        public const string ORGANIZATION_FLAG_REDIS_KEY = "ORGANIZATION_FLAG_REDIS_KEY";

        /// <summary>
        /// 职位缓存标识
        /// </summary>
        public const string POSTIONS_FLAG_REDIS_KEY = "POSTIONS_FLAG_REDIS_KEY";

        /// <summary>
        /// 单位缓存标识
        /// </summary>
        public const string UNITS_FLAG_REDIS_KEY = "UNITS_FLAG_REDIS_KEY";

        /// <summary>
        /// 资源文件缓存标识
        /// </summary>
        public const string RESOURCES_FLAG_REDIS_KEY = "RESOURCES_FLAG_REDIS_KEY";

        /// <summary>
        /// 流程信息缓存key
        /// </summary>
        public const string FLOWINFO_FLAG_REDIS_KEY = "FLOWINFO_FLAG_REDIS_KEY";

        /// <summary>
        /// 流程列表缓存
        /// </summary>
        public const string FLOWINFO_LIST_FLAG_REDIS_KEY = "FLOWINFO_LIST_FLAG_REDIS_KEY";
        #endregion

        #region 模块标识 
        /// <summary>
        /// 语言
        /// </summary>
        public const string LANGUAGE_FLAG_MODULE_KEY = "LANGUAGE";



        /// <summary>
        /// 流程设置
        /// </summary>
        public const string RESOURCES_FLAG_MODULE_FLOW_INFO_KEY = "FLOWDESIGN";

        /// <summary>
        /// 实列管理
        /// </summary>
        public const string RESOURCES_FLAG_MODULE_FLOW_INSTANCE_KEY = "INSTANCE";

        #endregion
        #region 内容标识
        public const string LANGUAGE_CONTENT_LANGUAGE_KEY = "LANGUAGE";
        #region 资源文件数据
        public const string RESOURCES_CONTENT_ENABLE_KEY = "ENABLE";
        public const string RESOURCES_CONTENT_DISABLE_KEY = "DISABLE";

        #endregion
        #endregion

        /// <summary>
        /// 根据$分割
        /// </summary>
        public const string SEPARATOR_DOLLAR = "$";
        public const string RESOURCES_FLAG_MODULE_FLOWCENTER_KEY = "FLOWCENTER";

        public const string FORM_APPLY_EMPLOYEEBASICINFO = "form_apply_employeebasicinfo";
        public const string FLOW_OPINION_AREA = "flow_opinion_area";

        public static string SEPARATOR_COMMA = ",";
        public static string[] ViewBtns = new string[] { "course", "picture", "policy", "export" };
        public static string[] ViewCCBtns = new string[] { "course", "picture", "policy", "export", "read" };
        /// <summary>
        /// 已完成的任务状态
        /// </summary>
        public static int[] TaskComplatedStatus = new int[] {
            (int)FlowTaskStatus.otherterminated,
            (int)FlowTaskStatus.terminated,
            (int)FlowTaskStatus.approved,
            (int)FlowTaskStatus.otherapproved,
            (int)FlowTaskStatus.returned,
            (int)FlowTaskStatus.otherreturned,
            (int)FlowTaskStatus.recycled,
            (int)FlowTaskStatus.assigned,
            (int)FlowTaskStatus.transfer,
            (int)FlowTaskStatus.approved
        };
        /// <summary>
        /// 申请人姓名
        /// </summary>
        public const string FORM_APPLY_EMPLOYEEBASICINFO_CNNAME = "cnname";
        /// <summary>
        /// 申请人姓名
        /// </summary>
        public const string FORM_APPLY_EMPLOYEEBASICINFO_EMPID = "empid";
        public const string FORM_EMPLOYEEBASICINFO_TABLENAME = "employee";

        /// <summary>
        /// 申请表主键
        /// </summary>
        public const string FROM_DATA_PRIMARYKEYF = "autoid";
        /// <summary>
        /// 申请表主键
        /// </summary>
        public const string FROM_DATA_FOREIGNKEYF = "foreignkey";

        /// <summary>
        /// 重复提交KEY
        /// </summary>
        public const string FLOW_FORM_TASK_REPEAT_SUBMIT = "FLOW_FORM_TASK_REPEAT_SUBMIT";

        /// <summary>
        /// 流程系统指定人员
        /// </summary>
        public const int SystemFlowPersonnel = 99999;

        public const string NODE_INFO_MEAT_END = "end";
        public const string NODE_INFO_MEAT_START = "start";
        public const string NODE_INFO_MEAT_BRANCH = "branch";
        public const string NODE_INFO_MEAT_ENBRANCH = "endbranch";
        public const string NODE_INFO_MEAT_CONDITION = "condition";
        public static List<ApprovalType> ViewApprovalTypes = new List<ApprovalType>() { ApprovalType.circulated, ApprovalType.view, ApprovalType.approval };

        public const string AuthorizationTokenKey = "Authorization";
        public const string GolbalData = "GolbalData";
    }
}
