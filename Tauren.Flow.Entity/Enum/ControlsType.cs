﻿/*
 * 描述：控件字段类型枚举 
 */
using System.Collections.Generic;
using Tauren.Flow.Entity.Output;

namespace Tauren.Flow.Entity.Enum
{

    /// <summary>
    /// 控件类型
    /// </summary>
    public enum ControlType
    {
        /// <summary>
        /// 单行文本
        /// </summary>
        text,
        /// <summary>
        /// 多行文本
        /// </summary>
        textarea,
        /// <summary>
        /// 日期时间
        /// </summary>
        datetime,
        /// <summary>
        /// 日期
        /// </summary>
        date,
        /// <summary>
        /// 年
        /// </summary>
        year,
        /// <summary>
        /// 月
        /// </summary>
        month,
        /// <summary>
        /// 日
        /// </summary>
        day,
        /// <summary>
        /// 时间
        /// </summary>
        time,
        /// <summary>
        /// 动态块多记录
        /// </summary>
        repeat,//动态块多记录
        /// <summary>
        /// 人员选择
        /// </summary>
        employee,//人员选择
        /// <summary>
        /// 组织选择
        /// </summary>
        organizational,//组织选择
        /// <summary>
        /// 职位选择
        /// </summary>
        position,//职位选择
        /// <summary>
        /// 树形参数
        /// </summary>
        tree,//树形参数
        /// <summary>
        /// 图片显示
        /// </summary>
        image,//图片显示
        /// <summary>
        /// 文件上传
        /// </summary>
        upload,//文件上传
        /// <summary>
        /// 常用参数选择
        /// </summary>
        parameter,//常用参数选择
        number,///数字型
        records,///多记录,
        icon,
        radio,
        role,
        permission,
        buttonsType,
        /// <summary>
        /// 表信息
        /// </summary>
        tableinfo,
        /// <summary>
        /// 头像
        /// </summary>
        avatar,
        unit,
    }

    public enum PageType
    {
        add,
        edit,
        view
    }
    public enum SelectType
    {
        radio,
        multi
    }
    public enum FieldType
    {
        /// <summary>
        /// 字符
        /// </summary>
        varchar,
        /// <summary>
        /// 整型
        /// </summary>
        bigint,
        /// <summary>
        /// 浮点型
        /// </summary>
        doubled,
        /// <summary>
        /// 日期
        /// </summary>
        date,
        /// <summary>
        /// 年
        /// </summary>
        year,
        /// <summary>
        /// 月
        /// </summary>
        month,
        /// <summary>
        /// 日
        /// </summary>
        day,
        /// <summary>
        /// 日期时间型
        /// </summary>
        datetime,
        /// <summary>
        /// 布尔
        /// </summary>
        bit,
        /// <summary>
        /// 短整型
        /// </summary>
        iint
    }

    public class EnumComm
    {
        public static EnumComm enumComm { get; set; }

        public EnumComm()
        {
            enumComm = new EnumComm();
        }

        public static List<Options> EnumControlType()
        {
            List<Options> list = new List<Options>();

            foreach (var e in System.Enum.GetValues(typeof(ControlType)))
            {
                Options m = new Options();
                m.Value = e.ToString();
                m.Label = e.ToString();
                list.Add(m);
            }
            return list;
        }
        public static List<Options> EnumSelectType()
        {
            List<Options> list = new List<Options>();

            foreach (var e in System.Enum.GetValues(typeof(SelectType)))
            {
                Options m = new Options();
                m.Value = (int)e;
                m.Label = e.ToString();
                list.Add(m);
            }
            return list;
        }
        public static List<Options> EnumFieldType(string resources = "")
        {
            List<Options> list = new List<Options>();

            foreach (var e in System.Enum.GetValues(typeof(FieldType)))
            {
                Options m = new Options();
                if (!string.IsNullOrEmpty(resources))
                {
                    switch ((FieldType)e)
                    {
                        case FieldType.varchar:
                            m.Conditions.Add(new Options() { Label = resources, Value = ControlType.buttonsType.ToString() });
                            m.Conditions.Add(new Options() { Label = resources, Value = ControlType.employee.ToString() });
                            m.Conditions.Add(new Options() { Label = resources, Value = ControlType.icon.ToString() });
                            m.Conditions.Add(new Options() { Label = resources, Value = ControlType.image.ToString() });
                            m.Conditions.Add(new Options() { Label = resources, Value = ControlType.organizational.ToString() });
                            m.Conditions.Add(new Options() { Label = resources, Value = ControlType.parameter.ToString() });
                            m.Conditions.Add(new Options() { Label = resources, Value = ControlType.permission.ToString() });
                            m.Conditions.Add(new Options() { Label = resources, Value = ControlType.position.ToString() });
                            m.Conditions.Add(new Options() { Label = resources, Value = ControlType.records.ToString() });
                            m.Conditions.Add(new Options() { Label = resources, Value = ControlType.role.ToString() });
                            m.Conditions.Add(new Options() { Label = resources, Value = ControlType.tableinfo.ToString() });
                            m.Conditions.Add(new Options() { Label = resources, Value = ControlType.text.ToString() });
                            m.Conditions.Add(new Options() { Label = resources, Value = ControlType.textarea.ToString() });
                            m.Conditions.Add(new Options() { Label = resources, Value = ControlType.upload.ToString() });
                            m.Conditions.Add(new Options() { Label = resources, Value = ControlType.repeat.ToString() });
                            break;
                        case FieldType.bigint:
                            m.Conditions.Add(new Options() { Label = resources, Value = ControlType.number.ToString() });
                            break;
                        case FieldType.iint:
                            m.Conditions.Add(new Options() { Label = resources, Value = ControlType.number.ToString() });
                            break;
                        case FieldType.doubled:
                            m.Conditions.Add(new Options() { Label = resources, Value = ControlType.number.ToString() });
                            break;
                        case FieldType.date:
                            m.Conditions.Add(new Options() { Label = resources, Value = ControlType.date.ToString() });
                            break;
                        case FieldType.year:
                            m.Conditions.Add(new Options() { Label = resources, Value = ControlType.year.ToString() });
                            break;
                        case FieldType.month:
                            m.Conditions.Add(new Options() { Label = resources, Value = ControlType.month.ToString() });
                            break;
                        case FieldType.day:
                            m.Conditions.Add(new Options() { Label = resources, Value = ControlType.day.ToString() });
                            break;
                        case FieldType.datetime:
                            m.Conditions.Add(new Options() { Label = resources, Value = ControlType.datetime.ToString() });
                            m.Conditions.Add(new Options() { Label = resources, Value = ControlType.time.ToString() });
                            break;
                        case FieldType.bit:
                            m.Conditions.Add(new Options() { Label = resources, Value = ControlType.radio.ToString() });
                            break;
                        default:
                            break;
                    }
                }
                m.Value = (int)e;
                m.Label = e.ToString();
                list.Add(m);
            }
            return list;
        }
        public static ControlType ConvertToControlType(string value)
        {
            return (ControlType)(System.Enum.Parse(typeof(ControlType), value));
        }
    }
}
