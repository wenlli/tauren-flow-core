﻿using System;

namespace Tauren.Flow.Entity.Enum
{
    /// <summary>
    /// 文件夹归属于哪一类----单位/组织/个人
    /// </summary>
    public enum FolderType
    {
        /// <summary>
        /// 单位
        /// </summary>
        unit,
        /// <summary>
        /// 组织
        /// </summary>
        organization,
        /// <summary>
        /// 个人
        /// </summary>
        personal,
        /// <summary>
        /// 职位
        /// </summary>
        position
    }

    /// <summary>
    /// 组织类型
    /// </summary>
    public enum OrgationalType
    {
        /// <summary>
        /// 单位
        /// </summary>
        unit,
        /// <summary>
        /// 组织
        /// </summary>
        organization,
        /// <summary>
        /// 个人
        /// </summary>
        personal,
        /// <summary>
        /// 职位
        /// </summary>
        position,
        /// <summary>
        /// 兼职
        /// </summary>
        partjob
    }
    /// <summary>
    /// OnlyOffice 识别文档的类型
    /// </summary>
    public enum FileType
    {
        /// <summary>
        /// 文件夹
        /// </summary>
        Folder,
        /// <summary>
        /// 文本文档类
        /// </summary>
        Text,
        /// <summary>
        /// 表格类
        /// </summary>
        Spreadsheet,
        /// <summary>
        /// 幻灯片类
        /// </summary>
        Presentation,


    }
    public enum TextType
    {
        /// <summary>
        /// 文件夹
        /// </summary>
        FOLDER,
        /// <summary>
        /// 文本文档类
        /// </summary>
        Word,
        /// <summary>
        /// 表格类
        /// </summary>
        Excel,
        /// <summary>
        /// 幻灯片类
        /// </summary>
        PPT,
        /// <summary>
        /// 其他类型
        /// </summary>
        Other,
    }

    public enum TrackerStatus
    {
        NotFound = 0,
        Editing = 1,
        MustSave = 2,
        Corrupted = 3,
        Closed = 4,
    }

    public enum SystemType
    {
        /// <summary>
        /// 桌面系统
        /// </summary>
        desktop,
        /// <summary>
        /// 移动端系统
        /// </summary>
        mobile,
        embedded,
    }
    public enum EditType
    {
        /// <summary>
        /// 编辑模式
        /// </summary>
        edit = 0,
        /// <summary>
        /// 预览模式
        /// </summary>
        review = 1,
        view = 2,
        /// <summary>
        /// 全屏模式
        /// </summary>
        fillForms = 3,
        /// <summary>
        /// 消息模式
        /// </summary>
        comment = 4,
        /// <summary>
        /// 锁定内容模式
        /// </summary>
        blockcontent = 5,
        /// <summary>
        /// 无边框预览模式
        /// </summary>
        embedded = 6,
        /// <summary>
        /// 条件模式
        /// </summary>
        filter = 7
    }

    public enum PositionType
    {
        principal,
        director,
        charge,
        employee,
        legal
    }

    /// <summary>
    /// 性别类型
    /// </summary>
    public enum Gender
    {
        /// <summary>
        /// 男士
        /// </summary>
        male,
        /// <summary>
        /// 女士
        /// </summary>
        female
    }

    /// <summary>
    /// 兼职
    /// </summary>
    public enum PartTimeJobType
    {
        main,
        parttime
    }
    public enum DictionaryType
    {
        flow,
        emp,
        org,
        pos,
        other
    }
    public enum EnTableView
    {
        table,
        form
    }

    public enum FlowStatus
    {
        /// <summary>
        /// 删除
        /// </summary>
        delete,
        /// <summary>
        /// 已卸载
        /// </summary>
        unloaded,
        /// <summary>
        /// 设计中
        /// </summary>
        design,
        /// <summary>
        /// 已发布
        /// </summary>
        installed
    }
    public enum FlowType
    {
        /// <summary>
        /// 流程类型
        /// </summary>
        flow,
        business,
    }
    /// <summary>
    /// 流程任务状态
    /// </summary>
    public enum FlowTaskStatus
    {
        /// <summary>
        /// 待发起
        /// </summary>
        momentum,
        /// <summary>
        /// 待审批
        /// </summary>
        audit,
        /// <summary>
        /// 待他人审批
        /// </summary>
        otheraudit,
        /// <summary>
        /// 已终止
        /// </summary>
        terminated,
        /// <summary>
        /// 他人已终止
        /// </summary>
        otherterminated,
        /// <summary>
        /// 已审批
        /// </summary>
        approved,
        /// <summary>
        /// 他人已审批
        /// </summary>
        otherapproved,
        /// <summary>
        /// 已退回
        /// </summary>
        returned,
        /// <summary>
        /// 他人已退回
        /// </summary>
        otherreturned,
        /// <summary>
        /// 已回收
        /// </summary>
        recycled,
        /// <summary>
        /// -已指派
        /// </summary>
        assigned,
        /// <summary>
        /// 未读
        /// </summary>
        unread,
        /// <summary>
        /// 已读
        /// </summary>
        red,
        /// <summary>
        /// 已转交
        /// </summary>
        transfer,
        temp,
    }

    /// <summary>
    /// 流程任务类型
    /// </summary>
    public enum FlowTaskType
    {
        /// <summary>
        /// 正常任务
        /// </summary>
        task,
        /// <summary>
        /// 委托任务
        /// </summary>
        delegation,
        /// <summary>
        /// 测试任务
        /// </summary>
        test,
        /// <summary>
        /// 传阅
        /// </summary>
        circulated,
        /// <summary>
        /// 抄送
        /// </summary>
        cc,
        branch,
        endbranch
    }

    /// <summary>
    /// 流程实例状态
    /// </summary>
    public enum FlowInstanceStatus
    {
        /// <summary>
        /// 待发起
        /// </summary>
        momentum,
        /// <summary>
        /// 进行中
        /// </summary>
        ongoing,
        /// <summary>
        /// 已完成
        /// </summary>
        completed,
        /// <summary>
        /// 已终止
        /// </summary>
        terminated,

    }
    public enum ProcessType
    {
        /// <summary>
        /// 保存
        /// </summary>
        Save,
        /// <summary>
        /// 审批
        /// </summary>
        Approval,

        /// <summary>
        /// 退回
        /// </summary>
        SendBack,
        /// <summary>
        /// 退回至具体步骤
        /// </summary>
        SednBackStep,
        /// <summary>
        /// 终止/拒绝
        /// </summary>
        Refused,
        /// <summary>
        /// 转交
        /// </summary>
        Transfer,
        /// <summary>
        /// 跳转
        /// </summary>
        Jump,
        /// <summary>
        /// 传阅
        /// </summary>
        Circulated
    }

    public enum ApprovalRangeType
    {
        /// <summary>
        /// 所有人
        /// </summary>
        Owner,
        /// <summary>
        /// 范围/职位
        /// </summary>
        Position,
        /// <summary>
        /// 人员
        /// </summary>
        Person,
        /// <summary>
        /// 不可选者人员
        /// </summary>
        UnChoosable,
        /// <summary>
        /// 并行分支
        /// </summary>
        Branch,
        /// <summary>
        ///  并行分支-结束
        /// </summary>
        EndBranch
    }
    public enum JudeNodeType
    {
        /// <summary>
        /// 直接提交
        /// </summary>
        DirectlySubmit,
        /// <summary>
        /// 可选-单选
        /// </summary>
        SingeChoosable,
        /// <summary>
        /// 多选-单选
        /// </summary>
        MultiChoosable,
    }
    public enum ApproverType
    { /// <summary>
      /// 无数据
      /// </summary>
        UnEmpty,
        Applicant,
        Initiator,
        Approver,
    }
    public enum StrategyType
    {
        /// <summary>
        /// 无数据
        /// </summary>
        UnEmpty,
        /// <summary>
        /// 所有用户
        /// </summary>
        Allperson,
        /// <summary>
        /// 范围
        /// </summary>
        Range,
        /// <summary>
        /// 默认审批人
        /// </summary>
        DefaultApprover,
        /// <summary>
        /// 人员
        /// </summary>
        Person,
        /// <summary>
        /// 组织
        /// </summary>
        Organization,
        /// <summary>
        /// 职位
        /// </summary>
        Position,
        /// <summary>
        /// 表单人员
        /// </summary>
        Form_Person,
        /// <summary>
        /// 组织-表单
        /// </summary>
        Form_Organization,
        /// <summary>
        /// 职位-表单
        /// </summary>
        Form_Position,
    }

    /// <summary>
    /// 按人员获取策略
    /// </summary>
    public enum Strategy_Personal
    {
        /// <summary>
        /// 空
        /// </summary>
        Empty,
        /// <summary>
        /// 申请人
        /// </summary>
        Flow_Applicant,

        /// <summary>
        /// 发起人
        /// </summary>
        Flow_Initiator,

        /// <summary>
        /// 当前处理人
        /// </summary>
        Flow_Current_Handler,

        /// <summary>
        /// 申请人直接上级
        /// </summary>
        Flow_Applicant_Immediate_Superior,

        /// <summary>
        /// 发起人直接上级
        /// </summary>
        Flow_Initiator_Immediate_Superior,

        /// <summary>
        /// 当前处理人直接上级
        /// </summary>
        Flow_Current_Handler_Immediate_Superior,

        /// <summary>
        /// 申请人间接上级
        /// </summary>
        Flow_Applicant_Indirect_Superior,

        /// <summary>
        /// 发起人间接上级
        /// </summary>
        Flow_Initiator_Indirect_Superior,

        /// <summary>
        /// 当前处理人间接上级
        /// </summary>
        Flow_Current_Handler_Indirect_Superior,

        /// <summary>
        /// 申请人所有上级
        /// </summary>
        Flow_Applicant_All_Superior,

        /// <summary>
        /// 发起人所有上级
        /// </summary>
        Flow_Initiator_All_Superior,

        /// <summary>
        /// 当前处理人所有上级
        /// </summary>
        Flow_Current_Handler_All_Superior

    }
    /// <summary>
    /// 按部门获取策略
    /// </summary>
    public enum Strategy_Organiztion
    {
        /// <summary>
        /// 空
        /// </summary>
        Empty,
        /// <summary>
        /// 申请人所在部门主管
        /// </summary>
        Flow_Applicant_At_Department_Director,

        /// <summary>
        /// 发起人所在部门主管
        /// </summary>
        Flow_Initiator_At_Department_Director,

        /// <summary>
        /// 当前处理人所在部门主管
        /// </summary>
        Flow_Current_Handler_At_Department_Director,

        /// <summary>
        /// 申请人所在部门上级部门主管
        /// </summary>
        Flow_Applicant_At_Immediate_Department_Director,

        /// <summary>
        /// 发起人所在部门上级部门主管
        /// </summary>
        Flow_Initiator_At_Immediate_Department_Director,

        /// <summary>
        /// 当前处理人所在部门上级部门主管
        /// </summary>
        Flow_Current_Handler_Immediate_At_Department_Director,

        /// <summary>
        /// 申请人所在部门所有用户
        /// </summary>
        Flow_Applicant_At_Department_ALL_Personal,

        /// <summary>
        /// 发起人所在部门所有用户
        /// </summary>
        Flow_Initiator_At_Department_ALL_Personal,

        /// <summary>
        /// 当前处理人所在部门所有用户
        /// </summary>
        Flow_Current_Handler_At_Department_ALL_Personal,

        /// <summary>
        /// 申请人所在部门上N级负责人
        /// </summary>
        Flow_Applicant_At_Department_Leve_N_Principal,

        /// <summary>
        /// 发起人所在部门上N级负责人
        /// </summary>
        Flow_Initiator_At_Department_Leve_N_Principal,

        /// <summary>
        /// 当前处理人所在部门上N级负责人
        /// </summary>
        Flow_Current_Handler_At_Department_Leve_N_Principal,
    }

    /// <summary>
    /// 按职位获取策略
    /// </summary>
    public enum Strategy_Position
    {
        /// <summary>
        /// 空
        /// </summary>
        Empty,
        /// <summary>
        /// 申请人直接上级
        /// </summary>
        Flow_Applicant_Immediate_Superior,

        /// <summary>
        /// 发起人直接上级
        /// </summary>
        Flow_Initiator_Immediate_Superior,

        /// <summary>
        /// 当前处理人直接上级
        /// </summary>
        Flow_Current_Handler_Immediate_Superior,

        /// <summary>
        /// 申请人间接上级
        /// </summary>
        Flow_Applicant_Indirect_Superior,

        /// <summary>
        /// 发起人间接上级
        /// </summary>
        Flow_Initiator_Indirect_Superior,

        /// <summary>
        /// 当前处理人间接上级
        /// </summary>
        Flow_Current_Handler_Indirect_Superior,

        /// <summary>
        /// 申请人所有上级
        /// </summary>
        Flow_Applicant_All_Superior,


        /// <summary>
        /// 发起人所有上级
        /// </summary>
        Flow_Initiator_All_Superior,

        /// <summary>
        /// 当前处理人所有上级
        /// </summary>
        Flow_Current_Handler_All_Superior
    }

    /// <summary>
    /// 表单人员字段策略
    /// </summary>
    public enum Strategy_Form_Personal_Field
    {
        /// <summary>
        /// 空
        /// </summary>
        Empty,
        /// <summary>
        /// 当前人员
        /// </summary>
        Flow_Form_Field_Current_Personal,

        /// <summary>
        /// 当前人员直接上级
        /// </summary>
        Flow_Form_Field_Current_Personal_Immediate_Superior,

        /// <summary>
        /// 当前人员间接上级
        /// </summary>
        Flow_Form_Field_Current_Personal_Indirect_Superior,

        /// <summary>
        /// 当前人员所有上级
        /// </summary>
        Flow_Form_Field_Current_Personal_All_Superior
    }
    /// <summary>
    /// 表单部门字段策略
    /// </summary>
    public enum Strategy_Form_Organization_Field
    {
        /// <summary>
        /// 空
        /// </summary>
        Empty,
        /// <summary>
        /// 当前部门
        /// </summary>
        Flow_Form_Field_Current_Department,

        /// <summary>
        /// 当前部门主管
        /// </summary>
        Flow_Form_Field_Current_Department_Director,

        /// <summary>
        /// 当部门上N级负责人
        /// </summary>
        Flow_Form_Field_Current_Department_Leve_N_Principal,

        /// <summary>
        /// 当前部门所有人
        /// </summary>
        Flow_Form_Field_Current_Department_All_Superior
    }

    /// <summary>
    /// 表单职位字段策略
    /// </summary>
    public enum Strategy_Form_Position_Field
    {
        /// <summary>
        /// 空
        /// </summary>
        Empty,
        /// <summary>
        /// 当前职位
        /// </summary>
        Flow_Form_Field_Current_Position,

        /// <summary>
        /// 当前职位直接上级
        /// </summary>
        Flow_Form_Field_Current_Position_Immediate_Superior,

        /// <summary>
        /// 当前职位间接上级
        /// </summary>
        Flow_Form_Field_Current_Position_Indirect_Superior,

        /// <summary>
        /// 当前职位所有上级
        /// </summary>
        Flow_Form_Field_Current_Position_All_Superior
    }
    public enum ApprovalStrategyType
    {
        /// <summary>
        /// 不启用
        /// </summary>
        Disable,
        /// <summary>
        /// 一人审批通过即可
        /// </summary>
        SingleAgreed,
        /// <summary>
        /// 所有审批通过即可
        /// </summary>
        AllAgree,
        /// <summary>
        /// 按比列处理任务
        /// </summary>
        Scale,
    }
    public enum BranchStrategyType
    {
        /// <summary>
        /// 一各分支通过即可
        /// </summary>
        SingleAgreed,
        /// <summary>
        /// 所有分支通过即可
        /// </summary>
        AllAgree,
        /// <summary>
        /// 按比列处理分支
        /// </summary>
        Scale,
    }
    /// <summary>
    /// 当前任务处理人兼职类型
    /// </summary>
    public enum ApproveParttimePostion
    {
        Empty,
        /// <summary>
        /// 主岗
        /// </summary>
        Min,
        /// <summary>
        /// 兼岗
        /// </summary>
        PartTime,
    }
    /// <summary>
    /// 当前节点任务处理人兼职类型
    /// </summary>
    public enum SendParttimePostion
    {
        Empty,
        /// <summary>
        /// 仅主岗
        /// </summary>
        Min,
        /// <summary>
        /// 仅兼岗
        /// </summary>
        PartTime,
        /// <summary>
        /// 主岗优先
        /// </summary>
        MinPriority,
        /// <summary>
        /// 兼岗优先
        /// </summary>
        PartTimePriority,
    }
    /// <summary>
    /// 任务类型
    /// </summary>
    public enum FlowProcessType
    {
        /// <summary>
        /// 正常审批
        /// </summary>
        Normal,
        /// <summary>
        /// 退回
        /// </summary>
        Back,
        /// <summary>
        /// 回收
        /// </summary>
        Revocation
    }
    public enum FlowCenterPageType
    {
        /// <summary>
        /// 待办事项
        /// </summary>
        todo,
        /// <summary>
        /// 已办事项
        /// </summary>
        done,
        /// <summary>
        /// 阅办事项
        /// </summary>
        circulated,
        /// <summary>
        /// 我的申请
        /// </summary>
        mayapply,
        /// <summary>
        /// 待办申请
        /// </summary>
        apply,
        /// <summary>
        /// 我的抄送
        /// </summary>
        cc
    }
    public enum SkipStrategyType
    {
        /// <summary>
        /// 不启用
        /// </summary>
        Disable,
        /// <summary>
        /// 相邻跳过
        /// </summary>
        Adjacent,
        /// <summary>
        /// 已审批过跳过
        /// </summary>
        Approval

    }
}
