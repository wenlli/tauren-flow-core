﻿using System;

namespace Tauren.Flow.Entity.Enum
{
    public class ErrorType
    {
        /// <summary>
        /// 数据库服务器错误
        /// </summary>
        public const int SQL_ERROR = 1001;
        /// <summary>
        /// 成功时编码
        /// </summary>
        public const int SUCCESS_CODE = 200;

        /// <summary>
        /// 当前账号登录成功！祝您使用愉快!
        /// </summary>
        public const int BUSINESS_LOGIN_SUCCESSED = 2001;

        /// <summary>
        /// 返回数据类型错误
        /// </summary>
        public const int RESPONDATATYPE_ERROR = 3001;

        public const int PERMISSIONSEXCEPTION = 4033;



        /// <summary>
        /// 500服务器错误
        /// </summary>
        public const int SERVER_ERROR = 500;
        /// <summary>
        /// 参数异常
        /// </summary>
        public const int SERVER_PARMETER = 5001;
        /// <summary>
        /// 空指针异常
        /// </summary>
        public const int SERVER_NULL_POINTER = 5002;

        #region 业务数据错误
        /// <summary>
        /// 租户用户信息重复
        /// </summary>
        public const int BUSINESS_TENANTINFO_REPEAT_CODE = 601;
        /// <summary>
        /// 未找到租户用户信息
        /// </summary>
        public const int BUSINESS_NOT_TENANTINFO = 602;


        #region 登录
        /// <summary>
        /// 未输入登录账号
        /// </summary>
        public const int BUSINESS_NOT_FOUND_LOGIN = 4001;
        /// <summary>
        /// 请输入用户名
        /// </summary>
        public const int BUSINESS_EMPTY_LOGIN_ACCOUNT = 4002;
        /// <summary>
        /// 请输入密码
        /// </summary>
        public const int BUSINESS_EMPTY_LOGIN_PASSWORD = 4003;
        /// <summary>
        /// 当前用户名不存在!请联系管理员
        /// </summary>
        public const int BUSINESS_EMPTY_NOTFOUND_ACCOUNTINFO = 4004;
        /// <summary>
        /// 当前用户密码不正确!请重新输入
        /// </summary>
        public const int BUSINESS_EMPTY_LOGIN_PASSWORD_ERROR = 4005;
        /// <summary>
        /// 登录令牌未成功生成，请联系管理员!
        /// </summary>
        public const int BUSINESS_EMPTY_LOGIN_TOKEN_ERROR = 4006;
        /// <summary>
        /// 当前员工基本信息不存在!请联系管理员
        /// </summary>
        public const int BUSINESS_EMPTY_NOTFOUND_EMPLOYEEINFO = 4007;

        /// <summary>
        /// 未输入账号信息
        /// </summary>
        public const int BUSINESS_LOGIN_SECRET_ERROR = 4008;
        public const int BUSINESS_WITHOUT_PERMISSION = 4011;
        #endregion

        #region 员工基本信息
        /// <summary>
        /// 请输入员工基本数据
        /// </summary>
        public const int BUSINESS_EMPLOYEE_NOTFOUND_INPUT_DATA_ERROR = 4101;

        /// <summary>
        /// 员工基本信息重复
        /// </summary>
        public const int BUSINESS_EMPLOYEE_REPEAT_CODE = 4102;
        /// <summary>
        /// 未查询到当前员工的基本信息!
        /// </summary>
        public const int BUSINESS_EMPLOYEE_DB_NOT_FOUND = 4103;

        #endregion

        #region 账号
        /// <summary>
        /// 请输入账号信息数据
        /// </summary>
        public const int BUSINESS_ACCOUNT_NOTFOUND_INPUT_DATA_ERROR = 4201;
        /// <summary>
        /// 账户信息重复
        /// </summary>
        public const int BUSINESS_ACCOUNTINFO_REPEAT_ACCOUNT = 4202;
        /// <summary>
        /// 未查询到当前账号的基本信息!
        /// </summary>
        public const int BUSINESS_ACCOUNT_DB_NOT_FOUND = 4203;
        /// <summary>
        /// 重置密码成功!
        /// </summary>
        public const int BUSINESS_ACCOUNT_RESET_PASSWORD_SUCCESS = 4206;
        /// <summary>
        /// 重置密码失败!
        /// </summary>
        public const int BUSINESS_ACCOUNT_RESET_PASSWORD_SAVE_FAILURE = 4207;
        #endregion 
        #region 菜单
        public const int BUSINESS_MENU_NOTFOUND_INPUT_DATA_ERROR = 4401;
        /// <summary>
        ///菜单信息重复
        /// </summary>
        public const int BUSINESS_MENU_REPEAT_ACCOUNT = 4402;
        /// <summary>
        /// 未查询到菜单信息!
        /// </summary>
        public const int BUSINESS_MENU_DB_NOT_FOUND = 4403;

        /// <summary>
        /// 查询到子菜单，请先删除子菜单然后再删除该菜单！
        /// </summary>
        public const int BUSINESS_MENU_UNREMOVE = 4404;

        /// <summary>
        /// 保存角色成功!
        /// </summary>
        public const int BUSSINESS_MEEU_ROLES_SUCCESS = 4422;

        /// <summary>
        /// 保存角色失败!
        /// </summary>
        public const int BUSSINESS_MENU_ROLES_FAILURE = 4450;
        #endregion
        #endregion
        #region 文件上传
        /// <summary>
        /// 上传文件大小超过限制大小
        /// </summary>
        public const int BUSSINESS_UPLOAD_FILE_SIZE = 4913;
        /// <summary>
        /// 不支持此文件类型,仅支持：.gif,.jpg,.jpeg,.png,.bmp,.docx,.xlsx,.csv,.pptx,.txt,.pdf
        /// </summary>
        public const int BUSSINESS_UPLOAD_FILE_SUFFIX = 4915;
        /// <summary>
        /// 不能上传空文件!
        /// </summary>
        public const int BUSSINESS_UPLOAD_FILE_SIZE_EMPTY = 4916;
        #endregion
    }
    public class ErrorMessage
    {

        /// <summary>
        /// 数据库服务器错误
        /// </summary>
        public const string SQL_ERROR = "执行数据脚本时出现错误！";
        /// <summary>
        /// 成功时编码
        /// </summary>
        public const string SUCCESS_CODE = "数据操作成功!";
        /// <summary>
        /// 返回数据类型错误
        /// </summary>
        public const string RESPONDATATYPE_ERROR = "返回数据类型错误!";
        /// <summary>
        /// 500服务器错误
        /// </summary>
        public const string SERVER_ERROR = "服务器错误!";

        /// <summary>
        /// 参数异常
        /// </summary>
        public const string SERVER_PARMETER = "参数错误!";
        /// <summary>
        /// 空指针异常
        /// </summary>
        public const string SERVER_NULL_POINTER = "空指针异常!";
        #region 业务数据错误

        #region 登录
        /// <summary>
        /// 租户用户信息重复
        /// </summary>
        public const string BUSINESS_TENANTINFO_REPEAT_CODE = "租户用户信息重复!";

        /// <summary>
        /// 未找到租户用户信息
        /// </summary>
        public const string BUSINESS_NOT_TENANTINFO = "未找到租户用户信息!";

        /// <summary>
        /// 请输入登录信息
        /// </summary>
        public const string BUSINESS_NOT_FOUND_LOGIN = "请输入登录信息!";
        /// <summary>
        /// 请输入用户名
        /// </summary>
        public const string BUSINESS_EMPTY_LOGIN_ACCOUNT = "请输入用户名!";
        /// <summary>
        /// 请输入密码
        /// </summary>
        public const string BUSINESS_EMPTY_LOGIN_PASSWORD = "请输入密码!";
        /// <summary>
        /// 当前用户名不存在!请联系管理员
        /// </summary>
        public const string BUSINESS_EMPTY_NOTFOUND_ACCOUNTINFO = "当前用户名不存在!请联系管理员!";
        /// <summary>
        /// 当前用户密码不正确!请重新输入
        /// </summary>
        public const string BUSINESS_EMPTY_LOGIN_PASSWORD_ERROR = "当前用户密码不正确!请重新输入";
        /// <summary>
        /// 登录令牌未成功生成，请联系管理员!
        /// </summary>
        public const string BUSINESS_EMPTY_LOGIN_TOKEN_ERROR = "登录令牌未成功生成，请联系管理员!";
        /// <summary>
        /// 当前员工基本信息不存在!请联系管理员!
        /// </summary>
        public const string BUSINESS_EMPTY_NOTFOUND_EMPLOYEEINFO = "当前员工基本信息不存在!请联系管理员!";

        /// <summary>
        /// 当前账号登录成功！祝您使用愉快!
        /// </summary>
        public const string BUSINESS_LOGIN_SUCCESSED = "当前账号登录成功！祝您使用愉快!";
        /// <summary>
        /// 未输入账号信息
        /// </summary>
        public const string BUSINESS_LOGIN_SECRET_ERROR = "未输入账号信息!";
        #endregion

        #region 员工基本信息
        /// <summary>
        /// 请输入员工基本数据
        /// </summary>
        public const string BUSINESS_EMPLOYEE_NOTFOUND_INPUT_DATA_ERROR = "请输入员工基本数据!";
        /// <summary>
        /// 员工基本信息重复
        /// </summary>
        public const string BUSINESS_EMPLOYEE_REPEAT_CODE = "员工基本信息重复";
        /// <summary>
        /// 未查询到当前员工的基本信息!
        /// </summary>
        public const string BUSINESS_EMPLOYEE_DB_NOT_FOUND = "未查询到当前员工的基本信息";
        #endregion

        #region 账号
        /// <summary>
        /// 请输入账号信息数据
        /// </summary>
        public const string BUSINESS_ACCOUNT_NOTFOUND_INPUT_DATA_ERROR = "请输入账号信息数据";
        /// <summary>
        /// 账户信息重复
        /// </summary>
        public const string BUSINESS_ACCOUNTINFO_REPEAT_ACCOUNT = "账户信息重复";
        /// <summary>
        /// 未查询到当前账号的基本信息!
        /// </summary>
        public const string BUSINESS_ACCOUNT_DB_NOT_FOUND = "未查询到当前账号的基本信息";
        #endregion

        #region 单位
        /// <summary>
        /// 请输入单位信息数据
        /// </summary>
        public const String BUSINESS_UNITINOF_NOTFOUND_INPUT_DATA_ERROR = "请输入单位信息数据";

        /// <summary>
        /// 账户信息重复
        /// </summary>
        public const string BUSINESS_UNITINFO_REPEAT_ACCOUNT = "单位信息重复";

        /// <summary>
        /// 未查询到单位的基本信息!
        /// </summary>
        public const string BUSINESS_UNITINFO_DB_NOT_FOUND = "未查询到单位的基本信息";
        #endregion

        #region 菜单 
        /// <summary>
        /// 请输入菜单信息数据
        /// </summary>
        public const string BUSINESS_MENU_NOTFOUND_INPUT_DATA_ERROR = "请输入菜单信息数据";
        /// <summary>
        ///菜单信息重复
        /// </summary>
        public const string BUSINESS_MENU_REPEAT_ACCOUNT = "菜单信息重复";
        /// <summary>
        /// 未查询到菜单信息!
        /// </summary>
        public const string BUSINESS_MENU_DB_NOT_FOUND = "未查询到菜单信息";

        /// <summary>
        /// 查询到子菜单，请先删除子菜单然后再删除该菜单！
        /// </summary>
        public const string BUSINESS_MENU_UNREMOVE = "查询到子菜单，请先删除子菜单然后再删除该菜单！";
        #endregion
        #endregion

    }

    public class GlobalErrorType
    {
        /// <summary>
        /// 未找到页面输入的数据,请联系管理员！
        /// </summary>
        public const int GLOBAL_NOT_FOUND_INPUT_DATA = 9444;

        /// <summary>
        /// 未查询到当前数据信息!
        /// </summary>
        public const int GLOBAL_NOT_FOUND_DB_DATA = 9404;

        /// <summary>
        /// 当前编码已存在,请更换编码!
        /// </summary>
        public const int GLOBAL_REPEAT_CODE = 9441;

        /// <summary>
        /// 保存数据成功!
        /// </summary>
        public const int GLOBAL_SAVE_SUCCESSINFO = 9420;
        /// <summary>
        /// 保存数据失败!
        /// </summary>
        public const int GLOBAL_SAVE_FAILURE = 9450;
        /// <summary>
        /// 删除数据成功!
        /// </summary>
        public const int GLOBAL_DELETE_SUCCESSINFO = 9406;
        /// <summary>
        /// 删除数据失败!
        /// </summary>
        public const int GLOBAL_DELETE_FAILURE = 9405;
        /// <summary>
        /// 已启用!
        /// </summary>
        public const int GLOBAL_ENABLE_SUCCESSINFO = 9408;
        /// <summary>
        /// 启用失败!
        /// </summary>
        public const int GLOBAL_ENABLE_FAILURE = 9409;
        /// <summary>
        /// 已禁用!
        /// </summary>
        public const int GLOBAL_DISABLE_SUCCESSINFO = 9407;
        /// <summary>
        /// 禁用失败!
        /// </summary>
        public const int GLOBAL_DISABLE_FAILURE = 9410;

        /// <summary>
        /// 语言：{0}已存在!
        /// </summary>
        public const int GLOBAL_LANGUAGE_REPEAT_ERROR = 9411;

        /// <summary>
        /// 名称多语言不能为空!
        /// </summary>
        public const int GLOBAL_RES_LANGUAGE_REPEAT_ERROR = 9440;

        /// <summary>
        /// 已存在相同名称的数据!
        /// </summary>
        public const int GLOBAL_RES_LANGUAGE_REPEAT_NNAME_ERROR = 9100;

        #region 流程相关
        /// <summary>
        /// 全局失败!
        /// </summary>
        public const int GLOBAL_RES_FLOW_ERROR = 500500;
        /// <summary>
        /// 未找到当前流程信息！
        /// </summary>
        public const int GLOBAL_RES_FLOW_NOTFOUNT_FLOWINFO_ERROR = 500501;
        /// <summary>
        /// 未找到当前表单信息！
        /// </summary>
        public const int GLOBAL_RES_FLOW_NOTFOUNT_FORMINFO_ERROR = 500502;
        /// <summary>
        /// 未找到当前步骤信息！
        /// </summary>
        public const int GLOBAL_RES_FLOW_NOTFOUNT_NODEINFO_ERROR = 500503;
        /// <summary>
        /// 未找到当前任务信息！
        /// </summary>
        public const int GLOBAL_RES_FLOW_NOTFOUNT_CURRENTTASK_ERROR = 500504;
        /// <summary>
        /// 当前数据已提交，请勿重复提交!
        /// </summary>
        public const int GLOBAL_RES_FLOW_CURRENTDATA_REPART_SUMBIT = 500505;

        /// <summary>
        /// 未找到当前节点信息！
        /// </summary>
        public const int GLOBAL_RES_FLOW_NOTFOUNT_CURRENTSTEP_ERROR = 500506;

        /// <summary>
        /// 后续连线条件均不满足，流程不能进行审批!
        /// </summary>
        public const int GLOBAL_RES_FLOW_NOTFOUNT_NEXTLINE_ERROR = 500507;

        /// <summary>
        /// 后续连线条有多个满足条件，流程不能进行审批!
        /// </summary>

        public const int GLOBAL_RES_FLOW_MORET_NEXTLINE_ERROR = 500508;
        /// <summary>
        /// 未找到下一节点信息！
        /// </summary>
        public const int GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_ERROR = 500509;
        /// <summary>
        /// 未查询到后续步骤处理人，请联系管理员!
        /// </summary>
        public const int GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR = 500510;
        /// <summary>
        /// 当前任务已被处理，请联系管理员!
        /// </summary>
        public const int GLOBAL_RES_FLOW_NOT_RUN_CURRENT_TASK_ERROR = 500511;
        /// <summary>
        /// 完成任务失败!
        /// </summary>
        public const int GLOBAL_RES_FLOW_RUN_CURRENT_TASK_COMPLETE_ERROR = 500512;

        /// <summary>
        /// 当前任务不是审批任务，不能进行撤回！
        /// </summary>
        public const int GLOBAL_RES_FLOW_NOTFOUNT_CURRENTTASK_FLOWSTATUS_ERROR = 500513;
        /// <summary>
        /// 下一任务已被处理，不能进行撤回！
        /// </summary>
        public const int GLOBAL_RES_FLOW_NEXT_APPROVAL_ERROR = 500514;

        /// <summary>
        /// 当前任务已被处理，不能进行撤回！
        /// </summary>
        public const int GLOBAL_RES_FLOW_CURRENT_INSTANCE_UN_RECYCLING_ERROR = 500519;

        /// <summary>
        /// 当前任务已被撤回，请勿重复回收!
        /// </summary>
        public const int GLOBAL_RES_FLOW_CURRENTDATA__RECYCKLINE_REPART_SUMBIT = 500515;
        /// <summary>
        /// 当前任务回收成功!
        /// </summary>
        public const int GLOBAL_RES_FLOW_CURRENT_TASE_RECYCKLINE_SUCCESS = 500516;
        /// <summary>
        /// 当前任务已被处理，请勿重复处理!
        /// </summary>
        public const int GLOBAL_RES_FLOW_CURRENT_TASK_END_APPROVALED_ERROR = 500517;
        /// <summary>
        /// 当前流程已终止!
        /// </summary>
        public const int GLOBAL_RES_FLOW_CURRENT_TASK_END_APPROVALED_SUCCESS = 500518;
        /// <summary>
        /// 当前流程终止失败，请联系管理员！
        /// </summary>
        public const int GLOBAL_RES_FLOW_CURRENT_TASK_END_ERROR = 500520;
        /// <summary>
        /// 当前任务已读，请勿重复处理!
        /// </summary>
        public const int GLOBAL_RES_FLOW_CURRENTDATA_READ_REPART_SUMBIT = 500521;
        /// <summary>
        /// 当前任务不是抄送和传阅任务，不能进行处理！
        /// </summary>
        public const int GLOBAL_RES_FLOW_NOTFOUNT_CURRENTTASK_UNCC_ERROR = 500522;
        /// <summary>
        /// 当前任务处理成功！
        /// </summary>
        public const int GLOBAL_RES_FLOW_CURRENT_TASK_COMPLATED_SUCCESS = 500523;
        /// <summary>
        /// 当前任务已被处理，不能进行退回！
        /// </summary>
        public const int GLOBAL_RES_FLOW_CURRENT_INSTANCE_UN_BACK_ERROR = 50024;

        /// <summary>
        /// 删除流程实例成功！
        /// </summary>
        public const int GLOBAL_RES_FLOW_DELETE_INSTANCE_ERROR = 500526;

        /// <summary>
        /// 删除流程实例失败！
        /// </summary>
        public const int GLOBAL_RES_FLOW_DELETE_INSTANCE_FUILER_ERROR = 500525;
        /// <summary>
        /// 不能进行跳过！
        /// </summary>
        public const int GLOBAL_RES_FLOW_NOT_SKIP_NODE_FUILER_ERROR = 500527;
        public const int GLOBAL_RES_FLOW_NOT_APPROVER_SKIP_NODE_FUILER_ERROR = 500528;

        /// <summary>
        /// 并行分支/多人审批节点存在已审批和终止的任务，您为最后审批人，任务不能退回！
        /// </summary>
        public const int GLOBAL_RES_FLOW_NOT_BACK_ERROR = 500529;
        /// <summary>
        /// 并行分支/多人审批节点存在已审批/已退回的任务，您为最后审批人，任务不能终止！
        /// </summary>
        public const int GLOBAL_RES_FLOW_NOT_END_ERROR = 500530;
        #endregion
    }
}
