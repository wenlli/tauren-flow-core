﻿namespace Tauren.Flow.Entity.Enum
{
    public class LanguageConst
    {
    }
    public enum LanguageType
    {
        /// <summary>
        /// 中文
        /// </summary>
        zh_cn,
        /// <summary>
        /// 英文
        /// </summary>
        en,
        /// <summary>
        /// 俄罗斯
        /// </summary>
        ru,
        /// <summary>
        /// 巴基斯坦
        /// </summary>
        pk
    }

    /// <summary>
    /// 按钮类型
    /// </summary>
    public enum ButtomType
    {
        add,
        edit,
        enable,
        delete,
        role,
        detaile,
        disable,
        export,
        import,
        save,
        confirm,
        batchdelete,
        restpassword,
        batchregister,
        onviewdoc,
        ondetaildoc,
        oneditdoc,
        upload,
        download,
        unloaded,
        set,
        /// <summary>
        /// 流程快捷字段
        /// </summary>
        quickfield,
        /// <summary>
        /// 流程实例表单字段
        /// </summary>
        instancefield,
        /// <summary>
        ///流程政策
        /// </summary>
        policy,
        /// <summary>
        /// 回收
        /// </summary>
        recycle
    }
    public enum ButtomLocation
    {
        top,
        center,
        bottom
    }

    public enum BtnShapeType
    {
        plain,
        round,
        circle,
    }
    public enum BtnColorType
    {
        danger,
        primary,
        success,
        info,
        warning
    }
    public enum BtnSizeType
    {
        medium,
        small,
        mini
    }
    public enum SortType
    {
        custom
    } 
}
