﻿namespace Tauren.Flow.Entity.Enum
{
    public enum LogType
    {
        /// <summary>
        /// 登录信息日志
        /// </summary>
        LogInInfo,

        /// <summary>
        /// 登录错误日志
        /// </summary>
        LogInError,

        /// <summary>
        /// 信息
        /// </summary>
        Info,

        /// <summary>
        /// 错误
        /// </summary>
        Error,

        /// <summary>
        /// 警告
        /// </summary>
        Warning

    }
    public enum ApprovalType
    {
        /// <summary>
        /// 发起
        /// </summary>
        apply,
        /// <summary>
        /// 审批
        /// </summary>
        approval,
        /// <summary>
        /// 阅办
        /// </summary>
        circulated, 
        /// <summary>
        /// 详情
        /// </summary>
        view,
        /// <summary>
        /// 新增
        /// </summary>
        add,
        /// <summary>
        /// 编辑
        /// </summary>
        edit,

    }
}
