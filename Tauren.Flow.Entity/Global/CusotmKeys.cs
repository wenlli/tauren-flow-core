﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tauren.Flow.Entity.Global
{
    public class CusotmKey
    {
        public string kty { get; set; }
        public string use { get; set; }
        public string kid { get; set; }
        public string e { get; set; }
        public string n { get; set; }
        public string alg { get; set; }

    }
    public class CustomKeys
    {
        public List<CusotmKey> keys { get; set; }
    }
}
