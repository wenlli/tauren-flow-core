﻿using System;
using System.Collections.Generic;
using System.Text;
using Tauren.Flow.Entity.Enum;
using Tauren.Flow.Entity.Output;

namespace Tauren.Flow.Entity.Global
{

    public class Parameter
    {
        /// <summary>
        /// 流程ID
        /// </summary>
        public virtual long FlowId { get; set; }

        /// <summary>
        /// 流程版本ID
        /// </summary>
        public virtual long FlowVersionId { get; set; }
        /// <summary>
        /// 表单ID
        /// </summary>
        public virtual long FormId { get; set; }
        /// <summary>
        /// 表单版本
        /// </summary>
        public virtual long FormVersionId { get; set; }

        /// <summary>
        /// 任务ID
        /// </summary>
        public virtual long TaskId { get; set; }

        public virtual string SecretKey { get; set; }
    }
    public class FlowParameter : Parameter
    {
        /// <summary>
        /// 审批类型
        /// </summary>
        public virtual ApprovalType ApprovalType { get; set; }
        public virtual string StepId { get; set; }
        public virtual int ApplyEmpId { get; set; }

        public virtual List<NextNodeInfoOutPut> NodeInfo { get; set; } = new List<NextNodeInfoOutPut>();
        /// <summary>
        /// 意见
        /// </summary>
        public virtual string Opinion { get; set; } = "";
    }
    public class FlowRquestParameter : Parameter
    {
        public virtual Dictionary<string, object> Model { get; set; } = new Dictionary<string, object>();
        public virtual ProcessType ProcessType { get; set; }
    }
    public class FlowFieldParameter
    {
        public virtual string AreaCode { get; set; } = "";
        public virtual string ControlsType { get; set; } = "";
        public virtual string FieldCode { get; set; } = "";
        public virtual string FieldType { get; set; } = "";
        public virtual object FieldValue { get; set; }
        public virtual List<FieldValues> FieldValues { get; set; } = new List<FieldValues>();
        public virtual string FlowTableCode { get; set; } = "";
        public virtual string TableCode { get; set; } = "";
        public virtual string MainTable { get; set; } = "";
        public virtual long FieldTag { get; set; }
        public virtual string BusinessId { get; set; }
    }

    public class FieldValues
    {
        public virtual string Value { get; set; } = "";
        public virtual string CNName { get; set; } = "";
        public virtual string ENName { get; set; } = "";
        public string ParameterCode { get; set; }
        public string PositionCode { get; set; }
        public string OrgCode { get; set; }
    }

    public class ApproverModel
    {
        public virtual int EmpId { get; set; }
        public virtual string CNName { get; set; } = "";
        public virtual string ENName { get; set; } = ""; 
    }
    public class FlowDataAreaParameter
    { 
        public virtual string FlowTableCode { get; set; } = "";
        public virtual string TableCode { get; set; } = "";
        public virtual string MainTable { get; set; } = "";
        public virtual string ForeignKey { get; set; } = "";
        public virtual string ForeignValue { get; set; } = "";

        public virtual string PrimaryKey { get; set; } = "";
        public virtual string PrimaryValue  { get; set; } = "";
        public virtual List<FlowFieldParameter> FieldParameters { get; set; } = new List<FlowFieldParameter>();
    }

    public class ComplatedParameter
    {
        public virtual string TableCode { get; set; } = "";
        public virtual string PrimaryKey { get; set; } = "";
        public virtual string PrimaryValue { get; set; } = "";
        public virtual FlowInstanceStatus InstanceStatus { get; set; }
    }
    public class RequestParameter
    {
        public virtual long TaskId { get; set; }
        public virtual string Opinion { get; set; }
    }

}
