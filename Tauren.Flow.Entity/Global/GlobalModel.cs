﻿namespace Tauren.Flow.Entity.Global
{
    /// <summary>
    /// 全局会话信息
    /// </summary>
    public class GlobalModel
    {
        /// <summary>
        /// 用户ID
        /// </summary>
        public virtual int UserId { get; set; }
        /// <summary>
        /// 账号
        /// </summary>
        public virtual string Account { get; set; }
        /// <summary>
        /// 语言
        /// </summary>
        public virtual string Language { get; set; }

        /// <summary>
        /// 是否是中文环境
        /// </summary>
        public virtual bool IsChinese { get { return Language == "zh-cn" || string.IsNullOrEmpty(this.Language); } }
        /// <summary>
        /// 员工id
        /// </summary>
        public virtual int EmpId { get; set; }
        /// <summary>
        /// 员工cnnmae
        /// </summary>
        public virtual string EmpName { get; set; }
        /// <summary>
        /// 员工EnName
        /// </summary>
        public virtual string EmpEnName { get; set; }
        /// <summary>
        /// 组织名称
        /// </summary>
        public virtual string OrgName { get; set; }
        /// <summary>
        /// 组织id
        /// </summary>
        public virtual int OrgId { get; set; }
        /// <summary>
        /// 职位名称
        /// </summary>
        public virtual string PostionName { get; set; }
        /// <summary>
        /// 职位id
        /// </summary>
        public virtual int PositionId { get; set; }
        /// <summary>
        /// 是否是系统管理员
        /// </summary>
        public virtual bool IsAdmin { get; set; }
        /// <summary>
        /// 租户编码
        /// </summary>
        public virtual string TenementCode { get; set; } 
        /// <summary>
        /// 业务库数据链接
        /// </summary>
        public virtual string BasicDataConnection { get; set; }
        /// <summary>
        /// 流程数据库数据链接
        /// </summary>
        public virtual string FlowConnection { get; set; }
        /// <summary>
        /// 表单业务库数据链接
        /// </summary>
        public virtual string FormConnection { get; set; }
        /// <summary>
        /// 文件务库数据链接
        /// </summary>
        public virtual string FileConnection { get; set; }
        /// <summary>
        /// 系统资源文件
        /// </summary>
        public virtual string ResConnection { get; set; }
        /// <summary>
        /// 租户名称
        /// </summary>
        public virtual string TenementName { get; set; }
        /// <summary>
        /// 租户电话
        /// </summary>
        public virtual string TenementPhone { get; set; }

        public virtual string EmpCode { get; set; }
    }
}
