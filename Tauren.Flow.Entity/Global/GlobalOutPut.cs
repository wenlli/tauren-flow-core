﻿using System.Collections.Generic;
using Tauren.Flow.Entity.Enum;
using Tauren.Flow.Entity.Model;

namespace Tauren.Flow.Entity.Global
{
    public class GlobalOutPut : EntityBase
    {
        public virtual long RowNo { get; set; }
        /// <summary>
        /// 禁用下行的样式
        /// </summary>
        public string Row_Style { get; set; }
        /// <summary>
        /// 行操作
        /// </summary>
        public virtual List<SystemButton> Operations { get; set; }
        public virtual string OtherType { get; set; }
        public virtual bool Enable { get; set; }
        /// <summary>
        /// 字体加粗
        /// </summary>
        public virtual bool FontWeight { get; set; }
    }
    public class Result<T>
    {
        public Result(T data)
        {
            this.Success = true;
            this.Code = 200200;
            this.Data = data;
        } 
        public virtual bool Success { get; set; }
        public virtual int Code { get; set; }
        public virtual T Data { get; set; }
        /// <summary>
        /// 失败
        /// </summary>
        /// <param name="code"></param>
        public void OnFailure(int code = GlobalErrorType.GLOBAL_RES_FLOW_ERROR)
        {
            this.Success = false;
            this.Code = code;
        }
        /// <summary>
        /// 失败
        /// </summary>
        /// <param name="code"></param>
        public void OnSuccess(T data,int code =200)
        {
            this.Success = true;
            this.Code = code;
            this.Data = data;
        }
    }
    public class Result
    { 
        public Result() { }
        public Result(object data)
        {
            this.Success = true;
            this.Code = 200200;
            this.Data = data;
        }
        public virtual bool Success { get; set; }
        public virtual int Code { get; set; }
        public virtual object Data { get; set; }
        /// <summary>
        /// 失败
        /// </summary>
        /// <param name="code"></param>
        public void OnFailure(int code = GlobalErrorType.GLOBAL_RES_FLOW_ERROR)
        {
            this.Success = false;
            this.Code = code;
        }
        /// <summary>
        /// 成功
        /// </summary>
        /// <param name="data"></param>
        /// <param name="code"></param>
        public void OnSuccess(object data, int code = 200200)
        {
            this.Success = true;
            this.Code = code;
            this.Data = data; 
        }
    }
}
