﻿namespace Tauren.Flow.Entity.Global
{
    public class PersonInfo
    {
        /// <summary>
        /// 员工编号
        /// </summary>
        public virtual int EmpId { get; set; }
        /// <summary>
        /// 中文名
        /// </summary>
        public virtual string CNName { get; set; }

        /// <summary>
        /// 英文名
        /// </summary>
        public virtual string ENName { get; set; }
        public virtual int OnParttime { get; set; }
        public virtual string EmpCode { get; set; }
    }

    public class PersonInfo_InPut : PersonInfo
    {

    }
    public class PersonInfo_OutPut : PersonInfo
    {

    }
}
