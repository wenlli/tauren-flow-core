﻿using System.Collections.Generic;
using Tauren.Flow.Entity.Enum;
using Tauren.Flow.Entity.Output;

namespace Tauren.Flow.Entity.Global
{
    public class QueryModel
    {
        /// <summary>
        /// 关键字
        /// </summary>
        public string KeyWord { get; set; }

        /// <summary>
        /// 排序字段
        /// </summary>
        public string SortField { get; set; }

        /// <summary>
        /// 顺序排序还是倒序排序
        /// </summary>
        public bool IsAsc { get; set; }

        /// <summary>
        /// 当前页面
        /// </summary>
        public long Page { get; set; }

        /// <summary>
        /// 当前页展示多少条数据
        /// </summary>
        public long PageSize { get; set; }

        /// <summary>
        /// 员工ID
        /// </summary>
        public int EmpId { get; set; }
        /// <summary>
        /// 组织ID
        /// </summary>
        public int OrgId { get; set; }

        /// <summary>
        /// 职位ID
        /// </summary>
        public int PositionId { get; set; }

        /// <summary>
        /// 公司ID
        /// </summary>
        public int CompanyId { get; set; }

        /// <summary>
        /// 是否含兼职
        /// </summary>
        public bool IsPartTime { get; set; }

        public string keyWordField { get; set; }
        public object keyWordFieldValue { get; set; }
        public List<Options> paramters { get; set; } = new List<Options>();
        public bool UseDisabledData { get; set; } = true;
        public List<Options> ExceptDataField = new List<Options>();

        public bool CheckPermission { get; set; } = true; 
    }
    public class TableListModel<TEntity> where TEntity : class, new()
    {
        /// <summary>
        /// 记录总数
        /// </summary>
        public long Total { get; set; }

        /// <summary>
        /// 数据体
        /// </summary>
        public List<TEntity> Body { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        public List<TableListHeaderModel> Header { get; set; }

        /// <summary>
        /// 页面操作--按钮
        /// </summary>
        public virtual List<SystemButton> Operations { get; set; } = new List<SystemButton>();

    }
    /// <summary>
    /// 表头定义
    /// </summary>
    public class TableListHeaderModel
    {
        /// <summary>
        /// 字段
        /// </summary>
        public string Key { get; set; }
        /// <summary>
        /// 字段名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 宽度
        /// </summary>
        public int Width { get; set; }
        /// <summary>
        /// 类型  
        /// 主键=primary_key
        /// 主键名称=primary_name
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// 是否隐藏
        /// </summary>
        public bool Hide { get; set; }

        /// <summary>
        /// 格式
        /// </summary>
        public string Format { get; set; }

        /// <summary>
        /// 是否是操作栏
        /// </summary>
        public bool IsOperation { get; set; }

        /// <summary>
        /// 排序字段
        /// </summary>
        public object SorTable { get; set; }

        public bool IsTag { get; set; }

        public bool IsButton { get; set; }

        public bool IsOtherStyle { get; set; }
        public bool IsIcon { get; set; }

        private string _Align;

        /// <summary>
        /// 是否是svg
        /// </summary>
        public bool IsSvg { get; set; }


        public TableListHeaderModel()
        {
            Options = new List<Options>();
            Rules = new List<Dictionary<object, object>>();
        }

        public string Align
        {
            get => _Align; set
            {
                if (value is null)
                    _Align = "";
                else _Align = value;
            }
        }
        public List<Options> Options { get; set; }
        public List<Dictionary<object, object>> Rules { get; set; }
        public bool Show { get; set; }
        public bool Editor { get; set; }
        public ControlType ControlType { get; set; }
        public bool Fixed { get; set; }
        public bool Hyperlink { get; set; }
    }
    public class EmployeeListModel : GlobalOutPut
    {

        /// <summary>
        /// 基本信息的ID
        /// </summary>
        public int EmpId { get; set; }

        /// <summary>
        /// 中文名
        /// </summary>
        public string CNName { get; set; }

        /// <summary>
        /// 英文名
        /// </summary>
        public string ENName { get; set; }

        /// <summary>
        /// 员工编号
        /// </summary>
        public string EmpCode { get; set; }

        /// <summary>
        /// 组织ID
        /// </summary>
        public int OrganizationId { get; set; }

        /// <summary>
        /// 组织名称
        /// </summary>
        public string OrganizationName { get; set; }

        /// <summary>
        /// 职位ID
        /// </summary>
        public int PositionId { get; set; }

        /// <summary>
        /// 职位名称
        /// </summary>
        public string PositionName { get; set; }

        /// <summary>
        /// 单位ID
        /// </summary>
        public int UnitInfoId { get; set; }

        /// <summary>
        /// 单位名称
        /// </summary>

        public string UnitInfoName { get; set; }

        /// <summary>
        /// 兼职ID
        /// </summary>
        public int OnPartTime { get; set; }
        /// <summary>
        /// 兼职名称
        /// </summary>
        public string Dic_OnPartTime { get; set; } 
        public string Dic_Enable { get; set; }
        public string Dic_Creator { get; set; }
        public string Dic_Creatdate { get; set; }
    }
    public class AccountListModel : GlobalOutPut
    {


        /// <summary>
        /// 基本信息的ID
        /// </summary>
        public int AccountId { get; set; }

        /// <summary>
        /// 中文名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 账号
        /// </summary>
        public string Account { get; set; }

        /// <summary>
        /// 员工id
        /// </summary>
        public int EmpId { get; set; }

        /// <summary>
        /// 员工名称
        /// </summary>
        public string EmpName { get; set; }

        /// <summary>
        /// 组织ID
        /// </summary>
        public int OrgId { get; set; }

        /// <summary>
        /// 组织名称
        /// </summary>
        public string OrganizationName { get; set; }
        public string Dic_Enable { get; set; }
        public string Dic_Creator { get; set; }
        public string Dic_Creatdate { get; set; } 
    }
    public class UnitListModel
    {
        public long RowNo { get; set; }
        public int UnitId { get; set; }
        public string UnitCode { get; set; }
        public string CNName { get; set; }
        public string ENName { get; set; }
        public int ParentId { get; set; }
        public string UnitAddress { get; set; }
        public string Legal { get; set; }
        public string Contact { get; set; }
        public string Phone { get; set; }
    }

    public class FromTableHeader
    {

        public string FieldKey { get; set; }
        public bool Show { get; set; } = true;
        public bool Primary { get; set; }
    }
}
