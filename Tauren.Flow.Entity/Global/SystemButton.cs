﻿namespace Tauren.Flow.Entity.Global
{
    public class SystemButton
    {
        /// <summary>
        /// 名称
        /// </summary>
        public virtual string Label { get; set; }
        /// <summary>
        /// 调用方法
        /// </summary>
        public virtual string Value { get; set; }
        /// <summary>
        /// 按钮
        /// </summary>
        public virtual string Type { get; set; }
        /// <summary>
        /// 大小
        /// </summary>
        public virtual string Size { get; set; }

        /// <summary>
        /// 图标
        /// </summary>
        public virtual string Icon { get; set; }

        /// <summary>
        /// 圆角按钮
        /// </summary>
        public virtual string Shape { get; set; }

        public virtual string KeyFieldValue { get; set; }
        public virtual long PrimaryKeyValue { get; set; }
        public virtual string Location { get; set; }
        public virtual object Parameters { get; set; }


    }
}
