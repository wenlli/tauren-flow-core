﻿using System;
using System.Collections.Generic;
using System.Text;
using Tauren.Flow.Entity.Global;

namespace Tauren.Flow.Entity.Input
{
    public class Field_Form_InPut
    {
        /// <summary>
        /// 主键
        /// </summary>
        public virtual long AutoId { get; set; }
        /// <summary>
        /// 表单中文名
        /// </summary>
        public virtual string CNName { get; set; }

        /// <summary>
        /// 表单英文名
        /// </summary>
        public virtual string ENName { get; set; }

        /// <summary>
        /// 当前表单版本
        /// </summary>
        public virtual long CurrentVersion { get; set; }


        /// <summary>
        /// 可使用申请人ID
        /// </summary>
        public virtual bool UseApplicantEmpId { get; set; }

        /// <summary>
        /// 表单状态
        /// </summary>
        public virtual int Status { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public virtual double Sequence { get; set; }

        /// <summary>
        /// 主表数据
        /// </summary>
        public virtual List<Form_Field_Primary_TableInfo_InPut> Tables { get; set; } = new List<Form_Field_Primary_TableInfo_InPut>();
    }
}
