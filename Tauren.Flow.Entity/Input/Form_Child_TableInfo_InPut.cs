﻿using Dapper.Contrib.Extensions;

namespace Tauren.Flow.Entity.Input
{
    public class Form_Child_TableInfo_InPut  
    {
        [Key]
        public virtual int AutoId { get; set; }
        /// <summary>
        /// 表名
        /// </summary>
        public virtual string TableCode { get; set; } 
        public virtual string ForeignKey { get; set; }
        /// <summary>
        /// /由于流程对应的表名
        /// </summary>
        public virtual string FlowTableCode { get; set; }

    }
}
