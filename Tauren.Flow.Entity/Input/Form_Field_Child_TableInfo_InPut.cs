﻿using Tauren.Flow.Entity.Model;

namespace Tauren.Flow.Entity.Input
{
    public class Form_Field_Child_TableInfo_InPut
    {
        /// 主键
        /// </summary>
        public virtual long AutoId { get; set; }

        /// <summary>
        /// 表名
        /// </summary>
        public virtual string TableCode { get; set; }

        /// <summary>
        /// 外键字段
        /// </summary>
        public virtual string ForeignKey { get; set; }

        /// <summary>
        /// 可使用申请人ID
        /// </summary>
        public virtual long PrimaryTableId { get; set; }

        /// <summary>
        /// /由于流程对应的表名
        /// </summary>
        public virtual string FlowTableCode { get; set; }
    }
}
