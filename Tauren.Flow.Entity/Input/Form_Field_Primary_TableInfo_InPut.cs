﻿using System.Collections.Generic;

namespace Tauren.Flow.Entity.Input
{
    public class Form_Field_Primary_TableInfo_InPut
    { /// <summary>
      /// 主键
      /// </summary>
        public virtual long AutoId { get; set; }

        /// <summary>
        /// 表单ID
        /// </summary>
        public virtual long FormId { get; set; }

        /// <summary>
        /// 表名
        /// </summary>
        public virtual string TableCode { get; set; }

        /// <summary>
        /// /由于流程对应的表名
        /// </summary>
        public virtual string FlowTableCode { get; set; }
        /// <summary>
        /// 外键字段
        /// </summary>
        public virtual string ForeignKey { get; set; }
        public virtual long TableId { get; set; }
        /// <summary>
        /// 子表数据
        /// </summary>
        public virtual List<Form_Field_Child_TableInfo_InPut> Childs { get; set; } = new List<Form_Field_Child_TableInfo_InPut>();
    }
}
