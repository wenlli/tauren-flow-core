﻿namespace Tauren.Flow.Entity.Input
{
    public class Form_SametableInfo_InPut
    {
        public virtual int AutoId { get; set; }
        /// <summary>
        /// 表名
        /// </summary>
        public virtual string TableCode { get; set; }
        /// <summary>
        /// /由于流程对应的表名
        /// </summary>
        public virtual string FlowTableCode { get; set; }
    }
}
