﻿using System.Collections.Generic;

namespace Tauren.Flow.Entity.Input
{
    public class Form_TableInfo_InPut 
    { 
        public virtual int AutoId { get; set; }

        /// <summary>
        /// 抛出到外键表做外键的字段
        /// </summary>
        public virtual string ForeignKey { get; set; }

        /// <summary>
        /// 表名
        /// </summary>
        public virtual string TableCode { get; set; }
        /// <summary>
        /// /由于流程对应的表名
        /// </summary>
        public virtual string FlowTableCode { get; set; }
        /// <summary>
        /// 表
        /// </summary>
        public virtual List<Form_SametableInfo_InPut> Tables { get; set; } = new List<Form_SametableInfo_InPut>();
        /// <summary>
        /// 互斥表
        /// </summary>
        public virtual List<Form_SametableInfo_InPut> Sametables { get; set; } = new List<Form_SametableInfo_InPut>();
        /// <summary>
        /// 子表
        /// </summary>
        public virtual List<Form_Child_TableInfo_InPut> ChildTables { get; set; } = new List<Form_Child_TableInfo_InPut>();

    }
}
