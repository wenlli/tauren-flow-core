﻿namespace Tauren.Flow.Entity.Input
{
    public class LogInModel
    {
        /// <summary>
        /// 登录的账号
        /// </summary>
        public virtual string Content { get; set; }
        /// <summary>
        /// 密钥
        /// </summary>
        public virtual string Key { get; set; }
        /// <summary>
        /// 租户编码
        /// </summary>
        public virtual string TenantCode { get; set; }
    }
    public class LogInUser
    {
        /// <summary>
        /// 账号
        /// </summary>
        public virtual string Account { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        public virtual string Password { get; set; }
    }
}
