﻿namespace Tauren.Flow.Entity.Input
{
    public class ModuleResoucesRes_Input
    { 
        /// <summary>
        /// 国际语言
        /// </summary>
        public virtual string Language { get; set; }

        /// <summary>
        /// 资源内容
        /// </summary>
        public virtual string Value { get; set; }
    }
}
