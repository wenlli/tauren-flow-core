﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Tauren.Flow.Entity.Input
{
    /// <summary>
    /// 资源主键表
    /// </summary>
    public class ModuleResouces_Input
    {
        /// <summary>
        /// 模块编号
        /// </summary>
        [Display(Name = "模块编号"), Required(ErrorMessage = "{0}不能为空!"), StringLength(20, ErrorMessage = "{0}不超过20个字符"), RegularExpression(@"^([a-zA-Z0-9]){1,20}$", ErrorMessage = "请输入英文字符、数字组成的{0}")]
        public virtual string MenuCode { get; set; }
        /// <summary>
        /// 资源编号
        /// </summary>
        /// </summary>
        [Display(Name = "资源编号"), Required(ErrorMessage = "{0}不能为空!"), StringLength(20, ErrorMessage = "{0}不超过30个字符"), RegularExpression(@"^([a-z0-9]){1,30}$", ErrorMessage = "请输入英文字符、数字组成的{0}")]
        public virtual string ResCode { get; set; }
        /// <summary>
        /// 资源类型-提示/错误/
        /// </summary>
        public virtual string ResoucesType { get; set; }
        /// <summary>
        /// 内容
        /// </summary>
        public virtual List<ModuleResoucesRes_Input> ChildFieldValues { get; set; }

        /// <summary>
        /// 主键ID
        /// </summary>

        public virtual long AutoID { get; set; }

        /// <summary>
        /// 主键ID
        /// </summary>

        public virtual long ResourceId { get; set; }
    }
}
