﻿#nullable disable

namespace Tauren.Flow.Entity.Model
{
    [Dapper.Contrib.Extensions.Table("buttons")]
    public class Buttons : EntityBase
    {
        [Dapper.Contrib.Extensions.Key]
        public virtual long AutoId { get; set; }
        public virtual long BtnId { get; set; }
        public virtual string Icon { get; set; }
        public virtual string Type { get; set; }
        public virtual string Size { get; set; }
        public virtual string Value { get; set; }
        public virtual string Shape { get; set; }
        public virtual bool IsUseFile { get; set; }
        public virtual double? Sequence { get; set; }
        public virtual string Location { get; set; }
    }

    public class ExtButtons: Buttons
    {
        public string Label { get; set; }
    }
}
