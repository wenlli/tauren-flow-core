﻿using Dapper.Contrib.Extensions;
using System;

namespace Tauren.Flow.Entity.Model
{
    [Table("employee")]
   public class Employee : EntityBase
    {
        /// <summary>
        /// 员工ID
        /// </summary>
        [Key]
        public virtual int EmpId { get; set; }
        /// <summary>
        /// 中文名
        /// </summary>
        public virtual string CNName { get; set; }

        /// <summary>
        /// 英文名
        /// </summary>
        public virtual string ENName { get; set; }
        /// <summary>
        /// 员工编码
        /// </summary>
        public virtual string EmpCode { get; set; }
        /// <summary>
        /// 邮箱
        /// </summary>
        public virtual string Email { get; set; }
        /// <summary>
        /// 手机
        /// </summary>
        public virtual string Phone { get; set; }

        /// <summary>
        /// 组织ID
        /// </summary>
        public virtual string OrgId { get; set; }

        /// <summary>
        /// 职位ID
        /// </summary>
        public virtual string PositionId { get; set; }

        /// <summary>
        /// 单位ID
        /// </summary>
        public virtual int UnitInfoId { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public virtual string Gender { get; set; }

        /// <summary>
        /// 年龄
        /// </summary>
        public virtual int? Age { get; set; }

        /// <summary>
        /// 身份证号
        /// </summary>
        public virtual string IdCard { get; set; }

        /// <summary>
        /// 状态,0-删除，1-在职 ，2-离职，3-停职
        /// </summary>
        public virtual int Status { get; set; }

        /// <summary>
        /// 汇报人
        /// </summary>
        public virtual int Repoter { get; set; }

        public virtual DateTime? BirthDay { get; set; }

        public virtual DateTime? JoinDate { get; set; }

        public virtual string Address { get; set; }
        /// <summary>
        /// 启用？
        /// </summary>
        public virtual bool Enable { get; set; }
        /// <summary>
        /// 顺序
        /// </summary>
        public virtual double Sequence { get; set; }

        public virtual bool HaveDelete { get; set; }


    }
}
