﻿using System;
/* * 
 * *目的：不用每个类去写创建人和修改人 
 * */

namespace Tauren.Flow.Entity.Model
{

    /// <summary>
    /// 实体基类
    /// </summary>
    public class EntityBase
    {
        /// <summary>
        /// 创建人
        /// </summary>
        public virtual int Creator { get; set; }
        /// <summary>
        /// 修改人
        /// </summary>
        public virtual int Modifier { get; set; }
        /// <summary>
        /// 创建日期
        /// </summary>
        public virtual DateTime? Creatdate { get; set; }
        /// <summary>
        /// 修改日期
        /// </summary>
        public virtual DateTime? Modifdate { get; set; }
    }
}
