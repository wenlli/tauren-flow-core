﻿using Dapper.Contrib.Extensions;
namespace Tauren.Flow.Entity.Model
{
    /// <summary>
    /// 表单
    /// </summary>
    [Table("field_form")]
    public class Field_Form:EntityBase
    {
        [Key]
        /// <summary>
        /// 主键
        /// </summary>
        public virtual long AutoId { get; set; }
        /// <summary>
        /// 表单中文名
        /// </summary>
        public virtual string CNName { get; set; } = "";

        /// <summary>
        /// 表单英文名
        /// </summary>
        public virtual string ENName { get; set; } = "";

        /// <summary>
        /// 当前表单版本
        /// </summary>
        public virtual long CurrentVersion { get; set; }

        /// <summary>
        /// 表单状态
        /// </summary>
        public virtual int Status { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public virtual double Sequence { get; set; }

        /// <summary>
        /// 是否删除
        /// </summary>
        public bool HaveDelete { get; set; }


        /// <summary>
        /// 可使用申请人ID
        /// </summary>
        public virtual bool UseApplicantEmpId { get; set; } = true;
    }
}
