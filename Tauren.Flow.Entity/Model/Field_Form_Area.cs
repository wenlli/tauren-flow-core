﻿using Dapper.Contrib.Extensions;

namespace Tauren.Flow.Entity.Model
{
    /// <summary>
    /// 表单区域表
    /// </summary>
    [Table("field_form_area")]
    public class Field_Form_Area : EntityBase
    {
       
        /// <summary>
        /// 主键
        /// </summary>
        public virtual long AutoId { get; set; }

        /// <summary>
        /// 表单版本id
        /// 表单版本id
        /// </summary>
        public virtual long VersionId { get; set; }

        /// <summary>
        /// 表单中文名
        /// </summary>
        public virtual string CNName { get; set; }

        /// <summary>
        /// 表单英文名
        /// </summary>
        public virtual string ENName { get; set; }


        /// <summary>
        /// 区域编码
        /// </summary>
        public virtual string AreaCode { get; set; }

        /// <summary>
        /// 区域对应表
        /// </summary>
        public virtual string TableCode { get; set; }


        /// <summary>
        /// 区域呈现方式
        /// </summary>
        public virtual int Presentation { get; set; }


        /// <summary>
        /// 表格时行数
        /// </summary>
        public virtual int Rowes { get; set; }

        /// <summary>
        /// 不是表格时列数
        /// </summary>
        public virtual int Columnes { get; set; }

        /// <summary>
        /// 是否是多记录
        /// </summary>
        public virtual bool Records { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public virtual double Sequence { get; set; }

        public virtual string MainTable { get; set; }

    }
}
