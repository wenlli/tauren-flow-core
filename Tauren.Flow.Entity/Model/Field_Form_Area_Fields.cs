﻿using Dapper.Contrib.Extensions;

namespace Tauren.Flow.Entity.Model
{
    /// <summary>
    /// 表单区域字段表
    /// </summary>
    [Table("field_form_area_fields")]
    public class Field_Form_Area_Fields : EntityBase
    {
     
        /// <summary>
        /// 主键
        /// </summary>
        public virtual long AutoId { get; set; }

        /// <summary>
        /// 字段区域ID
        /// </summary>
        public virtual long AreaId { get; set; }

        /// <summary>
        /// 字段编码
        /// </summary>
        public virtual string FieldCode { get; set; }

        /// <summary>
        /// 字段标识
        /// </summary>
        public virtual string FieldKey { get; set; }
        /// <summary>
        /// 中文名
        /// </summary>
        public virtual string CNName { get; set; }

        /// <summary>
        /// 字段英文名
        /// </summary>
        public virtual string ENName { get; set; }


        /// <summary>
        /// 控件类型
        /// </summary>
        public virtual string ControlsType { get; set; }

        /// <summary>
        /// 常用参数来源
        /// </summary>
        public virtual string ControlSouces { get; set; }

        /// <summary>
        /// 字段类型
        /// </summary>
        public virtual string Type { get; set; }

        /// <summary>
        /// 默认值
        /// </summary>
        public virtual string DefaultValue { get; set; }
        /// <summary>
        /// 最大值
        /// </summary>
        public virtual int? FLength { get; set; }

        /// <summary>
        /// 必填
        /// </summary>
        public virtual bool Required { get; set; }

        /// <summary>
        /// 最大值
        /// </summary>
        public virtual int? Max { get; set; }

        /// <summary>
        /// 最小值
        /// </summary>
        public virtual int? Min { get; set; }

        /// <summary>
        /// 精度
        /// </summary>
        public virtual int? Precisions { get; set; }

        /// <summary>
        /// 单选还是多选
        /// </summary>
        public virtual int? SelectType { get; set; }

        /// <summary>
        /// 是否显示
        /// </summary>
        public virtual bool Show { get; set; }

        /// <summary>
        /// 是否可编辑
        /// </summary>
        public virtual bool Editor { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public virtual double Sequence { get; set; }
    }
}
