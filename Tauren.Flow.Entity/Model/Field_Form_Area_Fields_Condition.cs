﻿using Dapper.Contrib.Extensions;



namespace Tauren.Flow.Entity.Model
{
    /// <summary>
    /// 字段条件
    /// </summary>
    [Table("field_form_area_fields_condition")]
    public class Field_Form_Area_Fields_Condition : EntityBase
    {
        [Key]
        /// <summary>
        /// 主键
        /// </summary>
        public virtual long AutoId { get; set; }

        /// <summary>
        /// 字段标识
        /// </summary>
        public virtual string FieldKey { get; set; } = "";

        /// <summary>
        /// 左边括号
        /// </summary>
        public virtual string LeftBracket { get; set; } = "";
        /// <summary>
        /// 级联字段
        /// </summary>
        public virtual string CascadeField { get; set; } = "";
        /// <summary>
        /// 条件
        /// </summary>
        public virtual string Condition { get; set; } = "";
        /// <summary>
        /// 级联字段值
        /// </summary>
        public virtual string CascadeFieldValue { get; set; } = "";
        /// <summary>
        /// 级联字段值
        /// </summary>
        public virtual string RightBracket { get; set; } = "";

        /// <summary>
        /// 关联条件
        /// </summary>
        public virtual string Associated { get; set; } = "";

    }
}
