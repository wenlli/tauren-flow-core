﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Tauren.Flow.Entity.Model
{
    [Table("field_form_area_fields_regular")]
    public class Field_Form_Area_Fields_Regular : EntityBase
    {

        /// <summary>
        /// 主键
        /// </summary>
        public virtual long AutoId { get; set; }

        /// <summary>
        /// 字段标识
        /// </summary>
        public virtual string FieldKey { get; set; } = "";

        public virtual string Regulars { get; set; } = "";

        public virtual long FieldId { get; set; }
    }
    public class Fields_Regular
    {
        public int? min { get; set; }
        public int? max { get; set; }
        public string message { get; set; }
        public string trigger { get; set; }
        public string pattern { get; set; }
    }
}
