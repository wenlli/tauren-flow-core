﻿using Dapper.Contrib.Extensions;

namespace Tauren.Flow.Entity.Model
{

    /// <summary>
    /// /表单版本
    /// </summary>
    [Table("field_form_version")]
    public class Field_Form_Version : EntityBase
    {
       
        /// <summary>
        /// 主键
        /// </summary>
        public virtual long AutoId { get; set; }

        /// <summary>
        /// 表单ID
        /// </summary>
        public virtual long FormId { get; set; }

        /// <summary>
        /// 版本号
        /// </summary>
        public virtual long VersionNo { get; set; }

    }
}
