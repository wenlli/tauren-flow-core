﻿using Dapper.Contrib.Extensions;

#nullable disable

namespace Tauren.Flow.Entity.Model
{
    [Table("fields")]
    public class Fields : EntityBase
    {
        [Key]
        public virtual int AutoId { get; set; }
        public virtual string TableCode { get; set; }
        public virtual string FieldCode { get; set; }
        public virtual string FieldName { get; set; }
        public virtual string FieldENName { get; set; }
        public virtual string ControlStype { get; set; }
        public virtual string ControlSouces { get; set; }
        public virtual int? FieldType { get; set; }
        public virtual string DefaultValue { get; set; }
        public virtual int FieldLength { get; set; }
        public virtual bool IsRequired { get; set; }
        public virtual bool IsInlay { get; set; }
        public virtual int Precisions { get; set; }
        public virtual bool Enable { get; set; }
        public virtual bool HaveDelete { get; set; }
        public virtual decimal? Sequence { get; set; }
        public virtual int? SelectType { get; set; }
    }
}
