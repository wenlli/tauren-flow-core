﻿namespace Tauren.Flow.Entity.Model
{

    [Dapper.Contrib.Extensions.Table("flowinfo")]
    public class FlowInfo : EntityBase
    {
        [Dapper.Contrib.Extensions.Key]
        /// <summary>
        /// 流程ID
        /// </summary> 
        public virtual long FlowId { get; set; }
        /// <summary>
        /// 流程中文名
        /// </summary>
        public virtual string CNName { get; set; } = "";
        /// <summary>
        /// 流程英文名
        /// </summary>
        public virtual string ENName { get; set; } = "";
        /// <summary>
        /// 流程类型
        /// </summary>
        public virtual long? Type { get; set; }
        /// <summary>
        /// 流程管理员
        /// </summary>
        public virtual string Manager { get; set; }
        /// <summary>
        /// 流程实例管理员
        /// </summary>
        public virtual string InstanceManager { get; set; }

        /// <summary>
        /// 表单ID
        /// </summary>
        public virtual long? FormId { get; set; }

        /// <summary>
        /// 可选择申请人
        /// </summary>
        public virtual bool? SelectApplyEmp { get; set; }

        /// <summary>
        /// 流程图标
        /// </summary>
        public virtual string FlowIcon { get; set; } = "";

        /// <summary>
        /// 流程内联样式
        /// </summary>
        public virtual string FlowStyle { get; set; } = "";

        /// <summary>
        /// 提示信息
        /// </summary>
        public virtual string Placeholder { get; set; } = "";

        /// <summary>
        /// 跳过规则
        /// </summary>
        public virtual int? SkipRules { get; set; }

        /// <summary>
        /// 是否向发起人发送消息
        /// </summary>
        public virtual bool SendMessage { get; set; }

        /// <summary>
        /// 当前流程版本
        /// </summary>
        public virtual long? CurrentVersion { get; set; }

        /// <summary>
        /// 流程信息状态
        /// </summary>
        public virtual int? Status { get; set; }
        /// <summary>
        /// 可查看实例详情
        /// </summary>
        public virtual bool? View_Instance { get; set; }

        /// <summary>
        /// 流程中也可回收
        /// </summary>
        public virtual bool? Recycled { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public virtual double? Sequence { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public virtual string Note { get; set; } = "";
    }
}
