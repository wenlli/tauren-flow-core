﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tauren.Flow.Entity.Model
{
    [Dapper.Contrib.Extensions.Table("flowsort")]
    public class FlowSort : EntityBase
    {
        public virtual long AutoId { get; set; }
        public virtual long FlowId { get; set; }
        public virtual int EmpId { get; set; }
        public virtual double Sequence { get; set; }
    }
    public class WorkFlowInfoSort
    {
        /// <summary>
        /// 流程版本id
        /// </summary>
        public virtual long FlowVersionId { get; set; }

        /// <summary>
        /// 流程id
        /// </summary>
        public virtual long FlowId { get; set; }

        /// <summary>
        /// 表单ID
        /// </summary>
        public virtual long FormId { get; set; }

        /// <summary>
        /// 流程表单版本ID
        /// </summary>
        public virtual long FormVersionId { get; set; }
        /// <summary>
        /// 英文名
        /// </summary>
        public virtual string CNName { get; set; }
        /// <summary>
        /// 中文名
        /// </summary>
        public virtual string ENName { get; set; }
        /// <summary>
        /// 流程图标
        /// </summary>
        public virtual string FlowIcon { get; set; } = "";

        /// <summary>
        /// 流程内联样式
        /// </summary>
        public virtual string FlowStyle { get; set; } = "";

        /// <summary>
        /// 提示信息
        /// </summary>
        public virtual string Placeholder { get; set; } = "";

        public virtual double Sequence { get; set; }
    }
}
