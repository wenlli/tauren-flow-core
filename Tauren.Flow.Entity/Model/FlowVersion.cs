﻿namespace Tauren.Flow.Entity.Model
{
    [Dapper.Contrib.Extensions.Table("flow_version")]
    public class FlowVersion : EntityBase
    {

        [Dapper.Contrib.Extensions.ExplicitKey]
        /// <summary>
        /// 流程版本ID
        /// </summary>
        public virtual long? VersionId { get; set; }

        /// <summary>
        /// 流程ID
        /// </summary>
        public virtual long? FlowId { get; set; }


        /// <summary>
        /// 版本名称
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// 版本编码
        /// </summary>
        public virtual long? VersionNo { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public virtual double? Sequence { get; set; }

        /// <summary>
        /// 表单ID
        /// </summary>
        public virtual long? FormId { get; set; }

        /// <summary>
        /// 表单版本ID
        /// </summary>
        public virtual long? FormVersionId { get; set; }

        /// <summary>
        /// 是否并行
        /// </summary>
        public virtual bool ParallelTask { get; set; }
        /// <summary>
        /// 直接提交到退回的步骤
        /// </summary>
        public virtual bool SumbitPrevStep { get; set; }
    }
}
