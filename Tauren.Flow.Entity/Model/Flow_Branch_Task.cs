﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Tauren.Flow.Entity.Model
{
    [Table("flow_branch_task")]
    public class Flow_Branch_Task : EntityBase
    {
        [Dapper.Contrib.Extensions.Key]
        public virtual long AutoId { get; set; }
        public virtual long InstanceId { get; set; }
        public virtual long GroupId { get; set; }
        public virtual string StepId { get; set; }
        public virtual long TaskId { get; set; }
        public virtual string Branch { get; set; }
        public virtual string BranchLine { get; set; }
    }
    public class Branchs
    {
        public virtual string Branch { get; set; }
        public virtual string Line { get; set; }
    }
}
