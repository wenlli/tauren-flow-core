﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tauren.Flow.Entity.Model
{
    [Dapper.Contrib.Extensions.Table("flow_buttons")]
    public class Flow_Buttons : EntityBase
    {
        /// <summary>
        /// 按钮ID
        /// </summary>
        public virtual long AutoId { get; set; }

        /// <summary>
        /// 步骤ID
        /// </summary>
        public virtual string StepId { get; set; }

        /// <summary>
        /// 版本ID
        /// </summary>
        public virtual long? VersionId { get; set; }

        /// <summary>
        /// 连线中文名
        /// </summary>
        public virtual string CNName { get; set; }

        /// <summary>
        /// 连线英文名
        /// </summary>
        public virtual string ENName { get; set; }

        /// <summary>
        /// 按钮图标
        /// </summary>
        public virtual string Icon { get; set; }

        /// <summary>
        /// 图标样式
        /// </summary>
        public virtual string Style { get; set; }

        /// <summary>
        /// 提示信息
        /// </summary>
        public virtual string ToolTips { get; set; }

        /// <summary>
        /// 按钮编码
        /// </summary>
        public virtual string BtnCode { get; set; }

        /// <summary>
        /// 按钮排序
        /// </summary>
        public virtual double Sequence { get; set; }
    }
}
