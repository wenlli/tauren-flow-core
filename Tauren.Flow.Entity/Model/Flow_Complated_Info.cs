﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tauren.Flow.Entity.Model
{
    [Dapper.Contrib.Extensions.Table("flow_complated_info")]
    public class Flow_Complated_Info : EntityBase
    {
        [Dapper.Contrib.Extensions.Key]
        public virtual string AutoId { get; set; }
        public virtual string TableCode { get; set; } = "";
        public virtual string FlowTableCode { get; set; } = "";
        public virtual bool IsUseEmpId { get; set; }
        public virtual long FlowId { get; set; }
        public virtual long FlowVersionId { get; set; }

        public virtual string FieldCode { get; set; } = "";
    }
}

