﻿using System;
using System.Collections.Generic;
using System.Text;
using Tauren.Flow.Entity.Enum;
using Tauren.Flow.Entity.Output;

/// <summary>
/// 流程实例状态
/// </summary>
namespace Tauren.Flow.Entity.Model
{
    [Dapper.Contrib.Extensions.Table("flow_instance")]
    public class Flow_Instance
    {
        /// <summary>
        /// 流程实例ID
        /// </summary>
        [Dapper.Contrib.Extensions.ExplicitKey]
        public virtual long InstanceId { get; set; }

        /// <summary>
        /// 表单ID
        /// </summary>
        public virtual long FormId { get; set; }

        /// <summary>
        /// 表单版本ID
        /// </summary>
        public virtual long FormVersionId { get; set; }
        /// <summary>
        /// 流程分组ID
        /// </summary>
        public virtual long GroupId { get; set; }


        /// <summary>
        /// 发起人
        /// </summary>
        public virtual int InitiatorEmpId { get; set; }
        /// <summary>
        /// 申请人ID
        /// </summary>
        public virtual int ApplyEmpId { get; set; }
        /// <summary>
        /// 流程状态
        /// </summary>
        public virtual int FlowStatus { get; set; }

        /// <summary>
        /// 申请日期
        /// </summary>
        public virtual DateTime ApplyDate { get; set; }
        /// <summary>
        /// 发起日期
        /// </summary>
        public virtual DateTime InitiatorDate { get; set; }

        /// <summary>
        /// 流程版本
        /// </summary>
        public virtual long FlowVersionId { get; set; }

        /// <summary>
        /// 流程ID
        /// </summary>
        public virtual long FlowId { get; set; }
        /// <summary>
        /// 申请任务ID
        /// </summary>
        public virtual long ApplyTaskId { get; set; }

        public virtual DateTime? ComplatedTime { get; set; }
    }
    public class ExtInstance
    {   /// <summary>
        /// 流程实例ID
        /// </summary>
        public virtual long InstanceId { get; set; }
        /// <summary> 
        /// <summary>
        /// 流程分组ID
        /// </summary>
        public virtual long GroupId { get; set; }
        public virtual long CurrentTaskId { get; set; }
        public virtual string CurrentStep { get; set; }
    }

    public class ExtTaskInfoEntity
    {
        /// <summary>
        /// 当前节点所有要处理的任务
        /// </summary>
        public virtual List<Flow_Task> CurrentStepAllTasks { get; set; } = new List<Flow_Task>();
        /// <summary>
        /// 要创建的新任务
        /// </summary>
        public virtual List<Flow_Task> NewTasks { get; set; } = new List<Flow_Task>();
        /// <summary>
        /// 当前任务信息
        /// </summary>
        public virtual FlowTaskInfo CurrentTask { get; set; }
        /// <summary>
        /// 流程信息
        /// </summary>
        public virtual Flow_OutPut Flow_OutPut { get; set; }

        /// <summary>
        /// 当前节点数据
        /// </summary>
        public virtual NodeInfo CurrentStep { get; set; }

        /// <summary>
        /// 当前节点信息历史
        /// </summary>
        public virtual List<Flow_Instance_CurrentTask> Flow_Instance_CurrentTasks { get; set; } = new List<Flow_Instance_CurrentTask>();
        
        /// <summary>
        /// 流程实例
        /// </summary>
        public virtual Flow_Instance Instance { get; set; }
        public virtual bool ParallelBranch { get; set; }  
        /// <summary>
        /// 分支任务关系
        /// </summary>
        public virtual List<Flow_Branch_Task> Flow_Branch_Tasks { get; set; } = new List<Flow_Branch_Task>();        
    }

}