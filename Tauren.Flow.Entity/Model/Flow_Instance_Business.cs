﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tauren.Flow.Entity.Model
{
    [Dapper.Contrib.Extensions.Table("flow_instance_business")]
    /// <summary>
    /// 流程实例对应表
    /// </summary>
    public class Flow_Instance_Business : EntityBase
    {
        [Dapper.Contrib.Extensions.Key]
        public virtual long AutoId { get; set; }
        public virtual long InstanceId { get; set; }

        /// <summary>
        /// 流程ID
        /// </summary>
        public virtual long FlowId { get; set; }

        /// <summary>
        /// 流程版本号
        /// </summary>
        public virtual long FlowVersionId { get; set; }

        /// <summary>
        /// 实例分组ID
        /// </summary>
        public virtual long GroupId { get; set; }

        /// <summary>
        /// 表名
        /// </summary>
        public virtual string TableCode { get; set; }

        /// <summary>
        /// 主表名
        /// </summary>
        public virtual string MainTable { get; set; }

        /// <summary>
        /// 主键
        /// </summary>
        public virtual string PrimaryKey { get; set; }
        /// <summary>
        /// 主键值
        /// </summary>
        public virtual string PrimaryValue { get; set; }

        /// <summary>
        /// 外键
        /// </summary>

        public virtual string ForeignKey { get; set; }

        /// <summary>
        /// 外简值
        /// </summary>
        public virtual string ForeignValue { get; set; }
    }
}