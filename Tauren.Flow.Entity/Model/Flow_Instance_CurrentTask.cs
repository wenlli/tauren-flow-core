﻿using System;
using System.Collections.Generic;
using System.Text;

/// <summary>
/// 流程实例状态
/// </summary>
namespace Tauren.Flow.Entity.Model
{
    [Dapper.Contrib.Extensions.Table("flow_instance_currenttask")]
    public class Flow_Instance_CurrentTask
    {

        /// <summary>
        /// 主键ID
        /// </summary>
        [Dapper.Contrib.Extensions.Key]
        public virtual long AutoId { get; set; }
        /// <summary>
        /// 流程实例ID
        /// </summary>
        public virtual long InstanceId { get; set; }

        /// <summary>
        /// 流程分组ID
        /// </summary>
        public virtual long GroupId { get; set; }
        /// <summary>
        /// 当前流程节点
        /// </summary>
        public virtual string CurrentStep { get; set; }

        /// <summary>
        /// 当前任务ID
        /// </summary>
        public virtual long CurrentTaskId { get; set; }

        /// <summary>
        /// 处理到此处的类型
        /// </summary>
        public virtual int ProcessType { get; set; }

        /// <summary>
        /// 流程分组ID
        /// </summary>
        public virtual string PrveStepId { get; set; }

        /// <summary>
        /// 流程分组ID
        /// </summary>
        public virtual long PrveTaskId { get; set; }


    }
}