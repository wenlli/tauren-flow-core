﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tauren.Flow.Entity.Model
{
    [Dapper.Contrib.Extensions.Table("flow_lines")]
    public class Flow_Lines:EntityBase
    {
        /// <summary>
        /// 连线ID
        /// </summary>
        public virtual string LinkId { get; set; }


        /// <summary>
        /// 版本ID
        /// </summary>
        public virtual long? VersionId { get; set; }

        /// <summary>
        /// 连线中文名
        /// </summary>
        public virtual string CNName { get; set; }

        /// <summary>
        /// 连线英文名
        /// </summary>
        public virtual string ENName { get; set; }


        /// <summary>
        /// 开始步骤
        /// </summary>
        public virtual string StartId { get; set; }


        /// <summary>
        /// 走向节点
        /// </summary>
        public virtual string EndId { get; set; }

        /// <summary>
        /// 开始坐标
        /// </summary>
        public virtual string StartAt { get; set; }

        /// <summary>
        /// 走向坐标
        /// </summary>
        public virtual string EndAt { get; set; }


        /// <summary>
        /// 业务判断条件
        /// </summary>
        public virtual string BusinessCondition { get; set; }

        /// <summary>
        /// 员工判断条件
        /// </summary>
        public virtual string PersonCondtion { get; set; }

        /// <summary>
        /// 组织判断条件
        /// </summary>
        public virtual string OrgCondtion { get; set; }

        /// <summary>
        /// 职位判断条件
        /// </summary>
        public virtual string PostionCondtion { get; set; }
        public virtual double Sequence { get; set; }
        /// <summary>
        /// 并行分支标记
        /// </summary>
        public virtual string BranchTag { get; set; }

    }
}
