﻿namespace Tauren.Flow.Entity.Model
{
    [Dapper.Contrib.Extensions.Table("flow_quick_field")]
    public class Flow_Quick_Field : Flow_Display_Field
    {
    }

    [Dapper.Contrib.Extensions.Table("flow_instance_field")]
    public class Flow_Instance_Field : Flow_Display_Field
    {
    }
    public class Flow_Display_Field : EntityBase
    {

        public virtual long AutoId { get; set; }

        /// <summary>
        /// 流程版本
        /// </summary>
        public virtual long? FlowVersionId { get; set; }

        /// <summary>
        /// 流程ID
        /// </summary>
        public virtual long? FlowId { get; set; }
        /// <summary>
        /// 字段编码
        /// </summary>
        public virtual string FieldCode { get; set; } = "";

        /// <summary>
        /// 字段标识
        /// </summary>
        public virtual string FieldKey { get; set; } = "";
        /// <summary>
        /// 表名
        /// </summary>
        public virtual string Table { get; set; } = "";
        /// <summary>
        /// 控件类型
        /// </summary>
        public virtual string ControlsType { get; set; } = "";
        /// <summary>
        /// 控件来源
        /// </summary>
        public virtual string ControlSouces { get; set; } = "";
    }
}
