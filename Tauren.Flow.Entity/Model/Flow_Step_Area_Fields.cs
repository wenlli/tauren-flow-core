﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tauren.Flow.Entity.Model
{
    [Dapper.Contrib.Extensions.Table("flow_step_area_fields")]
    public class Flow_Step_Area_Fields:EntityBase
    {
        /// <summary>
        /// 字段ID
        /// </summary>
        public virtual long? AutoId { get; set; }

        /// <summary>
        /// 区域ID
        /// </summary>
        public virtual long? AreaId { get; set; }

        /// <summary>
        /// 字段编码
        /// </summary>
        public virtual string FieldCode { get; set; }

        /// <summary>
        /// 字段key
        /// </summary>
        public virtual string FieldKey { get; set; }
        /// <summary>
        /// 字段必填
        /// </summary>
        public virtual bool Required { get; set; }

        /// <summary>
        /// 字段显示
        /// </summary>
        public virtual bool Show { get; set; } = true;

        /// <summary>
        /// 字段编辑
        /// </summary>
        public virtual bool Editor { get; set; } = true;
    }
}
