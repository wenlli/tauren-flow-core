﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tauren.Flow.Entity.Model
{
    [Dapper.Contrib.Extensions.Table("flow_step_field_area")]
    public class Flow_Step_Field_Area:EntityBase
    {
        /// <summary>
        /// 区域ID
        /// </summary>
        public virtual long? AutoId { get; set; }
        /// <summary>
        /// 步骤ID
        /// </summary>
        public virtual string StepId { get; set; }

        /// <summary>
        /// 流程版本ID
        /// </summary>
        public virtual long? VersionId { get; set; }

        /// <summary>
        /// 区域中文名
        /// </summary>
        public virtual string CNName { get; set; }

        /// <summary>
        /// 区域英文名
        /// </summary>
        public virtual string ENName { get; set; }

        /// <summary>
        /// 区域编码
        /// </summary>
        public virtual string AreaCode { get; set; }

        /// <summary>
        /// 表名
        /// </summary>
        public virtual string TableCode { get; set; }

        /// <summary>
        /// 主表数据
        /// </summary>
        public virtual string MainTable { get; set; }

        /// <summary>
        /// 显示
        /// </summary>
        public virtual bool Show { get; set; }
        /// <summary>
        /// 编辑性
        /// </summary>
        public virtual bool Editor { get; set; }
    }
}
