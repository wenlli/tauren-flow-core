﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tauren.Flow.Entity.Model
{
    [Dapper.Contrib.Extensions.Table("flow_step_info")]
    public class Flow_Step_Info:EntityBase
    {
        
        /// <summary>
        /// 步骤ID
        /// </summary>
        public virtual string StepId { get; set; }
        /// <summary>
        /// 流程版本ID
        /// </summary>
        public virtual long? VersionId { get; set; }

        /// <summary>
        /// 节点类型
        /// </summary>
        public virtual string Prop { get; set; }

        /// <summary>
        /// 节点中文名
        /// </summary>
        public virtual string CNName { get; set; }

        /// <summary>
        /// 节点英文名
        /// </summary>
        public virtual string ENName { get; set; }

        /// <summary>
        /// 节点宽度
        /// </summary>
        public virtual long Width { get; set; }

        /// <summary>
        /// 节点高度
        /// </summary>
        public virtual long Height { get; set; }


        /// <summary>
        /// 节点坐标
        /// </summary>
        public virtual string Coordinate { get; set; }

        /// <summary>
        /// 节点显示快捷字段
        /// </summary>
        public virtual bool ShowQuickField { get; set; } = true;

        /// <summary>
        /// 显示历史
        /// </summary>
        public virtual bool ShowHistory { get; set; } = true;

        /// <summary>
        /// 显示意见框
        /// </summary>
        public virtual bool ShowOpinion { get; set; } = true;


        /// <summary>
        /// 意见是否必填
        /// </summary>
        public virtual bool Required { get; set; }


        /// <summary>
        /// 默认审批意见
        /// </summary>
        public virtual string Opinion { get; set; }

        /// <summary>
        /// 默认退回意见
        /// </summary>
        public virtual string BackOpinion { get; set; }

        /// <summary>
        /// 默认拒绝意见
        /// </summary>
        public virtual string EndOpinion { get; set; }

        /// <summary>
        /// 当前节点处理人是否接收提醒
        /// </summary>
        public virtual bool HandlerMessage { get; set; }

        /// <summary>
        /// 当前节点抄送 人是否接收提醒
        /// </summary>
        public virtual bool CCMessage { get; set; }

        /// <summary>
        /// 备注信息
        /// </summary>
        public virtual string Note { get; set; }

        /// <summary>
        /// 分组信息
        /// </summary>
        public virtual string Groups { get; set; }
        /// <summary>
        /// 开始分支
        /// </summary>
        public virtual string StartGroup { get; set; }

        /// <summary>
        /// 分支审批策略
        /// </summary>
        public virtual string BranchStrategy { get; set; }
        /// <summary>
        /// 审批比列
        /// </summary>
        public virtual string ApprovalScale { get; set; }

        /// <summary>
        /// 分支终止策略
        /// </summary>
        public virtual string EndStrategy { get; set; }
        /// <summary>
        /// 审批比列
        /// </summary>
        public virtual string EndScale { get; set; }

        /// <summary>
        /// 分支退回策略
        /// </summary>
        public virtual string BackStrategy { get; set; }
        /// <summary>
        /// 审批比列
        /// </summary>
        public virtual string BackScale { get; set; }

        /// <summary>
        /// 并行分支标记
        /// </summary>
        public virtual string BranchTag { get; set; }
        /// <summary>
        /// 并行分支
        /// </summary>
        public virtual string Line { get; set; }


    }
}
