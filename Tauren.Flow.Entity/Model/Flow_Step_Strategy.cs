﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tauren.Flow.Entity.Model
{
    [Dapper.Contrib.Extensions.Table("flow_step_strategy")]
    public class Flow_Step_Strategy : EntityBase
    {
        public virtual long AutoId { get; set; }

        /// <summary>
        /// 节点
        /// </summary>
        public virtual string StepId { get; set; }

        /// <summary>
        /// 自动选择分支
        /// </summary>
        public virtual bool IsAutoSelectStep { get; set; }

        /// <summary>
        /// 多人审批策略
        /// </summary>
        public virtual int ApprovalStrategy { get; set; }

        /// <summary>
        /// 跳过策略
        /// </summary>
        public virtual bool SkipStrategy { get; set; }


        /// <summary>
        /// 策略类型
        /// </summary>
        public virtual int StrategyType { get; set; }

        /// <summary>
        /// 审批人兼职策略
        /// </summary>
        public virtual int ApproveParttimePostion { get; set; }

        /// <summary>
        /// 发送人兼职策略
        /// </summary>
        public virtual int SendParttimePostion { get; set; }

        /// <summary>
        /// 处理者类型
        /// </summary>
        public virtual int ApproverType { get; set; }

        /// <summary>
        /// 表单字段
        /// </summary>
        public virtual string FormField { get; set; }

        /// <summary>
        /// 表单字段处理者类型
        /// </summary>
        public virtual int FormFieldApproveType { get; set; }

        /// <summary>
        /// 审批人范围
        /// </summary>
        public virtual string AppoverRange { get; set; }

        /// <summary>
        /// 默认处理人
        /// </summary>
        public virtual string DefaultApprover { get; set; }

        /// <summary>
        /// 部门负责人层级
        /// </summary>
        public virtual int OrgLeve { get; set; }

        /// <summary>
        /// 抄送策略类型
        /// </summary>
        public virtual int CCStrategyType { get; set; }

        /// <summary>
        /// 抄送人兼职设置
        /// </summary>
        public virtual int CCApproveParttimePostion { get; set; }

        /// <summary>
        /// 抄送人兼职2
        /// </summary>
        public virtual int CCSendParttimePostion { get; set; }

        /// <summary>
        /// 抄送人类型
        /// </summary>
        public virtual int CCType { get; set; }

        /// <summary>
        /// 抄送表单字段
        /// </summary>
        public virtual string CCFormField { get; set; }

        /// <summary>
        /// 抄送表单字段处理者类型
        /// </summary>
        public virtual int CCFormFieldApproveType { get; set; }


        /// <summary>
        /// 默认抄送人
        /// </summary>
        public virtual string CCUser { get; set; }

        /// <summary>
        /// 抄送组织层级
        /// </summary>
        public virtual int CCOrgLeve { get; set; }
        /// <summary>
        /// 版本ID
        /// </summary>
        public virtual long? VersionId { get; set; }

        /// <summary>
        /// 审批比列
        /// </summary>
        public virtual string NodeApprovalScale { get; set; }
         

        /// <summary>
        /// 分支退回策略
        /// </summary>
        public virtual string NodeBackStrategy { get; set; }
        /// <summary>
        /// 审批比列
        /// </summary>
        public virtual string NodeBackScale { get; set; }

    }
}
