﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tauren.Flow.Entity.Model
{
    [Dapper.Contrib.Extensions.Table("flow_task")]
    public class Flow_Task : EntityBase
    {
        [Dapper.Contrib.Extensions.ExplicitKey]
        public virtual long TaskId { get; set; }
        public virtual string Title { get; set; }
        public virtual long InstanceId { get; set; }
        public virtual long GroupId { get; set; }
        public virtual string StepId { get; set; }

        public virtual long PrevTaskId { get; set; }
        public virtual string PrevStepId { get; set; }

        public virtual int Reviewer { get; set; }

        public virtual int Sender { get; set; }
        public virtual DateTime ReceivingTime { get; set; }
        public virtual DateTime? ProcessTime { get; set; }

        /// <summary>
        ///任务状态
        /// </summary>
        public virtual int Status { get; set; }

        /// <summary>
        /// 任务类型
        /// </summary>
        public virtual int Type { get; set; }

        /// <summary>
        /// 意见
        /// </summary>
        public virtual string Comment { get; set; }

        public virtual ulong Sort { get; set; }

        public virtual int Mandator { get; set; }

        public virtual bool Read { get; set; }

        public virtual int Urgency { get; set; }
        public virtual string ParallelGroupid { get; set; } = "";
    }
}
