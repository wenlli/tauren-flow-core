﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tauren.Flow.Entity.Model
{
    /// <summary>
    /// 资源主键表
    /// </summary>
    [Dapper.Contrib.Extensions.Table("ModuleResouces")]
    public class ModuleResouces : EntityBase
    {
        /// <summary>
        /// 主键ID
        /// </summary>
        [Dapper.Contrib.Extensions.Key]
        public virtual long AutoId { get; set; }
        /// <summary>
        /// 资源ID
        /// </summary> 
        public virtual long ResourceId { get; set; }
        /// <summary>
        /// 资源编号
        /// </summary>
        public virtual string ResCode { get; set; }
        /// <summary>
        /// 模块编号
        /// </summary>
        public virtual string MenuCode { get; set; }

        /// <summary>
        /// 启用？
        /// </summary>
        public virtual bool Enable { get; set; }


        /// <summary>
        /// 资源类型-提示/错误/警告
        /// </summary>
        public virtual string ResoucesType { get; set; }

        /// <summary>
        /// 删除
        /// </summary>
        public virtual bool IsDelete { get; set; }
    }
}
