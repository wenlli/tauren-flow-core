﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tauren.Flow.Entity.Model
{
    /// <summary>
    /// 资源内容表
    /// </summary>
    [Dapper.Contrib.Extensions.Table("ModuleResouces_res")]
    public class ModuleResoucesRes : EntityBase
    {
        /// <summary>
        /// 主键ID
        /// </summary>
        [Dapper.Contrib.Extensions.Key]
        public virtual long AutoId { get; set; }
        /// <summary>
        /// 资源主表ID
        /// </summary> 
        public virtual long ResourceId { get; set; }

        /// <summary>
        /// 国际语言
        /// </summary>
        public virtual string Language { get; set; }

        /// <summary>
        /// 资源内容
        /// </summary>
        public virtual string Value { get; set; }
    }
}
