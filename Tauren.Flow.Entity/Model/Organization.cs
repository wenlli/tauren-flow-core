﻿using Dapper.Contrib.Extensions;
#nullable disable

namespace Tauren.Flow.Entity.Model
{
    [Table("organization")]
    public class Organization : EntityBase
    {
        [Key]
        public virtual int OrgId { get; set; }
        public virtual string OrgCode { get; set; }
        public virtual string CNName { get; set; }
        public virtual string ENName { get; set; }
        public virtual int? ParentId { get; set; }
        public virtual int? UnitId { get; set; }
        public virtual string Phone { get; set; }
        public virtual string Contact { get; set; }
        public virtual double? Sequence { get; set; }
        public virtual bool Enable { get; set; }
        public virtual bool HaveDelete { get; set; }
    }
}
