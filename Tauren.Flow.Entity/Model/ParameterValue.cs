﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic; 
using System.Text;

namespace Tauren.Flow.Entity.Model
{
    [Table("parametervalue")]
    public class ParameterValue : EntityBase
    {
        [Key]
        public virtual long AutoId { get; set; }
        public virtual string CNName { get; set; }
        public virtual string ENName { get; set; }
        public virtual string Code { get; set; }
        public virtual string MainCode { get; set; }
        public virtual bool? Enable { get; set; }
        public virtual decimal? Sequence { get; set; }
    }
}
