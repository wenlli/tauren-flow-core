﻿using Dapper.Contrib.Extensions;
#nullable disable

namespace Tauren.Flow.Entity.Model
{
    [Table("parttimeinfo")]
    public class Parttimeinfo:EntityBase
    {
        [Key]
        public virtual int AutoId { get; set; }
        public virtual int EmpId { get; set; }
        public virtual int PositionId { get; set; }
        public virtual int OrgId { get; set; }
        public virtual ulong? Enable { get; set; }
    }
}
