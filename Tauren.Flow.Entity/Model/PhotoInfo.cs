﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Tauren.Flow.Entity.Model
{
    [Table("photoinfo")]
    public class PhotoInfo : EntityBase
    {
        /// <summary>
        /// 主键ID
        /// </summary>
        [Key]
        public virtual long AutoId { get; set; }
        /// <summary>
        /// 员工
        /// </summary>
        public virtual int EmpId { get; set; }
        /// <summary>
        /// 文件ID
        /// </summary>
        public virtual long FileId { get; set; }
    }
    public class ExPhotoInfo : PhotoInfo
    {
        /// <summary>
        /// 文件地址
        /// </summary>
        public string FileUri { get; set; }
    }
}
