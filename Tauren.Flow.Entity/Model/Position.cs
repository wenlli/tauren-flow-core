﻿#nullable disable
using Dapper.Contrib.Extensions;
namespace Tauren.Flow.Entity.Model
{
    [Table("position")]
    public class Position : EntityBase
    {
        [Key]
        public virtual int PositionId { get; set; }
        public virtual int ParentId { get; set; }
        public virtual string PositionCode { get; set; }
        public virtual string CNName { get; set; }
        public virtual string ENName { get; set; }
        public virtual int? OrgId { get; set; }
        public virtual string OrgType { get; set; }
        public virtual bool Enable { get; set; }
        public virtual double? Sequence { get; set; }
        public virtual bool HaveDelete { get; set; }

        public virtual int PositionType { get; set; }
    }
}
