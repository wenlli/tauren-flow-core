﻿using Dapper.Contrib.Extensions;
#nullable disable

namespace Tauren.Flow.Entity.Model
{
    [Table("roleinfo")]
    public class Roleinfo:EntityBase
    {
        [Key]
        public virtual long AutoId { get; set; } 
        public virtual string Code { get; set; }
        public virtual bool  Enable { get; set; }
        public virtual double? Sequence { get; set; }
        public virtual bool HaveSystemRole { get; set; }
        public virtual bool HaveDelete { get; set; }
    }
}
