﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tauren.Flow.Entity.Model
{
    [Dapper.Contrib.Extensions.Table("SystemLog")]
    public class SystemLog : EntityBase
    {
        /// <summary>
        /// 主键ID
        /// </summary>
        [Dapper.Contrib.Extensions.Key]
        public virtual long AutoId { get; set; }

        /// <summary>
        /// 账号ID
        /// </summary>
        public virtual long UserId { get; set; }

        /// <summary>
        /// 账号
        /// </summary>
        public virtual string Account { get; set; }

        /// <summary>
        /// 租户名称
        /// </summary>
        public virtual string TCnname { get; set; }

        /// <summary>
        /// 租户联系方式
        /// </summary>
        public virtual string Phone { get; set; }

        /// <summary>
        /// 租户编码
        /// </summary>
        public virtual string Code { get; set; }

        /// <summary>
        /// 日志类型
        /// </summary>
        public virtual int LogType { get; set; }

        /// <summary>
        /// 请求地址
        /// </summary>
        public virtual string IPAddress { get; set; }

        /// <summary>
        /// 接口服务器ip地址
        /// </summary>
        public virtual string ServerAddress { get; set; }

        /// <summary>
        /// 语言环境
        /// </summary>
        public virtual string Language { get; set; }

        /// <summary>
        /// 员工基本信息ID
        /// </summary>
        public virtual long EmpId { get; set; }

        /// <summary>
        /// 员工中文名
        /// </summary>
        public virtual string CNName { get; set; }

        /// <summary>
        /// 员工英文名
        /// </summary>
        public virtual string ENName { get; set; }
        /// <summary>
        /// 员工编码
        /// </summary>
        public virtual string EmpCode { get; set; }

        /// <summary>
        /// 组织ID
        /// </summary>
        public virtual int OrgId { get; set; }

        /// <summary>
        /// 职位ID
        /// </summary>
        public virtual int PositionId { get; set; }
        /// <summary>
        /// 组织名称
        /// </summary>
        public virtual string OrgName { get; set; }

        /// <summary>
        /// 职位名称
        /// </summary>
        public virtual string PositionName { get; set; }

        /// <summary>
        /// 执行时间
        /// </summary>
        public virtual DateTime ExecuteDate { get; set; }

        /// <summary>
        /// 执行类
        /// </summary>
        public virtual string ExecuteClass { get; set; }

        /// <summary>
        /// 执行方法
        /// </summary>
        public virtual string ExecuteMethod { get; set; }

        /// <summary>
        /// 执行参数
        /// </summary>
        public virtual string ExecuteParameter { get; set; }

        /// <summary>
        /// 执行结果
        /// </summary>
        public virtual string ExecuteResult { get; set; }

        /// <summary>
        /// 完成时间
        /// </summary>
        public virtual DateTime ExecuteCompletiontime { get; set; }

        /// <summary>
        /// 接口地址
        /// </summary>
        public virtual string InterfacePath { get; set; }

        /// <summary>
        /// 客户端地址
        /// </summary>
        public virtual string ClientPath { get; set; }

        /// <summary>
        /// 客户端类型
        /// </summary>
        public virtual string UserAgent { get; set; }

        /// <summary>
        /// 请求参数
        /// </summary>
        public virtual string RequestParamtetr { get; set; }

    }
}
