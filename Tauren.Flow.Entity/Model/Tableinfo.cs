﻿using System;
using System.Collections.Generic;
using Dapper.Contrib.Extensions;
#nullable disable

namespace Tauren.Flow.Entity.Model
{
    [Table("tableinfo")]
    public class TableInfo:EntityBase
    {
        [Key]
        public virtual long AutoId { get; set; }
        public virtual string CNName { get; set; }
        public virtual string ENName { get; set; }
        public virtual string TableCode { get; set; }
        public virtual int? GroupType { get; set; }
        public virtual int? ViewType { get; set; }
        public virtual bool? IsPrimary { get; set; }
        public virtual string PkTable { get; set; }
        public virtual bool? Enable { get; set; }
        public virtual bool? HaveDelete { get; set; }
        public virtual bool? IsSystem { get; set; }         
        public virtual bool? IsFlow { get; set; }
        public virtual bool? Records { get; set; }
        public virtual decimal? Sequence { get; set; }
    }
}
