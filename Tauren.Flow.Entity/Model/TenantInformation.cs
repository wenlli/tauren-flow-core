﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tauren.Flow.Entity.Model
{
    [Dapper.Contrib.Extensions.Table("TenantInformation")]
    public class TenantInformation
    {
        [Dapper.Contrib.Extensions.Key]
        public virtual int AutoId { get; set; }

        /// <summary>
        /// 租户名
        /// </summary>
        public virtual string CNName { get; set; }

        /// <summary>
        /// 租户的联系方式
        /// </summary>
        public virtual string Phone { get; set; }

        /// <summary>
        /// 租户编码
        /// </summary>
        public virtual string Code { get; set; }

        /// <summary>
        /// 基础数据所在库
        /// </summary>
        public virtual string dbConnection { get; set; }

        /// <summary>
        /// 流程所在数据
        /// </summary>
        public virtual string wf_dbConnection { get; set; }

        /// <summary>
        /// 流程表单所在数据库
        /// </summary>
        public virtual string wf_form_Dbconnection { get; set; }

        /// <summary>
        /// 文件信息所在数据库
        /// </summary>
        public virtual string files_Dbconnection { get; set; }

        /// <summary>
        /// 文件信息所在数据库
        /// </summary>
        public virtual string Res_Dbconnection { get; set; }

        /// <summary>
        /// 是否启用
        /// </summary>
        public virtual bool Enable { get; set; }

        /// <summary>
        /// 过期日期
        /// </summary>
        public virtual DateTime? ExPirationDate { get; set; }

        /// <summary>
        /// 创建日期
        /// </summary>
        public virtual DateTime? CreatDate { get; set; }
    }
}
