﻿#nullable disable

namespace Tauren.Flow.Entity.Model
{
    [Dapper.Contrib.Extensions.Table("Unitinfo")]
   public class Unitinfo:EntityBase
    {
        /// <summary>
        /// 单位ID
        /// </summary>
        [Dapper.Contrib.Extensions.Key]
        public virtual int UnitId { get; set; }
        /// <summary>
        /// 单位编码
        /// </summary>
        public virtual string UnitCode { get; set; }
        /// <summary>
        /// 中文名称
        /// </summary>
        public virtual string CNName { get; set; }
        /// <summary>
        /// 英文名称
        /// </summary>
        public virtual string ENName { get; set; }
        /// <summary>
        /// 上级单位id
        /// </summary>
        public virtual int? ParentId { get; set; }
        /// <summary>
        /// 单位地址
        /// </summary>
        public virtual string UnitAddress { get; set; }
        /// <summary>
        /// 法人
        /// </summary>
        public virtual string Legal { get; set; } 
        /// <summary>
        /// 移动电话
        /// </summary>
        public virtual string Phone { get; set; }
        /// <summary>
        /// 连线方式
        /// </summary>
        public virtual string Contact { get; set; }
        /// <summary>
        /// 禁用
        /// </summary>
        public virtual bool Enable { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public virtual double? Sequence { get; set; } 

        /// <summary>
        /// 是否已删除
        /// </summary>
        public virtual bool HaveDelete { get; set; }
    }
}
