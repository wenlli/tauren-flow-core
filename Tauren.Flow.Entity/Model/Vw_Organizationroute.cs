﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tauren.Flow.Entity.Model
{
    public class Vw_Organizationroute
    {
        /// <summary>
        /// 组织ID
        /// </summary>
        public virtual int Id { get; set; }
        /// <summary>
        /// 组织层级
        /// </summary>
        public virtual int Level { get; set; }

        /// <summary>
        /// 组织中文名
        /// </summary>
        public virtual string CNName { get; set; }
        /// <summary>
        /// 组织英文名
        /// </summary>
        public virtual string ENName { get; set; }
        /// <summary>
        /// 父级ID
        /// </summary>
        public virtual int PId { get; set; }

        /// <summary>
        /// 父级的父级ID
        /// </summary>
        public virtual int PuId { get; set; }

        /// <summary>
        /// 组织类型
        /// </summary>
        public virtual string OrgType { get; set; }
        /// <summary>
        /// 公司ID
        /// </summary>
        public virtual int UnitId { get; set; }

        /// <summary>
        /// 当前组织类型 
        /// </summary>
        public virtual string CurrentType { get; set; }
        public virtual int EmpId { get; set; }
    }
}
