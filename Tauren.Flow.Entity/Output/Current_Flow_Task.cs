﻿using System;
using System.Collections.Generic;
using System.Text;
using Tauren.Flow.Entity.Model;

namespace Tauren.Flow.Entity.Output
{
    public class Current_Flow_Task : Flow_Task
    {
        public virtual string Branch { get; set; }
        public virtual string BranchLine { get; set; }
    }
}
