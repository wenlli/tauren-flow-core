﻿using System;
using System.Collections.Generic;
using System.Text;
using model = Tauren.Flow.Entity.Model;
namespace Tauren.Flow.Entity.Output
{
    public class Field_Form_Version_Put
    {
        /// <summary>
        /// 主键
        /// </summary>
        public virtual long AutoId { get; set; }

        /// <summary>
        /// 表单ID
        /// </summary>
        public virtual long FormId { get; set; }

        /// <summary>
        /// 版本号
        /// </summary>
        public virtual long VersionNo { get; set; }

        /// <summary>
        /// 字段区域
        /// </summary>
        public virtual List<Field_Form_Area_Put> Areas { get; set; } = new List<Field_Form_Area_Put>();
        /// <summary>
        /// 可使用申请人ID
        /// </summary>
        public virtual bool UseApplicantEmpId { get; set; }
        /// <summary>
        /// 表单中文名
        /// </summary>
        public virtual string FormCNName { get; set; }
        /// <summary>
        /// 表单英文名文名
        /// </summary>
        public virtual string FormENName { get; set; }
    }

    public class Field_Form_Version_InPut : Field_Form_Version_Put
    {

    }

    /// <summary>
    /// 字段区域
    /// </summary>
    public class Field_Form_Area_Put
    {
        /// <summary>
        /// 主键
        /// </summary>
        public virtual long AreaId { get; set; }

        /// <summary>
        /// 表单中文名
        /// </summary>
        public virtual string AreaCNName { get; set; }

        /// <summary>
        /// 表单英文名
        /// </summary>
        public virtual string AreaENName { get; set; }


        /// <summary>
        /// 区域编码
        /// </summary>
        public virtual string AreaCode { get; set; }

        /// <summary>
        /// 区域对应表
        /// </summary>
        public virtual string TableCode { get; set; }
        /// <summary>
        /// 区域对应表
        /// </summary>
        public virtual string MainTable { get; set; } = "";

        /// <summary>
        /// 主表名称
        /// </summary>
        public virtual string MainTableName { get; set; } = "";

        /// <summary>
        /// 主表名称
        /// </summary>
        public virtual string MainTableENName { get; set; } = "";
        /// <summary>
        /// 排序
        /// </summary>
        public virtual double AreaSeq { get; set; }
        /// <summary>
        /// 字段
        /// </summary>
        public virtual List<Field_Form_Area_Fields_Put> Fields { get; set; } = new List<Field_Form_Area_Fields_Put>();

        /// <summary>
        /// 记录中的表是否可使用
        /// </summary>
        public virtual bool Usable { get; set; }

        /// <summary>
        /// 显示
        /// </summary>
        public virtual bool Show { get; set; }
        /// <summary>
        /// 编辑性
        /// </summary>
        public virtual bool Editor { get; set; }
    }
    /// <summary>
    /// 字段
    /// </summary>
    public class Field_Form_Area_Fields_Put
    {
        /// <summary>
        /// 主键
        /// </summary>
        public virtual long AutoId { get; set; }

        /// <summary>
        /// 字段区域ID
        /// </summary>
        public virtual long AreaId { get; set; }

        /// <summary>
        /// 字段编码
        /// </summary>
        public virtual string FieldCode { get; set; }

        /// <summary>
        /// 字段标识
        /// </summary>
        public virtual string FieldKey { get; set; }
        /// <summary>
        /// 中文名
        /// </summary>
        public virtual string CNName { get; set; }

        /// <summary>
        /// 字段英文名
        /// </summary>
        public virtual string ENName { get; set; }

        /// <summary>
        /// 必填
        /// </summary>
        public virtual bool Required { get; set; }
        /// <summary>
        /// 记录中的表是否可使用
        /// </summary>
        public virtual bool Usable { get; set; } = true;
        /// <summary>
        /// 是否显示
        /// </summary>
        public virtual bool Show { get; set; }

        /// <summary>
        /// 是否可编辑
        /// </summary>
        public virtual bool Editor { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public virtual double Sequence { get; set; }
        public virtual string ControlStype { get; set; }
        /// <summary>
        /// 仓用参数来源
        /// </summary>
        public virtual string ControlSouces { get; set; } = "";
        /// <summary>
        /// 是否选中
        /// </summary>
        public virtual bool Selected { get; set; }

    }

    public class Field_Form_VersionExt
    {

        /// <summary>
        /// 表达那版本ID
        /// </summary>
        public virtual long VersionId { get; set; }


        /// <summary>
        /// 表单ID
        /// </summary>
        public virtual long FormId { get; set; }

        /// <summary>
        /// 主键
        /// </summary>
        public virtual long AreaId { get; set; }

        /// <summary>
        /// 表单中文名
        /// </summary>
        public virtual string AreaCNName { get; set; } = "";

        /// <summary>
        /// 表单英文名
        /// </summary>
        public virtual string AreaENName { get; set; } = "";

        /// <summary>
        /// 记录中的表是否可使用
        /// </summary>
        public virtual bool AreaUsable { get; set; } = true;

        /// <summary>
        /// 区域编码
        /// </summary>
        public virtual string AreaCode { get; set; } = "";

        /// <summary>
        /// 区域对应表
        /// </summary>
        public virtual string TableCode { get; set; } = "";
        /// <summary>
        /// 区域对应表
        /// </summary>
        public virtual string MainTable { get; set; } = "";

        /// <summary>
        /// 主表名称
        /// </summary>
        public virtual string MainTableName { get; set; } = "";

        /// <summary>
        /// 主表名称
        /// </summary>
        public virtual string MainTableENName { get; set; } = "";

        /// <summary>
        /// 排序
        /// </summary>
        public virtual double AreaSeq { get; set; }

        /// <summary>
        /// 是否显示
        /// </summary>
        public virtual bool AreaShow { get; set; }

        /// <summary>
        /// 是否可编辑
        /// </summary>
        public virtual bool AreaEditor { get; set; }

        /// <summary>
        /// 字段ID
        /// </summary>
        public virtual long FieldId { get; set; }

        /// <summary>
        /// 字段编码
        /// </summary>
        public virtual string FieldCode { get; set; } = "";

        /// <summary>
        /// 字段标识
        /// </summary>
        public virtual string FieldKey { get; set; } = "";
        /// <summary>
        /// 中文名
        /// </summary>
        public virtual string CNName { get; set; } = "";

        /// <summary>
        /// 字段英文名
        /// </summary>
        public virtual string ENName { get; set; } = "";

        /// <summary>
        /// 必填
        /// </summary>
        public virtual bool Required { get; set; }

        /// <summary>
        /// 是否显示
        /// </summary>
        public virtual bool Show { get; set; }

        /// <summary>
        /// 是否可编辑
        /// </summary>
        public virtual bool Editor { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public virtual double Sequence { get; set; }
        /// <summary>
        /// 记录中的表是否可使用
        /// </summary>
        public virtual bool FieldUsable { get; set; } = true;
        /// <summary>
        /// 步骤节点
        /// </summary>
        public virtual string StepId { get; set; }
        /// <summary>
        /// 控件类型
        /// </summary>
        public virtual string ControlStype { get; set; }
        public virtual bool Selected { get; set; }
        public virtual bool Records { get; set; }
        /// <summary>
        /// 仓用参数来源
        /// </summary>
        public virtual string ControlSouces { get; set; } = "";
    }

    public class TableFields
    {
        public string TableCode { get; set; } = "";
        public virtual bool TableEnable { get; set; }
        public virtual bool TableHaveDelete { get; set; }
        public virtual string TableName { get; set; }
        public virtual string TableENName { get; set; }
        public string FieldCode { get; set; } = "";
        public virtual bool FieldEnable { get; set; }
        public virtual bool FieldHaveDelete { get; set; }
        public virtual bool Records { get; set; }
        public virtual double sequence { get; set; }
        public virtual double seq { get; set; }
        public string FieldName { get; set; } = "";
        public string FieldENName { get; set; } = "";
    }
    public class FormModel
    {
        /// <summary>
        /// 主键
        /// </summary>
        public virtual long AutoId { get; set; }
        /// <summary>
        /// 表单中文名
        /// </summary>
        public virtual string CNName { get; set; } = "";

        /// <summary>
        /// 表单英文名
        /// </summary>
        public virtual string ENName { get; set; } = "";

        /// <summary>
        /// 当前表单版本
        /// </summary>
        public virtual long CurrentVersion { get; set; }

        /// <summary>
        /// 当前表单版本ID
        /// </summary>
        public virtual long CurrentVersionId { get; set; }

    }
    public class Form_InPut : FormModel
    {
    }
    public class Form_OutPut : FormModel
    {

    }


}
