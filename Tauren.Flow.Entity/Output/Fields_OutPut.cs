﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tauren.Flow.Entity.Output
{
    public class Fields_OutPut
    {
    }
    public class WriteField
    {
        public virtual string FieldCode { get; set; } = "";
        public virtual string TableCode { get; set; } = "";
    }
    public class WriteData
    {
        public virtual string TableCode { get; set; } = "";
        public virtual bool IsAdd { get; set; }
        public virtual List<WriteField> WriteFields { get; set; }
        public virtual long BusinessId { get; set; }
        public virtual string MainTableCode { get; set; } = "";
        public virtual long InstanceId { get; set; }
    }
}
