﻿using System;
using System.Collections.Generic;
using System.Text;
using Tauren.Flow.Entity.Model;

namespace Tauren.Flow.Entity.Output
{
    public class FieldConfig
    {
        /// <summary>
        /// 可使用申请人ID
        /// </summary>
        public virtual bool UseApplicantEmpId { get; set; }
        /// <summary>
        /// 表单中文名
        /// </summary>
        public virtual string FormCNName { get; set; }
        /// <summary>
        /// 表单英文名文名
        /// </summary>
        public virtual string FormENName { get; set; }

        /// <summary>
        /// 表达那版本ID
        /// </summary>
        public virtual long VersionId { get; set; }

        /// <summary>
        /// 版本号
        /// </summary>
        public virtual long VersionNo { get; set; }

        /// <summary>
        /// 表单ID
        /// </summary>
        public virtual long FormId { get; set; }

        /// <summary>
        /// 主键
        /// </summary>
        public virtual long AreaId { get; set; }


        /// <summary>
        /// 表单中文名
        /// </summary>
        public virtual string AreaCNName { get; set; } = "";

        /// <summary>
        /// 表单英文名
        /// </summary>
        public virtual string AreaENName { get; set; } = "";


        /// <summary>
        /// 是否显示
        /// </summary>
        public virtual bool AreaShow { get; set; }
        /// <summary>
        /// 是否可编辑
        /// </summary>
        public virtual bool AreaEditor { get; set; }

        /// <summary>
        /// 记录中的表是否可使用
        /// </summary>
        public virtual bool AreaUsable { get; set; } = true;

        /// <summary>
        /// 区域编码
        /// </summary>
        public virtual string AreaCode { get; set; } = "";

        /// <summary>
        /// 区域对应表
        /// </summary>
        public virtual string TableCode { get; set; } = "";
        /// <summary>
        /// 区域对应表
        /// </summary>
        public virtual string MainTable { get; set; } = "";

        /// <summary>
        /// 主表名称
        /// </summary>
        public virtual string MainTableName { get; set; } = "";

        /// <summary>
        /// 主表名称
        /// </summary>
        public virtual string MainTableENName { get; set; } = "";

        /// <summary>
        /// 区域呈现方式
        /// </summary>
        public virtual int Presentation { get; set; }


        /// <summary>
        /// 表格时行数
        /// </summary>
        public virtual int Rowes { get; set; }

        /// <summary>
        /// 不是表格时列数
        /// </summary>
        public virtual int Columnes { get; set; }

        /// <summary>
        /// 是否是多记录
        /// </summary>
        public virtual bool Records { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public virtual double AreaSeq { get; set; }

        /// <summary>
        /// 字段ID
        /// </summary>
        public virtual long FieldId { get; set; }

        /// <summary>
        /// 字段编码
        /// </summary>
        public virtual string FieldCode { get; set; } = "";

        /// <summary>
        /// 字段标识
        /// </summary>
        public virtual string FieldKey { get; set; } = "";
        /// <summary>
        /// 中文名
        /// </summary>
        public virtual string CNName { get; set; } = "";

        /// <summary>
        /// 字段英文名
        /// </summary>
        public virtual string ENName { get; set; } = "";


        /// <summary>
        /// 控件类型
        /// </summary>
        public virtual string ControlsType { get; set; } = "";

        /// <summary>
        /// 常用参数来源
        /// </summary>
        public virtual string ControlSouces { get; set; } = "";

        /// <summary>
        /// 字段类型
        /// </summary>
        public virtual string Type { get; set; } = "";

        /// <summary>
        /// 默认值
        /// </summary>
        public virtual string DefaultValue { get; set; } = "";

        /// <summary>
        /// 最大值
        /// </summary>
        public virtual int FLength { get; set; }

        /// <summary>
        /// 必填
        /// </summary>
        public virtual bool Required { get; set; }

        /// <summary>
        /// 最大值
        /// </summary>
        public virtual int Max { get; set; }

        /// <summary>
        /// 最小值
        /// </summary>
        public virtual int Min { get; set; }

        /// <summary>
        /// 精度
        /// </summary>
        public virtual int Precisions { get; set; }

        /// <summary>
        /// 单选还是多选
        /// </summary>
        public virtual int SelectType { get; set; }

        /// <summary>
        /// 是否显示
        /// </summary>
        public virtual bool Show { get; set; }

        /// <summary>
        /// 是否可编辑
        /// </summary>
        public virtual bool Editor { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public virtual double Sequence { get; set; }
        /// <summary>
        /// 记录中的表是否可使用
        /// </summary>
        public virtual bool FieldUsable { get; set; } = true;
        /// <summary>
        /// 左边括号
        /// </summary>
        public virtual string LeftBracket { get; set; } = "";
        /// <summary>
        /// 级联字段
        /// </summary>
        public virtual string CascadeField { get; set; } = "";
        /// <summary>
        /// 条件
        /// </summary>
        public virtual string Condition { get; set; } = "";
        /// <summary>
        /// 级联字段值
        /// </summary>
        public virtual string CascadeFieldValue { get; set; } = "";
        /// <summary>
        /// 级联字段值
        /// </summary>
        public virtual string RightBracket { get; set; } = "";

        /// <summary>
        /// 关联条件
        /// </summary>
        public virtual string Associated { get; set; } = "";


        public virtual string Regulars { get; set; } = "";
    }
    public class FormConfig
    {
        /// <summary>
        /// 主键
        /// </summary>
        public virtual long AutoId { get; set; }

        /// <summary>
        /// 表单ID
        /// </summary>
        public virtual long FormId { get; set; }

        /// <summary>
        /// 版本号
        /// </summary>
        public virtual long VersionNo { get; set; }

        /// <summary>
        /// 字段区域
        /// </summary>
        public virtual List<Form_Area> Areas { get; set; } = new List<Form_Area>();
        /// <summary>
        /// 可使用申请人ID
        /// </summary>
        public virtual bool UseApplicantEmpId { get; set; }
        /// <summary>
        /// 表单中文名
        /// </summary>
        public virtual string FormCNName { get; set; }
        /// <summary>
        /// 表单英文名
        /// </summary>
        public virtual string FormENName { get; set; }
        /// <summary>
        /// 按钮
        /// </summary>
        public virtual List<Flow_Buttons_Put> Buttons { get; set; } = new List<Flow_Buttons_Put>();
        /// <summary>
        /// 流程中文名
        /// </summary>
        public virtual string FlowCNName { get; set; }
        /// <summary>
        /// 流程英文名
        /// </summary>
        public virtual string FlowENName { get; set; }
        /// <summary>
        /// 申请人ID
        /// </summary>
        public virtual int ApplyEmpId { get; set; }
        public virtual Form_Area_Fields Opinion { get; set; }
    }

    /// <summary>
    /// 字段区域
    /// </summary>
    public class Form_Area
    {
        /// <summary>
        /// 主键
        /// </summary>
        public virtual long AreaId { get; set; }


        /// <summary>
        /// 表单中文名
        /// </summary>
        public virtual string AreaCNName { get; set; }

        /// <summary>
        /// 表单英文名
        /// </summary>
        public virtual string AreaENName { get; set; }


        /// <summary>
        /// 区域编码
        /// </summary>
        public virtual string AreaCode { get; set; }


        /// <summary>
        /// 区域对应表
        /// </summary>
        public virtual string TableCode { get; set; }
        /// <summary>
        /// 区域对应表
        /// </summary>
        public virtual string MainTable { get; set; } = "";

        /// <summary>
        /// 主表名称
        /// </summary>
        public virtual string MainTableName { get; set; } = "";

        /// <summary>
        /// 主表名称
        /// </summary>
        public virtual string MainTableENName { get; set; } = "";


        /// <summary>
        /// 区域呈现方式
        /// </summary>
        public virtual int Presentation { get; set; }

        /// <summary>
        /// 表格时行数
        /// </summary>
        public virtual int Rowes { get; set; }

        /// <summary>
        /// 不是表格时列数
        /// </summary>
        public virtual int Columnes { get; set; }

        /// <summary>
        /// 是否是多记录
        /// </summary>
        public virtual bool Records { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public virtual double AreaSeq { get; set; }
        /// <summary>
        /// 字段
        /// </summary>
        public virtual List<Form_Area_Fields> Fields { get; set; } = new List<Form_Area_Fields>();

        /// <summary>
        /// 记录中的表是否可使用
        /// </summary>
        public virtual bool Usable { get; set; }
        /// <summary>
        /// 显示
        /// </summary>
        public virtual bool Show { get; set; }
        /// <summary>
        /// 编辑性
        /// </summary>
        public virtual bool Editor { get; set; }
    }
    /// <summary>
    /// 字段
    /// </summary>
    public class Form_Area_Fields : Field_Form_Area_Fields
    {

        /// <summary>
        /// 记录中的表是否可使用
        /// </summary>
        public virtual bool Usable { get; set; } = true;

        /// <summary>
        /// 字段条件
        /// </summary>
        public virtual List<Field_Form_Area_Fields_Condition> Conditions { get; set; } = new List<Field_Form_Area_Fields_Condition>();
        /// <summary>
        /// 字段规则
        /// </summary>
        public virtual List<Field_Form_Area_Fields_Regular> Regulars { get; set; } = new List<Field_Form_Area_Fields_Regular>();
        /// <summary>
        /// 数据校验规则
        /// </summary>
        public virtual List<Dictionary<object, object>> Rules { get; set; } = new List<Dictionary<object, object>>();
    }
    public class FormTable
    {
        /// <summary>
        /// 区域对应表
        /// </summary>
        public virtual string TableCode { get; set; }
        /// <summary>
        /// 对应的审批表
        /// </summary>
        public virtual string FlowTableCode { get; set; }

        public virtual string ForeignKey { get; set; }

        public virtual long FormId { get; set; }
    }
}
