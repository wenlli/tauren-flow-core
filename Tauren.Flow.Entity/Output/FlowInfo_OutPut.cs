﻿using System;
using System.Collections.Generic;
using System.Text;
using Tauren.Flow.Entity.Enum;
using Tauren.Flow.Entity.Global;

namespace Tauren.Flow.Entity.Output
{
    public class FlowInfo_OutPut
    {
        /// <summary>
        /// 流程ID
        /// </summary> 
        public virtual long FlowId { get; set; }
        /// <summary>
        /// 流程中文名
        /// </summary>
        public virtual string CNName { get; set; } = "";
        /// <summary>
        /// 流程英文名
        /// </summary>
        public virtual string ENName { get; set; } = "";
        /// <summary>
        /// 流程类型
        /// </summary>
        public virtual long? Type { get; set; }
        /// <summary>
        /// 流程管理员
        /// </summary>
        public virtual string Manager { get; set; }
        /// <summary>
        /// 流程实例管理员
        /// </summary>
        public virtual string InstanceManager { get; set; }

        /// <summary>
        /// 表单ID
        /// </summary>
        public virtual long? FormId { get; set; }

        /// <summary>
        /// 可选择申请人
        /// </summary>
        public virtual bool? SelectApplyEmp { get; set; }

        /// <summary>
        /// 流程图标
        /// </summary>
        public virtual string FlowIcon { get; set; } = "";

        /// <summary>
        /// 流程内联样式
        /// </summary>
        public virtual string FlowStyle { get; set; } = "";

        /// <summary>
        /// 提示信息
        /// </summary>
        public virtual string Placeholder { get; set; } = "";

        /// <summary>
        /// 跳过规则
        /// </summary>
        public virtual int? SkipRules { get; set; }

        /// <summary>
        /// 是否向发起人发送消息
        /// </summary>
        public virtual bool SendMessage { get; set; }

        /// <summary>
        /// 当前流程版本
        /// </summary>
        public virtual long? CurrentVersion { get; set; }

        /// <summary>
        /// 流程信息状态
        /// </summary>
        public virtual int? Status { get; set; }
        /// <summary>
        /// 可查看实例详情
        /// </summary>
        public virtual bool? View_Instance { get; set; }

        /// <summary>
        /// 流程中也可回收
        /// </summary>
        public virtual bool? Recycled { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public virtual double? Sequence { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public virtual string Note { get; set; } = "";
        /// <summary>
        /// 创建人显示名
        /// </summary>
        public virtual string Dic_Creator { get; set; }

        /// <summary>
        /// 修改人显示名
        /// </summary>
        public virtual string Dic_Modifier { get; set; }
        /// <summary>
        /// 创建日期-显示
        /// </summary>
        public virtual string Dic_Creatdate { get; set; }
        /// <summary>
        /// 修改日期-显示
        /// </summary>
        public virtual string Dic_Modifdate { get; set; }

        /// <summary>
        /// 状态-显示
        /// </summary>
        public virtual string Dic_Status { get; set; }

        /// <summary>
        /// 流程管理员
        /// </summary>
        public virtual List<PersonInfo_OutPut> Managers { get; set; } = new List<PersonInfo_OutPut>();
        /// <summary>
        /// 流程实例管理员
        /// </summary>
        public virtual List<PersonInfo_OutPut> InstanceManagers { get; set; } = new List<PersonInfo_OutPut>();

        /// <summary>
        /// 表单ID
        /// </summary>
        public virtual List<Form_OutPut> Form { get; set; } = new List<Form_OutPut>();
    }
    public class PageFlowInfoTable : GlobalOutPut
    {

        /// <summary>
        /// 流程ID
        /// </summary> 
        public virtual long FlowId { get; set; }
        /// <summary>
        /// 流程中文名
        /// </summary>
        public virtual string CNName { get; set; } = "";
        /// <summary>
        /// 流程英文名
        /// </summary>
        public virtual string ENName { get; set; } = "";
        /// <summary>
        /// 流程类型
        /// </summary>
        public virtual long? Type { get; set; }
        /// <summary>
        /// 流程类型显示
        /// </summary>
        public virtual string Dic_Type { get; set; }

        /// <summary>
        /// 流程图标
        /// </summary>
        public virtual string FlowIcon { get; set; } = "";
        /// <summary>
        /// 当前流程版本
        /// </summary>
        public virtual long? CurrentVersion { get; set; }

        /// <summary>
        /// 当前流程版本-显示名称
        /// </summary>
        public virtual string VersionName { get; set; }
        public virtual long? CurrentVersionId { get; set; }
        public virtual string Dic_Creator { get; set; }
        public virtual string Dic_Creatdate { get; set; }
        public virtual int Status { get; set; }
        public virtual string Dic_Status { get; set; }
    }
    public class Flow_NodeInfos
    {
        /// <summary>
        /// 步骤ID
        /// </summary>
        public virtual string StepId { get; set; }
        /// <summary>
        /// 流程版本ID
        /// </summary>
        public virtual long? VersionId { get; set; }

        /// <summary>
        /// 节点类型
        /// </summary>
        public virtual string Prop { get; set; }

        /// <summary>
        /// 节点中文名
        /// </summary>
        public virtual string CNName { get; set; }

        /// <summary>
        /// 节点英文名
        /// </summary>
        public virtual string ENName { get; set; }

        /// <summary>
        /// 节点宽度
        /// </summary>
        public virtual long Width { get; set; }

        /// <summary>
        /// 节点高度
        /// </summary>
        public virtual long Height { get; set; }


        /// <summary>
        /// 节点坐标
        /// </summary>
        public virtual string Coordinate { get; set; }

        /// <summary>
        /// 节点显示快捷字段
        /// </summary>
        public virtual bool ShowQuickField { get; set; } = true;

        /// <summary>
        /// 显示历史
        /// </summary>
        public virtual bool ShowHistory { get; set; } = true;

        /// <summary>
        /// 显示意见框
        /// </summary>
        public virtual bool ShowOpinion { get; set; } = true;


        /// <summary>
        /// 意见是否必填
        /// </summary>
        public virtual bool Required { get; set; }


        /// <summary>
        /// 默认审批意见
        /// </summary>
        public virtual string Opinion { get; set; }

        /// <summary>
        /// 默认退回意见
        /// </summary>
        public virtual string BackOpinion { get; set; }

        /// <summary>
        /// 默认拒绝意见
        /// </summary>
        public virtual string EndOpinion { get; set; }

        /// <summary>
        /// 当前节点处理人是否接收提醒
        /// </summary>
        public virtual bool HandlerMessage { get; set; }

        /// <summary>
        /// 当前节点抄送 人是否接收提醒
        /// </summary>
        public virtual bool CCMessage { get; set; }

        /// <summary>
        /// 备注信息
        /// </summary>
        public virtual string Note { get; set; }
        public virtual string Groups { get; set; }
        public virtual string StartGroup { get; set; }

        /// <summary>
        /// 分支审批策略
        /// </summary>
        public virtual string BranchStrategy { get; set; }
        /// <summary>
        /// 审批比列
        /// </summary>
        public virtual string ApprovalScale { get; set; }

        /// <summary>
        /// 分支终止策略
        /// </summary>
        public virtual string EndStrategy { get; set; }
        /// <summary>
        /// 审批比列
        /// </summary>
        public virtual string EndScale { get; set; }

        /// <summary>
        /// 分支退回策略
        /// </summary>
        public virtual string BackStrategy { get; set; }
        /// <summary>
        /// 审批比列
        /// </summary>
        public virtual string BackScale { get; set; }

        /// <summary>
        /// 自动选择分支
        /// </summary>
        public virtual bool IsAutoSelectStep { get; set; }

        /// <summary>
        /// 多人审批策略
        /// </summary>
        public virtual int ApprovalStrategy { get; set; }

        /// <summary>
        /// 跳过策略
        /// </summary>
        public virtual bool SkipStrategy { get; set; }


        /// <summary>
        /// 策略类型
        /// </summary>
        public virtual int StrategyType { get; set; }

        /// <summary>
        /// 审批人兼职策略
        /// </summary>
        public virtual int ApproveParttimePostion { get; set; }

        /// <summary>
        /// 发送人兼职策略
        /// </summary>
        public virtual int SendParttimePostion { get; set; }

        /// <summary>
        /// 处理者类型
        /// </summary>
        public virtual int ApproverType { get; set; }

        /// <summary>
        /// 表单字段
        /// </summary>
        public virtual string FormField { get; set; }

        /// <summary>
        /// 表单字段处理者类型
        /// </summary>
        public virtual int FormFieldApproveType { get; set; }

        /// <summary>
        /// 审批人范围
        /// </summary>
        public virtual string AppoverRange { get; set; }

        /// <summary>
        /// 默认处理人
        /// </summary>
        public virtual string DefaultApprover { get; set; }

        /// <summary>
        /// 部门负责人层级
        /// </summary>
        public virtual int OrgLeve { get; set; }

        /// <summary>
        /// 抄送策略类型
        /// </summary>
        public virtual int CCStrategyType { get; set; }

        /// <summary>
        /// 抄送人兼职设置
        /// </summary>
        public virtual int CCApproveParttimePostion { get; set; }

        /// <summary>
        /// 抄送人兼职2
        /// </summary>
        public virtual int CCSendParttimePostion { get; set; }

        /// <summary>
        /// 抄送人类型
        /// </summary>
        public virtual int CCType { get; set; }

        /// <summary>
        /// 抄送表单字段
        /// </summary>
        public virtual string CCFormField { get; set; }

        /// <summary>
        /// 抄送表单字段处理者类型
        /// </summary>
        public virtual int CCFormFieldApproveType { get; set; }


        /// <summary>
        /// 默认抄送人
        /// </summary>
        public virtual string CCUser { get; set; }

        /// <summary>
        /// 抄送组织层级
        /// </summary>
        public virtual int CCOrgLeve { get; set; }
        /// <summary>
        /// 审批比列
        /// </summary>
        public virtual string NodeApprovalScale { get; set; }
         

        /// <summary>
        /// 分支退回策略
        /// </summary>
        public virtual string NodeBackStrategy { get; set; }
        /// <summary>
        /// 审批比列
        /// </summary>
        public virtual string NodeBackScale { get; set; }

        /// <summary>
        /// 并行分支标记
        /// </summary>
        public virtual string BranchTag { get; set; }
        /// <summary>
        /// 并行分支
        /// </summary>
        public virtual string Line { get; set; }
    }

    public class ExtFlowInfo : FlowInfo_OutPut
    {
        public long VersionId { get; set; }
        public string VersionName { get; set; }
        public long VersionNo { get; set; }
        /// <summary>
        /// 表单版本ID
        /// </summary>
        public virtual long? FormVersionId { get; set; }
        /// <summary>
        /// 是否并行
        /// </summary>
        public virtual bool ParallelTask { get; set; }

        /// <summary>
        /// 直接提交到退回的步骤
        /// </summary>
        public virtual bool SumbitPrevStep { get; set; }
    }
    public class FlowInstanceTree
    {
        public virtual string CNName { get; set; } = "";
        public virtual string ENName { get; set; } = "";
        public virtual bool View_Instance { get; set; }
        public virtual long FlowId { get; set; }
        public virtual long VerId { get; set; }
        public virtual long VerNo { get; set; }
        public virtual long CurVer { get; set; }
        public virtual string VerName { get; set; }
        public virtual double FSeq { get; set; }
        public virtual double Vseq { get; set; }
    }
    public class PageFlowInstanceTable : GlobalOutPut
    {

        /// <summary>
        /// 流程实例ID
        /// </summary>
        public virtual long InstanceId { get; set; }

        /// <summary>
        /// 任务ID
        /// </summary>
        public virtual long TaskId { get; set; }

        /// <summary>
        /// 流程任务标题
        /// </summary>
        public virtual string Title { get; set; }

        /// <summary>
        /// 流程分组ID
        /// </summary>
        public virtual long GroupId { get; set; }
        /// <summary>
        /// 步骤ID
        /// </summary>
        public virtual string StepId { get; set; }

        /// <summary>
        /// 步骤名称
        /// </summary>
        public virtual string Dic_StepId { get; set; }

        /// <summary>
        /// 当前审批人
        /// </summary>
        public virtual int? Reviewer { get; set; }
        /// <summary>
        /// 当前审批人
        /// </summary>
        public virtual string Dic_Reviewer { get; set; }

        /// <summary>
        /// 任务发送人
        /// </summary>
        public virtual int? Sender { get; set; }

        /// <summary>
        /// 任务发送人
        /// </summary>
        public virtual string Dic_Sender { get; set; }

        /// <summary>
        /// 任务接收时间
        /// </summary>
        public virtual DateTime? ReceivingTime { get; set; }

        public virtual string Dic_ReceivingTime { get; set; }

        /// <summary>
        /// 任务处理时间
        /// </summary>
        public virtual DateTime? ProcessTime { get; set; }
        public virtual string Dic_ProcessTime { get; set; }

        /// <summary>
        /// 任务状态
        /// </summary>
        public virtual int? Status { get; set; }
        public virtual string Dic_Status { get; set; }

        /// <summary>
        /// 任务类型
        /// </summary>
        public virtual int? Type { get; set; }
        public virtual string Dic_Type { get; set; } 
          

        /// <summary>
        /// 表单ID
        /// </summary>
        public long? FormId { get; set; }

        /// <summary>
        /// 表单版本ID
        /// </summary>
        public long? FormVersionId { get; set; }

        /// <summary>
        /// 流程ID
        /// </summary>
        public long? FlowId { get; set; }
        public virtual string Dic_FlowId { get; set; }

        /// <summary>
        /// 流程版本ID
        /// </summary>
        public long? FlowVersionId { get; set; }

        /// <summary>
        /// 发起人ID
        /// </summary>
        public int? InitiatorEmpId { get; set; }
        public virtual string Dic_InitiatorEmpId { get; set; }

        /// <summary>
        /// 申请人ID
        /// </summary>
        public int? ApplyEmpId { get; set; }
        public virtual string Dic_ApplyEmpId { get; set; }

        /// <summary>
        /// 流程状态
        /// </summary>
        public int? FlowStatus { get; set; }
        public virtual string Dic_FlowStatus { get; set; }

        /// <summary>
        /// 申请日期
        /// </summary>
        public DateTime? ApplyDate { get; set; }
        public virtual string Dic_ApplyDate { get; set; }
        /// <summary>
        /// 发起日期
        /// </summary>
        public DateTime? InitiatorDate { get; set; }
        public virtual string Dic_InitiatorDate { get; set; }         
         
        public virtual DateTime? ComplatedTime { get; set; }
        public virtual string Dic_ComplatedTime { get; set; }
        /// <summary>
        /// 任务处理类型
        /// </summary>
        public virtual ApprovalType ProcessType { get; set; }

    }
}
