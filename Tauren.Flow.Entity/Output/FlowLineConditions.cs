﻿using System;
using System.Collections.Generic;
using System.Text;
using Tauren.Flow.Entity.Enum;

namespace Tauren.Flow.Entity.Output
{
    /// <summary>
    /// 连线条件
    /// </summary>
    public class FlowLineConditions
    {
        /// <summary>
        /// 区域编码
        /// </summary>
        public virtual string AreaCode { get; set; } = "";

        /// <summary>
        /// 表名
        /// </summary>
        public virtual string TableCode { get; set; } = "";

        /// <summary>
        /// 左括号
        /// </summary>
        public virtual string AreaLk { get; set; } = "";

        /// <summary>
        /// 右括号
        /// </summary>
        public virtual string AreaRk { get; set; } = "";

        /// <summary>
        /// 条件-并且/或则
        /// </summary>
        public virtual string AreaFor { get; set; } = "";

        public virtual List<LineConditions> Conditions { get; set; } = new List<LineConditions>();
    }

    public class LineConditions
    {
        /// <summary>
        /// 字段编码
        /// </summary>
        public virtual string FieldCode { get; set; } = "";

        /// <summary>
        /// 字段KEY
        /// </summary>
        public virtual string FieldKey { get; set; } = "";

        /// <summary>
        /// 字段左括号
        /// </summary>
        public virtual string FieldLk { get; set; } = "";

        /// <summary>
        /// 字段右括号
        /// </summary>
        public virtual string FieldRk { get; set; } = "";

        /// <summary>
        /// 查询条件
        /// </summary>
        public virtual string FieldIn { get; set; } = "";

        /// <summary>
        /// 关联条件--或者/并且
        /// </summary>
        public virtual string FieldFor { get; set; } = "";

        /// <summary>
        ///控件类型
        /// </summary>
        public virtual string ControlStype { get; set; }
        /// <summary>
        /// 常用参数来源
        /// </summary>
        public virtual string ControlSouces { get; set; }

        /// <summary>
        /// 值
        /// </summary>
        public virtual object FieldValue { get; set; }

        /// <summary>
        /// 值集合
        /// </summary>
        public virtual List<Global.FieldValues> FieldValues { get; set; } = new List<Global.FieldValues>();

        /// <summary>
        /// 人员类型
        /// </summary>
        public virtual string PersonType { get; set; }

        public virtual ApproverType ApproverType { get => (ApproverType)int.Parse(PersonType); }
    }
}
