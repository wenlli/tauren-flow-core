﻿using System;
using System.Collections.Generic;
using System.Text;
using Tauren.Flow.Entity.Model;

namespace Tauren.Flow.Entity.Output
{

    public class FlowTaskInfo
    {

        public virtual long TaskId { get; set; }
        public virtual long InstanceId { get; set; }
        public virtual long GroupId { get; set; }
        public virtual string StepId { get; set; }
        public virtual string CurrentStep { get; set; }
        public virtual long CurrentTaskId { get; set; }
        /// <summary>
        /// 流程状态
        /// </summary>
        public virtual int FlowStatus { get; set; }

        /// <summary>
        /// 流程ID
        /// </summary>
        public virtual long FlowId { get; set; }

        /// <summary>
        /// 流程版本ID
        /// </summary>
        public virtual long FlowVersionId { get; set; }
        /// <summary>
        /// 表单ID
        /// </summary>
        public virtual long FormId { get; set; }
        /// <summary>
        /// 表单版本
        /// </summary>
        public virtual long FormFersionId { get; set; }

        /// <summary>
        /// 发起人
        /// </summary>
        public virtual int InitiatorEmpId { get; set; }
        /// <summary>
        /// 申请人
        /// </summary>
        public virtual int ApplyEmpId { get; set; }
        public virtual string Title { get; set; }

        /// <summary>
        /// 上一任务ID
        /// </summary>
        public virtual long PrevTaskId { get; set; }

        /// <summary>
        /// 上一步骤ID
        /// </summary>
        public virtual string PrevStepId { get; set; }
        /// <summary>
        /// 当前审批人
        /// </summary>
        public virtual int Reviewer { get; set; }

        /// <summary>
        /// 发送人
        /// </summary>
        public virtual int Sender { get; set; }

        /// <summary>
        /// 任务接收时间
        /// </summary>
        public virtual DateTime ReceivingTime { get; set; }

        /// <summary>
        /// 任务完成时间
        /// </summary>
        public virtual DateTime ProcessTime { get; set; }

        /// <summary>
        ///任务状态
        /// </summary>
        public virtual int Status { get; set; }

        /// <summary>
        /// 任务类型
        /// </summary>
        public virtual int Type { get; set; }

        /// <summary>
        /// 意见
        /// </summary>
        public virtual string Comment { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public virtual ulong Sort { get; set; }

        /// <summary>
        /// 委托人
        /// </summary>
        public virtual int Mandator { get; set; }

        /// <summary>
        /// 任务是否已读
        /// </summary>
        public virtual bool Read { get; set; }

        /// <summary>
        /// 紧急层度
        /// </summary>
        public virtual int Urgency { get; set; }
        /// <summary>
        /// 当前审批人
        /// </summary>
        public virtual int ApproverEmpId { get => Reviewer; }
    } 

    public class ExtFlow_Instance_CurrentTask: Flow_Instance_CurrentTask
    {
        /// <summary>
        /// 审批人
        /// </summary>
        public virtual int Reviewer { get; set; }
        /// <summary>
        /// 审批人
        /// </summary>
        public virtual string ReCNName { get; set; }
        /// <summary>
        /// 审批人
        /// </summary>
        public virtual string ReENName { get; set; }
        /// <summary>
        /// 当前状态
        /// </summary>
        public virtual int Status { get; set; }
        public virtual string ReStatus { get; set; }
    }
}
