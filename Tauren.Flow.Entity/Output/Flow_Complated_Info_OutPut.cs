﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tauren.Flow.Entity.Output
{
    public class Flow_Complated_Info_OutPut
    {
        public virtual string TableCode { get; set; } = "";
        public virtual string FlowTableCode { get; set; } = "";
        public virtual bool IsUseEmpId { get; set; }
        public virtual long FlowId { get; set; }
        public virtual long FlowVersionId { get; set; }
        public virtual string CNName { get; set; } = "";
        public virtual string ENName { get; set; } = "";
        public virtual string FieldCode { get; set; } = "";
        public virtual string FieldCNName { get; set; } = "";
        public virtual string FieldENName { get; set; } = "";
        public virtual bool Choose { get; set; }
        public virtual bool Enable { get; set; }
        public virtual bool FieldEnable { get; set; }
    }

    public class Flow_Complated_Fields
    {
        public virtual bool IsUseEmpId { get; set; }
        public virtual string CNName { get; set; } = "";
        public virtual string ENName { get; set; } = "";
        public virtual string TableCode { get; set; } = "";
        public virtual string FlowTableCode { get; set; } = "";
        public virtual List<Flow_Complated_Info_OutPut> Fields { get; set; } = new List<Flow_Complated_Info_OutPut>();
        public virtual bool Enable { get; set; }
    }
}
