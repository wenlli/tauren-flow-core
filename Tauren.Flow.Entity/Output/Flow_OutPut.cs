﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tauren.Flow.Entity.Const;
using Tauren.Flow.Entity.Model;

namespace Tauren.Flow.Entity.Output
{
    public class Flow_OutPut
    {

        public ExtFlowInfo FlowProperty { get; set; }
        public List<NodeInfo> NodeList { get; set; } = new List<NodeInfo>();
        public List<LineInfo> LinkList { get; set; } = new List<LineInfo>();
        public List<Flow_Complated_Info_OutPut> Complateds { get; set; } = new List<Flow_Complated_Info_OutPut>();
        public StepView StepView { get; set; }
    }

    public class NodeInfo
    {
        /// <summary>
        /// 节点ID
        /// </summary>
        public virtual string Id { get; set; }

        /// <summary>
        /// 节点宽度
        /// </summary>
        public virtual long Width { get; set; }

        /// <summary>
        /// 节点高度
        /// </summary>
        public virtual long Height { get; set; }


        /// <summary>
        /// 节点坐标
        /// </summary>
        public virtual decimal[] Coordinate { get; set; }

        /// <summary>
        /// 节点属性
        /// </summary>
        public virtual Entity.Model.Flow_Step_Info Meta { get; set; } = new Model.Flow_Step_Info();


        /// <summary>
        /// 策略
        /// </summary>
        public virtual Entity.Model.Flow_Step_Strategy Strategy { get; set; } = new Model.Flow_Step_Strategy();

        /// <summary>
        /// 按钮信息
        /// </summary>
        public virtual List<Flow_Buttons_Put> Btns { get; set; } = new List<Flow_Buttons_Put>();
        public Field_Form_Version_Put Form { get; set; }

        /// <summary>
        /// 获取表单字段编码
        /// </summary>
        /// <returns></returns>
        public string GetFieldCode()
        {
            if (Strategy is null) return "";
            if (string.IsNullOrEmpty(Strategy.FormField)) return "";
            var spFields = Strategy.FormField.Split(new string[] { GlobalConst.SEPARATOR_DOLLAR }, StringSplitOptions.RemoveEmptyEntries);
            if (spFields.Any() && spFields.Length >= 3)
            {
                return spFields[2];
            }
            else return "";
        }
        /// <summary>
        /// 获取表单字段编码
        /// </summary>
        /// <returns></returns>
        public string GetTableCode()
        {
            if (Strategy is null) return "";
            if (string.IsNullOrEmpty(Strategy.FormField)) return "";
            var spFields = Strategy.FormField.Split(new string[] { GlobalConst.SEPARATOR_DOLLAR }, StringSplitOptions.RemoveEmptyEntries);
            if (spFields.Any() && spFields.Length >= 2)
            {
                return $"{spFields[1]}";
            }
            else return "";
        }

    }

    public class LineInfo
    {
        /// <summary>
        /// 连线ID
        /// </summary>
        public virtual string Id { get; set; }

        /// <summary>
        /// 开始节点
        /// </summary>
        public virtual string StartId { get; set; }

        /// <summary>
        /// 走向节点
        /// </summary>
        public virtual string EndId { get; set; }


        /// <summary>
        /// 开始坐标
        /// </summary>
        public virtual int[] StartAt { get; set; }

        /// <summary>
        /// 走向坐标
        /// </summary>
        public virtual int[] EndAt { get; set; }

        /// <summary>
        /// 连线属性
        /// </summary>
        public virtual Entity.Output.Flow_Lines_OutPut Meta { get; set; } = new Output.Flow_Lines_OutPut();
        /// <summary>
        /// 连线状态
        /// </summary>
        public virtual string LineStatus { get; set; }
        public virtual List<LineInfo> BranchLines { get;set; } = new List<LineInfo>();
    }
    public class Form_TableInfoExt
    {
        public virtual int AutoId { get; set; }
        /// <summary>
        /// 表名
        /// </summary>
        public virtual string TableCode { get; set; }


        /// <summary>
        /// 启用？
        /// </summary>
        public virtual bool Enable { get; set; }

        /// <summary>
        /// 判断是否删除
        /// </summary>
        public virtual bool HaveDelete { get; set; }
        /// <summary>
        /// 表名称
        /// </summary>
        public virtual string TableName { get; set; }


        /// <summary>
        /// 英文名
        /// </summary>
        public virtual string TableENName { get; set; }
    }
    public class StepView
    {
        /// <summary>
        /// 激活的步骤
        /// </summary>
        public virtual int ActivitStep { get; set; }
        public virtual List<StepDetile> Steps { get; set; }
    }
    public class StepDetile
    {
        public virtual string Title { get; set; } = "";
        public virtual ulong Seq { get; set; }
        public virtual List<PageFlowCenterList> Lists { get; set; } = new List<PageFlowCenterList>();
        public virtual string StepId { get; set; }
    }
}
