﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tauren.Flow.Entity.Output
{
    public class GlobalModel_OutPut
    { /// <summary>
      /// 用户ID
      /// </summary>
        public virtual int UserId { get; set; }
        /// <summary>
        /// 账号
        /// </summary>
        public virtual string Account { get; set; }
        /// <summary>
        /// 语言
        /// </summary>
        public virtual string Language { get; set; }
        /// <summary>
        /// 员工id
        /// </summary>
        public virtual int EmpId { get; set; }
        /// <summary>
        /// 员工cnnmae
        /// </summary>
        public virtual string EmpName { get; set; }
        /// <summary>
        /// 员工EnName
        /// </summary>
        public virtual string EmpEnName { get; set; }
        /// <summary>
        /// 组织名称
        /// </summary>
        public virtual string OrgName { get; set; }
        /// <summary>
        /// 组织id
        /// </summary>
        public virtual int OrgId { get; set; }
        /// <summary>
        /// 职位名称
        /// </summary>
        public virtual string PositionName { get; set; }
        /// <summary>
        /// 职位id
        /// </summary>
        public virtual int PositionId { get; set; }
    }
}
