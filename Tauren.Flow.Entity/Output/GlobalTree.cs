﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tauren.Flow.Entity.Output
{
    public class GlobalTree
    {
        public virtual long Id { get; set; }
        public virtual string Code { get; set; }
        public virtual string Label { get; set; }
        public virtual string Icon { get; set; }
        public virtual bool Disabled { get; set; }
        public virtual long ParentId { get; set; }
        public virtual string CNName { get; set; }
        public virtual string ENName { get; set; }
        public virtual List<GlobalTree> Children { get; set; } = new List<GlobalTree>();
        public virtual bool UseNodeCss { get; set; }
        public virtual string NodeType { get; set; }
        public virtual List<Options> Parameters { get; set; } = new List<Options>();
        public virtual bool Hyplink { get; set; }
    }
}
