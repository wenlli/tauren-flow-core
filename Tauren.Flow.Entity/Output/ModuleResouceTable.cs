﻿using Tauren.Flow.Entity.Global;

namespace Tauren.Flow.Entity.Output
{
    public class ModuleResouceTable : GlobalOutPut
    {
        public virtual long AutoId { get; set; }

        /// <summary>
        /// 模块编号
        /// </summary>
        public virtual string MenuCode { get; set; }

        /// <summary>
        /// 资源编号
        /// </summary>
        public virtual string ResCode { get; set; }

        /// <summary>
        /// 资源类型-提示/错误/警告
        /// </summary>
        public virtual string ResoucesType { get; set; }

        /// <summary>
        /// 国际语言
        /// </summary>
        public virtual string Language { get; set; }

        /// <summary>
        /// 资源内容
        /// </summary>
        public virtual string Value { get; set; }
        /// <summary>
        /// 资源主表ID
        /// </summary> 
        public virtual long ResourceId { get; set; }

    }
    public class PageModuleResouceTable : ModuleResouceTable
    { 

        /// <summary>
        /// 禁用？启用？
        /// </summary>
        public virtual bool Enable { get; set; }
        /// <summary>
        /// 禁用？启用？
        /// </summary>
        public virtual string Dic_Enable { get; set; }


        /// <summary>
        /// 创建人显示名
        /// </summary>
        public virtual string Dic_Creator { get; set; }

        /// <summary>
        /// 修改人显示名
        /// </summary>
        public virtual string Dic_Modifier { get; set; }
        /// <summary>
        /// 创建日期-显示
        /// </summary>
        public virtual string Dic_Creatdate { get; set; }
        /// <summary>
        /// 修改日期-显示
        /// </summary>
        public virtual string Dic_Modifdate { get; set; }

    }

}
