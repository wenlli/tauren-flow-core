﻿namespace Tauren.Flow.Entity.Output
{
    public class ModuleResoucesRes_OutPut : Model.ModuleResoucesRes
    {
        /// <summary>
        /// 创建人显示名
        /// </summary>
        public virtual string Dic_Creator { get; set; }

        /// <summary>
        /// 修改人显示名
        /// </summary>
        public virtual string Dic_Modifier { get; set; }
        /// <summary>
        /// 创建日期-显示
        /// </summary>
        public virtual string Dic_Creatdate { get; set; }
        /// <summary>
        /// 修改日期-显示
        /// </summary>
        public virtual string Dic_Modifdate { get; set; }
    }
}
