﻿using System.Collections.Generic;

namespace Tauren.Flow.Entity.Output
{
    /// <summary>
    /// 资源主键表
    /// </summary> 
    public class ModuleResouces_OutPut : Model.ModuleResouces
    {

        /// <summary>
        /// 创建人显示名
        /// </summary>
        public virtual string Dic_Creator { get; set; }

        /// <summary>
        /// 修改人显示名
        /// </summary>
        public virtual string Dic_Modifier { get; set; }
        /// <summary>
        /// 创建日期-显示
        /// </summary>
        public virtual string Dic_Creatdate { get; set; }
        /// <summary>
        /// 修改日期-显示
        /// </summary>
        public virtual string Dic_Modifdate { get; set; }

        /// <summary>
        /// 内容
        /// </summary>
        public virtual List<ModuleResoucesRes_OutPut> ChildFieldValues { get; set; }
    }
}
