﻿using System;
using System.Collections.Generic;
using System.Text;
using Tauren.Flow.Entity.Enum;
using Tauren.Flow.Entity.Global;
using Tauren.Flow.Entity.Model;

namespace Tauren.Flow.Entity.Output
{
    public class NextNodeInfoOutPut
    {
        /// <summary>
        /// 人员选择类型
        /// </summary>
        public virtual ApprovalRangeType ApprovalRangeType { get; set; }

        /// <summary>
        /// 下一步任务节点
        /// </summary>
        public virtual string NextStepId { get; set; }

        /// <summary>
        /// 下一节点中文名
        /// </summary>
        public virtual string NextStepCNName { get; set; }

        /// <summary>
        /// 下一节点英文名
        /// </summary>
        public virtual string NextStepENName { get; set; }

        /// <summary>
        /// 审批人empid字符串
        /// </summary>
        public virtual List<int> NextStepApproverEmpId { get; set; } = new List<int>();

        /// <summary>
        /// 审批人字符串
        /// </summary>
        public virtual string NextStepApprover { get; set; }

        /// <summary>
        /// 审批人集合
        /// </summary>

        public virtual List<ApproverModel> NextStepApproverEmps { get; set; } = new List<ApproverModel>();

        /// <summary>
        /// 无人跳过节点数据
        /// </summary>
        public virtual List<SkipNodeInfo> SkipNodes { get; set; } = new List<SkipNodeInfo>();

        /// <summary>
        /// 是否多选
        /// </summary>
        public virtual bool IsMultiSelect { get; set; }

        /// <summary>
        /// 任务类型
        /// </summary>
        public virtual FlowTaskType FlowTaskType { get; set; }

        /// <summary>
        /// 委托任务
        /// </summary>
        public virtual int Mandator { get; set; }
        public virtual List<NextNodeInfoOutPut> BranchNodes { get; set; } = new List<NextNodeInfoOutPut>();
    }

    public class SkipNodeInfo
    {
        public virtual string CurrentStepId { get; set; } = "";
        public virtual string NextStepId { get; set; } = "";
        public virtual int[] NextStepApproverEmpId { get; set; }
    }

    /// <summary>
    /// 获取节点信息的参数
    /// </summary>
    public class NextNodeInfoRequestParameter : NextNodeInfoParameter
    {
        /// <summary>
        /// 下一步连线信息节点
        /// </summary>
        public virtual List<LineInfo> NextLineInfos { get; set; }
    }
    public class NextNodeInfoParameter
    {
        /// 流程信息
        /// </summary>
        public virtual Flow_OutPut Flow_OutPut { get; set; }
        /// <summary>
        /// 当前任务
        /// </summary>
        public virtual FlowTaskInfo CurrentTask { get; set; }
        /// <summary>
        /// 当前步骤
        /// </summary>
        public virtual NodeInfo CurrentStep { get; set; }
        /// <summary>
        /// 任务实例数据
        /// </summary>
        public virtual List<Flow_Instance_Business> Instance_Businesses { get; set; }
    }
    /// <summary>
    /// 单个连线步骤数据
    /// </summary>
    public class SingeNextNodeInfoRequestParameter : NextNodeInfoParameter
    {
        /// <summary>
        /// 下一步连线信息节点
        /// </summary>
        public virtual NodeInfo NextStep { get; set; }
        /// <summary>

    }
    public class NextNodeInfo_OutPut
    {
        public JudeNodeType JudeNodeType { get; set; }
        public List<NextNodeInfoOutPut> NextNode { get; set; } = new List<NextNodeInfoOutPut>();
    }
}
