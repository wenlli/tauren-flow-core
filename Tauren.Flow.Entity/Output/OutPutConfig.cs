﻿using System.Collections.Generic;
using Tauren.Flow.Entity.Enum;
using Tauren.Flow.Entity.Global;

namespace Tauren.Flow.Entity.Output
{
    public class OutPutConfig
    {
        public OutPutConfig()
        {
            ChildFields = new List<OutPutConfig>();
            Options = new List<Options>();
            Rules = new List<Dictionary<object, object>>();
            Tables = new List<TableListHeaderModel>();
        }

        public string MenuCode { get; set; }
        public string FieldKey { get; set; }
        public ControlType ControlType { get; set; }
        public object Label { get; set; }
        public long? Max { get; set; }
        public long? Min { get; set; }
        public long? Precision { get; set; }
        public object DefaultValue { get; set; }
        public bool Show { get; set; }
        public bool Editor { get; set; }
        public bool Required { get; set; }
        public List<OutPutConfig> ChildFields { get; set; }
        public List<Options> Options { get; set; }
        public List<Dictionary<object, object>> Rules { get; set; }
        public List<TableListHeaderModel> Tables { get; set; }
        public SelectType SelectType { get; set; }
        /// <summary>
        /// 页面操作--按钮
        /// </summary>
        public virtual List<SystemButton> Operations { get; set; } = new List<SystemButton>();
        /// <summary>
        /// 排除当前数据
        /// </summary>
        public virtual List<Options> ExceptDataField { get; set; } = new List<Options>();
        public List<Options> Conditions { get; set; } = new List<Options>();
        /// <summary>
        /// 参数
        /// </summary>
        public List<Options> paramters { get; set; } = new List<Options>();
        /// <summary>
        /// 下拉框修改后需要联动改值
        /// </summary>
        public string ValueField { get; set; } = "";

        public List<Options> Results { get; set; } = new List<Options>();
    }
    /// <summary>
    /// 输出数据类型
    /// </summary>
    public class OutPutModel
    {
        public OutPutModel()
        {
            ChildFields = new List<ChildOutPutMode>();
        }

        public string FieldKey { get; set; }
        public object FieldValue { get; set; }
        public string Unit { get; set; }
        public string MenuCode { get; set; }
        public List<ChildOutPutMode> ChildFields { get; set; }
    }

    public class ChildOutPutMode
    {
        public List<OutPutModel> Vlaues { get; set; }
    }

    public class Options
    {
        public string Label { get; set; }
        public object Value { get; set; }
        public List<Options> Conditions { get; set; } = new List<Options>();
        public bool FixedValeu { get; set; }
    } 
}
