﻿using System;
using System.Collections.Generic;
using System.Text;
using Tauren.Flow.Entity.Enum;
using Tauren.Flow.Entity.Global;
using Tauren.Flow.Entity.Model;

namespace Tauren.Flow.Entity.Output
{
    /// <summary>
    /// 流程中心列表
    /// </summary>
    public class PageFlowCenterList : GlobalOutPut
    {

        /// <summary>
        /// 流程实例ID
        /// </summary>
        public virtual long InstanceId { get; set; }

        /// <summary>
        /// 任务ID
        /// </summary>
        public virtual long TaskId { get; set; }

        /// <summary>
        /// 流程任务标题
        /// </summary>
        public virtual string Title { get; set; }

        /// <summary>
        /// 流程分组ID
        /// </summary>
        public virtual long GroupId { get; set; }
        /// <summary>
        /// 步骤ID
        /// </summary>
        public virtual string StepId { get; set; }

        /// <summary>
        /// 步骤名称
        /// </summary>
        public virtual string Dic_StepId { get; set; }

        /// <summary>
        /// 当前审批人
        /// </summary>
        public virtual int? Reviewer { get; set; }
        /// <summary>
        /// 当前审批人
        /// </summary>
        public virtual string Dic_Reviewer { get; set; }

        /// <summary>
        /// 任务发送人
        /// </summary>
        public virtual int? Sender { get; set; }

        /// <summary>
        /// 任务发送人
        /// </summary>
        public virtual string Dic_Sender { get; set; }

        /// <summary>
        /// 任务接收时间
        /// </summary>
        public virtual DateTime? ReceivingTime { get; set; }

        public virtual string Dic_ReceivingTime { get; set; }

        /// <summary>
        /// 任务处理时间
        /// </summary>
        public virtual DateTime? ProcessTime { get; set; }
        public virtual string Dic_ProcessTime { get; set; }

        /// <summary>
        /// 任务状态
        /// </summary>
        public virtual int? Status { get; set; }
        public virtual string Dic_Status { get; set; }

        /// <summary>
        /// 任务类型
        /// </summary>
        public virtual int? Type { get; set; }
        public virtual string Dic_Type { get; set; }

        /// <summary>
        /// 委托人
        /// </summary>
        public virtual int? Mandator { get; set; }
        public virtual string Dic_Mandator { get; set; }

        /// <summary>
        /// 是否已读
        /// </summary>
        public virtual bool Read { get; set; }

        /// <summary>
        /// 紧急程度
        /// </summary>
        public virtual int? Urgency { get; set; }
        public virtual string Dic_Urgency { get; set; }

        /// <summary>
        /// 表单ID
        /// </summary>
        public long? FormId { get; set; }

        /// <summary>
        /// 表单版本ID
        /// </summary>
        public long? FormVersionId { get; set; }

        /// <summary>
        /// 流程ID
        /// </summary>
        public long? FlowId { get; set; }
        public virtual string Dic_FlowId { get; set; }

        /// <summary>
        /// 流程版本ID
        /// </summary>
        public long? FlowVersionId { get; set; }

        /// <summary>
        /// 发起人ID
        /// </summary>
        public int? InitiatorEmpId { get; set; }
        public virtual string Dic_InitiatorEmpId { get; set; }

        /// <summary>
        /// 申请人ID
        /// </summary>
        public int? ApplyEmpId { get; set; }
        public virtual string Dic_ApplyEmpId { get; set; }

        /// <summary>
        /// 流程状态
        /// </summary>
        public int? FlowStatus { get; set; }
        public virtual string Dic_FlowStatus { get; set; }

        /// <summary>
        /// 申请日期
        /// </summary>
        public DateTime? ApplyDate { get; set; }
        public virtual string Dic_ApplyDate { get; set; }
        /// <summary>
        /// 发起日期
        /// </summary>
        public DateTime? InitiatorDate { get; set; }
        public virtual string Dic_InitiatorDate { get; set; }

        /// <summary>
        /// 当前步骤
        /// </summary>
        public string CurrentStep { get; set; }
        public virtual string Dic_CurrentStep { get; set; }

        /// <summary>
        /// 当前任务
        /// </summary>
        public long CurrentTaskId { get; set; }

        /// <summary>
        /// 任务处理类型
        /// </summary>
        public ApprovalType ProcessType { get; set; }
        public virtual DateTime? ComplatedTime { get; set; }
        public virtual string Dic_ComplatedTime { get; set; }

        public virtual List<ExtFlow_Instance_CurrentTask> CurrentTasks { get; set; } = new List<ExtFlow_Instance_CurrentTask>();
    }
}
