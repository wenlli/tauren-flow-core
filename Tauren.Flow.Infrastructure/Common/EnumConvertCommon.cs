﻿using System;
using System.Collections.Generic;
using System.Text;
using Tauren.Flow.Entity.Enum;

namespace Tauren.Flow.Infrastructure.Common
{
    /// <summary>
    /// 枚举转换工具类
    /// </summary>
    public static class EnumConvertCommon
    {
        /// <summary>
        /// 将流程中心列表转换成枚举
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static FlowCenterPageType ConvertFlowCenterPageType(this string value)
        {
            return (FlowCenterPageType)(System.Enum.Parse(typeof(FlowCenterPageType), value));
        }
        /// <summary>
        /// 将流程中心列表转换成枚举
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static T ConvertType<T>(this string value)
        {
            return (T)(System.Enum.Parse(typeof(T), value));
        }
    }
}
