﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tauren.Flow.Infrastructure.Common
{
    public static class Extensions
    {
        /// <summary>
        /// 判断一个整型是否包含在指定的值内
        /// </summary>
        /// <param name="i"></param>
        /// <returns></returns>
        public static bool In(this int i, params int[] ints)
        {
            foreach (int k in ints)
            {
                if (i == k)
                {
                    return true;
                }
            }
            return false;
        } 
    }
}
