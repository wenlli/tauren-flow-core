﻿using System.Collections.Generic;
using System.Linq;
using Tauren.Flow.Entity.Output;

namespace Tauren.Flow.Infrastructure.Common
{
    public class FlowTableFieldKey
    {
        public static FlowTableFieldKey Instance { get => new FlowTableFieldKey(); }
        private List<Options> options = new List<Options>()
        {
            new Options(){Label="employee",Value="empid"},
            new Options(){Label="organization",Value="orgid"},
            new Options(){Label="position",Value="positionId"},
        };

        public Options GetForeignKey(string tablecode)
        {
            if (string.IsNullOrEmpty(tablecode)) return null;
            return options.FirstOrDefault(o => o.Label == tablecode);
        }
        public List<Options> GetPrimaryKey(bool isChinese)
        {
            return new List<Options>()
           {
                  new Options(){
                      Label=isChinese?"员工ID":"EmpLoyee ID",
                      Value="empid",
                      Conditions=new List<Options>()
                      {
                          new Options{ Label="special",Value="employee"}
                      }
                  }
                  ,new Options(){
                      Label=isChinese?"部门ID":"Organization ID",
                      Value="orgid",
                      Conditions=new List<Options>()
                      {
                          new Options{ Label="special",Value="organization"}
                      }
                  } ,new Options(){
                      Label=isChinese?"职位ID":"Position ID",
                      Value="positionid",
                      Conditions=new List<Options>()
                      {
                          new Options{ Label="special",Value="position"}
                      }
                  } ,new Options(){
                      Label=isChinese?"主键字段":"Primary key",
                      Value="autoid",
                      Conditions=new List<Options>()
                      {
                          new Options{ Label="special",Value="special"}
                      }
                  },
           };
        }
    }
}
