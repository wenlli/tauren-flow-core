﻿using Microsoft.AspNetCore.Mvc;
using ServiceStack.Text;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;

namespace Tauren.Flow.Infrastructure.Common
{
    public static class JsonEvent
    {
        public static object ToJson(this object obj)
        {
            var str = "";
            var responseMsg = new HttpResponseMessage();

            if (obj is string || obj is char)
            {
                str = obj.ToString();
            }
            else
            {
                str = JsonSerializer.SerializeToString(obj);
            }

            responseMsg.Content = new StringContent(str, Encoding.GetEncoding("utf-8"), "text/html");

            return responseMsg;
        }

        public static object ToImage(this object obj, string contentType = "")
        {
            var type = contentType == "" ? "PNG" : contentType;
            var responseMsg = new HttpResponseMessage(HttpStatusCode.OK);

            if (obj != null)
            {
                if (obj is byte[])
                {
                    responseMsg.Content = new ByteArrayContent((byte[])obj);
                    responseMsg.Content.Headers.ContentType = new MediaTypeHeaderValue(type);
                }
            }

            return responseMsg;
        }

        /// <summary>
        /// 将字符串转换成实体类
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Json"></param>
        /// <returns></returns>
        public static T ToEntity<T>(this string Json)
        {
            return JsonSerializer.DeserializeFromString<T>(Json);
        }
        /// <summary>
        /// 将实体类转换成字符串
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Json"></param>
        /// <returns></returns>
        public static string ToJsonString(this object Json)
        {
            return JsonSerializer.SerializeToString(Json);
        }

        public static JsonResult ToJsonResult(this object data, bool disableNull = false)
        {
            var setting = new Newtonsoft.Json.JsonSerializerSettings()
            {
                ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver(),
            };
            if (disableNull) setting.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
            //修改时间的序列化方式
            setting.Converters.Add(new Newtonsoft.Json.Converters.IsoDateTimeConverter() { DateTimeFormat = "yyyy/MM/dd HH:mm:ss" });
            // 忽略循环引用
            setting.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            return new JsonResult(data, setting);
        }
        /// <summary>
        /// 将token中的数据转化成数据对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="User"></param>
        /// <returns></returns>
        public static T ToUser<T>(this ClaimsPrincipal User) where T : class
        {
            var claimsIdentity = User.Identity as ClaimsIdentity;
            string userString = claimsIdentity.FindFirst(ClaimTypes.Name)?.Value;
            return userString.ToEntity<T>();
        }
    }
}
