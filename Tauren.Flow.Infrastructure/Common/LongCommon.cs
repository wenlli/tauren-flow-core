﻿using System;
using System.Text;

namespace Tauren.Flow.Infrastructure.Common
{
    public sealed class LongCommon
    {/// <summary>
     /// 64位ID生成器,最高位为符号位,始终为0,可用位数63.
     /// 实例编号占10位,范围为0-1023
     /// 时间戳和索引共占53位
     /// </summary>
        private static readonly Random r = new Random();
        private static readonly LongCommon _default = new LongCommon(0, 12);

        private readonly long instanceID;//实例编号
        private readonly int indexBitLength;//索引可用位数
        private readonly long tsMax = 0;//时间戳最大值
        private readonly long indexMax = 0;
        private readonly object m_lock = new object();

        private long timestamp = 0;//当前时间戳
        private long index = 0;//索引/计数器

        /// <summary>
        /// 
        /// </summary>
        /// <param name="instanceID">实例编号(0-1023)</param>
        /// <param name="indexBitLength">索引可用位数(1-32).每秒可生成ID数等于2的indexBitLength次方.大并发情况下,当前秒内ID数达到最大值时,将使用下一秒的时间戳,不影响获取ID.</param>
        /// <param name="initTimestamp">初始化时间戳,精确到秒.当之前同一实例生成ID的timestamp值大于当前时间的时间戳时,
        /// 有可能会产生重复ID(如持续一段时间的大并发请求).设置initTimestamp比最后的时间戳大一些,可避免这种问题</param>
        public LongCommon(int instanceID, int indexBitLength, long? initTimestamp = null)
        {
            if (instanceID < 0)
            {
                //这里给每个实例随机生成个实例编号
                this.instanceID = r.Next(0, 1024);
            }
            else
            {
                this.instanceID = instanceID % 1024;
            }

            if (indexBitLength < 1)
            {
                this.indexBitLength = 1;
            }
            else if (indexBitLength > 32)
            {
                this.indexBitLength = 32;
            }
            else
            {
                this.indexBitLength = indexBitLength;
            }
            tsMax = Convert.ToInt64(new string('1', 53 - indexBitLength), 2);
            indexMax = Convert.ToInt64(new string('1', indexBitLength), 2);

            if (initTimestamp != null)
            {
                this.timestamp = initTimestamp.Value;
            }
        }

        /// <summary>
        /// 默认每实例每秒生成65536个ID,从1970年1月1日起,累计可使用4358年
        /// </summary>
        /// <param name="instanceID">实例编号(0-1023)</param>
        public LongCommon(int instanceID) : this(instanceID, 16)
        {

        }

        /// <summary>
        /// 默认每秒生成65536个ID,从1970年1月1日起,累计可使用4358年
        /// </summary>
        public LongCommon() : this(-1)
        {

        }

        public long GetTimestamp()
        {
            TimeSpan ts = DateTime.Now - new DateTime(1970, 1, 1);//ToUniversalTime()转换为标准时区的时间,去掉的话直接就用北京时间
            return (long)ts.TotalMilliseconds; //精确到毫秒
            //return (long)ts.TotalSeconds;//获取10位
        }

        /// <summary>
        /// 生成64位ID
        /// </summary>
        /// <returns></returns>
        public long Create()
        {
            long id = 0;

            lock (m_lock)
            {
                //增加时间戳部分
                long ts = GetTimestamp() / 1000;

                ts = ts % tsMax;  //如果超过时间戳允许的最大值,从0开始
                id = ts << (10 + indexBitLength);//腾出后面部分,给实例编号和索引编号使用

                //增加实例部分
                id = id | (instanceID << indexBitLength);

                //获取计数
                if (timestamp < ts)
                {
                    timestamp = ts;
                    index = 0;
                }
                else
                {
                    if (index > indexMax)
                    {
                        timestamp++;
                        index = 0;
                    }
                }

                id = id | index;

                index++;
            }

            return id;
        }

        /// <summary>
        /// 获取当前实例的时间戳
        /// </summary>
        public long CurrentTimestamp
        {
            get
            {
                return this.timestamp;
            }
        }

        /// <summary>
        /// 默认每实例每秒生成65536个ID,从1970年1月1日起,累计可使用4358年
        /// </summary>
        public static LongCommon Default
        {
            get
            {
                return _default;
            }
        }
        /// <summary>
        ///生成制定位数的随机码（数字）
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        public long GenerateRandomCode(int length = 12)
        {
            var result = new StringBuilder();
            for (var i = 0; i < length; i++)
            {
                var r = new Random(Guid.NewGuid().GetHashCode());
                result.Append(r.Next(0, 10));
            }
            string res = result.ToString();
            if (string.IsNullOrEmpty(res)) return 0;
            else return long.Parse(res);
        }
    }
}
