﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Data.Common;
using Tauren.Flow.Entity.Config;
using Tauren.Flow.Entity.Enum;

namespace Tauren.Flow.Infrastructure
{
    public class CustomExceptionFilter : IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            ResponseMessage msg = new ResponseMessage { Status = false, Code = 200, Body = null };
            msg.Message = ErrorMessage.SERVER_ERROR;
            msg.Code = ErrorType.SERVER_ERROR;
            if (context.Exception is DbException)
            {
                msg.Code = ErrorType.SQL_ERROR;
                msg.Message = ErrorMessage.SQL_ERROR;
            }
            else if (context.Exception is ArgumentException)
            {
                msg.Code = ErrorType.SERVER_PARMETER;
                msg.Message = ErrorMessage.SERVER_PARMETER;
            }
            else if (context.Exception is NullReferenceException)
            {
                msg.Code = ErrorType.SERVER_NULL_POINTER;
                msg.Message = ErrorMessage.SERVER_NULL_POINTER;
            }
            else
            {
                msg.Code = ErrorType.SERVER_ERROR;
                msg.Message = ErrorMessage.SERVER_ERROR;
            }
            JsonResult result = new JsonResult(msg)
            {
                StatusCode = ErrorType.SUCCESS_CODE
            };
            context.Result = result;
        }
    }
}
