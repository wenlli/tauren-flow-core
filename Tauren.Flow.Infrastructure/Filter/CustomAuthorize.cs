﻿
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Threading.Tasks;
using Tauren.Flow.Entity.Config;

namespace Tauren.Flow.Infrastructure.Filter
{
    public class CustomAuthorize : AuthorizationHandler<CustomAauthorizeRequirement>
    {

        /// <summary>
        /// 验证方案提供对象
        /// </summary>
        public IAuthenticationSchemeProvider Schemes { get; set; }
        public IHttpContextAccessor HttpContextAccessor { get; set; }

        public CustomAauthorizeRequirement customAauthorizeRequirement { get; set; }

        public CustomAuthorize(IAuthenticationSchemeProvider schemes, IHttpContextAccessor httpContextAccessor)
        {
            Schemes = schemes;
            HttpContextAccessor = httpContextAccessor;
        }

        protected override async Task HandleRequirementAsync(AuthorizationHandlerContext context, CustomAauthorizeRequirement requirement)
        {
            customAauthorizeRequirement = requirement;
            //判断请求是否拥有凭据，即有没有登录
            var defaultAuthenticate = await Schemes.GetDefaultAuthenticateSchemeAsync();
            if (defaultAuthenticate != null)
            {
                var result = await HttpContextAccessor.HttpContext.AuthenticateAsync(defaultAuthenticate.Name);
                if (result.Succeeded && result?.Principal != null)
                {
                    //.currentUser = result.Principal.Identity.Name;
                    //httpContext.User = result.Principal;                                      
                    context.Succeed(requirement);
                }
                else
                {
                    context.Fail();
                }

            }
        }
    }

    public class CustomAauthorizeRequirement : IAuthorizationRequirement
    {
        public CustomAauthorizeRequirement(string _ClaimType, SigningCredentials signingCredentials)
        {
            TokenHelper tokenHelper = new TokenHelperExtension().tokenHelper;
            ClaimType = _ClaimType;
            Issuer = tokenHelper.Issuer;
            Audience = tokenHelper.Audience;
            Expiration = TimeSpan.FromMinutes(double.Parse(tokenHelper.Expiration));
            SigningCredentials = signingCredentials;
        }
        /// <summary>
        /// 认证授权类型
        /// </summary>
        public string ClaimType { internal get; set; }
        /// <summary>
        /// 发行人
        /// </summary>
        public string Issuer { get; set; }
        /// <summary>
        /// 订阅人
        /// </summary>
        public string Audience { get; set; }
        /// <summary>
        /// 过期时间
        /// </summary>
        public TimeSpan Expiration { get; set; } = TimeSpan.FromMinutes(5000);
        /// <summary>
        /// 签名验证
        /// </summary>
        public SigningCredentials SigningCredentials { get; set; }
    }
}
