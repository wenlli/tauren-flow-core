﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using Tauren.Flow.Infrastructure.RedisHelpe;

namespace Tauren.Flow.Infrastructure.Globals
{
    public class ConnectionBLLBase : ConnectionBase, IConnectionBLLBase
    {
        public ConnectionBLLBase(IHttpContextAccessor httpContext, IMapper _mapper, IRedisClient redisClient, Microsoft.Extensions.Configuration.IConfiguration _Configuration) : base(httpContext, _Configuration)
        {
            _Mapper = _mapper;
            _RedisClient = redisClient;
        }
        /// <summary>
        /// AutoMapper
        /// </summary>
        public IMapper Mapper { get => _Mapper; }
        private IMapper _Mapper;
        /// <summary>
        /// redis缓存
        /// </summary>
        public IRedisClient RedisClient { get => _RedisClient; }
        private IRedisClient _RedisClient;
    }
}
