﻿using Microsoft.AspNetCore.Http;
using MySql.Data.MySqlClient;
using System.Data;
using Tauren.Flow.Entity.Const;
using Tauren.Flow.Entity.Global;
using Tauren.Flow.Infrastructure.Common;
using Newtonsoft.Json;

namespace Tauren.Flow.Infrastructure.Globals
{
    /// <summary>
    /// 进行会话天年
    /// </summary>
    public class ConnectionBase : IConnectionBase
    {
        public ConnectionBase(IHttpContextAccessor httpContext, Microsoft.Extensions.Configuration.IConfiguration _configuration)
        {
            _Global = new GlobalModel();
            if (httpContext != null)
            {
                if (httpContext.HttpContext.Items.ContainsKey(GlobalConst.GolbalData) && !string.IsNullOrEmpty(httpContext.HttpContext.Items[GlobalConst.GolbalData] + ""))
                {
                    _Global = JsonConvert.DeserializeObject<GlobalModel>(httpContext.HttpContext.Items[GlobalConst.GolbalData] + "");
                }
                _Global.Language = (httpContext == null) ? "zh-cn" : string.IsNullOrEmpty(httpContext.HttpContext.Request.Headers["language"] + "") ? "zh-cn" : httpContext.HttpContext.Request.Headers["language"] + "";
                if (!(_Global is null))
                {
                    _ResConnection = new MySqlConnection(_Global.ResConnection);
                }
                _HttpContextAccessor = httpContext;
            }
            _Configuration = _configuration;
        }
        public IHttpContextAccessor HttpContextAccessor { get => _HttpContextAccessor; }
        private IHttpContextAccessor _HttpContextAccessor;
        public GlobalModel Global { get => _Global; }
        public GlobalModel _Global;
        public Microsoft.Extensions.Configuration.IConfiguration Configuration { get => _Configuration; }
        private Microsoft.Extensions.Configuration.IConfiguration _Configuration;
        public IDbConnection ResConnection { get => _ResConnection; }
        private IDbConnection _ResConnection;

    }
}
