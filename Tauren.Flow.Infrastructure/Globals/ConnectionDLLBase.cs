﻿using Microsoft.AspNetCore.Http;
using MySql.Data.MySqlClient;
using System.Data;

namespace Tauren.Flow.Infrastructure.Globals
{
    public class ConnectionDLLBase : ConnectionBase, IConnectionDLLBase
    {
        public ConnectionDLLBase(IHttpContextAccessor httpContext, IDbConnection dbConnection, Microsoft.Extensions.Configuration.IConfiguration _Configuration) : base(httpContext, _Configuration)
        {
            if (!(Global is null))
            {
                _BusinessConnection = new MySqlConnection(Global.FlowConnection);
                _BasicDataConnection = new MySqlConnection(Global.BasicDataConnection);
                _FormConnection = new MySqlConnection(Global.FormConnection);
                _FileConnection = new MySqlConnection(Global.FileConnection);
            }
            _MainConnection = dbConnection;
        }
        /// <summary>
        /// 存租户信息的数据库
        /// </summary>
        private IDbConnection _MainConnection;
        public IDbConnection MainConnection { get => _MainConnection; }
        /// <summary>
        /// 业务数据库链接
        /// </summary>
        private IDbConnection _BusinessConnection;

        public IDbConnection BusinessConnection { get => _BusinessConnection; }

        /// <summary>
        /// 基础操作数据库链接
        /// </summary>
        private IDbConnection _BasicDataConnection;
        public IDbConnection BasicDataConnection { get => this._BasicDataConnection; }

        /// <summary>
        /// 表单数据库链接
        /// </summary>
        private IDbConnection _FormConnection;
        public IDbConnection FormConnection { get => this._FormConnection; }
        private IDbConnection _FileConnection;
        public IDbConnection FileConnection { get => this._FileConnection; }
    }
}
