﻿using AutoMapper;
using Tauren.Flow.Infrastructure.RedisHelpe;

namespace Tauren.Flow.Infrastructure.Globals
{
    public interface IConnectionBLLBase : IConnectionBase
    {    
        /// <summary>
        /// 数据mapping
        /// </summary>
        IMapper Mapper { get; }
        /// <summary>
        /// redis缓存
        /// </summary>
        IRedisClient RedisClient { get;  }

    }
}
