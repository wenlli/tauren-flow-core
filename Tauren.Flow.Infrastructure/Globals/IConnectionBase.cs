﻿using Microsoft.AspNetCore.Http;
using System.Data;
using Tauren.Flow.Entity.Global;

namespace Tauren.Flow.Infrastructure.Globals
{
    public interface IConnectionBase
    {
        /// <summary>
        /// 会话信息
        /// </summary>
        GlobalModel Global { get; }
        /// <summary>
        /// http头数据
        /// </summary>
        IHttpContextAccessor HttpContextAccessor { get; }

        /// <summary>
        /// 资源数据库链接
        /// </summary>
        IDbConnection ResConnection { get; }
    }
}
