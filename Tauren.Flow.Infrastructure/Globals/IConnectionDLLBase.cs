﻿using System.Data;

namespace Tauren.Flow.Infrastructure.Globals
{
    public interface IConnectionDLLBase : IConnectionBase
    {
        /// <summary>
        /// 业务数据库链接
        /// </summary>
        IDbConnection BusinessConnection { get; }

        /// <summary>
        ///主数据库链接
        /// </summary>
        IDbConnection MainConnection { get; }

        /// <summary>
        /// 基础数据库连接
        /// </summary>
        IDbConnection BasicDataConnection { get; }

        /// <summary>
        /// 表单数据库连接
        /// </summary>
        IDbConnection FormConnection { get; }
        IDbConnection FileConnection { get; }
    }
}
