﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Tauren.Flow.Entity.Const;
using Tauren.Flow.Entity.Model;
using Tauren.Flow.Entity.Output;

namespace Tauren.Flow.Mapper
{
    public class FlowInfofile : Profile, ICommProfile
    {
        public FlowInfofile()
        {
            InitProfile();
        }

        public void InitProfile()
        {
            CreateMap<Entity.Model.FlowInfo, Entity.Output.FlowInfo_OutPut>().AfterMap((data, viw) =>
            {
                viw.Dic_Creatdate = data.Creatdate is null ? "" : data.Creatdate.Value.ToString(GlobalConst.DATE_FORMAT_SECONDS);
                viw.Dic_Modifdate = data.Modifdate is null ? "" : data.Modifdate.Value.ToString(GlobalConst.DATE_FORMAT_SECONDS);
            });
            CreateMap<Entity.Input.FlowInfo_InPut, Entity.Model.FlowInfo>();
            CreateMap<Entity.Output.Form_InPut, Entity.Output.FormModel>();
            CreateMap<Entity.Output.FormModel, Entity.Output.Form_OutPut>();
            CreateMap<Entity.Model.Flow_Buttons, Entity.Output.Flow_Buttons_Put>();
            CreateMap<Entity.Output.Flow_Buttons_Put, Entity.Model.Flow_Buttons>();
            CreateMap<NodeInfo, NodeInfo>().AfterMap((data, mode) =>
            {
                mode.Strategy.ApproveParttimePostion = mode.Strategy.CCApproveParttimePostion;
                mode.Strategy.FormField = mode.Strategy.CCFormField;
                mode.Strategy.FormFieldApproveType = mode.Strategy.CCFormFieldApproveType;
                mode.Strategy.OrgLeve = mode.Strategy.CCOrgLeve;
                mode.Strategy.SendParttimePostion = mode.Strategy.CCSendParttimePostion;
                mode.Strategy.StrategyType = mode.Strategy.CCStrategyType;
                mode.Strategy.ApproverType = mode.Strategy.CCType;
                mode.Strategy.DefaultApprover = mode.Strategy.CCUser;
            });
            CreateMap<Flow_Complated_Info_OutPut, Flow_Complated_Info>();
        }
    }
}
