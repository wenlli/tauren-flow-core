﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Tauren.Flow.Entity.Const;

namespace Tauren.Flow.Mapper
{
    public class PersonInfofile : Profile, ICommProfile
    {
        public PersonInfofile()
        {
            InitProfile();
        }

        public void InitProfile()
        {
            CreateMap<Entity.Global.PersonInfo, Entity.Global.PersonInfo_OutPut>();
            CreateMap<Entity.Global.PersonInfo_InPut, Entity.Global.PersonInfo>();

        }
    }
}
