﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using ServiceStack.Text;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using Tauren.Flow.DLL.Base;
using Tauren.Flow.DLL.Resouces;
using Tauren.Flow.Entity.Config;
using Tauren.Flow.Entity.Const;
using Tauren.Flow.Entity.Enum;
using Tauren.Flow.Entity.Global;
using Tauren.Flow.Entity.Model;
using Tauren.Flow.Entity.Output;
using Tauren.Flow.Infrastructure.Globals;
using Tauren.Flow.Infrastructure.RedisHelpe;

namespace Tauren.Flow.Service.Base
{
    public class BaseService
    {
        public BaseService(IBaseDLL baseDLL, IConnectionBLLBase connectionBLL, IMenuResourcesBusiness resourcesBusiness = null)
        {
            connectionBase = connectionBLL;
            this.baseDLL = baseDLL;
            if (!(connectionBLL is null))
            {
                context = connectionBLL.HttpContextAccessor.HttpContext;
                Global = connectionBLL.Global;
                Mapper = connectionBLL.Mapper;
                RedisClient = connectionBLL.RedisClient;
                this.EmpDictionary = EmpDictionarys();
                this.OrgDictionary = OrgDictionarys();
                this.UnitDictionary = this.UnitiInfoDictionarys();
                this.PosDictionary = this.PositionIdDictionarys();
                this._resourcesBusiness = resourcesBusiness;
                Global.IsAdmin = baseDLL.IsCurrentRoleByAdmin(Global.EmpId);
                GetLanagues();
            }
        }
        #region 属性注入
        /// <summary>
        /// 属性注入数据库链接
        /// </summary>
        protected IConnectionBLLBase connectionBase { get; set; }
        /// <summary>
        /// 请求类型
        /// </summary>
        protected HttpContext context { get; set; }

        /// <summary>
        /// 全局会话
        /// </summary>
        protected Entity.Global.GlobalModel Global { get; set; }

        /// <summary>
        /// 数据映射辅助类
        /// </summary>
        protected IMapper Mapper { get; set; }

        /// <summary>
        /// redis缓存辅助类
        /// </summary>
        protected IRedisClient RedisClient { get; set; }

        protected IBaseDLL baseDLL { get; set; }
        /// <summary>
        /// 资源类
        /// </summary>
        public IMenuResourcesBusiness resourcesBusiness { get => _resourcesBusiness; }

        private IMenuResourcesBusiness _resourcesBusiness;

        /// <summary>
        /// 人员
        /// </summary>
        protected Dictionary<int, string> EmpDictionary { get; set; }

        /// <summary>
        /// 组织
        /// </summary>
        protected Dictionary<int, string> OrgDictionary { get; set; }

        /// <summary>
        /// 职位
        /// </summary>
        protected Dictionary<int, string> PosDictionary { get; set; }

        /// <summary>
        /// 单位
        /// </summary>
        protected Dictionary<int, string> UnitDictionary { get; set; }
        protected List<Options> Options;

        protected Dictionary<string, Dictionary<string, string>> ResourcesInfo { get; set; }
        protected Dictionary<string, Dictionary<string, string>> CommonTexts { get; set; }
        /// <summary>
        /// 有权限的按扭
        /// </summary>
        protected List<ExtButtons> Buttons;

        /// <summary>
        /// 判断是否有权限
        /// </summary>
        protected bool HavePermission { get; set; }
        #endregion

        #region 辅助类
        /// <summary>
        /// 将数据库实体转换成页面需要的实体
        /// </summary>
        /// <typeparam name="TModel">数据库实体</typeparam>
        /// <typeparam name="TViewModel">输出实体</typeparam>
        /// <param name="model">数据</param>
        /// <param name="t2">输出数据</param>
        protected void ToViewModel<TModel, TViewModel>(TModel model, out TViewModel view) where TModel : class, new() where TViewModel : class, new()
        {
            view = Mapper.Map<TModel, TViewModel>(model);
        }

        /// <summary>
        /// 将数据库实体集转换成页面需要的实体集
        /// </summary>
        /// <typeparam name="TModel">数据库实体</typeparam>
        /// <typeparam name="TViewModel">输出实体</typeparam>
        /// <param name="model">数据</param>
        /// <param name="t2">输出数据</param>
        protected void ToViewModels<TModel, TViewModel>(List<TModel> model, out List<TViewModel> view) where TModel : class, new() where TViewModel : class, new()
        {
            view = Mapper.Map<List<TModel>, List<TViewModel>>(model);
        }

        /// <summary>
        /// 将页面数据转换程数据数据
        /// </summary>
        /// <typeparam name="TModel">数据库实体</typeparam>
        /// <typeparam name="TInputModel">输出实体</typeparam>
        /// <param name="input">输入数据</param>
        /// <param name="model">数据库数据</param>
        protected void ToModel<TModel, TInputModel>(TInputModel input, out TModel model) where TModel : class, new() where TInputModel : class, new()
        {
            model = Mapper.Map<TInputModel, TModel>(input);
        }

        /// <summary>
        /// 将页面数据集转换程数据集
        /// </summary>
        /// <typeparam name="TModel">数据库实体</typeparam>
        /// <typeparam name="TInputModel">输出实体</typeparam>
        /// <param name="input">输入数据</param>
        /// <param name="model">数据库数据</param>
        protected void ToModels<TModel, TInputModel>(List<TInputModel> input, out List<TModel> model) where TModel : class, new() where TInputModel : class, new()
        {
            model = Mapper.Map<List<TInputModel>, List<TModel>>(input);
        }

        #region 单个转换
        /// <summary>
        /// 将EmpID转换成用户名
        /// </summary>
        /// <returns></returns>
        protected string ToSingeUserInfoName(int EmpId, string lan)
        {
            string redisKey = $"{Global.TenementCode.ToUpper()}_{GlobalConst.EMPLOYEE_FLAG_REDIS_KEY}";
            string redisHashField = $"{Global.TenementCode.ToUpper()}_{GlobalConst.EMPLOYEE_FLAG_REDIS_KEY}_{EmpId}";
            Entity.Model.Employee employee = RedisClient.HashGet<Entity.Model.Employee>(redisKey, redisHashField);
            if (!(employee is null))
                return lan == "Chinese" ? employee.CNName : employee.ENName;
            else
            {
                employee = baseDLL.Single<Entity.Model.Employee>(EmpId);
                if (!(employee is null))
                {
                    RedisClient.HashDelete(redisKey, redisHashField);
                    RedisClient.HashSet(redisKey, redisHashField, employee);
                    return lan == "Chinese" ? employee.CNName : employee.ENName;
                }
                else
                    return "";
            }
        }

        /// <summary>
        /// 将职位ID转换成
        /// </summary>
        /// <param name="PosId"></param>
        /// <param name="lan"></param>
        /// <returns></returns>
        protected string ToSingePostionInfoName(int PosId, string lan)
        {
            string redisKey = $"{Global.TenementCode.ToUpper()}_{GlobalConst.POSTIONS_FLAG_REDIS_KEY}";
            string redisHashField = $"{Global.TenementCode.ToUpper()}_{GlobalConst.POSTIONS_FLAG_REDIS_KEY}_{PosId}";
            Entity.Model.Position Postion = RedisClient.HashGet<Entity.Model.Position>(redisKey, redisHashField);
            if (!(Postion is null))
                return lan == "Chinese" ? Postion.CNName : Postion.ENName;
            else
            {
                Postion = baseDLL.Single<Entity.Model.Position>(PosId);
                if (!(Postion is null))
                {
                    RedisClient.HashDelete(redisKey, redisHashField);
                    RedisClient.HashSet(redisKey, redisHashField, Postion);
                    return lan == "Chinese" ? Postion.CNName : Postion.ENName;
                }
                else
                    return "";
            }
        }
        /// <summary>
        /// 将单位ID转换成中文
        /// </summary>
        /// <param name="PosId"></param>
        /// <param name="lan"></param>
        /// <returns></returns>
        protected string ToSingeUnitInfoName(int UnitId, string lan)
        {
            string redisKey = $"{Global.TenementCode.ToUpper()}_{GlobalConst.UNITS_FLAG_REDIS_KEY}";
            string redisHashField = $"{Global.TenementCode.ToUpper()}_{GlobalConst.UNITS_FLAG_REDIS_KEY}_{UnitId}";
            Entity.Model.Unitinfo Unitinfo = RedisClient.HashGet<Entity.Model.Unitinfo>(redisKey, redisHashField);
            if (!(Unitinfo is null))
                return lan == "Chinese" ? Unitinfo.CNName : Unitinfo.ENName;
            else
            {
                Unitinfo = baseDLL.Single<Entity.Model.Unitinfo>(UnitId);
                if (!(Unitinfo is null))
                {
                    RedisClient.HashDelete(redisKey, redisHashField);
                    RedisClient.HashSet(redisKey, redisHashField, Unitinfo);
                    return lan == "Chinese" ? Unitinfo.CNName : Unitinfo.ENName;
                }
                else
                    return "";
            }
        }
        #endregion

        /// <summary>
        /// 将组织ID转换成
        /// </summary>
        /// <param name="OrgId"></param>
        /// <param name="lan"></param>
        /// <returns></returns>
        protected string ToSingeOrgInfoName(int OrgId, string lan)
        {
            string redisKey = $"{Global.TenementCode.ToUpper()}_{GlobalConst.ORGANIZATION_FLAG_REDIS_KEY}";
            string redisHashField = $"{Global.TenementCode.ToUpper()}_{GlobalConst.ORGANIZATION_FLAG_REDIS_KEY}_{OrgId}";
            Entity.Model.Organization Org = RedisClient.HashGet<Entity.Model.Organization>(redisKey, redisHashField);
            if (!(Org is null))
                return lan == "Chinese" ? Org.CNName : Org.ENName;
            else
            {
                Org = baseDLL.Single<Entity.Model.Organization>(OrgId);
                if (!(Org is null))
                {
                    RedisClient.HashDelete(redisKey, redisHashField);
                    RedisClient.HashSet(redisKey, redisHashField, Org);
                    return lan == "Chinese" ? Org.CNName : Org.ENName;
                }
                else
                    return "";
            }
        }

        /// <summary>
        /// 将EmpID转换成用户名
        /// </summary>
        /// <returns></returns>
        protected List<Entity.Model.Employee> ToUserInfos()
        {
            string redisKey = $"{Global.TenementCode.ToUpper()}_{GlobalConst.EMPLOYEE_FLAG_REDIS_KEY}";
            string redisHashField = $"{Global.TenementCode.ToUpper()}_{GlobalConst.EMPLOYEE_FLAG_REDIS_KEY}_";
            RedisValue[] EmpInfos = RedisClient.HashValues(redisKey);
            List<Entity.Model.Employee> employees = new List<Entity.Model.Employee>();
            if (EmpInfos != null && EmpInfos.Any())
            {
                foreach (RedisValue item in EmpInfos)
                {
                    employees.Add(JsonSerializer.DeserializeFromString<Entity.Model.Employee>(item));
                }
            }
            else
            {
                employees = baseDLL.GetAll<Entity.Model.Employee>().Where(o => o.Enable && o.Status != 0).ToList();
                if (employees.Any())
                {
                    employees.ForEach(o =>
                    {
                        RedisClient.HashDelete(redisKey, redisHashField + o.EmpId);
                        RedisClient.HashSet(redisKey, redisHashField + o.EmpId, o);
                    });
                }
            }
            return employees;
        }

        /// <summary>
        /// 缓存中获取所有组织数据
        /// </summary>
        /// <returns></returns>
        protected List<Entity.Model.Organization> ToOrgInfos()
        {
            string redisKey = $"{Global.TenementCode.ToUpper()}_{GlobalConst.ORGANIZATION_FLAG_REDIS_KEY}";
            string redisHashField = $"{Global.TenementCode.ToUpper()}_{GlobalConst.ORGANIZATION_FLAG_REDIS_KEY}_";
            RedisValue[] orgInfos = RedisClient.HashValues(redisKey);
            List<Entity.Model.Organization> orgs = new List<Entity.Model.Organization>();
            if (orgInfos != null && orgInfos.Any())
            {
                foreach (RedisValue item in orgInfos)
                {
                    orgs.Add(JsonSerializer.DeserializeFromString<Entity.Model.Organization>(item));
                }
            }
            else
            {
                orgs = baseDLL.GetAll<Entity.Model.Organization>().Where(o => o.Enable == true).ToList();
                if (orgs.Any())
                {
                    orgs.ForEach(o =>
                    {
                        RedisClient.HashDelete(redisKey, redisHashField + o.OrgId);
                        RedisClient.HashSet(redisKey, redisHashField + o.OrgId, o);
                    });
                }
            }
            return orgs;
        }


        /// <summary>
        /// 缓存中获取所有职位数据
        /// </summary>
        /// <returns></returns>
        protected List<Entity.Model.Position> ToPositionsInfos()
        {
            string redisKey = $"{Global.TenementCode.ToUpper()}_{GlobalConst.POSTIONS_FLAG_REDIS_KEY}";
            string redisHashField = $"{Global.TenementCode.ToUpper()}_{GlobalConst.POSTIONS_FLAG_REDIS_KEY}_";
            RedisValue[] Positions = RedisClient.HashValues(redisKey);
            List<Entity.Model.Position> positions = new List<Entity.Model.Position>();
            if (Positions != null && Positions.Any())
            {
                foreach (RedisValue item in Positions)
                {
                    positions.Add(JsonSerializer.DeserializeFromString<Entity.Model.Position>(item));
                }
            }
            else
            {
                positions = baseDLL.GetAll<Entity.Model.Position>().Where(o => o.Enable == true).ToList();
                if (positions.Any())
                {
                    positions.ForEach(o =>
                    {
                        RedisClient.HashDelete(redisKey, redisHashField + o.PositionId);
                        RedisClient.HashSet(redisKey, redisHashField + o.PositionId, o);
                    });
                }
            }
            return positions;
        }


        /// <summary>
        /// 缓存中获取所有单位数据
        /// </summary>
        /// <returns></returns>
        private List<Entity.Model.Unitinfo> ToUnitiInfos()
        {
            string redisKey = $"{Global.TenementCode.ToUpper()}_{GlobalConst.UNITS_FLAG_REDIS_KEY}";
            string redisHashField = $"{Global.TenementCode.ToUpper()}_{GlobalConst.UNITS_FLAG_REDIS_KEY}_";
            RedisValue[] Unitinfos = RedisClient.HashValues(redisKey);
            List<Entity.Model.Unitinfo> Unitis = new List<Entity.Model.Unitinfo>();
            if (Unitinfos != null && Unitinfos.Any())
            {
                foreach (RedisValue item in Unitinfos)
                {
                    Unitis.Add(JsonSerializer.DeserializeFromString<Entity.Model.Unitinfo>(item));
                }
            }
            else
            {
                Unitis = baseDLL.GetAll<Entity.Model.Unitinfo>().Where(o => o.Enable == true).ToList();
                if (Unitis.Any())
                {
                    Unitis.ForEach(o =>
                    {
                        RedisClient.HashDelete(redisKey, redisHashField + o.UnitId);
                        RedisClient.HashSet(redisKey, redisHashField + o.UnitId, o);
                    });
                }
            }
            return Unitis;
        }
        /// <summary>
        /// 人员转换成字典
        /// </summary>
        /// <returns></returns>
        private Dictionary<int, string> EmpDictionarys()
        {
            List<Entity.Model.Employee> emps = ToUserInfos();
            Dictionary<int, string> keyValuePairs = new Dictionary<int, string>();
            if (emps.Any())
            {
                emps.ForEach(o =>
                {
                    keyValuePairs.Remove(o.EmpId);
                    keyValuePairs.Add(o.EmpId, Global.Language == "zh-cn" ? o.CNName : o.ENName);
                });
            }
            return keyValuePairs;
        }

        /// <summary>
        /// 组织转换成字典
        /// </summary>
        /// <returns></returns>
        private Dictionary<int, string> OrgDictionarys()
        {
            List<Entity.Model.Organization> orgs = ToOrgInfos();
            Dictionary<int, string> keyValuePairs = new Dictionary<int, string>();
            if (orgs.Any())
            {
                orgs.ForEach(o =>
                {
                    keyValuePairs.Remove(o.OrgId);
                    keyValuePairs.Add(o.OrgId, Global.Language == "zh-cn" ? o.CNName : o.ENName);
                });
            }
            return keyValuePairs;
        }

        /// <summary>
        /// 职位转换成字典
        /// </summary>
        /// <returns></returns>
        private Dictionary<int, string> PositionIdDictionarys()
        {
            List<Entity.Model.Position> orgs = ToPositionsInfos();
            Dictionary<int, string> keyValuePairs = new Dictionary<int, string>();
            if (orgs.Any())
            {
                orgs.ForEach(o =>
                {
                    keyValuePairs.Remove(o.PositionId);
                    keyValuePairs.Add(o.PositionId, Global.Language == "zh-cn" ? o.CNName : o.ENName);
                });
            }
            return keyValuePairs;
        }

        /// <summary>
        /// 职位转换成字典
        /// </summary>
        /// <returns></returns>
        private Dictionary<int, string> UnitiInfoDictionarys()
        {
            List<Entity.Model.Unitinfo> orgs = ToUnitiInfos();
            Dictionary<int, string> keyValuePairs = new Dictionary<int, string>();
            if (orgs.Any())
            {
                orgs.ForEach(o =>
                {
                    keyValuePairs.Remove(o.UnitId);
                    keyValuePairs.Add(o.UnitId, Global.Language == "zh-cn" ? o.CNName : o.ENName);
                });
            }
            return keyValuePairs;
        }
        #endregion

        #region 获取资源数据
        /// <summary>
        /// 获取语言的数据
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        protected string Languages(string ResourcesKey)
        {
            ResourcesKey = ResourcesKey.ToLower();
            Dictionary<string, Dictionary<string, string>> Resources = this.GetResources(GlobalConst.LANGUAGE_FLAG_MODULE_KEY);
            if (Resources.Any())
            {
                if (Resources.ContainsKey(ResourcesKey))
                {
                    if (Resources[ResourcesKey].Any() && Resources[ResourcesKey].ContainsKey(Global.Language))
                    {
                        string value = Resources[ResourcesKey][Global.Language];
                        if (string.IsNullOrEmpty(value))
                        {
                            string lan = LanguageType.zh_cn.ToString().Replace("_", "-");
                            value = Resources[ResourcesKey].ContainsKey(lan) ? Resources[ResourcesKey][lan] : "";
                        }
                        return value;
                    }
                }
            }
            return "";
        }
        /// <summary>
        /// 获取资源数据
        /// </summary>
        /// <param name="ResourcesKey"></param>
        /// <param name="Language"></param>
        /// <returns></returns>
        protected string Resources(string ResourcesKey)
        {
            ResourcesKey = ResourcesKey.ToLower();
            if (ResourcesInfo.Any())
            {
                if (ResourcesInfo.ContainsKey(ResourcesKey))
                {
                    if (ResourcesInfo[ResourcesKey].Any() && ResourcesInfo[ResourcesKey].ContainsKey(Global.Language))
                    {
                        string value = ResourcesInfo[ResourcesKey][Global.Language];
                        if (string.IsNullOrEmpty(value))
                        {
                            string lan = LanguageType.zh_cn.ToString().Replace("_", "-");
                            value = ResourcesInfo[ResourcesKey].ContainsKey(lan) ? ResourcesInfo[ResourcesKey][lan] : "";
                        }
                        return value;
                    }
                }
            }
            return "";
        }

        protected string Resources(int ResourcesKey)
        {
            string ResourKey = ResourcesKey.ToString().ToLower();
            if (ResourcesInfo.Any())
            {
                if (ResourcesInfo.ContainsKey(ResourKey))
                {
                    if (ResourcesInfo[ResourKey].Any() && ResourcesInfo[ResourKey].ContainsKey(Global.Language))
                    {
                        string value = ResourcesInfo[ResourKey][Global.Language];
                        if (string.IsNullOrEmpty(value))
                        {
                            string lan = LanguageType.zh_cn.ToString().Replace("_", "-");
                            value = ResourcesInfo[ResourKey].ContainsKey(lan) ? ResourcesInfo[ResourKey][lan] : "";
                        }
                        return value;
                    }
                }
            }
            return "";
        }
        public Dictionary<string, Dictionary<string, string>> GetResources(string ModelCode)
        {

            string redisKey = $"{Global.TenementCode.ToUpper()}_{ModelCode}_{GlobalConst.RESOURCES_FLAG_REDIS_KEY}";
            string redisHashField = $"{redisKey}_";
            RedisValue[] Resources = RedisClient.HashValues(redisKey);
            Dictionary<string, Dictionary<string, string>> Result = new Dictionary<string, Dictionary<string, string>>();
            if (!(Resources is null) && Resources.Any())
            {
                foreach (var item in Resources)
                {
                    var re = JsonSerializer.DeserializeFromString<Dictionary<string, Dictionary<string, string>>>(item);
                    foreach (var r in re)
                    {
                        if (!Result.ContainsKey(r.Key))
                        {
                            Result.Add(r.Key, r.Value);
                        }
                    }
                }
            }
            else
            {
                var Lans = resourcesBusiness.GetByModuleCode(ModelCode.ToLower());
                if (Lans.Any())
                {
                    Lans.GroupBy(o => new { o.ResCode }).ToList().ForEach(p =>
                    {
                        Dictionary<string, string> contents = new Dictionary<string, string>();
                        Dictionary<string, Dictionary<string, string>> res = new Dictionary<string, Dictionary<string, string>>();
                        p.ToList().ForEach(n =>
                        {
                            if (!contents.ContainsKey(n.Language))
                                contents.Add(n.Language, n.Value);
                        });
                        if (contents.Any())
                        {
                            if (!Result.ContainsKey(p.Key.ResCode))
                            {
                                res.Add(p.Key.ResCode, contents);
                                Result.Add(p.Key.ResCode, contents);
                                RedisClient.HashDelete(redisKey, redisHashField + p.Key.ResCode);
                                RedisClient.HashSet(redisKey, redisHashField + p.Key.ResCode, res);
                            }

                        }
                    });
                }
            }
            this.CommonTexts = GetCommonResources();
            return Result;
        }
        public Dictionary<string, Dictionary<string, string>> GetCommonResources()
        {
            string redisKey = $"{Global.TenementCode.ToUpper()}_COMMON_{GlobalConst.RESOURCES_FLAG_REDIS_KEY}";
            string redisHashField = $"{redisKey}_";
            RedisValue[] Resources = RedisClient.HashValues(redisKey);
            Dictionary<string, Dictionary<string, string>> Result = new Dictionary<string, Dictionary<string, string>>();
            if (!(Resources is null) && Resources.Any())
            {
                foreach (var item in Resources)
                {
                    var re = JsonSerializer.DeserializeFromString<Dictionary<string, Dictionary<string, string>>>(item);
                    foreach (var r in re)
                    {
                        if (!Result.ContainsKey(r.Key))
                        {
                            Result.Add(r.Key, r.Value);
                        }
                    }
                }
            }
            else
            {
                var Lans = resourcesBusiness.GetByModuleCode("common");
                if (Lans.Any())
                {
                    Lans.GroupBy(o => new { o.ResCode }).ToList().ForEach(p =>
                    {
                        Dictionary<string, string> contents = new Dictionary<string, string>();
                        Dictionary<string, Dictionary<string, string>> res = new Dictionary<string, Dictionary<string, string>>();
                        p.ToList().ForEach(n =>
                        {
                            if (!contents.ContainsKey(n.Language))
                                contents.Add(n.Language, n.Value);
                        });
                        if (contents.Any())
                        {
                            if (!Result.ContainsKey(p.Key.ResCode))
                            {
                                res.Add(p.Key.ResCode, contents);
                                Result.Add(p.Key.ResCode, contents);
                                RedisClient.HashDelete(redisKey, redisHashField + p.Key.ResCode);
                                RedisClient.HashSet(redisKey, redisHashField + p.Key.ResCode, res);
                            }

                        }
                    });
                }
            }
            return Result;
        }

        protected string CommonText(string ResourcesKey)
        {
            ResourcesKey = ResourcesKey.ToLower();
            if (CommonTexts.Any())
            {
                if (CommonTexts.ContainsKey(ResourcesKey))
                {
                    if (CommonTexts[ResourcesKey].Any() && CommonTexts[ResourcesKey].ContainsKey(Global.Language))
                    {
                        string value = CommonTexts[ResourcesKey][Global.Language];
                        if (string.IsNullOrEmpty(value))
                        {
                            string lan = LanguageType.zh_cn.ToString().Replace("_", "-");
                            value = CommonTexts[ResourcesKey].ContainsKey(lan) ? CommonTexts[ResourcesKey][lan] : "";
                        }
                        return value;
                    }
                }
            }
            return "";
        }

        protected string CommonText(int ResourcesKey)
        {
            string ResourKey = ResourcesKey.ToString().ToLower();
            if (CommonTexts.Any())
            {
                if (CommonTexts.ContainsKey(ResourKey))
                {
                    if (CommonTexts[ResourKey].Any() && CommonTexts[ResourKey].ContainsKey(Global.Language))
                    {
                        string value = CommonTexts[ResourKey][Global.Language];
                        if (string.IsNullOrEmpty(value))
                        {
                            string lan = LanguageType.zh_cn.ToString().Replace("_", "-");
                            value = CommonTexts[ResourKey].ContainsKey(lan) ? CommonTexts[ResourKey][lan] : "";
                        }
                        return value;
                    }
                }
            }
            return "";
        }
        protected void JudePrmission(string meunCode)
        {
            if (Global.IsAdmin)
            {
                HavePermission = true;
            }
            else
            {
                Buttons = baseDLL.JudgePermissions(meunCode);
                if (!(Buttons is null) && Buttons.Any())
                {
                    HavePermission = true;
                }
            }
        }
        /// <summary>
        /// 添加资源到缓存
        /// </summary>
        /// <param name="ModuleCode"></param>
        /// <param name="ResCode"></param>
        /// <param name="Contents"></param>
        public void AddResources(string ModuleCode, string ResCode, List<Entity.Model.ModuleResoucesRes> Contents)
        {
            string redisKey = $"{Global.TenementCode.ToUpper()}_{ModuleCode.ToUpper()}_{GlobalConst.RESOURCES_FLAG_REDIS_KEY}";
            string redisHashField = $"{redisKey}_{ResCode}";
            Dictionary<string, Dictionary<string, string>> res = new Dictionary<string, Dictionary<string, string>>();
            Dictionary<string, string> contents = new Dictionary<string, string>();
            foreach (var item in Contents)
            {
                if (!contents.ContainsKey(item.Language))
                    contents.Add(item.Language, item.Value);
            }
            res.Add(ResCode, contents);
            RedisClient.HashDelete(redisKey, redisHashField);
            RedisClient.HashSet(redisKey, redisHashField, res);
        }

        public void RemoveResources(string ModuleCode, string ResCode)
        {
            string redisKey = $"{Global.TenementCode.ToUpper()}_{ModuleCode.ToUpper()}_{GlobalConst.RESOURCES_FLAG_REDIS_KEY}";
            string redisHashField = $"{redisKey}_{ResCode}";
            RedisClient.HashDelete(redisKey, redisHashField);
        }

        public void GetLanagues()
        {
            var Lans = resourcesBusiness.GetByResCodeAndMoCoed("language", "language", 0);
            if (Lans.Any())
            {
                Options = new List<Options>();
                Lans.GroupBy(o => new { o.ResCode }).ToList().ForEach(p =>
                {
                    p.OrderBy(g => g.AutoId).ToList().ForEach(n =>
                    {
                        if (!Options.Any(o => (o.Value + "") == n.Language))
                            Options.Add(new Entity.Output.Options() { Label = this.Languages(n.Language.Replace("-", "")), Value = n.Language });
                    });
                });
            }
        }
        #endregion

        #region 按钮
        protected void BuliderButtons(List<SystemButton> options, string KeyFieldValue, long PrimaryKeyValue, ButtomType buttomType, string Shape = "", object Paras = null)
        {
            SystemButton btn = new SystemButton();
            CreateButtonCss(buttomType, btn);
            btn.KeyFieldValue = KeyFieldValue;
            btn.Shape = Shape;
            btn.PrimaryKeyValue = PrimaryKeyValue;
            btn.Parameters = Paras;
            options.Add(btn);
        }
        protected void BuliderButtons(List<SystemButton> options, ButtomType buttomType)
        {
            SystemButton btn = new SystemButton();
            CreateButtonCss(buttomType, btn);
            btn.KeyFieldValue = "";
            btn.Shape = "";
            btn.PrimaryKeyValue = 0;
            options.Add(btn);
        }

        private void CreateButtonCss(ButtomType buttomType, SystemButton btn)
        {
            switch (buttomType)
            {
                case ButtomType.add:
                    btn.Icon = "el-icon-plus";
                    break;
                case ButtomType.edit:
                    btn.Icon = "el-icon-edit";
                    break;
                case ButtomType.enable:
                    btn.Icon = "el-icon-open";
                    break;
                case ButtomType.delete:
                    btn.Icon = "el-icon-delete";
                    break;
                case ButtomType.unloaded:
                    btn.Icon = "el-icon-delete";
                    break;
                case ButtomType.role:
                    btn.Icon = "el-icon-lx-friend";
                    break;
                case ButtomType.detaile:
                    btn.Icon = "el-icon-lx-attention";
                    break;
                case ButtomType.disable:
                    btn.Icon = "el-icon-turn-off";
                    break;
                case ButtomType.export:
                    btn.Icon = "el-icon-download";
                    break;
                case ButtomType.import:
                    btn.Icon = "el-icon-upload2";
                    break;
                case ButtomType.save:
                    btn.Icon = "el-icon-thumb";
                    break;
                case ButtomType.confirm:
                    btn.Icon = "el-icon-finished";
                    break;
                case ButtomType.batchdelete:
                    btn.Icon = "el-icon-delete";
                    break;
                case ButtomType.restpassword:
                    btn.Icon = "el-icon-lx-unlock";
                    break;
                case ButtomType.batchregister:
                    btn.Icon = "el-icon-lx-friendadd";
                    break;
                case ButtomType.oneditdoc:
                    btn.Icon = "el-icon-lx-edit";
                    break;
                case ButtomType.ondetaildoc:
                    btn.Icon = "el-icon-lx-attentionfill";
                    break;
                case ButtomType.upload:
                    btn.Icon = "el-icon-upload";
                    break;
                case ButtomType.download:
                    btn.Icon = "el-icon-lx-down";
                    break;
                case ButtomType.set:
                    btn.Icon = "el-icon-setting";
                    break;
                case ButtomType.quickfield:
                    btn.Icon = "el-icon-place";
                    break;
                case ButtomType.policy:
                    btn.Icon = "el-icon-suitcase-1";
                    break;
                case ButtomType.instancefield:
                    btn.Icon = "el-icon-place";
                    break;
                case ButtomType.recycle:
                    btn.Icon = "el-icon-refresh";
                    break;
                default:
                    break;
            }
            btn.Label = this.CommonText($"btn{buttomType.ToString()}");
            btn.Value = buttomType.ToString();
            btn.Type = buttomType == ButtomType.disable || buttomType == ButtomType.delete ? BtnColorType.danger.ToString() : "";
            btn.Size = "mini";
        }
        protected virtual void BuliedTableResConfig(List<TableListHeaderModel> Headers, string pageType)
        {

        }
        protected virtual void BuliedTableConfig(List<TableListHeaderModel> Headers, string pageType, List<FromTableHeader> Fields, List<SystemButton> Operations)
        {
            foreach (var item in Fields)
            {
                Headers.Add(new TableListHeaderModel()
                {
                    Key = item.FieldKey,
                    Name = item.FieldKey != "value" ? CommonText($"th{item.FieldKey}") : CommonText("thbtnValue"),
                    Align = "left",
                    Show = item.Show,
                    Type = item.Primary ? "primary_key" : ""
                });
            }
            if (HavePermission)
            {
                BuliderButtons(Operations, ButtomType.add);
                BuliderButtons(Operations, ButtomType.delete);
            }
        }
        #endregion

        public IResponseMessage GetResponse(string url, string method, string data, HttpConfig config)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = method;
                request.Referer = config.Referer;
                //有些页面不设置用户代理信息则会抓取不到内容
                request.UserAgent = config.UserAgent;
                request.Timeout = config.Timeout;
                request.Accept = config.Accept;
                request.Headers.Set("Accept-Encoding", config.AcceptEncoding);
                request.ContentType = config.ContentType;
                request.KeepAlive = config.KeepAlive;
                request.Headers.Set("Authorization", context.Request.Headers["Authorization"] + "");
                request.Headers.Set("language", context.Request.Headers["language"] + "");
                request.Headers.Set("api-version", context.Request.Headers["api-version"] + "");

                if (url.ToLower().StartsWith("https"))
                {
                    //这里加入解决生产环境访问https的问题--Could not establish trust relationship for the SSL/TLS secure channel
                    ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(RemoteCertificateValidate);
                }

                if (method.ToUpper() == "POST")
                {
                    if (!string.IsNullOrEmpty(data))
                    {
                        byte[] bytes = Encoding.UTF8.GetBytes(data);

                        if (config.GZipCompress)
                        {
                            using (MemoryStream stream = new MemoryStream())
                            {
                                using (GZipStream gZipStream = new GZipStream(stream, CompressionMode.Compress))
                                {
                                    gZipStream.Write(bytes, 0, bytes.Length);
                                }
                                bytes = stream.ToArray();
                            }
                        }

                        request.ContentLength = bytes.Length;
                        request.GetRequestStream().Write(bytes, 0, bytes.Length);
                    }
                    else
                    {
                        request.ContentLength = 0;
                    }
                }

                return GetResponseStreamData((HttpWebResponse)request.GetResponse());
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        private bool RemoteCertificateValidate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true; //总是接受
        }
        /// <summary>
        /// 获取请求后返回的数据
        /// </summary>
        /// <param name="response"></param>
        /// <returns></returns>
        public IResponseMessage GetResponseStreamData(HttpWebResponse response)
        {
            IResponseMessage result = new ResponseMessage();
            if (response != null)
            {
                Stream myResponseStream = response.GetResponseStream();
                StreamReader myStreamReader = new StreamReader(myResponseStream, Encoding.UTF8);
                string retString = myStreamReader.ReadToEnd();
                result = JsonSerializer.DeserializeFromString<IResponseMessage>(retString);
            }
            else
            {
                result.Status = false;
                result.Message = "请求服务失败!";
            }
            return result;

        }

        protected List<Entity.Model.Employee> EmployeesList { get => ToUserInfos(); }
        protected List<Entity.Model.Organization> OrganizationsList { get => ToOrgInfos(); }
        protected List<Entity.Model.Position> PositionsList { get => ToPositionsInfos(); }
        protected List<Entity.Model.Unitinfo> UnitInofList { get => ToUnitiInfos(); }

        #region 防止数据重复提交
        protected string SecretKey { get; set; }
        protected string TaskSecretKey { get; set; }
        /// <summary>
        /// 判断数据是否重复提交
        /// </summary>
        /// <param name="Parameter"></param>
        /// <returns></returns>
        protected bool JudeRepeatSubmit(FlowRquestParameter Parameter)
        {
            this.SecretKey = $"from_{Parameter.SecretKey}";
            this.TaskSecretKey = $"flow_{Parameter.TaskId}";
            string SecretKeyValue = RedisClient.HashGet(GlobalConst.FLOW_FORM_TASK_REPEAT_SUBMIT, SecretKey) + "";
            //TaskSecretKey = RedisClient.HashGet(GlobalConst.FLOW_FORM_TASK_REPEAT_SUBMIT, $"flow_{Parameter.TaskId}") + "";
            RedisClient.HashSet(GlobalConst.FLOW_FORM_TASK_REPEAT_SUBMIT, SecretKey, SecretKey);
            RedisClient.HashSet(GlobalConst.FLOW_FORM_TASK_REPEAT_SUBMIT, TaskSecretKey, TaskSecretKey);
            return string.IsNullOrEmpty(SecretKeyValue);
        }
        protected bool JudeTaskRepeatSubmit(FlowRquestParameter Parameter)
        {
            this.SecretKey = $"from_{Parameter.SecretKey}";
            this.TaskSecretKey = $"flow_{Parameter.TaskId}";
            string TaskSecretKeyValue = RedisClient.HashGet(GlobalConst.FLOW_FORM_TASK_REPEAT_SUBMIT, TaskSecretKey) + "";
            RedisClient.HashSet(GlobalConst.FLOW_FORM_TASK_REPEAT_SUBMIT, SecretKey, SecretKey);
            RedisClient.HashSet(GlobalConst.FLOW_FORM_TASK_REPEAT_SUBMIT, TaskSecretKey, TaskSecretKey);
            return string.IsNullOrEmpty(TaskSecretKeyValue);
        }
        protected bool JudeTaskRunepeatSubmit(FlowParameter Parameter, string processKey = "run")
        {
            this.SecretKey = $"from_{Parameter.SecretKey}";
            this.TaskSecretKey = $"flow_{Parameter.TaskId}";
            string TaskSecretKeyValue = RedisClient.HashGet(GlobalConst.FLOW_FORM_TASK_REPEAT_SUBMIT, $"{processKey}_{TaskSecretKey}") + "";
            RedisClient.HashSet(GlobalConst.FLOW_FORM_TASK_REPEAT_SUBMIT, $"{processKey}_{TaskSecretKey}", $"{processKey}_{TaskSecretKey}");
            return string.IsNullOrEmpty(TaskSecretKeyValue);
        }
        /// <summary>
        /// 移除重复提交数据
        /// </summary>
        /// <param name="Parameter"></param>
        /// <returns></returns>
        public void RemoveRepeatSubmit(string processKey = "run")
        {
            string SecretKeyValue = RedisClient.HashGet(GlobalConst.FLOW_FORM_TASK_REPEAT_SUBMIT, SecretKey) + "";
            if (!string.IsNullOrEmpty(SecretKeyValue))
                RedisClient.HashDelete(GlobalConst.FLOW_FORM_TASK_REPEAT_SUBMIT, SecretKey);
            string TaskSecretKeyValue = RedisClient.HashGet(GlobalConst.FLOW_FORM_TASK_REPEAT_SUBMIT, TaskSecretKey) + "";
            if (!string.IsNullOrEmpty(TaskSecretKeyValue))
                RedisClient.HashDelete(GlobalConst.FLOW_FORM_TASK_REPEAT_SUBMIT, TaskSecretKey);
            string TaskRunSecretKeyValue = RedisClient.HashGet(GlobalConst.FLOW_FORM_TASK_REPEAT_SUBMIT, $"{processKey}_{TaskSecretKey}") + "";
            if (!string.IsNullOrEmpty(TaskRunSecretKeyValue))
                RedisClient.HashDelete(GlobalConst.FLOW_FORM_TASK_REPEAT_SUBMIT, $"{processKey}_{TaskSecretKey}");
        }
        /// <summary>
        /// 移除任务放重复提交数据
        /// </summary>
        /// <param name="Parameter"></param>
        protected void RemoveRepeatTaskSubmit()
        {
            string TaskSecretKeyValue = RedisClient.HashGet(GlobalConst.FLOW_FORM_TASK_REPEAT_SUBMIT, TaskSecretKey) + "";
            if (!string.IsNullOrEmpty(TaskSecretKeyValue))
                RedisClient.HashDelete(GlobalConst.FLOW_FORM_TASK_REPEAT_SUBMIT, TaskSecretKey);
        }

        protected NextNodeInfoOutPut BuilderEmpNextNodeInfoOutPut(NodeInfo NodeInfo)
        {
            return new NextNodeInfoOutPut()
            {
                ApprovalRangeType = ApprovalRangeType.UnChoosable,
                NextStepId = NodeInfo.Meta.StepId,
                NextStepCNName = NodeInfo.Meta.CNName,
                NextStepENName = NodeInfo.Meta.ENName,
            };
        }
        /// <summary>
        /// 获取表单字段值
        /// </summary>
        /// <returns></returns>
        protected virtual List<object> GetFormFieldValue(SingeNextNodeInfoRequestParameter Parameter)
        {
            string tableCode = Parameter.NextStep.GetTableCode();
            string field = Parameter.NextStep.GetFieldCode();
            var instance = Parameter.Instance_Businesses.Where(o => o.InstanceId == Parameter.CurrentTask.InstanceId && o.TableCode == tableCode);
            List<object> list = new List<object>();
            if (instance != null && instance.Any())
            {
                List<dynamic> res = baseDLL.GetBasicListByQuery(instance.FirstOrDefault().PrimaryKey, $"'{(string.Join("','", instance.Select(o => o.PrimaryValue)))}'", $"{tableCode}apply");
                if (res.Any())
                {
                    foreach (var item in res)
                    {
                        IDictionary<string, object> pairs = item as IDictionary<string, object>;
                        if (pairs.ContainsKey(field) && !string.IsNullOrEmpty(pairs[field] + ""))
                        {
                            string fieldValue = pairs[field] + "";
                            fieldValue.Split(new string[] { GlobalConst.SEPARATOR_COMMA }, StringSplitOptions.RemoveEmptyEntries).ToList().ForEach(o => list.Add(o));
                        }
                    }
                }
            }
            return list;
        }

        /// <summary>
        /// 组装策略人员数据返回前端
        /// </summary>
        /// <param name="outPut"></param>
        /// <param name="nextNodeInfo"></param>
        protected void BuilderEmp(NextNodeInfoOutPut outPut, NodeInfo nextNodeInfo)
        {
            var Values = new List<FieldValues>();
            Values = outPut.NextStepApproverEmpId.Join(this.EmployeesList, a => a, b => b.EmpId, (a, b) =>
            {
                FieldValues res = new FieldValues { Value = a + "", CNName = b.CNName, ENName = b.ENName };
                return res;
            }).ToList();
            if (Values.Any())
            {
                if (Values.Count > 1 && (ApprovalStrategyType)nextNodeInfo.Strategy.ApprovalStrategy == ApprovalStrategyType.Disable)
                    outPut.ApprovalRangeType = ApprovalRangeType.Person;
                Values.ForEach(o =>
                {
                    if (!string.IsNullOrEmpty(o.Value))
                    {
                        //outPut.NextStepApproverEmpId.Add(int.Parse(o.Value));
                        outPut.NextStepApproverEmps.Add(new ApproverModel() { CNName = o.CNName, ENName = o.ENName, EmpId = int.Parse(o.Value) });
                    }
                });
            }
        }
        #endregion
        /// <summary>
        /// 将EmpID转换成用户名
        /// </summary>
        /// <returns></returns>
        protected List<Entity.Model.FlowInfo> ToFlowInfos()
        {
            string redisKey = $"{Global.TenementCode.ToUpper()}_{GlobalConst.FLOWINFO_LIST_FLAG_REDIS_KEY}";
            string redisHashField = $"{Global.TenementCode.ToUpper()}_{GlobalConst.FLOWINFO_LIST_FLAG_REDIS_KEY}_";
            RedisValue[] EmpInfos = RedisClient.HashValues(redisKey);
            List<Entity.Model.FlowInfo> employees = new List<Entity.Model.FlowInfo>();
            if (EmpInfos != null && EmpInfos.Any())
            {
                foreach (RedisValue item in EmpInfos)
                {
                    employees.Add(JsonSerializer.DeserializeFromString<Entity.Model.FlowInfo>(item));
                }
            }
            else
            {
                employees = baseDLL.All<Entity.Model.FlowInfo>();
                if (employees.Any())
                {
                    employees.ForEach(o =>
                    {
                        RedisClient.HashDelete(redisKey, redisHashField + o.FlowId);
                        RedisClient.HashSet(redisKey, redisHashField + o.FlowId, o);
                    });
                }
            }
            return employees;
        }
        /// <summary>
        /// 流程转换成字典
        /// </summary>
        /// <returns></returns>
        protected Dictionary<long, FlowInfo> FlowDictionarys()
        {
            List<Entity.Model.FlowInfo> emps = ToFlowInfos();
            Dictionary<long, FlowInfo> keyValuePairs = new Dictionary<long, FlowInfo>();
            if (emps.Any())
            {
                emps.ForEach(o =>
                {
                    keyValuePairs.Remove(o.FlowId);
                    keyValuePairs.Add(o.FlowId, o);
                });
            }
            return keyValuePairs;
        }
    }
}
