﻿using System.Collections.Generic;
using System.Linq;
using Tauren.Flow.DLL.FlowCenterDLL;
using Tauren.Flow.DLL.Resouces;
using Tauren.Flow.Entity.Config;
using Tauren.Flow.Entity.Const;
using Tauren.Flow.Entity.Enum;
using Tauren.Flow.Entity.Global;
using Tauren.Flow.Entity.Model;
using Tauren.Flow.Entity.Output;
using Tauren.Flow.Infrastructure.Common;
using Tauren.Flow.Infrastructure.Globals;
using Tauren.Flow.Service.FlowInfoBLL;

namespace Tauren.Flow.Service.FlowCenterBLL
{
    /// <summary>
    /// 流程中心相关操作
    /// </summary>
    public class FlowCenterService : Base.BaseService, IFlowCenterService
    {
        private IFlowCenterBusiness business;
        private IFlowInfoService flowInfoService;
        public FlowCenterService(IFlowInfoService _flowInfoService, IFlowCenterBusiness _business, IConnectionBLLBase connectionBLL, IMenuResourcesBusiness resourcesBusiness) : base(_business, connectionBLL, resourcesBusiness)
        {
            this.business = _business;
            this.ResourcesInfo = this.GetResources(GlobalConst.RESOURCES_FLAG_MODULE_FLOWCENTER_KEY);
            this.JudePrmission(GlobalConst.RESOURCES_FLAG_MODULE_FLOWCENTER_KEY.ToLower());
            this.flowInfoService = _flowInfoService;
        }

        /// <summary>
        /// 获取列表数据
        /// </summary>
        /// <param name="queryModel"></param>
        /// <returns></returns>
        public IResponseMessage Pages(QueryModel queryModel)
        {
            if (!HavePermission && queryModel.CheckPermission) return ErrorType.BUSINESS_WITHOUT_PERMISSION.Failure(this.CommonText(ErrorType.BUSINESS_WITHOUT_PERMISSION));
            TableListModel<PageFlowCenterList> tableListModel = new TableListModel<PageFlowCenterList>();
            List<TableListHeaderModel> Headers = BuildTableListHeaderModel();
            List<PageFlowCenterList> Data = new List<PageFlowCenterList>();
            long Total = 0;
            ///判断列表类型
            if (queryModel.paramters.Any(o => (o.Label + "").ToLower() == "tasktype"))
            {
                queryModel.paramters.Add(new Entity.Output.Options() { Label = "Reviewer", Value = Global.EmpId });
                var tasktype = queryModel.paramters.FirstOrDefault(o => (o.Label + "").ToLower() == "tasktype");
                queryModel.paramters = queryModel.paramters.Where(o => (o.Label + "").ToLower() != "tasktype").ToList();
                FlowCenterPageType flowCenterPageType = (tasktype.Value + "").ConvertFlowCenterPageType();
                switch (flowCenterPageType)
                {
                    case FlowCenterPageType.todo:
                        Data = business.ToDoPages(queryModel, out Total);
                        break;
                    case FlowCenterPageType.done:
                        Data = business.ToDonePages(queryModel, out Total);
                        break;
                    case FlowCenterPageType.circulated:
                        Data = business.ToCirculatedPages(queryModel, out Total);
                        break;
                    case FlowCenterPageType.mayapply:
                        Data = business.ToMyApplyPages(queryModel, out Total);
                        break;
                    case FlowCenterPageType.apply:
                        Data = business.ToApplyPages(queryModel, out Total);
                        break;
                    case FlowCenterPageType.cc:
                        Data = business.ToMyCCPages(queryModel, out Total);
                        break;
                }
                if (Data.Any())
                {

                    Dictionary<long, FlowInfo> flowNames = FlowDictionarys();
                    Dictionary<long, bool> recycleInfos = business.GetTaskRecycleInfo(Data.Select(o => o.TaskId).ToArray());
                    Dictionary<long, List<ExtFlow_Instance_CurrentTask>> currentTask = business.GetCurrentTaskInfos(Data.Select(o => o.GroupId).ToArray());
                    var stepIds = Data.Select(o => o.StepId).ToList();
                    foreach (var item in currentTask)
                    {
                        if (item.Value != null)
                            stepIds.AddRange(item.Value.Select(o => o.CurrentStep).ToList());
                    }
                    Dictionary<string, string> stepNames = business.GetStepNames(stepIds.ToArray());
                    Data.ForEach(o =>
                    {
                        this.BuildTaskData(o, stepNames, flowNames, currentTask.ContainsKey(o.GroupId) ? currentTask[o.GroupId] : new List<ExtFlow_Instance_CurrentTask>());
                        this.BuildTaskButtons(o, flowCenterPageType, flowNames, recycleInfos);
                        this.BuildProcessType(o);

                    });
                }
            }
            tableListModel.Body = Data;
            tableListModel.Total = Total;
            tableListModel.Header = Headers;
            return tableListModel.Succeed();
        }

        /// <summary>
        /// 查看流程图
        /// </summary>
        /// <param name="flowId"></param>
        /// <param name="versionId"></param>
        /// <returns></returns>
        public IResponseMessage Diagram(long flowId, long versionId, long taskId)
        {
            if (!HavePermission) return ErrorType.BUSINESS_WITHOUT_PERMISSION.Failure(this.CommonText(ErrorType.BUSINESS_WITHOUT_PERMISSION));
            Flow_OutPut flow_OutPut = flowInfoService.BuildViewFlowInfo(flowId, versionId);
            ///当任务大于零时
            if (taskId > 0)
            {
                List<Flow_Task> flow_Tasks = business.GetTasksByTaskId(taskId).Where(o => o.Status != (int)FlowTaskStatus.temp).ToList();
                var currentTask = flow_Tasks.FirstOrDefault(o => o.Status == (int)FlowTaskStatus.audit || o.Status == (int)FlowTaskStatus.momentum);
                if (flow_Tasks.Any())
                {
                    ulong sort = 0;
                    var backStep = flow_Tasks.Where(o => o.Status == (int)FlowTaskStatus.returned);
                    if (backStep.Any()) sort = backStep.Max(o => o.Sort);
                    Dictionary<string, bool> res = new Dictionary<string, bool>();
                    var groups = flow_Tasks.Where(o => o.Sort >= sort).GroupBy(o => o.StepId);
                    List<StepDetile> stepDetiles = new List<StepDetile>();
                    foreach (var item in groups)
                    {
                        res[item.Key] = !item.Any(o => o.Status == (int)FlowTaskStatus.audit || o.Status == (int)FlowTaskStatus.temp || o.Status == (int)FlowTaskStatus.momentum || (o.Type == (int)FlowTaskType.circulated && o.Status == (int)FlowTaskStatus.unread));
                    }
                    if (flow_OutPut != null)
                        ///判断节点是否被处理了
                        flow_OutPut.NodeList.ForEach(o =>
                        {
                            if (o != null && o.Meta != null && res.ContainsKey(o.Meta.StepId))
                            {

                                switch (o.Meta.Prop)
                                {
                                    case "end": o.Meta.Prop = res[o.Meta.StepId] ? "cend" : "end"; break;
                                    case "start": o.Meta.Prop = res[o.Meta.StepId] ? "cstart" : "start"; break;
                                    default: o.Meta.Prop = res[o.Meta.StepId] ? "completed" : "doing"; break;
                                }
                            }
                        });
                    flow_OutPut.LinkList.ForEach(o =>
                    {
                        if (o != null && o.Meta != null && res.ContainsKey(o.Meta.EndId))
                        {
                            o.LineStatus = res[o.Meta.EndId] ? "completed" : "doing";
                            o.Meta.LineStatus = res[o.Meta.EndId] ? "completed" : "doing";
                        }
                    });
                    BuildDiagrm(flow_OutPut.StepView = new StepView(), flow_Tasks, flow_OutPut.NodeList);
                }
            }
            return flow_OutPut.OnSucceed();
        }

        private void BuildDiagrm(StepView stepView, List<Flow_Task> flow_Tasks, List<NodeInfo> nodeInfos)
        {
            var groups = flow_Tasks.GroupBy(o => new { o.StepId, o.Sort });
            List<StepDetile> stepDetiles = new List<StepDetile>();
            List<int> sort = new List<int>();
            int i = 0;
            foreach (var item in groups)
            {
                var node = nodeInfos.FirstOrDefault(o => o.Meta.StepId == item.Key.StepId);
                StepDetile stepDetile = new StepDetile();
                stepDetile.StepId = node.Meta.StepId;
                stepDetile.Seq = item.Key.Sort;
                stepDetile.Title = node == null ? "" : (Global.IsChinese ? node.Meta.CNName : node.Meta.ENName);
                if (item.Any(o => o.Status == (int)FlowTaskStatus.audit || o.Status == (int)FlowTaskStatus.momentum || (o.Type == (int)FlowTaskType.circulated && o.Status == (int)FlowTaskStatus.unread)))
                {
                    sort.Add(i);
                }
                item.ToList().ForEach(o =>
                {
                    PageFlowCenterList flowCenterList = new PageFlowCenterList()
                    {
                        Dic_ComplatedTime = !o.ProcessTime.HasValue ? "" : (o.ProcessTime.Value + "") == "0001-01-01 00:00:00" ? "" : o.ProcessTime.Value.ToString(GlobalConst.DATE_FORMAT_MINUTES),
                        Dic_ReceivingTime = o.ReceivingTime.ToString(GlobalConst.DATE_FORMAT_MINUTES),
                        Title = o.Title,
                        Dic_Reviewer = !EmpDictionary.ContainsKey(o.Reviewer) ? "" : EmpDictionary[o.Reviewer],
                        Dic_Sender = !EmpDictionary.ContainsKey(o.Sender) ? "" : EmpDictionary[o.Sender],
                        Dic_Status = Resources($"taskstaus{((FlowTaskStatus)o.Status).ToString()}"),
                        Dic_FlowStatus = o.Comment,
                        Status = o.Status
                    };
                    stepDetile.Lists.Add(flowCenterList);
                });
                stepDetiles.Add(stepDetile);
                i++;
            }

            if (sort.Any()) stepView.ActivitStep = sort.FirstOrDefault();
            else stepView.ActivitStep = stepDetiles.Count;
            stepView.Steps = stepDetiles.OrderBy(o => o.Seq).ToList();
        }

        /// <summary>
        /// 组装列表标题
        /// </summary>
        /// <returns></returns>
        private List<TableListHeaderModel> BuildTableListHeaderModel()
        {
            PageFlowCenterList o = new PageFlowCenterList();
            List<TableListHeaderModel> headerModels = new List<TableListHeaderModel>();
            headerModels.Add(new TableListHeaderModel() { Key = "rowNo", Name = CommonText("rowNo"), Width = 160, Align = "center", Fixed = true });
            headerModels.Add(new TableListHeaderModel() { Key = "instanceId", Name = Resources("flowinstanceid"), Width = 160, Align = "center", Fixed = true });
            headerModels.Add(new TableListHeaderModel() { Key = "dic_Type", Name = CommonText("flowtasktype"), Width = 120, Align = "center", Fixed = true, IsTag = true, SorTable = SortType.custom.ToString() });
            headerModels.Add(new TableListHeaderModel() { Key = "dic_Urgency", Name = CommonText("flowurgency"), Width = 160, Align = "center", Fixed = true, IsTag = true, SorTable = SortType.custom.ToString() });
            headerModels.Add(new TableListHeaderModel() { Key = "taskId", Name = Resources("taskid"), Width = 70, Hide = true, Type = "primary_key", Align = "center", SorTable = SortType.custom.ToString(), Fixed = true });
            headerModels.Add(new TableListHeaderModel() { Key = "title", Name = Resources("thtitle"), Align = "left", IsOperation = true, Fixed = true, Width = 260, SorTable = SortType.custom.ToString(), Hyperlink = true });
            headerModels.Add(new TableListHeaderModel() { Key = "dic_FlowId", Name = Resources("thflowname"), Align = "left", Fixed = true, Width = 260, SorTable = SortType.custom.ToString(), });
            headerModels.Add(new TableListHeaderModel() { Key = "dic_StepId", Name = Resources("flowcurrentstep"), Width = 260, Align = "left", SorTable = SortType.custom.ToString(), Hyperlink = true });
            headerModels.Add(new TableListHeaderModel() { Key = "dic_ApplyEmpId", Name = Resources("flowapplyempid"), Width = 160, Align = "center", SorTable = SortType.custom.ToString() });
            headerModels.Add(new TableListHeaderModel() { Key = "dic_InitiatorEmpId", Name = Resources("flowinitiatorempid"), Width = 160, Align = "center", SorTable = SortType.custom.ToString() });
            headerModels.Add(new TableListHeaderModel() { Key = "dic_Reviewer", Name = Resources("flowreviewer"), Width = 160, Align = "center", SorTable = SortType.custom.ToString(), Hyperlink = true });
            headerModels.Add(new TableListHeaderModel() { Key = "dic_ApplyDate", Name = Resources("flowapplydate"), Width = 160, Align = "center", SorTable = SortType.custom.ToString() });
            headerModels.Add(new TableListHeaderModel() { Key = "dic_InitiatorDate", Name = Resources("flowinitiatordate"), Width = 160, Align = "center", SorTable = SortType.custom.ToString() });
            headerModels.Add(new TableListHeaderModel() { Key = "dic_ComplatedTime", Name = Resources("flowcomplatedtime"), Width = 160, Align = "center", SorTable = SortType.custom.ToString() });
            headerModels.Add(new TableListHeaderModel() { Key = "dic_Status", Name = Resources("flowstatus"), Width = 160, Align = "center", IsTag = true, SorTable = SortType.custom.ToString() });
            headerModels.Add(new TableListHeaderModel() { Key = "dic_FlowStatus", Name = Resources("flowflowstatus"), Width = 160, Align = "center", IsTag = true, SorTable = SortType.custom.ToString() });
            return headerModels;
        }

        /// <summary>
        /// 组装跳转流程类型
        /// </summary>
        /// <param name="o"></param>
        private void BuildProcessType(PageFlowCenterList o)
        {
            FlowTaskType taskType = (FlowTaskType)o.Type;
            FlowTaskStatus taskStatus = (FlowTaskStatus)o.Status;
            switch (taskType)
            {
                case FlowTaskType.task:
                    switch (taskStatus)
                    {
                        case FlowTaskStatus.momentum:
                            o.ProcessType = ApprovalType.apply;
                            break;
                        case FlowTaskStatus.audit:
                            o.ProcessType = ApprovalType.approval;
                            break;
                        default:
                            o.ProcessType = ApprovalType.view;
                            break;
                    }
                    break;
                case FlowTaskType.delegation:
                    o.ProcessType = ApprovalType.view;
                    break;
                case FlowTaskType.test:
                    o.ProcessType = ApprovalType.view;
                    break;
                case FlowTaskType.circulated:
                    switch (taskStatus)
                    {
                        case FlowTaskStatus.unread:
                            o.ProcessType = ApprovalType.circulated;
                            break;
                        default:
                            o.ProcessType = ApprovalType.view;
                            break;
                    }
                    break;
                case FlowTaskType.cc:
                    o.ProcessType = ApprovalType.view;
                    break;
                default:
                    o.ProcessType = ApprovalType.view;
                    break;
            }
        }

        private void BuildTaskData(PageFlowCenterList o, Dictionary<string, string> stepNames, Dictionary<long, FlowInfo> flowNames, List<ExtFlow_Instance_CurrentTask> currentTasks)
        {
            o.Dic_FlowId = o.FlowId == null ? "" : flowNames.ContainsKey(o.FlowId.Value) ? Global.IsChinese ? flowNames[o.FlowId.Value].CNName : flowNames[o.FlowId.Value].ENName : "";
            string key = $"{o.FlowVersionId}_{o.StepId}";
            currentTasks.ForEach(p =>
            {
                p.ReCNName = !EmpDictionary.ContainsKey(p.Reviewer) ? "" : EmpDictionary[p.Reviewer];
                string cuKey = $"{o.FlowVersionId}_{p.CurrentStep}";
                p.CurrentStep = string.IsNullOrWhiteSpace(p.CurrentStep) ? "" : stepNames.ContainsKey(cuKey) ? stepNames[cuKey] : p.CurrentStep;
                p.ReStatus = Resources($"taskstaus{((FlowTaskStatus)p.Status).ToString()}");
            });
            o.CurrentTasks = currentTasks;
            if (currentTasks.Count <= 1)
            {
                if (currentTasks.Any())
                {
                    var currentStep = currentTasks.FirstOrDefault();
                    o.Dic_StepId = currentStep.CurrentStep;
                    o.Dic_Reviewer = EmpDictionary.ContainsKey(currentStep.Reviewer) ? EmpDictionary[currentStep.Reviewer] : "";
                }
                else
                {
                    o.Dic_StepId = string.IsNullOrWhiteSpace(o.StepId) ? "" : stepNames.ContainsKey(key) ? stepNames[key] : "";
                    o.Dic_Reviewer = o.Reviewer == null ? "" : EmpDictionary.ContainsKey(o.Reviewer.Value) ? EmpDictionary[o.Reviewer.Value] : "";
                }
            }
            else
            {

                o.Dic_StepId = Resources("flowviewdetail");
                o.Dic_Reviewer = Resources("flowviewdetail");
            }
            o.Dic_ApplyDate = o.ApplyDate == null ? "" : o.ApplyDate.Value.ToString(GlobalConst.DATE_FORMAT_SECONDS);
            o.Dic_InitiatorDate = o.InitiatorDate == null ? "" : o.InitiatorDate.Value.ToString(GlobalConst.DATE_FORMAT_SECONDS);
            o.Dic_ReceivingTime = o.ReceivingTime == null ? "" : o.ReceivingTime.Value.ToString(GlobalConst.DATE_FORMAT_SECONDS);
            o.Dic_ComplatedTime = o.ComplatedTime == null ? "" : o.ComplatedTime.Value.ToString(GlobalConst.DATE_FORMAT_SECONDS);
            o.Dic_ProcessTime = o.ProcessTime == null ? "" : o.ProcessTime.Value.ToString(GlobalConst.DATE_FORMAT_SECONDS);
            o.Dic_ApplyEmpId = o.ApplyEmpId == null ? "" : EmpDictionary.ContainsKey(o.ApplyEmpId.Value) ? EmpDictionary[o.ApplyEmpId.Value] : "";
            o.Dic_InitiatorEmpId = o.InitiatorEmpId == null ? "" : EmpDictionary.ContainsKey(o.InitiatorEmpId.Value) ? EmpDictionary[o.InitiatorEmpId.Value] : "";

            o.Dic_Sender = o.Sender == null ? "" : EmpDictionary.ContainsKey(o.Sender.Value) ? EmpDictionary[o.Sender.Value] : "";
            o.Dic_Status = o.Status == null ? "" : Resources($"taskstaus{((FlowTaskStatus)o.Status.Value).ToString()}");
            o.Dic_FlowStatus = o.FlowStatus == null ? "" : Resources($"flowstatus{((FlowInstanceStatus)o.FlowStatus.Value).ToString()}");
            o.Dic_Type = o.Type == null ? "" : Resources($"prcoesstype{((FlowTaskType)o.Type.Value).ToString()}");
            o.Dic_Urgency = o.Urgency == null ? "" : Resources($"urgencytype{((FlowTaskType)o.Urgency.Value).ToString()}");
            o.FontWeight = !o.Read;
            o.Row_Style = "";
            o.Enable = true;
        }
        /// <summary>
        /// 组装每个任务的按钮
        /// </summary>
        /// <param name="o"></param>
        /// <param name="stepNames"></param>
        /// <param name="flowNames"></param>
        private void BuildTaskButtons(PageFlowCenterList o, FlowCenterPageType flowCenterPageType, Dictionary<long, FlowInfo> flowNames, Dictionary<long, bool> recycleInfos)
        {
            FlowInfo flowInfo = flowNames.ContainsKey(o.FlowId.Value) ? flowNames[o.FlowId.Value] : null;
            FlowTaskType taskType = (FlowTaskType)o.Type;
            FlowTaskStatus taskStatus = (FlowTaskStatus)o.Status;
            o.Operations = new List<SystemButton>();
            dynamic Args = new { flowId = o.FlowId, flowVersionId = o.FlowVersionId, formId = o.FormId, formVersionId = o.FormVersionId, taskId = o.TaskId, groupId = o.GroupId };
            switch (flowCenterPageType)
            {
                case FlowCenterPageType.todo:
                case FlowCenterPageType.circulated:
                case FlowCenterPageType.cc:
                    BuliderButtons(o.Operations, o.GroupId + "", o.TaskId, ButtomType.detaile, "", Args);
                    break;
                case FlowCenterPageType.done:
                    BuliderButtons(o.Operations, o.GroupId + "", o.TaskId, ButtomType.detaile, "", Args);
                    ///下一步是否有人已审核
                    if (recycleInfos.ContainsKey(o.TaskId) && recycleInfos[o.TaskId])
                        BuliderButtons(o.Operations, o.GroupId + "", o.TaskId, ButtomType.recycle);
                    break;
                case FlowCenterPageType.mayapply:
                    BuliderButtons(o.Operations, o.GroupId + "", o.TaskId, ButtomType.detaile, "", Args);
                    ///判断配置是否开启或者下一步是否有人已审核
                    if (flowInfo != null && (flowInfo.Recycled != null && flowInfo.Recycled.Value && o.FlowStatus.Value == (int)FlowInstanceStatus.ongoing) || (recycleInfos.ContainsKey(o.TaskId) && recycleInfos[o.TaskId]))
                        BuliderButtons(o.Operations, o.GroupId + "", o.TaskId, ButtomType.recycle);
                    break;
                case FlowCenterPageType.apply:
                    BuliderButtons(o.Operations, o.GroupId + "", o.TaskId, ButtomType.detaile, "", Args);
                    BuliderButtons(o.Operations, o.GroupId + "", o.TaskId, ButtomType.delete);
                    break;
            }
        }

        public IResponseMessage GetIndexPages(QueryModel queryModel)
        {
            if (!HavePermission && queryModel.CheckPermission) return ErrorType.BUSINESS_WITHOUT_PERMISSION.Failure(this.CommonText(ErrorType.BUSINESS_WITHOUT_PERMISSION));
            TableListModel<PageFlowCenterList> tableListModel = new TableListModel<PageFlowCenterList>();
            List<TableListHeaderModel> Headers = BuildIndexTableListHeaderModel();
            List<PageFlowCenterList> Data = new List<PageFlowCenterList>();
            long Total = 0;
            ///判断列表类型
            if (queryModel.paramters.Any(o => (o.Label + "").ToLower() == "tasktype"))
            {
                queryModel.paramters.Add(new Entity.Output.Options() { Label = "Reviewer", Value = Global.EmpId });
                var tasktype = queryModel.paramters.FirstOrDefault(o => (o.Label + "").ToLower() == "tasktype");
                queryModel.paramters = queryModel.paramters.Where(o => (o.Label + "").ToLower() != "tasktype").ToList();
                FlowCenterPageType flowCenterPageType = (tasktype.Value + "").ConvertFlowCenterPageType();
                switch (flowCenterPageType)
                {
                    case FlowCenterPageType.todo:
                        Data = business.ToDoPages(queryModel, out Total);
                        break;
                    case FlowCenterPageType.circulated:
                        Data = business.ToCirculatedPages(queryModel, out Total);
                        break;
                    case FlowCenterPageType.apply:
                        Data = business.ToApplyPages(queryModel, out Total);
                        break;
                }
                if (Data.Any())
                {

                    Dictionary<long, FlowInfo> flowNames = FlowDictionarys();
                    Dictionary<long, List<ExtFlow_Instance_CurrentTask>> currentTask = business.GetCurrentTaskInfos(Data.Select(o => o.GroupId).ToArray());
                    var stepIds = Data.Select(o => o.StepId).ToList();
                    foreach (var item in currentTask)
                    {
                        if (item.Value != null)
                            stepIds.AddRange(item.Value.Select(o => o.CurrentStep).ToList());
                    }
                    Dictionary<string, string> stepNames = business.GetStepNames(stepIds.ToArray());
                    Data.ForEach(o =>
                    {
                        this.BuildTaskData(o, stepNames, flowNames, currentTask.ContainsKey(o.GroupId) ? currentTask[o.GroupId] : new List<ExtFlow_Instance_CurrentTask>());
                        this.BuildProcessType(o);

                    });
                }
            }
            tableListModel.Body = Data;
            tableListModel.Header = Headers;
            return tableListModel.Succeed();
        }
        private List<TableListHeaderModel> BuildIndexTableListHeaderModel()
        {
            PageFlowCenterList o = new PageFlowCenterList();
            List<TableListHeaderModel> headerModels = new List<TableListHeaderModel>();
            headerModels.Add(new TableListHeaderModel() { Key = "instanceId", Name = Resources("flowinstanceid"), Width = 180, Align = "center", Fixed = true });
            headerModels.Add(new TableListHeaderModel() { Key = "title", Name = Resources("thtitle"), Align = "left", Fixed = true, SorTable = SortType.custom.ToString(), Hyperlink = true });
            headerModels.Add(new TableListHeaderModel() { Key = "dic_FlowId", Name = Resources("thflowname"), Align = "left", Fixed = true, Width = 180, SorTable = SortType.custom.ToString(), });
            headerModels.Add(new TableListHeaderModel() { Key = "dic_Status", Name = Resources("flowstatus"), Width = 140, Align = "center", IsTag = true, SorTable = SortType.custom.ToString() });
            headerModels.Add(new TableListHeaderModel() { Key = "dic_FlowStatus", Name = Resources("flowflowstatus"), Width = 140, Align = "center", IsTag = true, SorTable = SortType.custom.ToString() });
            return headerModels;
        }

        /// <summary>
        /// 根据流程ID获取流程历史
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        public IResponseMessage FlowHistory(long flowId, long versionId, long taskId)
        {
            if (!HavePermission) return ErrorType.BUSINESS_WITHOUT_PERMISSION.Failure(this.CommonText(ErrorType.BUSINESS_WITHOUT_PERMISSION));
            Flow_OutPut flow_OutPut = flowInfoService.BuildViewFlowInfo(flowId, versionId);

            StepView stepView = null;
            ///当任务大于零时
            if (taskId > 0)
            {
                List<Flow_Task> flow_Tasks = business.GetTasksByTaskId(taskId);
                if (flow_Tasks.Any())
                {
                    var cuTask = flow_Tasks.FirstOrDefault(o => o.TaskId == taskId);
                    if (cuTask != null)
                    {
                        if (!flow_OutPut.NodeList.Any(o => o.Meta.ShowHistory && o.Meta.StepId == cuTask.StepId))
                        {
                            flow_Tasks = new List<Flow_Task>();
                        }
                    }
                    BuildHistory(stepView = new StepView(), flow_Tasks, flow_OutPut.NodeList);
                }
            }
            return stepView.OnSucceed();
        }
        private void BuildHistory(StepView stepView, List<Flow_Task> flow_Tasks, List<NodeInfo> nodeInfos)
        {
            var groups = flow_Tasks.Where(o => o.Status != (int)FlowTaskStatus.temp).GroupBy(o => new { o.StepId, o.Sort });
            List<int> sort = new List<int>();

            List<StepDetile> stepDetiles = new List<StepDetile>();
            int i = 0;
            foreach (var item in groups.OrderBy(o => o.Key.Sort))
            {
                var node = nodeInfos.FirstOrDefault(o => o.Meta.StepId == item.Key.StepId);
                StepDetile stepDetile = new StepDetile();
                stepDetile.StepId = node.Meta.StepId;
                stepDetile.Seq = item.Key.Sort;
                stepDetile.Title = node == null ? "" : (Global.IsChinese ? node.Meta.CNName : node.Meta.ENName);
                if (item.Any(o => o.Status == (int)FlowTaskStatus.audit || o.Status == (int)FlowTaskStatus.momentum || (o.Type == (int)FlowTaskType.circulated && o.Status == (int)FlowTaskStatus.unread)))
                {
                    sort.Add(i);
                }
                item.ToList().ForEach(o =>
                {
                    PageFlowCenterList flowCenterList = new PageFlowCenterList()
                    {
                        Dic_ComplatedTime = !o.ProcessTime.HasValue ? "" : (o.ProcessTime.Value + "") == "0001-01-01 00:00:00" ? "" : o.ProcessTime.Value.ToString(GlobalConst.DATE_FORMAT_MINUTES),
                        Dic_ReceivingTime = o.ReceivingTime.ToString(GlobalConst.DATE_FORMAT_MINUTES),
                        Title = o.Title,
                        Dic_Reviewer = !EmpDictionary.ContainsKey(o.Reviewer) ? "" : EmpDictionary[o.Reviewer],
                        Dic_Sender = !EmpDictionary.ContainsKey(o.Sender) ? "" : EmpDictionary[o.Sender],
                        Dic_Status = Resources($"taskstaus{((FlowTaskStatus)o.Status).ToString()}"),
                        Dic_FlowStatus = o.Comment,
                        Status = o.Status
                    };
                    stepDetile.Lists.Add(flowCenterList);
                });
                stepDetiles.Add(stepDetile);
                i++;
            }
            if (sort.Any()) stepView.ActivitStep = sort.FirstOrDefault();
            else stepView.ActivitStep = stepDetiles.Count;
            stepView.Steps = stepDetiles.OrderBy(o => o.Seq).ToList();
        }
    }
}
