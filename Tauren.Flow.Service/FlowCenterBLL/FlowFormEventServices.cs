﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Tauren.Flow.DLL.Base;
using Tauren.Flow.DLL.Field_FormDLL;
using Tauren.Flow.DLL.FlowEngine;
using Tauren.Flow.DLL.Resouces;
using Tauren.Flow.Entity.Config;
using Tauren.Flow.Entity.Const;
using Tauren.Flow.Entity.Enum;
using Tauren.Flow.Entity.Global;
using Tauren.Flow.Entity.Model;
using Tauren.Flow.Entity.Output;
using Tauren.Flow.Infrastructure.Common;
using Tauren.Flow.Infrastructure.Globals;
using Tauren.Flow.Service.ProcessBasicInfoBLL;

namespace Tauren.Flow.Service.FlowCenterBLL
{
    public class FlowFormEventServices : Base.BaseService, IFlowFormEventServices
    {
        private LongCommon longCommon;
        private FilePath filePath;
        public IProcessBasicInfoService processBasicInfoService { get; set; }
        public IField_FormBusiness formBusiness { get; set; }
        public IFlowEnineFormDataDLL formDataDLL { get; set; }
        public FlowFormEventServices(IConfiguration configuration, IBaseDLL baseDLL, IConnectionBLLBase connectionBLL, IMenuResourcesBusiness resourcesBusiness) : base(baseDLL, connectionBLL, resourcesBusiness)
        {
            this.ResourcesInfo = this.GetResources(GlobalConst.RESOURCES_FLAG_MODULE_FLOWCENTER_KEY);
            this.JudePrmission(GlobalConst.RESOURCES_FLAG_MODULE_FLOWCENTER_KEY.ToLower());
            ///获取配置文件中的数据 
            filePath = configuration.Get<ApiVersionsConfig>().FilePath;
            longCommon = new LongCommon();
        }
        /// <summary>
        /// 进行表单数据保存
        /// </summary>
        /// <param name="Parameter"></param>
        /// <returns></returns>
        public IResponseMessage FormSave(FlowRquestParameter Parameter)
        {
            if (!HavePermission) return ErrorType.BUSINESS_WITHOUT_PERMISSION.Failure(this.CommonText(ErrorType.BUSINESS_WITHOUT_PERMISSION));
            bool RepeatSubmit = JudeRepeatSubmit(Parameter);
            if (!RepeatSubmit) return GlobalErrorType.GLOBAL_RES_FLOW_CURRENTDATA_REPART_SUMBIT.Failure(CommonText(GlobalErrorType.GLOBAL_RES_FLOW_CURRENTDATA_REPART_SUMBIT)); 
            List<FlowFieldParameter> fieldParameters = new List<FlowFieldParameter>();
            this.BuildDataModel(fieldParameters, Parameter.Model);
            string ApplyEmpId = this.GetApplyEmpIdbyDataModel(fieldParameters);
            List<FormTable> formTables = formBusiness.GetFormTables(Parameter.FormId);
            List<FlowDataAreaParameter> areaParameters = new List<FlowDataAreaParameter>();
            List<Flow_Instance_Business> flow_Instance_data = new List<Flow_Instance_Business>();
            long instanceId = longCommon.GenerateRandomCode(12);
            Flow_Instance _Instance = new Flow_Instance() { ApplyDate = DateTime.Now, ApplyEmpId = int.Parse(ApplyEmpId), InitiatorEmpId = Global.EmpId, FlowStatus = 0, InstanceId = instanceId, FormId = Parameter.FormId, FormVersionId = Parameter.FormVersionId, InitiatorDate = DateTime.Now, FlowId = Parameter.FlowId, FlowVersionId = Parameter.FlowVersionId };
            FlowTaskInfo CurrentTask = null;
            Flow_Task task = null; 
            try
            {
                if (Parameter.TaskId > 0)
                {
                    ///获取当前任务数据
                    CurrentTask = processBasicInfoService.QueryCurrentFlowTaskInfo(Parameter.TaskId);
                    if (CurrentTask is null)
                    {
                        RemoveRepeatSubmit();
                        return GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_CURRENTTASK_ERROR.Failure(CommonText(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_CURRENTTASK_ERROR));
                    }
                    _Instance = baseDLL.GetBasicListByQuery<Flow_Instance>("instanceid", CurrentTask.InstanceId + "").FirstOrDefault();
                    _Instance.ApplyEmpId = int.Parse(ApplyEmpId);
                    _Instance.InitiatorEmpId = Global.EmpId;
                    _Instance.InitiatorDate = DateTime.Now;
                    task = new Flow_Task() { TaskId = Parameter.TaskId, InstanceId = CurrentTask.InstanceId, GroupId = CurrentTask.GroupId, StepId = CurrentTask.StepId };
                    ///获取流程实例数据
                    List<Flow_Instance_Business> flow_Instance = baseDLL.GetBasicListByQuery<Flow_Instance_Business>("instanceid", CurrentTask.InstanceId + "");
                    ///组装表单数据
                    this.BulidFieldDataMode(areaParameters, fieldParameters.Where(o => o.AreaCode != GlobalConst.FORM_APPLY_EMPLOYEEBASICINFO).ToList(), formTables, ApplyEmpId, flow_Instance);
                }
                else
                {
                    ///获取流程
                    var flow = processBasicInfoService.GetFlowInfo(Parameter.FlowId, Parameter.FlowVersionId);
                    ///组装第一步任务
                    task = GenerateTask(instanceId, flow);
                    ///组装流程实例
                    _Instance.GroupId = task.GroupId;
                    _Instance.ApplyTaskId = task.TaskId;
                    ///组装表单数据
                    this.BulidFieldDataMode(areaParameters, fieldParameters.Where(o => o.AreaCode != GlobalConst.FORM_APPLY_EMPLOYEEBASICINFO).ToList(), formTables, ApplyEmpId);
                }
                ///组装流程实例表单数据映射表
                areaParameters.ForEach(o =>
                {
                    flow_Instance_data.Add(new Flow_Instance_Business() { AutoId = longCommon.GenerateRandomCode(8), Creatdate = DateTime.Now, Creator = Global.EmpId, FlowId = Parameter.FlowId, FlowVersionId = Parameter.FlowVersionId, ForeignKey = o.ForeignKey, ForeignValue = o.ForeignValue, GroupId = _Instance.GroupId, InstanceId = _Instance.InstanceId, MainTable = o.MainTable, PrimaryKey = o.PrimaryKey, PrimaryValue = o.PrimaryValue, TableCode = o.TableCode, Modifdate = DateTime.Now, Modifier = Global.EmpId });
                });
                bool b = true;
                using (TransactionScope ts1 = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions() { IsolationLevel = IsolationLevel.ReadCommitted }))
                {

                    b = this.formDataDLL.FlowFormDataSave(areaParameters);
                    if (CurrentTask == null)
                    {
                        b = formDataDLL.AddBasicBusiness(task) >= 0;
                        b = formDataDLL.AddBasicBusiness(_Instance) >= 0;
                        b = formDataDLL.AddBasicBusiness(flow_Instance_data) >= 0;
                        b = formDataDLL.AddCurrentTask(new List<Flow_Instance_CurrentTask>() {
                                new Flow_Instance_CurrentTask() {
                                InstanceId = instanceId,
                                GroupId=task.GroupId,
                                ProcessType=(int)FlowProcessType.Normal,
                                PrveStepId="0",
                                PrveTaskId=0,
                                CurrentStep=task.StepId,
                                CurrentTaskId=task.TaskId,
                            }
                        }, task.GroupId);
                    }
                    else
                    {
                        b = formDataDLL.UpdateInstanceId(_Instance);
                        b = formDataDLL.DeleteFlowAndFromDataMaspping(_Instance.InstanceId);
                        b = formDataDLL.AddBasicBusiness(flow_Instance_data) >= 0;
                    }
                    if (b)
                    {
                        RemoveRepeatTaskSubmit();
                        ts1.Complete();
                        return BuildExtInstacne(_Instance, task).OnSucceed(this.CommonText(GlobalErrorType.GLOBAL_SAVE_SUCCESSINFO.ToString()));
                    }
                    else
                    {
                        RemoveRepeatSubmit();
                        return GlobalErrorType.GLOBAL_SAVE_FAILURE.OnSucceed(this.CommonText(GlobalErrorType.GLOBAL_SAVE_FAILURE));
                    }

                }
            }
            catch (Exception ex)
            {
                RemoveRepeatSubmit();
                throw ex;
            }

        }
        #region Private
        private ExtInstance BuildExtInstacne(Flow_Instance _Instance, Flow_Task flowTask)
        {
            return new ExtInstance()
            {
                InstanceId = _Instance.InstanceId,
                GroupId = _Instance.GroupId,
                CurrentStep = flowTask.StepId,
                CurrentTaskId = flowTask.TaskId,
            };
        }

        /// <summary>
        /// 生产任务
        /// </summary>
        /// <param name=""></param>
        private Flow_Task GenerateTask(long instanceId, Flow_OutPut flow)
        {
            return new Flow_Task()
            {
                Creatdate = DateTime.Now,
                Creator = Global.EmpId,
                GroupId = longCommon.GenerateRandomCode(12),
                InstanceId = instanceId,
                Modifier = Global.EmpId,
                Modifdate = DateTime.Now,
                PrevStepId = "0",
                PrevTaskId = 0,
                ReceivingTime = DateTime.Now,
                Sender = Global.EmpId,
                Reviewer = Global.EmpId,
                Sort = 0,
                Type = (int)FlowTaskType.task,
                Status = (int)FlowTaskStatus.momentum,
                TaskId = longCommon.GenerateRandomCode(12),
                Title = $"{flow.FlowProperty.CNName}-待发起流程",
                StepId = flow.NodeList.FirstOrDefault(o => o.Meta.Prop == GlobalConst.NODE_INFO_MEAT_START).Meta.StepId
            };
        }
        /// <summary>
        /// 组装表单字段集合
        /// </summary>
        /// <param name="areaParameters"></param>
        /// <param name="fieldParameters"></param>
        /// <param name="formTables"></param>
        /// <param name="applyEmpId"></param>
        private void BulidFieldDataMode(List<FlowDataAreaParameter> areaParameters, List<FlowFieldParameter> fieldParameters, List<FormTable> formTables, string applyEmpId, List<Flow_Instance_Business> flow_Instance = null)
        {
            var groups = fieldParameters.GroupBy(o => new { o.TableCode, o.FlowTableCode, o.MainTable }).Where(o => string.IsNullOrEmpty(o.Key.MainTable)).ToList();
            groups.ForEach(g =>
            {
                #region 主表数据
                var fromTable = formTables.FirstOrDefault(o => o.TableCode == g.Key.TableCode);
                if (fromTable != null)
                {
                    g.GroupBy(o => o.FieldTag).ToList().ForEach(f =>
                    {
                        string autoId = longCommon.GenerateRandomCode(8) + "";
                        if (flow_Instance != null && flow_Instance.Any())
                        {
                            var instance = flow_Instance.FirstOrDefault(o => o.PrimaryValue == g.FirstOrDefault().BusinessId);
                            if (instance != null)
                                autoId = instance.PrimaryValue;
                        }
                        FlowDataAreaParameter areaParameter = new FlowDataAreaParameter()
                        {
                            FlowTableCode = g.Key.FlowTableCode,
                            TableCode = g.Key.TableCode,
                            FieldParameters = f.ToList(),
                            ForeignKey = fromTable.ForeignKey,
                            ForeignValue = fromTable.TableCode == GlobalConst.FORM_EMPLOYEEBASICINFO_TABLENAME ? applyEmpId : autoId,
                            PrimaryKey = GlobalConst.FROM_DATA_PRIMARYKEYF,
                            PrimaryValue = autoId,
                            MainTable = g.Key.MainTable
                        };

                        areaParameter.FieldParameters.Add(new FlowFieldParameter()
                        {
                            ControlsType = ControlType.number.ToString(),
                            FieldCode = GlobalConst.FROM_DATA_PRIMARYKEYF,
                            FieldType = ((int)FieldType.bigint).ToString(),
                            FlowTableCode = g.Key.FlowTableCode,
                            TableCode = g.Key.TableCode,
                            MainTable = g.Key.MainTable,
                            FieldValue = autoId
                        });

                        if (!string.IsNullOrEmpty(applyEmpId))
                            areaParameter.FieldParameters.Add(new FlowFieldParameter()
                            {
                                ControlsType = ControlType.number.ToString(),
                                FieldCode = GlobalConst.FORM_APPLY_EMPLOYEEBASICINFO_EMPID,
                                FieldType = ((int)FieldType.bigint).ToString(),
                                FlowTableCode = g.Key.FlowTableCode,
                                TableCode = g.Key.TableCode,
                                MainTable = g.Key.MainTable,
                                FieldValue = applyEmpId
                            });
                        areaParameters.Add(areaParameter);
                        #region 子表数据 
                        this.BulidChildFieldDataMode(areaParameters, fieldParameters, formTables, autoId, g.Key.TableCode, flow_Instance);
                        #endregion
                    });
                }
                #endregion
            });
        }
        /// <summary>
        /// 组装子表数据
        /// </summary>
        /// <param name="areaParameters"></param>
        /// <param name="fieldParameters"></param>
        /// <param name="formTables"></param>
        /// <param name="primaryTableKeyValue"></param>
        /// <param name="primaryTableCode"></param>
        private void BulidChildFieldDataMode(List<FlowDataAreaParameter> areaParameters, List<FlowFieldParameter> fieldParameters, List<FormTable> formTables, string primaryTableKeyValue, string primaryTableCode, List<Flow_Instance_Business> flow_Instance)
        {
            #region 子表数据
            List<FlowFieldParameter> childFieldParameters = fieldParameters.Where(o => o.MainTable == primaryTableCode).ToList();
            var childGroups = childFieldParameters.GroupBy(o => new { o.TableCode, o.FlowTableCode, o.MainTable }).ToList();
            childGroups.ForEach(g =>
            {
                var childTable = formTables.FirstOrDefault(o => o.TableCode == g.Key.TableCode);
                if (childTable != null)
                {
                    g.GroupBy(o => o.FieldTag).ToList().ForEach(f =>
                    {
                        string childAutoId = longCommon.GenerateRandomCode(8) + "";
                        if (flow_Instance != null && flow_Instance.Any())
                        {
                            var instance = flow_Instance.FirstOrDefault(o => o.PrimaryValue == f.FirstOrDefault().BusinessId);
                            if (instance != null)
                                childAutoId = instance.PrimaryValue;
                        }
                        FlowDataAreaParameter areaParameter = new FlowDataAreaParameter()
                        {
                            FlowTableCode = g.Key.FlowTableCode,
                            TableCode = g.Key.TableCode,
                            FieldParameters = f.ToList(),
                            ForeignKey = childTable.ForeignKey,
                            ForeignValue = primaryTableKeyValue,
                            PrimaryKey = GlobalConst.FROM_DATA_PRIMARYKEYF,
                            PrimaryValue = childAutoId + "",
                            MainTable = g.Key.MainTable
                        };
                        areaParameter.FieldParameters.Add(new FlowFieldParameter()
                        {
                            ControlsType = ControlType.number.ToString(),
                            FieldCode = GlobalConst.FROM_DATA_PRIMARYKEYF,
                            FieldType = ((int)FieldType.bigint).ToString(),
                            FlowTableCode = g.Key.FlowTableCode,
                            TableCode = g.Key.TableCode,
                            MainTable = g.Key.MainTable,
                            FieldValue = childAutoId
                        });
                        areaParameter.FieldParameters.Add(new FlowFieldParameter()
                        {
                            ControlsType = ControlType.text.ToString(),
                            FieldCode = GlobalConst.FROM_DATA_FOREIGNKEYF,
                            FieldType = ((int)FieldType.varchar).ToString(),
                            FlowTableCode = g.Key.FlowTableCode,
                            TableCode = g.Key.TableCode,
                            MainTable = g.Key.MainTable,
                            FieldValue = primaryTableKeyValue
                        });
                        areaParameters.Add(areaParameter);
                    });
                }
            });
            #endregion
        }
        /// <summary>
        /// 将前端传输入特殊格式的数据转化成具体对象数据
        /// </summary>
        /// <param name="fieldParameters"></param>
        /// <param name="models"></param>
        private void BuildDataModel(List<FlowFieldParameter> fieldParameters, Dictionary<string, object> models)
        {
            if (models.Any())
            {
                foreach (KeyValuePair<string, object> item in models)
                {
                    JObject field = item.Value as JObject;
                    ////但记录数据转换
                    if (field.ContainsKey("tag") && "singe".Equals(field["tag"] + "") && field.ContainsKey("data") && field["data"] != null)
                    {
                        long i = longCommon.GenerateRandomCode(4);
                        var buinessId = field.ContainsKey("businessid") ? field["businessid"].ToString() : "0";
                        foreach (var fieldValue in (JObject)field["data"])
                        {
                            FlowFieldParameter flowFieldParameter = fieldValue.Value.ToObject<FlowFieldParameter>();
                            flowFieldParameter.FieldTag = i;
                            flowFieldParameter.BusinessId = buinessId;
                            fieldParameters.Add(flowFieldParameter);
                        }
                    }
                    ///多记录数据转换
                    if (field.ContainsKey("tag") && "many".Equals(field["tag"] + "") && field.ContainsKey("tables") && field["tables"] != null)
                    {
                        foreach (var arr in field["tables"])
                        {
                            long i = longCommon.GenerateRandomCode(4);
                            JObject obj = (JObject)arr;
                            foreach (var s in obj)
                            {
                                var buinessId = s.Key == "businessid" ? s.Value.ToString() : "0"; ;
                                if (s.Key != "rowTag" && s.Key != "businessid")
                                {
                                    FlowFieldParameter flowFieldParameter = s.Value.ToObject<FlowFieldParameter>();
                                    flowFieldParameter.FieldTag = i;
                                    flowFieldParameter.BusinessId = buinessId;
                                    fieldParameters.Add(flowFieldParameter);
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 获取表单申请人ID
        /// </summary>
        /// <param name="fieldParameters"></param>
        /// <returns></returns>
        public string GetApplyEmpIdbyDataModel(List<FlowFieldParameter> fieldParameters)
        {
            string ApplyEmpId = string.Empty;
            FlowFieldParameter applyFieldParameter = fieldParameters.FirstOrDefault(o => o.AreaCode == GlobalConst.FORM_APPLY_EMPLOYEEBASICINFO && o.FieldCode == GlobalConst.FORM_APPLY_EMPLOYEEBASICINFO_CNNAME);
            if (applyFieldParameter != null)
            {
                ApplyEmpId = applyFieldParameter.FieldValues.FirstOrDefault().Value;
            }
            else
            {
                FlowFieldParameter empFieldParameter = fieldParameters.FirstOrDefault(o => o.AreaCode != GlobalConst.FORM_APPLY_EMPLOYEEBASICINFO && o.TableCode == GlobalConst.FORM_EMPLOYEEBASICINFO_TABLENAME && o.FieldCode == GlobalConst.FORM_APPLY_EMPLOYEEBASICINFO_CNNAME);
                if (empFieldParameter != null)
                {
                    ApplyEmpId = empFieldParameter.FieldValues.FirstOrDefault().Value;
                }
            }
            return ApplyEmpId;
        }

        public IResponseMessage ClearRepeatKey(string SecretKey, string TaskId)
        {
            RedisClient.HashDelete(GlobalConst.FLOW_FORM_TASK_REPEAT_SUBMIT, $"from_{SecretKey}");
            RedisClient.HashDelete(GlobalConst.FLOW_FORM_TASK_REPEAT_SUBMIT, $"flow_{TaskId}");
            RedisClient.HashDelete(GlobalConst.FLOW_FORM_TASK_REPEAT_SUBMIT, $"run_flow_{TaskId}");
            RedisClient.HashDelete(GlobalConst.FLOW_FORM_TASK_REPEAT_SUBMIT, $"termination_flow_{TaskId}");
            RedisClient.HashDelete(GlobalConst.FLOW_FORM_TASK_REPEAT_SUBMIT, $"recyclin_flow_{TaskId}");
            RedisClient.HashDelete(GlobalConst.FLOW_FORM_TASK_REPEAT_SUBMIT, $"back_flow_{TaskId}");
            RedisClient.HashDelete(GlobalConst.FLOW_FORM_TASK_REPEAT_SUBMIT, $"read_flow_{TaskId}");
            return true.OnSucceed();
        }
        #endregion
    }
}
