﻿using System;
using System.Collections.Generic;
using System.Text;
using Tauren.Flow.DLL.Base;
using Tauren.Flow.DLL.Resouces;
using Tauren.Flow.Entity.Config;
using Tauren.Flow.Entity.Const;
using Tauren.Flow.Entity.Enum;
using Tauren.Flow.Entity.Global;
using Tauren.Flow.Entity.Output;
using Tauren.Flow.Infrastructure.Globals;
using Tauren.Flow.Service.ProcessBasicInfoBLL;
using System.Linq;
using Tauren.Flow.DLL.Field_FormDLL;
using Tauren.Flow.Entity.Model;
using Tauren.Flow.Infrastructure.Common;
using Microsoft.Extensions.Configuration;
using Tauren.Flow.DLL.FlowCenterDLL;

namespace Tauren.Flow.Service.FlowCenterBLL
{

    /// <summary>
    /// 流程中心
    /// </summary>
    public class FlowFromService : Base.BaseService, IFlowFromService
    {
        public IProcessBasicInfoService processBasicInfoService { get; set; }
        public IField_FormBusiness formBusiness { get; set; }
        private LongCommon longCommon;
        private FilePath filePath;
        public IFlowCenterBusiness business { get; set; }
        public FlowFromService(IConfiguration configuration, IBaseDLL baseDLL, IConnectionBLLBase connectionBLL, IMenuResourcesBusiness resourcesBusiness) : base(baseDLL, connectionBLL, resourcesBusiness)
        {
            this.ResourcesInfo = this.GetResources(GlobalConst.RESOURCES_FLAG_MODULE_FLOWCENTER_KEY);
            this.JudePrmission(GlobalConst.RESOURCES_FLAG_MODULE_FLOWCENTER_KEY.ToLower());
            ///获取配置文件中的数据 
            filePath = configuration.Get<ApiVersionsConfig>().FilePath;
            longCommon = new LongCommon();
        }

        public IResponseMessage FlowFormFieldConfig(FlowParameter Paramter)
        {
            if (!HavePermission) return ErrorType.BUSINESS_WITHOUT_PERMISSION.Failure(this.CommonText(ErrorType.BUSINESS_WITHOUT_PERMISSION));
            FlowTaskInfo currentTask = null;
            if (Paramter.TaskId > 0)
            {
                currentTask = processBasicInfoService.QueryCurrentFlowTaskInfo(Paramter.TaskId);
                if (currentTask is null) return GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_CURRENTTASK_ERROR.Failure(CommonText(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_CURRENTTASK_ERROR));
                Paramter.StepId = currentTask.StepId;
            }
            Flow_OutPut flow_OutPut = processBasicInfoService.GetFlowInfo(Paramter.FlowId, Paramter.FlowVersionId);
            Result<FormConfig> result = processBasicInfoService.GetFormFieldInfo(Paramter, flow_OutPut);
            if (result.Success)
            {
                this.BuilderViewButtons(result.Data, currentTask, Paramter, flow_OutPut);
                return result.Data.OnSucceed(this.CommonText(result.Code));
            }
            else
            {
                return result.Code.Failure(this.CommonText(result.Code));
            }
        }

        public IResponseMessage FlowFromData(FlowParameter Paramter)
        {
            FlowTaskInfo currentTask = null;
            if (Paramter.TaskId > 0)
            {
                currentTask = processBasicInfoService.QueryCurrentFlowTaskInfo(Paramter.TaskId);
                if (currentTask is null) return GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_CURRENTTASK_ERROR.Failure(CommonText(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_CURRENTTASK_ERROR));
                Paramter.StepId = currentTask.StepId;
            }
            Flow_OutPut flow_OutPut = processBasicInfoService.GetFlowInfo(Paramter.FlowId, Paramter.FlowVersionId);
            Result<FormConfig> result = processBasicInfoService.GetFormFieldInfo(Paramter, flow_OutPut);
            if (result.Success)
            {
                FormConfig config = result.Data;
                List<FormTable> formTables = formBusiness.GetFormTables(config.FormId);
                Dictionary<string, object> valuePairs = new Dictionary<string, object>();
                bool res = true;
                this.BuilderViewApplyInfo(valuePairs, config, Paramter, currentTask, out res);
                if (!res) return ErrorType.BUSINESS_EMPLOYEE_DB_NOT_FOUND.Failure(CommonText(ErrorType.BUSINESS_EMPLOYEE_DB_NOT_FOUND));
                this.BuilderViewBusinessInfo(valuePairs, config, Paramter, currentTask, formTables);
                return valuePairs.OnSucceed();
            }
            else
            {
                return result.Code.Failure(this.CommonText(result.Code));
            }
        }



        #region private 
        /// <summary>
        /// 组装其他字段
        /// </summary>
        /// <param name="valuePairs"></param>
        /// <param name="field"></param>
        /// <param name="DefaultValue"></param>
        /// <param name="area"></param>
        /// <param name="flowTableCode"></param>
        private void BuilderOther(Dictionary<string, object> valuePairs, Form_Area_Fields field, string DefaultValue, Form_Area area, string flowTableCode = "")
        {
            DefaultValue = string.IsNullOrEmpty(DefaultValue) ? field.DefaultValue : DefaultValue;
            if (field.ControlsType != ControlType.employee.ToString() && field.ControlsType != ControlType.organizational.ToString() && field.ControlsType != ControlType.position.ToString() &&
                field.ControlsType != ControlType.parameter.ToString() &&
                field.ControlsType != ControlType.radio.ToString() && field.ControlsType != ControlType.avatar.ToString())
                valuePairs[field.FieldKey] = new FlowFieldParameter
                {
                    FieldCode = field.FieldCode,
                    ControlsType = field.ControlsType,
                    FieldType = field.Type,
                    AreaCode = area.AreaCode,
                    TableCode = area.TableCode,
                    FieldValue = DefaultValue,
                    FieldValues = new List<FieldValues>(),
                    FlowTableCode = flowTableCode,
                    FieldTag = longCommon.GenerateRandomCode(),
                    MainTable = area.MainTable
                };
        }
        private void BuilderAvatar(Dictionary<string, object> valuePairs, Form_Area_Fields field, int empId, Form_Area area)
        {
            if (field.ControlsType == ControlType.avatar.ToString())
            {
                ExPhotoInfo photo = formBusiness.SingePhoto(empId);
                valuePairs[field.FieldKey] = new FlowFieldParameter
                {
                    FieldCode = field.FieldCode,
                    ControlsType = field.ControlsType,
                    FieldType = field.Type,
                    AreaCode = area.AreaCode,
                    TableCode = area.TableCode,
                    FieldValue = photo == null ? "" : $"{filePath.FielsUrl}{photo.FileUri}",
                    FieldTag = longCommon.GenerateRandomCode(),
                    MainTable = area.MainTable
                };
            }
        }
        /// <summary>
        /// 组装员工字段值
        /// </summary>
        /// <param name="valuePairs"></param>
        /// <param name="field"></param>
        private void BuilderEmp(Dictionary<string, object> valuePairs, Form_Area_Fields field, string DefaultValue, Form_Area area, string flowTableCode = "")
        {
            var empids = new string[] { };
            var FieldValues = new List<FieldValues>();
            DefaultValue = string.IsNullOrEmpty(DefaultValue) ? field.DefaultValue : DefaultValue;
            ///员工
            if (field.ControlsType == ControlType.employee.ToString())
            {
                if (!string.IsNullOrEmpty(DefaultValue) && DefaultValue != "0")
                {
                    empids = DefaultValue.Split(new string[] { GlobalConst.SEPARATOR_COMMA }, StringSplitOptions.RemoveEmptyEntries);
                    FieldValues = empids.Join(this.EmployeesList, a => a, b => b.EmpId + "", (a, b) =>
                  {
                      FieldValues res = new FieldValues { Value = a, CNName = b.CNName, ENName = b.ENName };
                      return res;
                  }).ToList();
                }
                valuePairs[field.FieldKey] = new FlowFieldParameter
                {
                    FieldCode = field.FieldCode,
                    ControlsType = field.ControlsType,
                    FieldType = field.Type,
                    AreaCode = area.AreaCode,
                    TableCode = area.TableCode,
                    FieldValue = empids,
                    FieldValues = FieldValues,
                    FlowTableCode = flowTableCode,
                    FieldTag = longCommon.GenerateRandomCode(),
                    MainTable = area.MainTable
                };
            }

        }
        /// <summary>
        /// 组装部门字段值
        /// </summary>
        /// <param name="valuePairs"></param>
        /// <param name="field"></param>
        private void BuilderOrg(Dictionary<string, object> valuePairs, Form_Area_Fields field, string DefaultValue, Form_Area area, string flowTableCode = "")
        {
            var orgids = new string[] { };
            DefaultValue = string.IsNullOrEmpty(DefaultValue) ? field.DefaultValue : DefaultValue;
            var FieldValues = new List<FieldValues>();

            ///员工
            if (field.ControlsType == ControlType.organizational.ToString())
            {
                if (!string.IsNullOrEmpty(DefaultValue) && DefaultValue != "0")
                {
                    orgids = DefaultValue.Split(new string[] { GlobalConst.SEPARATOR_COMMA }, StringSplitOptions.RemoveEmptyEntries);
                    FieldValues = orgids.Join(this.OrganizationsList, a => a, b => b.OrgId + "", (a, b) =>
                  {
                      FieldValues res = new FieldValues { Value = a, CNName = b.CNName, ENName = b.ENName, OrgCode = b.OrgCode };
                      return res;
                  }).ToList();
                }
                valuePairs[field.FieldKey] = new FlowFieldParameter
                {
                    FieldCode = field.FieldCode,
                    ControlsType = field.ControlsType,
                    FieldType = field.Type,
                    AreaCode = area.AreaCode,
                    TableCode = area.TableCode,
                    FieldValue = orgids,
                    FieldValues = FieldValues,
                    FlowTableCode = flowTableCode,
                    FieldTag = longCommon.GenerateRandomCode(),
                    MainTable = area.MainTable
                };
            }

        }
        /// <summary>
        /// 组装部门字段值
        /// </summary>
        /// <param name="valuePairs"></param>
        /// <param name="field"></param>
        private void BuilderUniInfo(Dictionary<string, object> valuePairs, Form_Area_Fields field, string DefaultValue, Form_Area area, string flowTableCode = "")
        {
            var unitids = new string[] { };
            DefaultValue = string.IsNullOrEmpty(DefaultValue) ? field.DefaultValue : DefaultValue;
            var FieldValues = new List<FieldValues>();

            ///员工
            if (field.ControlsType == ControlType.unit.ToString())
            {
                if (!string.IsNullOrEmpty(DefaultValue) && DefaultValue != "0")
                {
                    unitids = DefaultValue.Split(new string[] { GlobalConst.SEPARATOR_COMMA }, StringSplitOptions.RemoveEmptyEntries);
                    FieldValues = unitids.Join(this.UnitInofList, a => a, b => b.UnitId + "", (a, b) =>
                    {
                        FieldValues res = new FieldValues { Value = a, CNName = b.CNName, ENName = b.ENName, OrgCode = b.UnitCode };
                        return res;
                    }).ToList();
                }
                valuePairs[field.FieldKey] = new FlowFieldParameter
                {
                    FieldCode = field.FieldCode,
                    ControlsType = field.ControlsType,
                    FieldType = field.Type,
                    AreaCode = area.AreaCode,
                    TableCode = area.TableCode,
                    FieldValue = unitids,
                    FieldValues = FieldValues,
                    FlowTableCode = flowTableCode,
                    FieldTag = longCommon.GenerateRandomCode(),
                    MainTable = area.MainTable
                };
            }

        }

        /// <summary>
        /// 组装职位字段值
        /// </summary>
        /// <param name="valuePairs"></param>
        /// <param name="field"></param>
        private void BuilderPosition(Dictionary<string, object> valuePairs, Form_Area_Fields field, string DefaultValue, Form_Area area, string flowTableCode = "")
        {
            var postitionid = new string[] { };
            var FieldValues = new List<FieldValues>();
            DefaultValue = string.IsNullOrEmpty(DefaultValue) ? field.DefaultValue : DefaultValue;
            ///员工
            if (field.ControlsType == ControlType.position.ToString())
            {
                if (!string.IsNullOrEmpty(DefaultValue) && DefaultValue != "0")
                {
                    postitionid = DefaultValue.Split(new string[] { GlobalConst.SEPARATOR_COMMA }, StringSplitOptions.RemoveEmptyEntries);
                    FieldValues = postitionid.Join(this.PositionsList, a => a, b => b.PositionId + "", (a, b) =>
                  {
                      FieldValues res = new FieldValues { Value = a, CNName = b.CNName, ENName = b.ENName, PositionCode = b.PositionCode };
                      return res;
                  }).ToList();
                }
                valuePairs[field.FieldKey] = new FlowFieldParameter
                {
                    FieldCode = field.FieldCode,
                    ControlsType = field.ControlsType,
                    FieldType = field.Type,
                    AreaCode = area.AreaCode,
                    TableCode = area.TableCode,
                    FieldValue = postitionid,
                    FieldValues = FieldValues,
                    FlowTableCode = flowTableCode,
                    FieldTag = longCommon.GenerateRandomCode(),
                    MainTable = area.MainTable
                };
            }

        }
        /// <summary>
        /// 组装常用参数字段值
        /// </summary>
        /// <param name="valuePairs"></param>
        /// <param name="field"></param>
        private void BuilderParameter(Dictionary<string, object> valuePairs, Form_Area_Fields field, string DefaultValue, Form_Area area, string flowTableCode = "")
        {
            var paramtersid = new string[] { };
            var FieldValues = new List<FieldValues>();
            DefaultValue = string.IsNullOrEmpty(DefaultValue) ? field.DefaultValue : DefaultValue;
            ///常用参数
            if (field.ControlsType == ControlType.parameter.ToString())
            {
                if (!string.IsNullOrEmpty(DefaultValue))
                {
                    paramtersid = DefaultValue.Split(new string[] { GlobalConst.SEPARATOR_COMMA }, StringSplitOptions.RemoveEmptyEntries);

                    List<ParameterValue> values = this.baseDLL.GetBasicListByQuery<ParameterValue>("maincode", $"'{field.ControlSouces}'");
                    FieldValues = paramtersid.Join(values, a => a, b => b.Code + "", (a, b) =>
                  {
                      FieldValues res = new FieldValues { Value = a, CNName = b.CNName, ENName = b.ENName, ParameterCode = b.Code };
                      return res;
                  }).ToList();
                }
                valuePairs[field.FieldKey] = new FlowFieldParameter
                {
                    FieldCode = field.FieldCode,
                    ControlsType = field.ControlsType,
                    FieldType = field.Type,
                    AreaCode = area.AreaCode,
                    TableCode = area.TableCode,
                    FieldValue = paramtersid,
                    FieldValues = FieldValues,
                    FlowTableCode = flowTableCode,
                    FieldTag = longCommon.GenerateRandomCode(),
                    MainTable = area.MainTable
                };
            }
        }
        /// <summary>
        /// 组装常用参数字段值
        /// </summary>
        /// <param name="valuePairs"></param>
        /// <param name="field"></param>
        private void BuilderRadio(Dictionary<string, object> valuePairs, Form_Area_Fields field, string DefaultValue, Form_Area area, string flowTableCode = "")
        {
            DefaultValue = string.IsNullOrEmpty(DefaultValue) ? field.DefaultValue : DefaultValue;
            ///常用参数
            if (field.ControlsType == ControlType.radio.ToString())
            {
                valuePairs[field.FieldKey] = new FlowFieldParameter
                {
                    FieldCode = field.FieldCode,
                    ControlsType = field.ControlsType,
                    FieldType = field.Type,
                    AreaCode = area.AreaCode,
                    TableCode = area.TableCode,
                    FieldValue = string.IsNullOrEmpty(DefaultValue) ? false : DefaultValue == "1",
                    FlowTableCode = flowTableCode,
                    FieldTag = longCommon.GenerateRandomCode(),
                    MainTable = area.MainTable
                };
            }
        }
        /// <summary>
        /// 组装常用参数字段值
        /// </summary>
        /// <param name="valuePairs"></param>
        /// <param name="field"></param>
        private void BuilderNumber(Dictionary<string, object> valuePairs, Form_Area_Fields field, string DefaultValue, Form_Area area, string flowTableCode = "")
        {
            DefaultValue = string.IsNullOrEmpty(DefaultValue) ? field.DefaultValue : DefaultValue;
            ///常用参数
            if (field.ControlsType == ControlType.number.ToString())
            {
                if (field.Type == FieldType.bigint.ToString())
                {
                    valuePairs[field.FieldKey] = new FlowFieldParameter
                    {
                        FieldCode = field.FieldCode,
                        ControlsType = field.ControlsType,
                        FieldType = field.Type,
                        AreaCode = area.AreaCode,
                        TableCode = area.TableCode,
                        FieldValue = string.IsNullOrEmpty(DefaultValue) ? (object)null : long.Parse(DefaultValue),
                        FlowTableCode = flowTableCode,
                        FieldTag = longCommon.GenerateRandomCode(),
                        MainTable = area.MainTable
                    };
                }
                if (field.Type == FieldType.iint.ToString())
                {
                    valuePairs[field.FieldKey] = new FlowFieldParameter
                    {
                        FieldCode = field.FieldCode,
                        ControlsType = field.ControlsType,
                        FieldType = field.Type,
                        AreaCode = area.AreaCode,
                        TableCode = area.TableCode,
                        FieldValue = string.IsNullOrEmpty(DefaultValue) ? (object)null : int.Parse(DefaultValue),
                        FlowTableCode = flowTableCode,
                        FieldTag = longCommon.GenerateRandomCode(),
                        MainTable = area.MainTable
                    };
                }
                if (field.Type == FieldType.doubled.ToString())
                {
                    valuePairs[field.FieldKey] = new FlowFieldParameter
                    {
                        FieldCode = field.FieldCode,
                        ControlsType = field.ControlsType,
                        FieldType = field.Type,
                        AreaCode = area.AreaCode,
                        TableCode = area.TableCode,
                        FieldValue = string.IsNullOrEmpty(DefaultValue) ? (object)null : double.Parse(DefaultValue),
                        FlowTableCode = flowTableCode,
                        FieldTag = longCommon.GenerateRandomCode(),
                        MainTable = area.MainTable
                    };
                }
            }
        }

        /// <summary>
        /// 组装按钮
        /// </summary>
        private void BuilderViewButtons(FormConfig config, FlowTaskInfo currentTask, FlowParameter paramter, Flow_OutPut flow_OutPut)
        {
            if (paramter.ApprovalType == ApprovalType.view)
            {
                config.Buttons = config.Buttons.Where(o => GlobalConst.ViewBtns.Any(v => v == o.BtnCode)).ToList();
                if (currentTask.Type == (int)FlowTaskType.cc && currentTask.Status != (int)FlowTaskStatus.unread)
                    config.Buttons = config.Buttons.Where(o => GlobalConst.ViewCCBtns.Any(v => v == o.BtnCode)).ToList();
                else
                    config.Buttons = config.Buttons.Where(o => GlobalConst.ViewBtns.Any(v => v == o.BtnCode)).ToList();
                Dictionary<long, bool> recycleInfos = business.GetTaskRecycleInfo(new long[] { currentTask.TaskId });
                if (flow_OutPut.FlowProperty.Recycled != null && flow_OutPut.FlowProperty.Recycled.Value && currentTask.FlowStatus == (int)FlowInstanceStatus.ongoing)
                    config.Buttons.Add(new Flow_Buttons_Put() { BtnCode = "recycle", CNName = "收回", ENName = "Take back", Icon = "el-icon-refresh", Style = "danger", ToolTips = "收回" });
                else
                {
                    if (recycleInfos.Any() && recycleInfos.ContainsKey(currentTask.TaskId) && recycleInfos[currentTask.TaskId])
                    {
                        config.Buttons.Add(new Flow_Buttons_Put() { BtnCode = "recycle", CNName = "收回", ENName = "Take back", Icon = "el-icon-refresh", Style = "danger", ToolTips = "收回" });
                    }
                }
            }
            else
            {
                config.Buttons = config.Buttons.Where(o => o.BtnCode != "export").ToList();
            }
        }

        private void BuilderViewBusinessInfo(Dictionary<string, object> valuePairs, FormConfig config, FlowParameter Paramter, FlowTaskInfo currentTask, List<FormTable> formTables)
        {
            IDictionary<string, List<dynamic>> pairs = this.BuildFlowInstanceData(config, Paramter, currentTask, formTables);
            config.Areas.Where(area => string.IsNullOrEmpty(area.MainTable) && area.AreaCode != GlobalConst.FORM_APPLY_EMPLOYEEBASICINFO).ToList().ForEach(ar =>
            {
                FormTable table = formTables.FirstOrDefault(o => o.TableCode == ar.TableCode);

                ///不是多记录表
                if (!ar.Records)
                {
                    IDictionary<string, object> business = pairs.ContainsKey(table.FlowTableCode) ? pairs[table.FlowTableCode].FirstOrDefault() as IDictionary<string, object> : new Dictionary<string, object>();
                    Dictionary<string, object> mainPairs1 = new Dictionary<string, object>();
                    this.BuilderViewInstanceData(mainPairs1, ar, business, table);
                    Dictionary<string, object> airs1 = new Dictionary<string, object>();
                    airs1["tag"] = "singe";
                    airs1["data"] = mainPairs1;
                    airs1["maintable"] = ar.MainTable;
                    airs1["tablecode"] = ar.TableCode;
                    airs1["flowtablecode"] = table.FlowTableCode;
                    airs1["businessid"] = business == null || !business.Any() ? "0" : business[GlobalConst.FROM_DATA_PRIMARYKEYF] + "";
                    valuePairs[ar.AreaCode] = airs1;
                    ///子表数据
                    config.Areas.Where(o => o.MainTable == table.TableCode).ToList().ForEach(child =>
                    {
                        FormTable childTable = formTables.FirstOrDefault(o => o.TableCode == child.TableCode);
                        ///不是多记录表
                        if (!child.Records)
                        {
                            IDictionary<string, object> childBusiness = pairs.ContainsKey(childTable.FlowTableCode) ? pairs[childTable.FlowTableCode].FirstOrDefault() as IDictionary<string, object> : new Dictionary<string, object>();
                            Dictionary<string, object> valuePairs1 = new Dictionary<string, object>();
                            this.BuilderViewInstanceData(valuePairs1, ar, childBusiness, childTable);
                            Dictionary<string, object> childPairs1 = new Dictionary<string, object>();
                            childPairs1["tag"] = "singe";
                            childPairs1["data"] = valuePairs1;
                            childPairs1["maintable"] = child.MainTable;
                            childPairs1["tablecode"] = child.TableCode;
                            childPairs1["flowtablecode"] = childTable.FlowTableCode;
                            airs1["businessid"] = business == null || !business.Any() ? "0" : business[GlobalConst.FROM_DATA_PRIMARYKEYF] + "";
                            valuePairs[child.AreaCode] = childPairs1;
                        }
                        else
                        {
                            this.BuliderViewRecordsData(pairs, childTable, valuePairs, child);
                        }
                    });
                }
                else
                {
                    this.BuliderViewRecordsData(pairs, table, valuePairs, ar);
                }
            });
        }
        /// <summary>
        /// 组装数据
        /// </summary>
        /// <param name="valuePairs"></param>
        /// <param name="area"></param>
        /// <param name="business"></param>
        /// <param name="table"></param>
        public void BuilderViewInstanceData(Dictionary<string, object> valuePairs, Form_Area area, IDictionary<string, object> business, FormTable table)
        {
            area.Fields.ForEach(field =>
            {
                this.BuilderEmp(valuePairs, field, business == null || !business.ContainsKey(field.FieldCode) ? "" : business[field.FieldCode == GlobalConst.FORM_APPLY_EMPLOYEEBASICINFO_CNNAME ? GlobalConst.FORM_APPLY_EMPLOYEEBASICINFO_EMPID : field.FieldCode] + "", area, table.FlowTableCode);
                this.BuilderOrg(valuePairs, field, business == null || !business.ContainsKey(field.FieldCode) ? "" : business[field.FieldCode] + "", area, table.FlowTableCode);
                this.BuilderPosition(valuePairs, field, business == null || !business.ContainsKey(field.FieldCode) ? "" : business[field.FieldCode] + "", area, table.FlowTableCode);
                this.BuilderParameter(valuePairs, field, business == null || !business.ContainsKey(field.FieldCode) ? "" : business[field.FieldCode] + "", area, table.FlowTableCode);
                this.BuilderNumber(valuePairs, field, business == null || !business.ContainsKey(field.FieldCode) ? "" : business[field.FieldCode] + "", area, table.FlowTableCode);
                this.BuilderRadio(valuePairs, field, business == null || !business.ContainsKey(field.FieldCode) ? "" : business[field.FieldCode] + "", area, table.FlowTableCode);
                this.BuilderOther(valuePairs, field, business == null || !business.ContainsKey(field.FieldCode) ? "" : business[field.FieldCode] + "", area, table.FlowTableCode);
                this.BuilderUniInfo(valuePairs, field, business == null || !business.ContainsKey(field.FieldCode) ? "" : business[field.FieldCode] + "", area, table.FlowTableCode);
            });
        }
        /// <summary>
        /// 组装显示申请人
        /// </summary>
        private void BuilderViewApplyInfo(Dictionary<string, object> resultPairs, FormConfig config, FlowParameter Paramter, FlowTaskInfo currentTask, out bool errrorCode)
        {
            errrorCode = true;
            ///判断申请
            string empId = Global.EmpId + "";
            if (Paramter.ApprovalType == ApprovalType.apply)
            {
                ///更换申请人
                if (Paramter.ApplyEmpId > 0) empId = Paramter.ApplyEmpId + "";
                else empId = currentTask == null ? Global.EmpId + "" : currentTask.ApplyEmpId + "";
            }
            else
                empId = currentTask.ApplyEmpId + "";
            var employee = this.baseDLL.GetBasicListByQuery(GlobalConst.FORM_APPLY_EMPLOYEEBASICINFO_EMPID, empId, GlobalConst.FORM_EMPLOYEEBASICINFO_TABLENAME).FirstOrDefault() as IDictionary<string, object>;
            if (employee == null || (employee["enable"] + "") == "0" || (employee["havedelete"] + "") == "1") errrorCode = false;
            else
            {
                var applyinfos = config.Areas.FirstOrDefault(o => o.AreaCode == GlobalConst.FORM_APPLY_EMPLOYEEBASICINFO);
                if (applyinfos != null)
                {
                    Dictionary<string, object> valuePairs = new Dictionary<string, object>();
                    applyinfos.Fields.ForEach(field =>
                    {
                        this.BuilderEmp(valuePairs, field, !employee.ContainsKey(field.FieldCode) ? "" : employee[field.FieldCode == GlobalConst.FORM_APPLY_EMPLOYEEBASICINFO_CNNAME ? GlobalConst.FORM_APPLY_EMPLOYEEBASICINFO_EMPID : field.FieldCode] + "", applyinfos);
                        this.BuilderOrg(valuePairs, field, !employee.ContainsKey(field.FieldCode) ? "" : employee[field.FieldCode] + "", applyinfos);
                        this.BuilderPosition(valuePairs, field, !employee.ContainsKey(field.FieldCode) ? "" : employee[field.FieldCode] + "", applyinfos);
                        this.BuilderParameter(valuePairs, field, !employee.ContainsKey(field.FieldCode) ? "" : employee[field.FieldCode] + "", applyinfos);
                        this.BuilderNumber(valuePairs, field, !employee.ContainsKey(field.FieldCode) ? "" : employee[field.FieldCode] + "", applyinfos);
                        this.BuilderRadio(valuePairs, field, !employee.ContainsKey(field.FieldCode) ? "" : employee[field.FieldCode] + "", applyinfos);
                        this.BuilderOther(valuePairs, field, !employee.ContainsKey(field.FieldCode) ? "" : employee[field.FieldCode] + "", applyinfos);
                        this.BuilderUniInfo(valuePairs, field, !employee.ContainsKey(field.FieldCode) ? "" : employee[field.FieldCode] + "", applyinfos);
                        this.BuilderAvatar(valuePairs, field, int.Parse(empId), applyinfos);

                    });
                    Dictionary<string, object> valuePairs1 = new Dictionary<string, object>();
                    valuePairs1["tag"] = "singe";
                    valuePairs1["data"] = valuePairs;
                    valuePairs1["maintable"] = "";
                    valuePairs1["tablecode"] = GlobalConst.FORM_EMPLOYEEBASICINFO_TABLENAME;
                    valuePairs1["flowtablecode"] = "epmloyeeapply";
                    resultPairs[applyinfos.AreaCode] = valuePairs1;
                }
            }
        }
        /// <summary>
        /// /组装流程实例数据
        /// </summary>
        /// <param name="valuePairs"></param>
        /// <param name="config"></param>
        /// <param name="Paramter"></param>
        /// <param name="currentTask"></param>
        /// <returns></returns>
        private Dictionary<string, List<dynamic>> BuildFlowInstanceData(FormConfig config, FlowParameter Paramter, FlowTaskInfo currentTask, List<FormTable> formTables)
        {
            Dictionary<string, List<dynamic>> datas = new Dictionary<string, List<dynamic>>();
            Dictionary<string, string> Sql = new Dictionary<string, string>();
            ///取出所有主表
            config.Areas.Where(o => string.IsNullOrEmpty(o.MainTable) && o.AreaCode != GlobalConst.FORM_APPLY_EMPLOYEEBASICINFO).ToList().ForEach(area =>
              {
                  FormTable formTable = formTables.FirstOrDefault(table => table.TableCode == area.TableCode);
                  ///申请步骤
                  if (Paramter.ApprovalType == ApprovalType.apply)
                  {
                      if (Paramter.ApplyEmpId > 0 || currentTask == null)
                      {
                          Form_Area employee = config.Areas.FirstOrDefault(o => o.AreaCode != GlobalConst.FORM_APPLY_EMPLOYEEBASICINFO && o.TableCode == GlobalConst.FORM_EMPLOYEEBASICINFO_TABLENAME);
                          if (employee != null)
                          {
                              FormTable FormTable = formTables.FirstOrDefault(ctable => ctable.TableCode == employee.TableCode);
                              Sql[FormTable.FlowTableCode] = $"select * from {FormTable.TableCode} where empid={(Paramter.ApplyEmpId > 0 ? Paramter.ApplyEmpId : Global.EmpId)}";
                              ///子表数据
                              config.Areas.Where(o => o.MainTable == GlobalConst.FORM_EMPLOYEEBASICINFO_TABLENAME).ToList().ForEach(area =>
                                {
                                    FormTable chileTable = formTables.FirstOrDefault(o => o.TableCode == area.TableCode);
                                    Sql[chileTable.FlowTableCode] = $"select * from {chileTable.TableCode} where foreignkey='{(Paramter.ApplyEmpId > 0 ? Paramter.ApplyEmpId : Global.EmpId)}'";
                                });
                          }
                          else
                          {
                              this.CreateCurrentTaskDataSql(Sql, config, currentTask, formTables, formTable, area);
                          }
                      }
                      else
                      {
                          this.CreateCurrentTaskDataSql(Sql, config, currentTask, formTables, formTable, area);
                      }
                  }
                  else
                  {
                      this.CreateCurrentTaskDataSql(Sql, config, currentTask, formTables, formTable, area);
                  }
              });
            return formBusiness.GetFlowFormData(Sql);
        }
        /// <summary>
        /// 组装有数据的sql语句
        /// </summary>
        /// <param name="Sql"></param>
        /// <param name="config"></param>
        /// <param name="currentTask"></param>
        /// <param name="formTables"></param>
        /// <param name="formTable"></param>
        /// <param name="area"></param>
        private void CreateCurrentTaskDataSql(Dictionary<string, string> Sql, FormConfig config, FlowTaskInfo currentTask, List<FormTable> formTables, FormTable formTable, Form_Area area)
        {
            var flow_Instance = baseDLL.GetBasicListByQuery<Flow_Instance_Business>("instanceid", currentTask == null ? "0" : currentTask.InstanceId + "");
            if (flow_Instance.Any())
            {
                string pararSql = $"{flow_Instance.Where(o => o.TableCode == formTable.TableCode).FirstOrDefault().PrimaryKey} in( {string.Join(",", flow_Instance.Where(o => o.TableCode == formTable.TableCode).Select(o => $"'{o.PrimaryValue}'"))})";
                Sql[formTable.FlowTableCode] = $"select * from {formTable.FlowTableCode} where {pararSql}";
                config.Areas.Where(a => a.MainTable == area.TableCode).ToList().ForEach(ar =>
                {
                    FormTable childFormTable = formTables.FirstOrDefault(ctable => ctable.TableCode == ar.TableCode);
                    string childPararSql = $"{flow_Instance.Where(o => o.TableCode == childFormTable.TableCode).FirstOrDefault().PrimaryKey} in( {string.Join(",", flow_Instance.Where(o => o.TableCode == childFormTable.TableCode).Select(o => $"'{o.PrimaryValue}'"))})";
                    Sql[childFormTable.FlowTableCode] = $"select * from {childFormTable.FlowTableCode} where {childPararSql}";
                });
            }
        }

        /// <summary>
        /// 组装多记录数据
        /// </summary>
        /// <param name="pairs"></param>
        /// <param name="table"></param>
        /// <param name="valuePairs"></param>
        /// <param name="ar"></param>
        private void BuliderViewRecordsData(IDictionary<string, List<dynamic>> pairs, FormTable table, Dictionary<string, object> valuePairs, Form_Area ar)
        {
            List<Dictionary<string, object>> RecordDatas = new List<Dictionary<string, object>>();
            if (pairs.ContainsKey(table.FlowTableCode) && pairs[table.FlowTableCode].Any())
                pairs[table.FlowTableCode].ForEach(kv =>
                {
                    IDictionary<string, object> business = kv as IDictionary<string, object>;
                    Dictionary<string, object> value = new Dictionary<string, object>();
                    this.BuilderViewInstanceData(value, ar, business, table);
                    value.Add("rowTag", longCommon.GenerateRandomCode());
                    value["businessid"] = business == null || !business.Any() ? "0" : business[GlobalConst.FROM_DATA_PRIMARYKEYF] + "";
                    RecordDatas.Add(value);
                });
            else
            {
                Dictionary<string, object> value = new Dictionary<string, object>();
                this.BuilderViewInstanceData(value, ar, null, table);
                value.Add("rowTag", longCommon.GenerateRandomCode());
                value["businessid"] = "0";
                RecordDatas.Add(value);
            }
            Dictionary<string, object> valuePairs1 = new Dictionary<string, object>();
            valuePairs1["tag"] = "many";
            valuePairs1["tables"] = RecordDatas;
            valuePairs1["maintable"] = ar.MainTable;
            valuePairs1["tablecode"] = ar.TableCode;
            valuePairs1["flowtablecode"] = table.FlowTableCode;
            valuePairs[ar.AreaCode] = valuePairs1;
        }
        #endregion
    }
}
