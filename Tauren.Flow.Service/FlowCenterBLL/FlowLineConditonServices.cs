﻿using System;
using System.Collections.Generic;
using System.Text;
using Tauren.Flow.DLL.Base;
using Tauren.Flow.DLL.Resouces;
using Tauren.Flow.Entity.Const;
using Tauren.Flow.Entity.Global;
using Tauren.Flow.Entity.Model;
using Tauren.Flow.Entity.Output;
using Tauren.Flow.Infrastructure.Globals;
using Newtonsoft.Json;
using System.Linq;
using Tauren.Flow.Entity.Enum;
using Tauren.Flow.DLL.FlowEngine;
using System.Data;

namespace Tauren.Flow.Service.FlowCenterBLL
{
    /// <summary>
    /// 判断后续条件连线
    /// </summary>
    public class FlowLineConditonServices : Base.BaseService, IFlowLineConditonServices
    {
        public IFlowEnineFormDataDLL flowEnineFormDataDLL { get; set; }
        public FlowLineConditonServices(IBaseDLL baseDLL, IConnectionBLLBase connectionBLL, IMenuResourcesBusiness resourcesBusiness) : base(baseDLL, connectionBLL, resourcesBusiness)
        {
            this.ResourcesInfo = this.GetResources(GlobalConst.RESOURCES_FLAG_MODULE_FLOWCENTER_KEY);
            this.JudePrmission(GlobalConst.RESOURCES_FLAG_MODULE_FLOWCENTER_KEY.ToLower());
        }

        public Result<List<LineInfo>> EstimateLineInfo(FlowTaskInfo currentTask, List<Flow_Instance_Business> instance_Businesses, List<LineInfo> lineParamters)
        {
            List<LineInfo> outs = new List<LineInfo>();
            Result<List<LineInfo>> result = new Result<List<LineInfo>>(null);
            lineParamters.ForEach(o =>
            {
                ///业务
                bool isBusiness = EstimateBusinessLine(o, instance_Businesses, currentTask);
                ///人员
                bool isEmp = EstimateEmployeeLine(o, currentTask);
                ///组织
                bool isOrg = EstimateOrgloyeeLine(o, currentTask);
                ///职位
                bool isPosition = EstimatePositionloyeeLine(o, currentTask);
                if (isBusiness && isEmp && isOrg && isPosition)
                {
                    outs.Add(o);
                }
            });
            result.Data = outs;
            if (!outs.Any()) result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTLINE_ERROR);
            //if (outs.Count > 1) result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_MORET_NEXTLINE_ERROR);
            return result;
        }

        #region private

        /// <summary>
        /// 判断业务数据
        /// </summary>
        /// <param name="lineInfo"></param>
        /// <param name="instance_Businesses"></param>
        /// <param name="currentTask"></param>
        /// <returns></returns>
        private bool EstimateBusinessLine(LineInfo lineInfo, List<Flow_Instance_Business> instance_Businesses, FlowTaskInfo currentTask)
        {
            try
            {
                bool result = true;
                ///业务
                if (!string.IsNullOrEmpty(lineInfo.Meta.BusinessCondition))
                {
                    List<FlowLineConditions> flowLineConditions = JsonConvert.DeserializeObject<List<FlowLineConditions>>(lineInfo.Meta.BusinessCondition);
                    if (flowLineConditions.Any())
                    {
                        StringBuilder areaConditions = new StringBuilder();
                        flowLineConditions.ForEach(area =>
                        {
                            var instance = instance_Businesses.Where(o => o.TableCode == area.TableCode).ToList();
                            if (instance.Any())
                            {
                                string sql = $"   re.{instance.FirstOrDefault().PrimaryKey} in ('{(string.Join("','", instance.Select(o => o.PrimaryValue)))}') ";
                                areaConditions.Append($" {area.AreaLk} ");
                                StringBuilder sb = new StringBuilder();
                                sb.Append($@" SELECT * FROM {area.TableCode}apply re WHERE  {sql} {(area.Conditions.Any() ? "and" : "")}");
                                area.Conditions.ForEach(co =>
                                {
                                    sb.Append($"{co.FieldLk} re.{co.FieldCode} {GetLineFieldForIn(co)} {co.FieldRk} {co.FieldFor} ");
                                });
                                var res = flowEnineFormDataDLL.Query<Employee>(sb.ToString());
                                areaConditions.Append($" {(res.Any() ? "true" : "false")} ");
                                areaConditions.Append($" {area.AreaRk} ");
                                areaConditions.Append($" {area.AreaFor} ");
                            }
                        });
                        DataTable dt = new DataTable();
                        result = (bool)dt.Compute(areaConditions.ToString(), "");
                    }
                }
                return result;
            }
            catch (Exception)
            {
                return false;
            }
        }
        /// <summary>
        /// 判断人员数据
        /// </summary>
        /// <param name="lineInfo"></param>
        /// <param name="currentTask"></param>
        /// <returns></returns>
        private bool EstimateEmployeeLine(LineInfo lineInfo, FlowTaskInfo currentTask)
        {
            bool result = true;
            ///业务
            try
            {
                StringBuilder sb = new StringBuilder();
                ///业务
                if (!string.IsNullOrEmpty(lineInfo.Meta.PersonCondtion))
                {
                    List<FlowLineConditions> flowLineConditions = JsonConvert.DeserializeObject<List<FlowLineConditions>>(lineInfo.Meta.PersonCondtion);
                    if (flowLineConditions.Any())
                    {
                        FlowLineConditions flowLine = flowLineConditions.FirstOrDefault();
                        sb.Append($@" SELECT * FROM employee eb WHERE ");
                        flowLine.Conditions.ForEach(co =>
                        {
                            sb.Append($"{co.FieldLk} {BulidPersonSql(currentTask, co)} AND eb.{co.FieldCode} {GetLineFieldForIn(co)} {co.FieldRk} {co.FieldFor} ");
                        });
                        var res = flowEnineFormDataDLL.Query<Employee>(sb.ToString());
                        result = res.Any();
                    }

                }
            }
            catch (Exception)
            {
                result = false;
            }
            return result;
        }

        /// <summary>
        /// 判断组织数据
        /// </summary>
        /// <param name="lineInfo"></param>
        /// <param name="currentTask"></param>
        /// <returns></returns>
        private bool EstimateOrgloyeeLine(LineInfo lineInfo, FlowTaskInfo currentTask)
        {
            bool result = true;
            ///业务
            try
            {
                StringBuilder sb = new StringBuilder();
                ///业务
                if (!string.IsNullOrEmpty(lineInfo.Meta.OrgCondtion))
                {
                    List<FlowLineConditions> flowLineConditions = JsonConvert.DeserializeObject<List<FlowLineConditions>>(lineInfo.Meta.OrgCondtion);
                    if (flowLineConditions.Any())
                    {
                        FlowLineConditions flowLine = flowLineConditions.FirstOrDefault();
                        sb.Append($@" SELECT *  FROM  organization org INNER JOIN employee eb ON eb.orgId = org.orgid  WHERE ");
                        flowLine.Conditions.ForEach(co =>
                        {
                            sb.Append($"{co.FieldLk} {BulidPersonSql(currentTask, co)} AND org.{co.FieldCode} {GetLineFieldForIn(co)} {co.FieldRk} {co.FieldFor} ");
                        });
                        var res = flowEnineFormDataDLL.Query<Organization>(sb.ToString());
                        result = res.Any();
                    }
                }
            }
            catch (Exception)
            {
                result = false;
            }
            return result;
        }

        /// <summary>
        /// 判断组织数据
        /// </summary>
        /// <param name="lineInfo"></param>
        /// <param name="currentTask"></param>
        /// <returns></returns>
        private bool EstimatePositionloyeeLine(LineInfo lineInfo, FlowTaskInfo currentTask)
        {
            bool result = true;
            try
            {
                StringBuilder sb = new StringBuilder();
                ///业务
                if (!string.IsNullOrEmpty(lineInfo.Meta.PostionCondtion))
                {
                    List<FlowLineConditions> flowLineConditions = JsonConvert.DeserializeObject<List<FlowLineConditions>>(lineInfo.Meta.PostionCondtion);
                    if (flowLineConditions.Any())
                    {
                        FlowLineConditions flowLine = flowLineConditions.FirstOrDefault();
                        sb.Append($" SELECT * FROM position pos INNER JOIN employee eb ON eb.positionId = pos.positionId  WHERE ");
                        flowLine.Conditions.ForEach(co =>
                        {
                            sb.Append($"{co.FieldLk} {BulidPersonSql(currentTask, co)} AND pos.{co.FieldCode} {GetLineFieldForIn(co)} {co.FieldRk} {co.FieldFor} ");
                        });
                        var res = flowEnineFormDataDLL.Query<Position>(sb.ToString());
                        result = res.Any();
                    }
                }
            }
            catch (Exception)
            {
                result = false;
            }
            return result;
        }
        /// <summary>
        /// 判断人员类型
        /// </summary>
        /// <param name="currentTask"></param>
        /// <param name="line"></param>
        /// <returns></returns>
        private string BulidPersonSql(FlowTaskInfo currentTask, LineConditions line)
        {
            string result = "";
            ApproverType approverType = line.ApproverType;
            switch (approverType)
            {
                case ApproverType.Applicant:
                    result = $" eb.empid={currentTask.ApplyEmpId}";
                    break;
                case ApproverType.Initiator:
                    result = $" eb.empid={currentTask.InitiatorEmpId}";
                    break;
                case ApproverType.Approver:
                    result = $" eb.empid={(currentTask.Reviewer > 0 ? currentTask.Reviewer : Global.EmpId)}";
                    break;
                default:
                    break;
            }
            return result;
        }
        /// <summary>
        /// 组装参数值
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        private string GetLineFieldValue(LineConditions line)
        {
            ControlType controlType = EnumComm.ConvertToControlType(line.ControlStype);
            switch (controlType)
            {
                case ControlType.text:
                    return $"'{line.FieldValue}'";
                case ControlType.textarea:
                    return $"'{line.FieldValue}'";
                case ControlType.datetime:
                    return $"'{line.FieldValue}'";
                case ControlType.date:
                    return $"'{line.FieldValue}'";
                case ControlType.year:
                    return $"'{line.FieldValue}'";
                case ControlType.month:
                    return $"'{line.FieldValue}'";
                case ControlType.day:
                    return $"'{line.FieldValue}'";
                case ControlType.time:
                    return $"'{line.FieldValue}'";                
                case ControlType.unit:
                    string result = "";
                    if (line.FieldValues.Any())
                    {
                        result = string.Join(",", line.FieldValues.Select(o => o.Value));
                    }
                    return result;
                case ControlType.employee:
                case ControlType.organizational:
                case ControlType.position:
                case ControlType.tree: 
                case ControlType.parameter:
                    string result1 = "";
                    if (line.FieldValues.Any())
                    {
                        result1 = string.Join("','", line.FieldValues.Select(o => o.Value));
                    }
                    return $"'{result1}'";
                case ControlType.number:
                    return line.FieldValue + "";
                case ControlType.radio:
                    return line.FieldValue + "";
                default:
                    return line.FieldValue + "";
            }
        }
        /// <summary>
        /// 拼接查询条件值
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        private string GetLineFieldForIn(LineConditions line)
        {
            if (line.FieldIn == "in")
            {
                return $" in ({GetLineFieldValue(line)})   ";
            }
            else if (line.FieldIn == "like")
            {
                return $" like %{GetLineFieldValue(line)}%   ";
            }
            else
            {
                return $" {line.FieldIn} ({GetLineFieldValue(line)})   ";
            }
        }
        #endregion
    }
}
