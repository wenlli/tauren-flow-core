﻿using System;
using System.Collections.Generic;
using System.Text;
using Tauren.Flow.DLL.Base;
using Tauren.Flow.DLL.Resouces;
using Tauren.Flow.Entity.Config;
using Tauren.Flow.Entity.Const;
using Tauren.Flow.Entity.Enum;
using Tauren.Flow.Entity.Global;
using Tauren.Flow.Entity.Output;
using Tauren.Flow.Infrastructure.Globals;
using Tauren.Flow.Service.Base;
using Tauren.Flow.Service.ProcessBasicInfoBLL;
using System.Linq;
using Tauren.Flow.Entity.Model;
using Tauren.Flow.DLL.FlowEngine;
using Tauren.Flow.Service.Flow_SrategyBLL;
using MySqlX.XDevAPI.Common;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Mvc;

namespace Tauren.Flow.Service.FlowCenterBLL
{
    /// <summary>
    /// 流程获取下一步信息操作
    /// </summary>
    public class FlowNextTaskInfoEventService : BaseService, IFlowNextTaskInfoEventService
    {
        public IProcessBasicInfoService processBasicInfoService { get; set; }
        public IFlowLineConditonServices flowLineConditonServices { get; set; }
        public IFlow_Engine_SrategyService _Engine_SrategyService { get; set; }
        private IFlow_SrategyDLL flowEngineDLL;
        private IFlowEngineBusiness business;
        public FlowNextTaskInfoEventService(IFlow_SrategyDLL flowEngineDLL, IConnectionBLLBase connectionBLL, IMenuResourcesBusiness resourcesBusiness, IFlowEngineBusiness _business) : base(flowEngineDLL, connectionBLL, resourcesBusiness)
        {
            this.ResourcesInfo = this.GetResources(GlobalConst.RESOURCES_FLAG_MODULE_FLOWCENTER_KEY);
            this.JudePrmission(GlobalConst.RESOURCES_FLAG_MODULE_FLOWCENTER_KEY.ToLower());
            this.flowEngineDLL = flowEngineDLL;
            this.business = _business;
        }

        /// <summary>
        /// 获取下一个节点信息集合
        /// </summary>
        /// <param name="Paramter"></param>
        /// <returns></returns>
        public IResponseMessage NextNodeInfos(FlowRquestParameter Paramter)
        {
            Result<NextNodeInfo_OutPut> result = new Result<NextNodeInfo_OutPut>(null);
            try
            {
                if (!JudeTaskRepeatSubmit(Paramter)) return GlobalErrorType.GLOBAL_RES_FLOW_CURRENTDATA_REPART_SUMBIT.Failure(CommonText(GlobalErrorType.GLOBAL_RES_FLOW_CURRENTDATA_REPART_SUMBIT));
                FlowTaskInfo currentTask = null;
                ///后去当前任务
                currentTask = processBasicInfoService.QueryCurrentFlowTaskInfo(Paramter.TaskId);
                if (currentTask is null)
                {
                    RemoveRepeatSubmit();
                    return GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_CURRENTTASK_ERROR.Failure(CommonText(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_CURRENTTASK_ERROR));
                }
                ///获取流程信息
                Flow_OutPut flow_OutPut = processBasicInfoService.GetFlowInfo(Paramter.FlowId, Paramter.FlowVersionId);
                ///获取当前节点信息
                NodeInfo currentStep = flow_OutPut.NodeList.FirstOrDefault(o => o.Meta.StepId == currentTask.StepId);
                if (currentStep is null)
                {
                    RemoveRepeatSubmit();
                    return GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_CURRENTSTEP_ERROR.Failure(CommonText(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_CURRENTSTEP_ERROR));
                }
                if (currentStep.Meta.Prop == GlobalConst.NODE_INFO_MEAT_END)
                {
                    result.Data = new NextNodeInfo_OutPut() { JudeNodeType = JudeNodeType.DirectlySubmit };
                    return result.Data.OnSucceed();
                }
                ///获取流程实例表单数据
                List<Flow_Instance_Business> instance_Businesses = baseDLL.GetBasicListByQuery<Flow_Instance_Business>("instanceid", currentTask.InstanceId + "");
                NextNodeInfoRequestParameter requestParameter = new NextNodeInfoRequestParameter() { CurrentStep = currentStep, CurrentTask = currentTask, Flow_OutPut = flow_OutPut, Instance_Businesses = instance_Businesses };
                List<ExtFlow_Instance_CurrentTask> prevTask = processBasicInfoService.QueryPrevTask(currentTaskId: currentTask.TaskId).Where(o => o.ProcessType == (int)FlowProcessType.Back).ToList();

                ///当流程开启直接提交到退回或则撤回节点之前的节点时
                if (flow_OutPut.FlowProperty.SumbitPrevStep && prevTask.Any())
                {
                    result = BuildHistoryNextStepInfo(requestParameter, prevTask);
                }
                else
                {
                    result = BuildNextInfosApprove(requestParameter, Paramter);
                }
                if (result.Success)
                    return result.Data.OnSucceed();
                else
                {
                    RemoveRepeatSubmit();
                    return result.Code.Failure(CommonText(result.Code));
                }
            }
            catch (Exception ex)
            {
                RemoveRepeatSubmit();
                throw ex;
            }
        }

        /// <summary>
        /// 获取下一传阅节点信息
        /// </summary>
        /// <param name="paramter"></param>
        /// <returns></returns>
        public IResponseMessage CirculatedNextNodeInfo(FlowRquestParameter paramter)
        {
            Result<NextNodeInfo_OutPut> result = new Result<NextNodeInfo_OutPut>(null);
            try
            {
                if (!JudeTaskRepeatSubmit(paramter)) return GlobalErrorType.GLOBAL_RES_FLOW_CURRENTDATA_REPART_SUMBIT.Failure(CommonText(GlobalErrorType.GLOBAL_RES_FLOW_CURRENTDATA_REPART_SUMBIT));
                FlowTaskInfo currentTask = null;
                ///后去当前任务
                currentTask = processBasicInfoService.QueryCurrentFlowTaskInfo(paramter.TaskId);
                if (currentTask is null)
                {
                    RemoveRepeatSubmit();
                    return GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_CURRENTTASK_ERROR.Failure(CommonText(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_CURRENTTASK_ERROR));
                }
                ///获取流程信息
                Flow_OutPut flow_OutPut = processBasicInfoService.GetFlowInfo(paramter.FlowId, paramter.FlowVersionId);
                ///获取当前节点信息
                NodeInfo currentStep = flow_OutPut.NodeList.FirstOrDefault(o => o.Meta.StepId == currentTask.StepId);
                if (currentStep is null)
                {
                    RemoveRepeatSubmit();
                    return GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_CURRENTSTEP_ERROR.Failure(CommonText(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_CURRENTSTEP_ERROR));
                }
                ///获取流程实例表单数据
                List<Flow_Instance_Business> instance_Businesses = baseDLL.GetBasicListByQuery<Flow_Instance_Business>("instanceid", currentTask.InstanceId + "");
                NextNodeInfoRequestParameter requestParameter = new NextNodeInfoRequestParameter() { CurrentStep = currentStep, CurrentTask = currentTask, Flow_OutPut = flow_OutPut, Instance_Businesses = instance_Businesses };
                result = BuildCirculatedNextInfosApprove(requestParameter, paramter);
                if (result.Success)
                    return result.Data.OnSucceed();
                else
                {
                    RemoveRepeatSubmit();
                    return result.Code.Failure(CommonText(result.Code));
                }
            }
            catch (Exception ex)
            {
                RemoveRepeatSubmit();
                throw ex;
            }
        }

        /// <summary>
        /// 选择要退回的节点
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        public IResponseMessage BackStepNodeInfo(long taskId)
        {
            if (!HavePermission) return ErrorType.BUSINESS_WITHOUT_PERMISSION.Failure(this.CommonText(ErrorType.BUSINESS_WITHOUT_PERMISSION));
            Result<NextNodeInfo_OutPut> result = new Result<NextNodeInfo_OutPut>(null);
            try
            {
                ///后去当前任务
                FlowTaskInfo currentTask = processBasicInfoService.QueryCurrentFlowTaskInfo(taskId);
                if (currentTask is null)
                {
                    return GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_CURRENTTASK_ERROR.Failure(CommonText(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_CURRENTTASK_ERROR));
                }
                ///获取流程信息
                Flow_OutPut flow_OutPut = processBasicInfoService.GetFlowInfo(currentTask.FlowId, currentTask.FlowVersionId);
                result = BulidBackNextNodeInfo(currentTask, flow_OutPut);
                if (result.Success)
                    return result.Data.OnSucceed();
                else
                    return result.Code.Failure(CommonText(result.Code));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region private

        #region 组装正常的步骤信息
        /// <summary>
        /// 不提交到历史步骤上获取人员和节点
        /// </summary>
        /// <param name="result"></param>
        /// <param name="requestParameter"></param>
        /// <param name="Paramter"></param>
        private Result<NextNodeInfo_OutPut> BuildNextInfosApprove(NextNodeInfoRequestParameter requestParameter, FlowRquestParameter Paramter)
        {
            Result<NextNodeInfo_OutPut> result = new Result<NextNodeInfo_OutPut>(null);
            ///获取连线信息
            Result<List<LineInfo>> lineResult = this.NextLineInfos(requestParameter, Paramter.ProcessType);
            if (!lineResult.Success)
            {
                RemoveRepeatSubmit();
                result.OnFailure(lineResult.Code);
            }
            else
            {
                var circulateList = requestParameter.Flow_OutPut.NodeList.Where(o => o.Meta.Prop != "cc").ToList();
                requestParameter.NextLineInfos = lineResult.Data;
                ////获取节点信息
                result = this.BulidNextNodeInfo(requestParameter);
                if (!result.Success)
                {
                    ///系统控制时与连线只有一条时判断后续步骤是否自动审批
                    if (lineResult.Data.Count == 1)
                    {
                        var nextStep = requestParameter.Flow_OutPut.NodeList.FirstOrDefault(o => o.Meta.StepId == lineResult.Data[0].EndId);
                        if (nextStep != null && nextStep.Meta.Prop == GlobalConst.NODE_INFO_MEAT_END)
                        {
                            result.OnSuccess(BuildEmptyNodeInf(nextStep));
                        }
                        else
                        {
                            if (requestParameter.CurrentStep.Strategy.IsAutoSelectStep)
                            {
                                if ((nextStep != null && nextStep.Meta.Prop == GlobalConst.NODE_INFO_MEAT_BRANCH) || (nextStep != null && nextStep.Meta.Prop == GlobalConst.NODE_INFO_MEAT_ENBRANCH) || (nextStep != null && nextStep.Strategy.SkipStrategy && nextStep.Meta.Prop != "cc"))
                                {
                                    result.OnSuccess(BuildEmptyNodeInf(nextStep));
                                }
                                else
                                {
                                    RemoveRepeatSubmit();
                                    result.Code.Failure(CommonText(result.Code));
                                }
                            }
                            else
                            {
                                RemoveRepeatSubmit();
                                result.Code.Failure(CommonText(result.Code));
                            }
                        }
                    }
                    else
                    {
                        RemoveRepeatSubmit();
                        result.Code.Failure(CommonText(result.Code));
                    }
                }
                else
                {
                    if (result.Data != null && result.Data.NextNode.Any())
                    {
                        result.Data.NextNode = result.Data.NextNode.Join(circulateList, a => a.NextStepId, b => b.Meta.StepId, (a, b) => a).ToList();
                    }
                }
            }
            return result;
        }
        /// <summary>
        /// 组装空姐点数据
        /// </summary>
        /// <param name="nextStep"></param>
        /// <returns></returns>
        private NextNodeInfo_OutPut BuildEmptyNodeInf(NodeInfo nextStep)
        {
            return new NextNodeInfo_OutPut()
            {
                JudeNodeType = JudeNodeType.DirectlySubmit,
                NextNode = new List<NextNodeInfoOutPut>()
                {
                    new NextNodeInfoOutPut() {
                        ApprovalRangeType=ApprovalRangeType.UnChoosable,
                        NextStepApprover=GlobalConst.SystemFlowPersonnel.ToString(),
                        NextStepApproverEmpId=new List<int>{ GlobalConst.SystemFlowPersonnel},
                        NextStepApproverEmps=new List<ApproverModel>(){
                            new ApproverModel() {
                                CNName="系统指定人员",
                                ENName="System designee",
                                EmpId=GlobalConst.SystemFlowPersonnel
                            }
                        },
                        NextStepId=nextStep.Meta.StepId,
                        NextStepCNName=nextStep.Meta.CNName,
                        NextStepENName=nextStep.Meta.ENName
                    }
                }
            };
        }

        /// <summary>
        /// 提交数据到历史步骤中
        /// </summary>
        /// <param name="result"></param>
        /// <param name="flow_OutPut"></param>
        /// <param name="prevTask"></param>
        private Result<NextNodeInfo_OutPut> BuildHistoryNextStepInfo(NextNodeInfoRequestParameter requestParameter, List<ExtFlow_Instance_CurrentTask> prevTask)
        {
            requestParameter.NextLineInfos = new List<LineInfo>();
            prevTask.GroupBy(g => new { g.PrveStepId, g.PrveTaskId }).ToList().ForEach(tk =>
            {
                requestParameter.NextLineInfos.Add(new LineInfo() { Meta = new Flow_Lines_OutPut() { EndId = tk.Key.PrveStepId } });
            });
            Result<List<NextNodeInfoOutPut>> result = _Engine_SrategyService.LoadNextNodeInfo(requestParameter);
            Result<NextNodeInfo_OutPut> res = new Result<NextNodeInfo_OutPut>(new NextNodeInfo_OutPut { JudeNodeType = JudeNodeType.DirectlySubmit, NextNode = result.Data });
            if (result.Success && result.Data != null && result.Data.Any())
            {
                ///多个连线成立时判断流程是否开启并行审批
                if (result.Data.Count > 1) res.Data.JudeNodeType = requestParameter.Flow_OutPut.FlowProperty.ParallelTask ? JudeNodeType.MultiChoosable : JudeNodeType.SingeChoosable;
                ///单条连线时
                if (result.Data.Count == 1)
                {
                    var singe = result.Data.FirstOrDefault();
                    ///判断是否不能修改审批人
                    if (singe.ApprovalRangeType != ApprovalRangeType.UnChoosable)
                    {
                        res.Data.JudeNodeType = JudeNodeType.SingeChoosable;
                    }
                }
            }
            else res.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR);
            return res;
        }
        /// <summary>
        /// 获取连线
        /// </summary>
        /// <param name="flow_OutPut"></param>
        /// <param name="currentStep"></param>
        /// <returns></returns>
        private Result<List<LineInfo>> NextLineInfos(NextNodeInfoRequestParameter requestParameter, ProcessType processType)
        {
            Result<List<LineInfo>> result = new Result<List<LineInfo>>(null);
            ///判断当前任务的类型
            switch (processType)
            {
                case ProcessType.Approval:///审批
                    {
                        LineInfo line = requestParameter.Flow_OutPut.LinkList.FirstOrDefault(o => o.StartId == requestParameter.CurrentStep.Meta.StepId);
                        if (line is null) result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTLINE_ERROR);
                        NodeInfo nodeInfo = requestParameter.Flow_OutPut.NodeList.FirstOrDefault(o => o.Meta.StepId == line.EndId);
                        if (nodeInfo is null) result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTLINE_ERROR);
                        switch (nodeInfo.Meta.Prop)
                        {

                            ///结束分支
                            case GlobalConst.NODE_INFO_MEAT_ENBRANCH:
                            ///开始分支
                            case GlobalConst.NODE_INFO_MEAT_BRANCH:
                                {
                                    line.BranchLines = requestParameter.Flow_OutPut.LinkList.Where(o => o.StartId == nodeInfo.Meta.StepId).ToList();
                                    result.Data = result.Data = new List<LineInfo>() { line };
                                }
                                break;
                            ///条件后面数据分支
                            case GlobalConst.NODE_INFO_MEAT_CONDITION:
                                {
                                    List<LineInfo> lines = new List<LineInfo>();
                                    switch (requestParameter.CurrentStep.Strategy.IsAutoSelectStep)
                                    {
                                        case false:
                                            {
                                                result.Data = requestParameter.Flow_OutPut.LinkList.Where(o => o.StartId == nodeInfo.Meta.StepId).ToList();
                                            }
                                            break;
                                        default:
                                            {
                                                result = this.flowLineConditonServices.EstimateLineInfo(requestParameter.CurrentTask, requestParameter.Instance_Businesses, requestParameter.Flow_OutPut.LinkList.Where(o => o.Meta.StartId == nodeInfo.Meta.StepId).ToList());
                                            }
                                            break;
                                    }
                                    if (result.Success)
                                        result.Data.ForEach(o =>
                                        {
                                            if (requestParameter.Flow_OutPut.NodeList.Any(n => n.Meta.StepId == o.EndId && (n.Meta.Prop == GlobalConst.NODE_INFO_MEAT_BRANCH || n.Meta.Prop == GlobalConst.NODE_INFO_MEAT_ENBRANCH)))
                                            {
                                                o.BranchLines = requestParameter.Flow_OutPut.LinkList.Where(p => p.StartId == o.EndId).ToList();
                                            }
                                        });
                                }
                                break;
                            default:
                                {
                                    result.Data = new List<LineInfo>() { line };
                                }
                                break;
                        }
                    }
                    break;
                case ProcessType.Circulated:///传阅
                    {
                        result.Data = requestParameter.Flow_OutPut.LinkList.Where(o => o.StartId == requestParameter.CurrentStep.Meta.StepId).ToList();
                        if (!result.Data.Any())
                            result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTLINE_ERROR);
                    }
                    break;
            }
            return result;
        }
        /// <summary>
        /// 组装下一步信息
        /// </summary>
        /// <param name="nextNodes"></param>
        /// <param name="NextLineInfos"></param>
        /// <param name="flow_OutPut"></param>
        /// <param name="currentTask"></param>
        /// <param name="currentStep"></param>
        private Result<NextNodeInfo_OutPut> BulidNextNodeInfo(NextNodeInfoRequestParameter requestParameter)
        {

            Result<NextNodeInfo_OutPut> res = new Result<NextNodeInfo_OutPut>(new NextNodeInfo_OutPut { JudeNodeType = JudeNodeType.DirectlySubmit });
            if (requestParameter.CurrentStep.Strategy.IsAutoSelectStep)
            {
                Result<List<NextNodeInfoOutPut>> result = _Engine_SrategyService.LoadNextNodeInfo(requestParameter);
                if (result.Success && result.Data != null && result.Data.Any())
                {
                    res.Data.NextNode = result.Data;
                    if (result.Data.Count > 1) res.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_MORET_NEXTLINE_ERROR);
                    ///单条连线时
                    if (result.Data.Count == 1)
                    {
                        bool b = true;
                        var singe = result.Data.FirstOrDefault();
                        var nextStep = requestParameter.Flow_OutPut.NodeList.FirstOrDefault(c => c.Meta.StepId == singe.NextStepId);
                        NextNodeInfoOutPut put = new NextNodeInfoOutPut() { ApprovalRangeType = ApprovalRangeType.Owner, FlowTaskType = FlowTaskType.task, NextStepId = singe.NextStepId, IsMultiSelect = nextStep.Strategy.ApprovalStrategy > 0, NextStepCNName = nextStep.Meta.CNName, NextStepENName = nextStep.Meta.ENName };
                        if ((nextStep.Meta.Prop == ApprovalRangeType.Branch.ToString().ToLower() || nextStep.Meta.Prop == ApprovalRangeType.EndBranch.ToString().ToLower()))
                        {
                            List<LineInfo> branchs = requestParameter.Flow_OutPut.LinkList.Where(o => o.StartId == nextStep.Meta.StepId).ToList();
                            Result<List<NextNodeInfoOutPut>> rs = _Engine_SrategyService.LoadNextNodeInfo(new NextNodeInfoRequestParameter()
                            {
                                CurrentStep = requestParameter.CurrentStep,
                                CurrentTask = requestParameter.CurrentTask,
                                Flow_OutPut = requestParameter.Flow_OutPut,
                                Instance_Businesses = requestParameter.Instance_Businesses,
                                NextLineInfos = branchs
                            });
                            put.ApprovalRangeType = (nextStep.Meta.Prop == ApprovalRangeType.Branch.ToString().ToLower()) ? ApprovalRangeType.Branch : ApprovalRangeType.EndBranch;
                            put.FlowTaskType = (nextStep.Meta.Prop == ApprovalRangeType.Branch.ToString().ToLower()) ? FlowTaskType.branch : FlowTaskType.endbranch;
                            put.BranchNodes = rs.Data;
                            if (rs.Data.Count != branchs.Count)
                            {
                                b = false;
                            }
                            res.Data.NextNode = new List<NextNodeInfoOutPut>();
                            res.Data.NextNode.Add(put);
                            if (!b) res.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR);
                        }
                        else
                        {
                            ///判断是否不能修改审批人
                            if (singe.ApprovalRangeType != ApprovalRangeType.UnChoosable)
                            {
                                res.Data.JudeNodeType = JudeNodeType.SingeChoosable;
                            }
                        }
                    }
                }
                else res.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR);
            }
            else
            {
                res.Data.JudeNodeType = JudeNodeType.SingeChoosable;
                bool b = true;
                requestParameter.NextLineInfos.ForEach(o =>
                {
                    var nextStep = requestParameter.Flow_OutPut.NodeList.FirstOrDefault(c => c.Meta.StepId == o.Meta.EndId);
                    NextNodeInfoOutPut put = new NextNodeInfoOutPut() { ApprovalRangeType = ApprovalRangeType.Owner, FlowTaskType = FlowTaskType.task, NextStepId = o.Meta.EndId, IsMultiSelect = nextStep.Strategy.ApprovalStrategy > 0, NextStepCNName = nextStep.Meta.CNName, NextStepENName = nextStep.Meta.ENName };
                    if ((nextStep.Meta.Prop == ApprovalRangeType.Branch.ToString().ToLower() || nextStep.Meta.Prop == ApprovalRangeType.EndBranch.ToString().ToLower()) && o.BranchLines.Any())
                    {
                        Result<List<NextNodeInfoOutPut>> rs = _Engine_SrategyService.LoadNextNodeInfo(new NextNodeInfoRequestParameter()
                        {
                            CurrentStep = requestParameter.CurrentStep,
                            CurrentTask = requestParameter.CurrentTask,
                            Flow_OutPut = requestParameter.Flow_OutPut,
                            Instance_Businesses = requestParameter.Instance_Businesses,
                            NextLineInfos = o.BranchLines
                        });
                        put.ApprovalRangeType = (nextStep.Meta.Prop == ApprovalRangeType.Branch.ToString().ToLower()) ? ApprovalRangeType.Branch : ApprovalRangeType.EndBranch;
                        put.FlowTaskType = (nextStep.Meta.Prop == ApprovalRangeType.Branch.ToString().ToLower()) ? FlowTaskType.branch : FlowTaskType.endbranch;
                        put.BranchNodes = rs.Data;
                        if (rs.Data.Count != o.BranchLines.Count)
                        {
                            b = false;
                        }
                    }
                    res.Data.NextNode.Add(put);
                });
                if (!b) res.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR);
            }
            return res;
        }

        private Result<NextNodeInfo_OutPut> BuildCirculatedNextInfosApprove(NextNodeInfoRequestParameter requestParameter, FlowRquestParameter Paramter)
        {
            Result<NextNodeInfo_OutPut> result = new Result<NextNodeInfo_OutPut>(null);
            ///获取连线信息
            Result<List<LineInfo>> lineResult = this.NextLineInfos(requestParameter, Paramter.ProcessType);
            if (!lineResult.Success)
            {
                RemoveRepeatSubmit();
                result.OnFailure(lineResult.Code);
            }
            else
            {
                var circulateList = requestParameter.Flow_OutPut.NodeList.Where(o => o.Meta.Prop == "cc").ToList();
                requestParameter.NextLineInfos = lineResult.Data;
                ////获取节点信息
                result = this.BulidCirculatedNextNodeInfo(requestParameter);
                if (!result.Success)
                {
                    ///系统控制时与连线只有一条时判断后续步骤是否自动审批
                    if (lineResult.Data.Count == 1)
                    {
                        var nextStep = requestParameter.Flow_OutPut.NodeList.FirstOrDefault(o => o.Meta.StepId == lineResult.Data[0].EndId);
                        if (nextStep != null && nextStep.Meta.Prop == GlobalConst.NODE_INFO_MEAT_END)
                        {
                            result.OnSuccess(BuildEmptyNodeInf(nextStep));
                        }
                    }
                    else
                    {
                        RemoveRepeatSubmit();
                        result.Code.Failure(CommonText(result.Code));
                    }
                }
                else
                {
                    if (result.Data != null && result.Data.NextNode.Any())
                    {
                        result.Data.NextNode = result.Data.NextNode.Join(circulateList, a => a.NextStepId, b => b.Meta.StepId, (a, b) => a).ToList();
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// 组装下一步传阅信息
        /// </summary>
        /// <param name="nextNodes"></param>
        /// <param name="NextLineInfos"></param>
        /// <param name="flow_OutPut"></param>
        /// <param name="currentTask"></param>
        /// <param name="currentStep"></param>
        private Result<NextNodeInfo_OutPut> BulidCirculatedNextNodeInfo(NextNodeInfoRequestParameter requestParameter)
        {
            Result<List<NextNodeInfoOutPut>> result = _Engine_SrategyService.LoadNextNodeInfo(requestParameter);
            Result<NextNodeInfo_OutPut> res = new Result<NextNodeInfo_OutPut>(new NextNodeInfo_OutPut { JudeNodeType = JudeNodeType.DirectlySubmit, NextNode = result.Data });
            if (result.Success && result.Data != null && result.Data.Any())
            {
                ///多个连线成立时判断流程是否开启并行审批
                if (result.Data.Count > 1) res.Data.JudeNodeType = JudeNodeType.SingeChoosable;
                ///单条连线时
                if (result.Data.Count == 1)
                {
                    var singe = result.Data.FirstOrDefault();
                    ///判断是否不能修改审批人
                    if (singe.ApprovalRangeType != ApprovalRangeType.UnChoosable)
                    {
                        res.Data.JudeNodeType = JudeNodeType.SingeChoosable;
                    }
                }
            }
            else res.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR);
            return res;
        }
        #endregion

        #region 组装退回步骤数据

        /// <summary>
        /// 获取要退回到的节点
        /// </summary>
        /// <param name="currentTask"></param>
        /// <param name="flow_Out"></param>
        /// <returns></returns>
        private Result<NextNodeInfo_OutPut> BulidBackNextNodeInfo(FlowTaskInfo currentTask, Flow_OutPut flow_Out)
        {
            Result<NextNodeInfo_OutPut> res = new Result<NextNodeInfo_OutPut>(new NextNodeInfo_OutPut { JudeNodeType = JudeNodeType.SingeChoosable });
            List<Flow_Task> allTask = this.baseDLL.GetBasicListByQuery<Flow_Task>("groupid", currentTask.GroupId + "");
            List<Flow_Task> resTask = new List<Flow_Task>();
            ///获取当前节点信息
            NodeInfo currentStep = flow_Out.NodeList.FirstOrDefault(o => o.Meta.StepId == currentTask.StepId);
            switch (currentStep)
            {
                case null: res.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_CURRENTSTEP_ERROR); break;
                default:
                    {
                        ///判断是否在并行分支中 - 是
                        if (!string.IsNullOrEmpty(currentStep.Meta.Groups))
                        {
                            List<Branchs> branchs = JsonConvert.DeserializeObject<List<Branchs>>(currentStep.Meta.Groups);
                            if (branchs.Any())
                            {
                                List<NodeInfo> brnachSteps = flow_Out.NodeList.Where(o => o.Meta.Groups == currentStep.Meta.Groups).ToList();
                                allTask = allTask.Join(brnachSteps, a => a.StepId, b => b.Meta.StepId, (a, b) => a).ToList();
                            }
                        }
                    }
                    break;
            }
            resTask = RecursiveTaskBySort(allTask.FirstOrDefault(o => o.TaskId == currentTask.TaskId), allTask.Where(o => o.Status == (int)FlowTaskStatus.approved).ToList());
            #region 先合并步骤
            List<NextNodeInfoOutPut> nodes = new List<NextNodeInfoOutPut>();
            resTask.GroupBy(o => o.StepId).ToList().ForEach(g =>
                {
                    NodeInfo singleNode = flow_Out.NodeList.FirstOrDefault(o => o.Meta.StepId == g.Key);
                    if (singleNode != null)
                    {
                        nodes.Add(new NextNodeInfoOutPut()
                        {
                            ApprovalRangeType = ApprovalRangeType.UnChoosable,
                            FlowTaskType = FlowTaskType.task,
                            NextStepId = g.Key,
                            NextStepCNName = singleNode.Meta.CNName,
                            NextStepENName = singleNode.Meta.ENName,
                            NextStepApprover = string.Join(",", g.Select(o => o.Reviewer).ToArray()),
                            NextStepApproverEmpId = g.Select(o => o.Reviewer).ToList(),
                            NextStepApproverEmps = g.Join(EmployeesList, a => a.Reviewer, b => b.EmpId, (a, b) => new ApproverModel()
                            {
                                EmpId = b.EmpId,
                                CNName = b.CNName,
                                ENName = b.ENName,
                            }).ToList()
                        });
                    }
                });
            res.Data.NextNode = nodes;
            #endregion
            return res;
        }

        /// <summary>
        /// 根据排序ID获取
        /// </summary>
        /// <param name="currentTak"></param>
        /// <param name="allTask"></param>
        /// <returns></returns>
        private List<Flow_Task> RecursiveTaskBySort(Flow_Task currentTak, List<Flow_Task> allTask)
        {
            List<Flow_Task> res = new List<Flow_Task>();
            allTask.Where(o => o.Sort == currentTak.Sort - 1).ToList().ForEach(o =>
            {
                res.Add(o);
                res.AddRange(RecursiveTaskBySort(o, allTask));
            });
            return res;
        }

        /// <summary>
        /// 根据排序ID获取
        /// </summary>
        /// <param name="currentTak"></param>
        /// <param name="allTask"></param>
        /// <returns></returns>
        private List<Flow_Task> RecursiveTaskByPrevTaskId(Flow_Task currentTak, List<Flow_Task> allTask)
        {
            List<Flow_Task> res = new List<Flow_Task>();
            allTask.Where(o => o.TaskId == currentTak.PrevTaskId).ToList().ForEach(o =>
            {
                res.Add(o);
                res.AddRange(RecursiveTaskByPrevTaskId(o, allTask));
            });
            return res;
        }
        #endregion

        #region 获取跳过的步骤信息
        public IResponseMessage SkipNodeInfo(FlowRquestParameter Paramter)
        {
            Result<NextNodeInfo_OutPut> result = new Result<NextNodeInfo_OutPut>(null);
            try
            {
                if (!JudeTaskRepeatSubmit(Paramter)) return GlobalErrorType.GLOBAL_RES_FLOW_NOT_SKIP_NODE_FUILER_ERROR.Succeed(GlobalErrorType.GLOBAL_RES_FLOW_NOT_SKIP_NODE_FUILER_ERROR);
                FlowTaskInfo currentTask = null;
                ///获取当前任务
                currentTask = processBasicInfoService.QueryCurrentFlowTaskInfo(Paramter.TaskId);
                if (currentTask is null)
                {
                    return GlobalErrorType.GLOBAL_RES_FLOW_NOT_SKIP_NODE_FUILER_ERROR.Succeed(GlobalErrorType.GLOBAL_RES_FLOW_NOT_SKIP_NODE_FUILER_ERROR);
                }
                ///获取流程信息
                Flow_OutPut flow_OutPut = processBasicInfoService.GetFlowInfo(Paramter.FlowId, Paramter.FlowVersionId);
                ///获取当前节点信息
                NodeInfo currentStep = flow_OutPut.NodeList.FirstOrDefault(o => o.Meta.StepId == currentTask.StepId);
                if (currentStep is null)
                {
                    return GlobalErrorType.GLOBAL_RES_FLOW_NOT_SKIP_NODE_FUILER_ERROR.Succeed(GlobalErrorType.GLOBAL_RES_FLOW_NOT_SKIP_NODE_FUILER_ERROR);
                }
                if (currentStep.Meta.Prop == GlobalConst.NODE_INFO_MEAT_END)
                {
                    result.Data = new NextNodeInfo_OutPut() { JudeNodeType = JudeNodeType.DirectlySubmit };
                    return result.Data.OnSucceed();
                }
                bool isSkip = false;
                SkipStrategyType skipStrategyType = (SkipStrategyType)flow_OutPut.FlowProperty.SkipRules.Value;
                if (currentTask.Reviewer == GlobalConst.SystemFlowPersonnel && currentStep.Strategy.SkipStrategy)
                {
                    isSkip = true;
                }
                else
                {
                    ///判断规则是否启用，当前节点是否自动选择分支
                    if (flow_OutPut.FlowProperty.SkipRules > 0 && currentStep.Strategy.IsAutoSelectStep)
                    {
                        List<Flow_Task> flow_Tasks = this.business.GetTasks(currentTask.GroupId).Where(o => o.Status == (int)FlowTaskStatus.approved || o.Status == (int)FlowTaskStatus.otherapproved).ToList();
                        switch (skipStrategyType)
                        {
                            case SkipStrategyType.Adjacent:
                                isSkip = Global.EmpId == currentTask.Reviewer;
                                break;
                            case SkipStrategyType.Approval:
                                isSkip = flow_Tasks.Any(o => o.Reviewer == currentTask.Reviewer);
                                break;
                        }
                    }
                }
                if (isSkip)
                {
                    ///获取流程实例表单数据
                    List<Flow_Instance_Business> instance_Businesses = baseDLL.GetBasicListByQuery<Flow_Instance_Business>("instanceid", currentTask.InstanceId + "");
                    NextNodeInfoRequestParameter requestParameter = new NextNodeInfoRequestParameter() { CurrentStep = currentStep, CurrentTask = currentTask, Flow_OutPut = flow_OutPut, Instance_Businesses = instance_Businesses };
                    List<ExtFlow_Instance_CurrentTask> prevTask = processBasicInfoService.QueryPrevTask(currentTaskId: currentTask.TaskId).Where(o => o.ProcessType == (int)FlowProcessType.Back).ToList();
                    ///当流程开启直接提交到退回或则撤回节点之前的节点时
                    if (flow_OutPut.FlowProperty.SumbitPrevStep && prevTask.Any())
                    {
                        result = BuildHistoryNextStepInfo(requestParameter, prevTask);
                    }
                    else
                    {
                        result = BuildNextInfosApprove(requestParameter, Paramter);
                    }
                    if (result.Success)
                    {
                        ///是保留人员并且当前节点开跳过规则
                        if (currentTask.Reviewer == GlobalConst.SystemFlowPersonnel && currentStep.Strategy.SkipStrategy)
                        {
                            return result.Data.Succeed(GlobalErrorType.GLOBAL_RES_FLOW_NOT_APPROVER_SKIP_NODE_FUILER_ERROR);
                        }
                        else if (result.Data.JudeNodeType == JudeNodeType.DirectlySubmit)
                            return result.Data.OnSucceed();
                        else
                        {
                            return GlobalErrorType.GLOBAL_RES_FLOW_NOT_SKIP_NODE_FUILER_ERROR.Succeed(GlobalErrorType.GLOBAL_RES_FLOW_NOT_SKIP_NODE_FUILER_ERROR);
                        }
                    }
                    else
                    {
                        return GlobalErrorType.GLOBAL_RES_FLOW_NOT_SKIP_NODE_FUILER_ERROR.Succeed(GlobalErrorType.GLOBAL_RES_FLOW_NOT_SKIP_NODE_FUILER_ERROR);
                    }
                }
                else
                {
                    return GlobalErrorType.GLOBAL_RES_FLOW_NOT_SKIP_NODE_FUILER_ERROR.Succeed(GlobalErrorType.GLOBAL_RES_FLOW_NOT_SKIP_NODE_FUILER_ERROR);
                }
            }
            catch (Exception ex)
            {
                RemoveRepeatSubmit();
                throw ex;
            }
        }
        #endregion

        #endregion
    }
}
