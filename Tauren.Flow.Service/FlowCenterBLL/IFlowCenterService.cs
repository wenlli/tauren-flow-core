﻿using Tauren.Flow.Entity.Config;

namespace Tauren.Flow.Service.FlowCenterBLL
{

    /// <summary>
    /// 流程中心操作
    /// </summary>
    public interface IFlowCenterService
    {
        /// <summary>
        /// 分页获取信息
        /// </summary>
        /// <param name="queryModel"></param>
        /// <param name="Total"></param>
        /// <returns></returns>
        IResponseMessage Pages(Entity.Global.QueryModel queryModel);

        /// <summary>
        /// 查看流程图
        /// </summary>
        /// <param name="flowId"></param>
        /// <param name="versionId"></param>
        /// <returns></returns>
        IResponseMessage Diagram(long flowId, long versionId, long taskId);
        /// <summary>
        /// 首页的待办页面
        /// </summary>
        /// <param name="queryModel"></param>
        /// <returns></returns>
        IResponseMessage GetIndexPages(Entity.Global.QueryModel queryModel);
        /// <summary>
        /// 根据流程ID获取流程实例
        /// </summary>
        /// <param name="flowId"></param>
        /// <param name="versionId"></param>
        /// <param name="taskId"></param>
        /// <returns></returns>
        IResponseMessage FlowHistory(long flowId, long versionId, long taskId);
    }
}
