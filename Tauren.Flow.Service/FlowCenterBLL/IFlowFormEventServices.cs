﻿using System;
using System.Collections.Generic;
using System.Text;
using Tauren.Flow.Entity.Config;
using Tauren.Flow.Entity.Global;

namespace Tauren.Flow.Service.FlowCenterBLL
{
    /// <summary>
    /// 操作数据
    /// </summary>
    public interface IFlowFormEventServices
    {
        /// <summary>
        /// 保存表单数据
        /// </summary>
        /// <param name="flowRquestParameter"></param>
        /// <returns></returns>
        IResponseMessage FormSave(FlowRquestParameter Parameter);
        /// <summary>
        /// 清除重复数据
        /// </summary>
        /// <param name="flowRquestParameter"></param>
        /// <returns></returns>
        IResponseMessage ClearRepeatKey(string SecretKey, string TaskId);
    }
}
