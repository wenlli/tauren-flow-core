﻿using System;
using System.Collections.Generic;
using System.Text;
using Tauren.Flow.Entity.Config;
using Tauren.Flow.Entity.Global;

namespace Tauren.Flow.Service.FlowCenterBLL
{
    /// <summary>
    /// 流程中心
    /// </summary>
    public interface IFlowFromService
    {
        /// <summary>
        /// 获取表单字段
        /// </summary>
        /// <returns></returns>
        IResponseMessage FlowFormFieldConfig(FlowParameter Paramter);

        /// <summary>
        /// 获取表单数据
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        IResponseMessage FlowFromData(FlowParameter Paramter);
    }
}
