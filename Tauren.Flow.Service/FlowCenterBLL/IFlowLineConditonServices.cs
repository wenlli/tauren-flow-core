﻿using System;
using System.Collections.Generic;
using System.Text;
using Tauren.Flow.Entity.Global;
using Tauren.Flow.Entity.Model;
using Tauren.Flow.Entity.Output;

namespace Tauren.Flow.Service.FlowCenterBLL
{
    /// <summary>
    /// 连线判断业务
    /// </summary>
    public interface IFlowLineConditonServices
    {
        /// <summary>
        /// 判断条件是否满足条件
        /// </summary>
        /// <param name="currentTask"></param>
        /// <param name="instance_Businesses"></param>
        /// <param name="lineParamters"></param>
        /// <returns></returns>
        Result<List<LineInfo>> EstimateLineInfo(FlowTaskInfo currentTask, List<Flow_Instance_Business> instance_Businesses, List<LineInfo> lineParamters);
    }
}
