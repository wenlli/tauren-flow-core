﻿using System;
using System.Collections.Generic;
using System.Text;
using Tauren.Flow.Entity.Config;
using Tauren.Flow.Entity.Global;

namespace Tauren.Flow.Service.FlowCenterBLL
{
    /// <summary>
    /// 流程获取下一步信息操作
    /// </summary>
    public interface IFlowNextTaskInfoEventService
    {
        /// <summary>
        /// 获取下一个节点信息集合
        /// </summary>
        /// <param name="Paramter"></param>
        /// <returns></returns>
        IResponseMessage NextNodeInfos(FlowRquestParameter Paramter);

        /// <summary>
        ///  获取下一个节点信息集合
        /// </summary>
        /// <param name="paramter"></param>
        /// <returns></returns>
        IResponseMessage CirculatedNextNodeInfo(FlowRquestParameter paramter);

        /// <summary>
        /// 获取要退回的节点
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        IResponseMessage BackStepNodeInfo(long taskId);

        /// <summary>
        /// 获取要跳过的信息
        /// </summary>
        /// <param name="Paramter"></param>
        /// <returns></returns>
        IResponseMessage SkipNodeInfo(FlowRquestParameter Paramter);
    }
}
