﻿using System;
using System.Collections.Generic;
using System.Text;
using Tauren.Flow.DLL.FlowCenterDLL;
using Tauren.Flow.DLL.FlowEngine;
using Tauren.Flow.Entity.Enum;
using Tauren.Flow.Entity.Global;
using Tauren.Flow.Entity.Model;
using Tauren.Flow.Entity.Output;
using Tauren.Flow.Service.ProcessBasicInfoBLL;
using System.Linq;
using System.Transactions;
using Tauren.Flow.Service.Base;
using Tauren.Flow.Entity.Const;
using Tauren.Flow.Infrastructure.Common;
using Newtonsoft.Json;

namespace Tauren.Flow.Service.FlowEngineBLL
{

    /// <summary>
    /// 退回
    /// </summary>
    public class FlowBackService
    {
        private IProcessBasicInfoService processBasicInfoService;
        private IFlowEngineBusiness business;
        private IFlowCenterBusiness flowCenterBusiness;
        private FlowEngineCommonEvent commonEvent = FlowEngineCommonEvent.Instacne;
        private LongCommon longCommon;
        public FlowBackService(IProcessBasicInfoService _processBasicInfoService, IFlowEngineBusiness _business, IFlowCenterBusiness _flowCenterBusiness, LongCommon _longCommon)
        {
            processBasicInfoService = _processBasicInfoService;
            business = _business;
            flowCenterBusiness = _flowCenterBusiness;
            longCommon = _longCommon;
        }

        #region 退回至第一步
        /// <summary>
        /// 进行任务退回
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        internal Result<long> FlowTaskBackEvent(long taskId, string opinion)
        {
            Result<long> result = new Result<long>(0);
            ///获取当前任务
            FlowTaskInfo currentTask = processBasicInfoService.QueryCurrentFlowTaskInfo(taskId);
            switch (currentTask)
            {
                case null: result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_CURRENTTASK_ERROR); break;
                default:
                    {
                        currentTask.Comment = opinion;
                        FlowTaskStatus taskStatus = (FlowTaskStatus)currentTask.Status;
                        FlowTaskType taskType = (FlowTaskType)currentTask.Type;
                        if (taskStatus != FlowTaskStatus.audit || taskType != FlowTaskType.task)
                            result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_CURRENT_INSTANCE_UN_BACK_ERROR);
                        else
                        {
                            Flow_OutPut _flow = processBasicInfoService.GetFlowInfo(currentTask.FlowId, currentTask.FlowVersionId);
                            Flow_Instance _Instance = business.GetBasicListByQuery<Flow_Instance>("instanceid", currentTask.InstanceId + "").FirstOrDefault();
                            switch (_flow)
                            {
                                case null: { result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_FORMINFO_ERROR); } break;
                                default:
                                    {
                                        ///获取当前节点信息
                                        NodeInfo currentStep = _flow.NodeList.FirstOrDefault(o => o.Meta.StepId == currentTask.StepId);
                                        switch (currentStep)
                                        {
                                            case null: result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_CURRENTSTEP_ERROR); break;
                                            default:
                                                {
                                                    ///判断是否在并行分支中-不是
                                                    if (string.IsNullOrEmpty(currentStep.Meta.Groups))
                                                    {
                                                        result = this.FlowSingeBackEvent(currentTask, currentStep, _Instance);
                                                    }
                                                    else
                                                    {
                                                        ///判断是否在并行分支中 - 是
                                                        List<Branchs> branchs = JsonConvert.DeserializeObject<List<Branchs>>(currentStep.Meta.Groups);
                                                        if (branchs.Any())
                                                        {
                                                            result = this.FlowParallelBackEvent(currentTask, currentStep, _Instance, branchs, _flow);
                                                        }
                                                        ///判断是否在并行分支中-不是
                                                        else
                                                        {
                                                            result = this.FlowSingeBackEvent(currentTask, currentStep, _Instance);
                                                        }
                                                    }
                                                }
                                                break;
                                        }
                                    }
                                    break;
                            }
                        }
                    }
                    break;
            }
            return result;
        }

        /// <summary>
        /// 判断是否在并行分支中-不是---todo
        /// </summary>
        /// <returns></returns>
        private Result<long> FlowSingeBackEvent(FlowTaskInfo currentTask, NodeInfo currentStep, Flow_Instance _Instance)
        {
            Result<long> result = new Result<long>(_Instance.ApplyTaskId);
            List<Flow_Task> flow_Tasks = this.business.GetTasks(currentTask.GroupId);
            List<Flow_Task> deleteTasks = new List<Flow_Task>();
            List<Flow_Task> currentTasks = new List<Flow_Task>();
            Flow_Task newTask = null;
            List<Flow_Instance_CurrentTask> flow_Instance_CurrentTasks = new List<Flow_Instance_CurrentTask>();
            Flow_Task applyTask = business.BasicGet<Flow_Task>(_Instance.ApplyTaskId);
            ApprovalStrategyType approvalStrategyType = (ApprovalStrategyType)currentStep.Strategy.ApprovalStrategy;
            switch (approvalStrategyType)
            {
                case ApprovalStrategyType.Disable:
                case ApprovalStrategyType.SingleAgreed:
                    {
                        deleteTasks = flow_Tasks.Where(o => o.Sort == (currentTask.Sort + 1)).ToList();
                        currentTasks = flow_Tasks.Where(o => o.Sort == currentTask.Sort).ToList();
                        newTask = GenerateTask(currentTask, applyTask);
                        _Instance.FlowStatus = (int)FlowInstanceStatus.momentum;
                        _Instance.ApplyTaskId = applyTask.TaskId;
                    }
                    break;
                case ApprovalStrategyType.AllAgree:
                    {
                        currentTasks = flow_Tasks.Where(o => o.Sort == currentTask.Sort).ToList();
                        if (currentTasks.Any(o => o.Status.In((int)FlowTaskStatus.approved, (int)FlowTaskStatus.terminated)))
                        {
                            result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOT_BACK_ERROR);
                            return result;
                        }
                        else
                        {
                            if (!currentTasks.Any(o => o.Status == (int)FlowTaskStatus.audit && o.TaskId != currentTask.TaskId))
                            {
                                deleteTasks = flow_Tasks.Where(o => o.Sort == (currentTask.Sort + 1)).ToList();
                                currentTasks = flow_Tasks.Where(o => o.Sort == currentTask.Sort).ToList();
                                newTask = GenerateTask(currentTask, applyTask);
                                _Instance.FlowStatus = (int)FlowInstanceStatus.momentum;
                                _Instance.ApplyTaskId = applyTask.TaskId;
                            }
                            else
                            {
                                currentTasks = flow_Tasks.Where(o => o.TaskId == currentTask.TaskId).ToList();
                            }
                        }
                    }
                    break;
                case ApprovalStrategyType.Scale:
                    {
                        decimal currentTaskCount = flow_Tasks.Count(o => o.Sort == currentTask.Sort && (o.Status.In((int)FlowTaskStatus.audit, (int)FlowTaskStatus.approved, (int)FlowTaskStatus.returned, (int)FlowTaskStatus.terminated)));
                        decimal doingTaskCount = flow_Tasks.Count(o => o.Sort == currentTask.Sort && (o.Status.In((int)FlowTaskStatus.terminated)));
                        decimal scale = 0;
                        if (currentTaskCount > 0 && doingTaskCount > 0)
                            scale = (doingTaskCount + 1 / currentTaskCount) * 100;
                        ///当还没有达到比例时处理临时任务
                        if (!string.IsNullOrEmpty(currentStep.Strategy.NodeApprovalScale) && scale < decimal.Parse(currentStep.Strategy.NodeApprovalScale))
                        {
                            if (currentTasks.Any(o => o.Status.In((int)FlowTaskStatus.approved, (int)FlowTaskStatus.terminated)))
                            {
                                result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOT_BACK_ERROR);
                                return result;
                            }
                            else
                            {
                                currentTasks = flow_Tasks.Where(o => o.TaskId == currentTask.TaskId).ToList();
                            }
                        }
                        else
                        {
                            deleteTasks = flow_Tasks.Where(o => o.Sort == (currentTask.Sort + 1)).ToList();
                            currentTasks = flow_Tasks.Where(o => o.Sort == currentTask.Sort).ToList();
                            newTask = GenerateTask(currentTask, applyTask);
                            _Instance.FlowStatus = (int)FlowInstanceStatus.momentum;
                            _Instance.ApplyTaskId = applyTask.TaskId;
                        }
                    }
                    break;
            }
            bool b = RunTaskBackEvent(deleteTasks, currentTasks, currentTask, flow_Instance_CurrentTasks, _Instance, newTask);
            if (!b)
                result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_CURRENT_INSTANCE_UN_BACK_ERROR);
            return result;
        }
        /// <summary>
        /// 并行分支退回
        /// </summary>
        /// <returns></returns>--todo
        private Result<long> FlowParallelBackEvent(FlowTaskInfo currentTask, NodeInfo currentStep, Flow_Instance _Instance, List<Branchs> branchs, Flow_OutPut flowInfo)
        {
            Result<long> result = new Result<long>(_Instance.ApplyTaskId);
            List<Flow_Task> flow_Tasks = this.business.GetTasks(currentTask.GroupId);
            List<Flow_Task> deleteTasks = new List<Flow_Task>();
            List<Flow_Task> currentTasks = new List<Flow_Task>();
            Flow_Task newTask = null;
            NodeInfo startBranchNode = flowInfo.NodeList.FirstOrDefault(o => o.Meta.StepId == branchs[0].Branch);
            BranchStrategyType backStrategy = EnumConvertCommon.ConvertType<BranchStrategyType>(startBranchNode.Meta.BackStrategy);
            List<Flow_Branch_Task> current_Branch_TaskList = business.GetBasicListByQuery<Flow_Branch_Task>("groupid", currentTask.GroupId + "");
            List<Flow_Instance_CurrentTask> flow_Instance_CurrentTasks = new List<Flow_Instance_CurrentTask>();
            Flow_Task applyTask = business.BasicGet<Flow_Task>(_Instance.ApplyTaskId);
            ApprovalStrategyType approvalStrategyType = (ApprovalStrategyType)currentStep.Strategy.ApprovalStrategy;
            switch (approvalStrategyType)
            {
                case ApprovalStrategyType.Disable:
                case ApprovalStrategyType.SingleAgreed:
                    {
                        this.BranchBackStrategy(backStrategy, ref _Instance, ref current_Branch_TaskList, ref flow_Tasks, currentTask, startBranchNode, ref deleteTasks, ref currentTasks, ref newTask, applyTask, ref result);
                    }
                    break;
                case ApprovalStrategyType.AllAgree:
                    {
                        currentTasks = flow_Tasks.Where(o => o.Sort == currentTask.Sort).ToList();
                        if (currentTasks.Any(o => o.Status.In((int)FlowTaskStatus.approved, (int)FlowTaskStatus.terminated)))
                        {
                            result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOT_BACK_ERROR);
                            return result;
                        }
                        else
                        {
                            if (!currentTasks.Any(o => o.Status == (int)FlowTaskStatus.audit && o.TaskId != currentTask.TaskId))
                            {
                                this.BranchBackStrategy(backStrategy, ref _Instance, ref current_Branch_TaskList, ref flow_Tasks, currentTask, startBranchNode, ref deleteTasks, ref currentTasks, ref newTask, applyTask, ref result);
                            }
                            else
                            {
                                currentTasks = flow_Tasks.Where(o => o.TaskId == currentTask.TaskId).ToList();
                            }
                        }
                    }
                    break;
                case ApprovalStrategyType.Scale:
                    {
                        decimal currentTaskCount = flow_Tasks.Count(o => o.Sort == currentTask.Sort && (o.Status.In((int)FlowTaskStatus.audit, (int)FlowTaskStatus.approved, (int)FlowTaskStatus.returned, (int)FlowTaskStatus.terminated)));
                        decimal doingTaskCount = flow_Tasks.Count(o => o.Sort == currentTask.Sort && (o.Status.In((int)FlowTaskStatus.terminated)));
                        decimal scale = 0;
                        if (currentTaskCount > 0 && doingTaskCount > 0)
                            scale = (doingTaskCount + 1 / currentTaskCount) * 100;
                        ///当还没有达到比例时处理临时任务
                        if (!string.IsNullOrEmpty(currentStep.Strategy.NodeApprovalScale) && scale < decimal.Parse(currentStep.Strategy.NodeApprovalScale))
                        {
                            if (currentTasks.Any(o => o.Status.In((int)FlowTaskStatus.approved, (int)FlowTaskStatus.terminated)))
                            {
                                result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOT_BACK_ERROR);
                                return result;
                            }
                            else
                            {
                                currentTasks = flow_Tasks.Where(o => o.TaskId == currentTask.TaskId).ToList();
                            }
                        }
                        else
                        {
                            this.BranchBackStrategy(backStrategy, ref _Instance, ref current_Branch_TaskList, ref flow_Tasks, currentTask, startBranchNode, ref deleteTasks, ref currentTasks, ref newTask, applyTask, ref result);
                        }
                    }
                    break;
            }
            bool b = RunTaskBackEvent(deleteTasks, currentTasks, currentTask, flow_Instance_CurrentTasks, _Instance, newTask);
            if (!b)
                result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_CURRENT_INSTANCE_UN_BACK_ERROR);
            return result;
        }

        /// <summary>
        /// 根据分支终止策略执行
        /// </summary>
        /// <param name="endStrategy"></param>
        /// <param name="_Instance"></param>
        /// <param name="complatedParameters"></param>
        /// <param name="instance_Businesses"></param>
        /// <param name="current_Branch_TaskList"></param>
        /// <param name="flow_Tasks"></param>
        /// <param name="currentTask"></param>
        /// <param name="startBranchNode"></param>
        private void BranchBackStrategy(BranchStrategyType endStrategy, ref Flow_Instance _Instance, ref List<Flow_Branch_Task> current_Branch_TaskList, ref List<Flow_Task> flow_Tasks, FlowTaskInfo currentTask, NodeInfo startBranchNode, ref List<Flow_Task> deleteTasks, ref List<Flow_Task> currentTasks, ref Flow_Task newTask, Flow_Task applyTask, ref Result<long> result)
        {
            List<Flow_Branch_Task> lineTasks = current_Branch_TaskList.Where(o => !string.IsNullOrEmpty(o.BranchLine) && o.Branch == startBranchNode.Meta.BranchTag).ToList();
            Flow_Branch_Task branch_Task = lineTasks.FirstOrDefault(o => o.TaskId == currentTask.TaskId && !string.IsNullOrEmpty(o.BranchLine));
            var otherLineTasks = lineTasks.Where(o => o.BranchLine != branch_Task.BranchLine).ToList();
            var otherTasks = otherLineTasks.Join(flow_Tasks, a => a.TaskId, b => b.TaskId, (a, b) => b);
            switch (endStrategy)
            {
                ///一个分支终止则终止
                case BranchStrategyType.SingleAgreed:
                    {
                        deleteTasks = flow_Tasks.Where(o => o.Sort == (currentTask.Sort + 1)).ToList();
                        currentTasks = flow_Tasks.Where(o => o.Sort == currentTask.Sort).ToList();
                        newTask = GenerateTask(currentTask, applyTask);
                        _Instance.FlowStatus = (int)FlowInstanceStatus.momentum;
                        _Instance.ApplyTaskId = applyTask.TaskId;
                    }
                    break;
                ///所有分支终止则终止
                case BranchStrategyType.AllAgree:
                    {
                        if (otherTasks.Any(o => o.Status == (int)FlowTaskStatus.audit))
                        {
                            currentTasks = flow_Tasks.Where(o => o.TaskId == currentTask.TaskId).ToList();
                        }
                        else
                        {
                            if (otherTasks.Any(o => o.Status.In((int)FlowTaskStatus.approved, (int)FlowTaskStatus.terminated)))
                            {
                                result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOT_BACK_ERROR);
                            }
                            else
                            {
                                deleteTasks = flow_Tasks.Where(o => o.Sort == (currentTask.Sort + 1)).ToList();
                                currentTasks = flow_Tasks.Where(o => o.Sort == currentTask.Sort).ToList();
                                newTask = GenerateTask(currentTask, applyTask);
                                _Instance.FlowStatus = (int)FlowInstanceStatus.momentum;
                                _Instance.ApplyTaskId = applyTask.TaskId;
                            }
                        }
                    }
                    break;
                case BranchStrategyType.Scale:
                    {
                        var groups = lineTasks.GroupBy(o => new { o.Branch, o.BranchLine });
                        decimal cnt = groups.Count();
                        decimal doedCnt = 0;
                        foreach (var item in groups)
                        {
                            var task = item.Join(flow_Tasks, a => a.TaskId, b => b.TaskId, (a, b) => b);
                            if (task.Any(o => o.TaskId == currentTask.TaskId || o.Status == (int)FlowTaskStatus.returned))
                            {
                                doedCnt++;
                            }
                        }
                        decimal scale = 0;
                        if (cnt > 0 && doedCnt > 0)
                            scale = (doedCnt / cnt) * 100;
                        if (!string.IsNullOrEmpty(startBranchNode.Meta.EndScale) && scale < decimal.Parse(startBranchNode.Meta.EndScale))
                        {
                            if (otherTasks.Any(o => o.Status == (int)FlowTaskStatus.audit))
                            {
                                currentTasks = flow_Tasks.Where(o => o.TaskId == currentTask.TaskId).ToList();
                            }
                            else
                            {
                                result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOT_BACK_ERROR);
                            }
                        }
                        else
                        {
                            deleteTasks = flow_Tasks.Where(o => o.Sort == (currentTask.Sort + 1)).ToList();
                            currentTasks = flow_Tasks.Where(o => o.Sort == currentTask.Sort).ToList();
                            newTask = GenerateTask(currentTask, applyTask);
                            _Instance.FlowStatus = (int)FlowInstanceStatus.momentum;
                            _Instance.ApplyTaskId = applyTask.TaskId;
                        }
                    }
                    break;
            }
        }
        private bool RunTaskBackEvent(List<Flow_Task> deleteTasks, List<Flow_Task> currentTasks, FlowTaskInfo currentTask, List<Flow_Instance_CurrentTask> flow_Instance_CurrentTasks, Flow_Instance _Instance, Flow_Task newTask)
        {
            bool b = true;
            currentTasks.ForEach(cu =>
            {
                cu.Modifdate = DateTime.Now;
                cu.Modifier = currentTask.Reviewer;
                if (cu.TaskId == currentTask.TaskId)
                {
                    cu.Status = (int)FlowTaskStatus.returned;
                    cu.ProcessTime = DateTime.Now;
                    cu.Comment = currentTask.Comment;
                }
                else
                {
                    cu.Status = (int)FlowTaskStatus.otherreturned;
                }
            });
            using (TransactionScope ts1 = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions() { IsolationLevel = IsolationLevel.ReadCommitted }))
            {
                ///处理当前任务
                b = business.UpdateBasicBusiness(currentTasks);
                if (newTask != null)
                    b = b & business.AddBasicBusiness(new List<Flow_Task>() { newTask }) > 0;
                b = b & business.UpdateBasicBusiness(_Instance);
                b = b & business.DeleteCurrentTask(currentTasks.Select(o => o.TaskId).ToList());
                if (flow_Instance_CurrentTasks.Any())
                    b = b & business.AddBasicBusiness(flow_Instance_CurrentTasks) > 0;
                if (deleteTasks.Any())
                    b = b & business.RecyclingDeleteTask(deleteTasks);
                if (b) ts1.Complete();
            }
            return b;
        }
        /// <summary>
        /// 生产任务
        /// </summary>
        /// <param name=""></param>
        private Flow_Task GenerateTask(FlowTaskInfo currentTask, Flow_Task applyTask)
        {
            return new Flow_Task()
            {
                Creatdate = DateTime.Now,
                Creator = currentTask.Reviewer,
                GroupId = applyTask.GroupId,
                InstanceId = applyTask.InstanceId,
                Modifier = currentTask.Reviewer,
                Modifdate = DateTime.Now,
                PrevStepId = "0",
                PrevTaskId = 0,
                ReceivingTime = DateTime.Now,
                Sender = currentTask.Reviewer,
                Reviewer = applyTask.Reviewer,
                Sort = currentTask.Sort + 1,
                Type = (int)FlowTaskType.task,
                Status = (int)FlowTaskStatus.momentum,
                TaskId = longCommon.GenerateRandomCode(12),
                Title = applyTask.Title,
                StepId = applyTask.StepId
            };
        }
        #endregion

        #region 退回至具体步骤
        /// <summary>
        /// 退回至具体步骤
        /// </summary>
        /// <param name="paramter"></param>
        /// <returns></returns>
        internal Result<long> FlowTaskBackStepEvent(FlowParameter paramter)
        {
            Result<long> result = new Result<long>(0);
            ///获取当前任务
            FlowTaskInfo currentTask = processBasicInfoService.QueryCurrentFlowTaskInfo(paramter.TaskId);
            switch (currentTask)
            {
                case null: result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_CURRENTTASK_ERROR); break;
                default:
                    {
                        currentTask.Comment = paramter.Opinion;
                        FlowTaskStatus taskStatus = (FlowTaskStatus)currentTask.Status;
                        FlowTaskType taskType = (FlowTaskType)currentTask.Type;
                        if (taskStatus != FlowTaskStatus.audit || taskType != FlowTaskType.task)
                            result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_CURRENTTASK_FLOWSTATUS_ERROR);
                        else
                        {
                            Flow_OutPut _flow = processBasicInfoService.GetFlowInfo(currentTask.FlowId, currentTask.FlowVersionId);
                            Flow_Instance _Instance = business.GetBasicListByQuery<Flow_Instance>("instanceid", currentTask.InstanceId + "").FirstOrDefault();
                            switch (_flow)
                            {
                                case null: { result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_FORMINFO_ERROR); } break;
                                default:
                                    {
                                        ///获取当前节点信息
                                        NodeInfo currentStep = _flow.NodeList.FirstOrDefault(o => o.Meta.StepId == currentTask.StepId);
                                        switch (currentStep)
                                        {
                                            case null: result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_CURRENTSTEP_ERROR); break;
                                            default:
                                                {
                                                    result = this.FlowSingeBackStepEvent(currentTask, _flow.NodeList, _Instance, paramter.NodeInfo);
                                                }
                                                break;
                                        }
                                    }
                                    break;
                            }
                        }
                    }
                    break;
            }
            return result;
        }
        /// <summary>
        /// 分支退回具体步骤
        /// </summary>
        /// <param name="currentTask"></param>
        /// <param name="currentStep"></param>
        /// <param name="_Instance"></param>
        /// <returns></returns>
        [Obsolete("暂无使用，后期需求")]
        private Result<long> FlowParallelBackStepEvent(FlowTaskInfo currentTask, List<NodeInfo> nodeInfos, Flow_Instance _Instance, List<NextNodeInfoOutPut> NodeInfo)
        {
            Result<long> result = new Result<long>(0);
            List<Flow_Task> flow_Tasks = this.business.GetTasks(currentTask.GroupId);
            List<Flow_Task> deleteTasks = flow_Tasks.Where(o => o.Status == (int)FlowTaskStatus.temp).ToList();
            List<Flow_Task> currentTasks = flow_Tasks.Where(o => o.TaskId == currentTask.TaskId).ToList();
            bool isApply = !flow_Tasks.Any(o => o.Status == (int)FlowTaskStatus.audit && o.Sort != currentTask.Sort);
            var nextNodeInfos = nodeInfos.Join(NodeInfo, a => a.Meta.StepId, b => b.NextStepId, (a, b) => a).FirstOrDefault();
            if (nextNodeInfos != null)
            {
                List<Flow_Task> newTasks = new List<Flow_Task>();
                switch (nextNodeInfos.Meta.Prop)
                {
                    case GlobalConst.NODE_INFO_MEAT_START:
                        {
                            Flow_Task applyTask = business.BasicGet<Flow_Task>(_Instance.ApplyTaskId);
                            if (applyTask != null)
                            {
                                newTasks.Add(GenerateTask(currentTask, applyTask));
                                if (isApply)
                                    _Instance.FlowStatus = (int)FlowInstanceStatus.momentum;
                                _Instance.ApplyTaskId = applyTask.TaskId;
                            }
                        }; break;
                    default:
                        {
                            var nextNode = NodeInfo.FirstOrDefault();
                            nextNode.NextStepApproverEmpId.ForEach(o => newTasks.Add(GenerateBackStepData(currentTask, nextNode.NextStepId, o)));
                        }
                        break;
                }
                if (!SubmitBackStepDataEvent(currentTasks, currentTask, _Instance, newTasks, deleteTasks))
                    result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_CURRENT_INSTANCE_UN_BACK_ERROR);
            }
            else result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_CURRENT_INSTANCE_UN_BACK_ERROR);
            return result;
        }
        /// <summary>
        /// 单分支退回---todo
        /// </summary>
        /// <returns></returns>
        private Result<long> FlowSingeBackStepEvent(FlowTaskInfo currentTask, List<NodeInfo> nodeInfos, Flow_Instance _Instance, List<NextNodeInfoOutPut> NodeInfo)
        {
            Result<long> result = new Result<long>(_Instance.ApplyTaskId);
            List<Flow_Task> flow_Tasks = this.business.GetTasks(currentTask.GroupId);
            List<Flow_Task> deleteTasks = flow_Tasks.Where(o => o.Sort == (currentTask.Sort + 1)).ToList();
            List<Flow_Task> currentTasks = flow_Tasks.Where(o => o.Sort == currentTask.Sort).ToList();
            var nextNodeInfos = nodeInfos.Join(NodeInfo, a => a.Meta.StepId, b => b.NextStepId, (a, b) => a).FirstOrDefault();
            if (nextNodeInfos != null)
            {
                List<Flow_Task> newTasks = new List<Flow_Task>();
                switch (nextNodeInfos.Meta.Prop)
                {
                    case GlobalConst.NODE_INFO_MEAT_START:
                        {
                            Flow_Task applyTask = business.BasicGet<Flow_Task>(_Instance.ApplyTaskId);
                            if (applyTask != null)
                            {
                                newTasks.Add(GenerateTask(currentTask, applyTask));
                                _Instance.FlowStatus = (int)FlowInstanceStatus.momentum;
                                _Instance.ApplyTaskId = applyTask.TaskId;
                            }
                        }; break;
                    default:
                        {
                            var nextNode = NodeInfo.FirstOrDefault();
                            nextNode.NextStepApproverEmpId.ForEach(o => newTasks.Add(GenerateBackStepData(currentTask, nextNode.NextStepId, o)));
                        }
                        break;
                }
                if (!SubmitBackStepDataEvent(currentTasks, currentTask, _Instance, newTasks, deleteTasks))
                    result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_CURRENT_INSTANCE_UN_BACK_ERROR);
            }
            else result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_CURRENT_INSTANCE_UN_BACK_ERROR);
            return result;
        }
        /// <summary>
        /// 进行数据保存
        /// </summary>
        /// <returns></returns>
        private bool SubmitBackStepDataEvent(List<Flow_Task> currentTasks, FlowTaskInfo currentTask, Flow_Instance _Instance, List<Flow_Task> newTasks, List<Flow_Task> deleteTasks)
        {
            bool b = true;
            currentTasks.ForEach(cu =>
            {
                cu.Modifdate = DateTime.Now;
                cu.Modifier = currentTask.Reviewer;
                if (cu.TaskId == currentTask.TaskId)
                {
                    cu.Status = (int)FlowTaskStatus.returned;
                    cu.ProcessTime = DateTime.Now;
                    cu.Comment = currentTask.Comment;
                }
                else
                {
                    cu.Status = (int)FlowTaskStatus.otherreturned;
                }
            });
            List<Flow_Instance_CurrentTask> flow_Instance_CurrentTasks = this.commonEvent.CurrentInstanceTaskInfoEvent(currentTask, newTasks, FlowProcessType.Back);
            using (TransactionScope ts1 = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions() { IsolationLevel = IsolationLevel.ReadCommitted }))
            {
                b = true;
                ///处理当前任务
                b = business.UpdateBasicBusiness(currentTasks);
                b = b & business.AddBasicBusiness(newTasks) > 0;
                b = b & business.UpdateBasicBusiness(_Instance);
                b = b & business.DeleteCurrentTask(currentTask.PrevTaskId);
                if (flow_Instance_CurrentTasks.Any())
                    b = b & business.AddBasicBusiness(flow_Instance_CurrentTasks) > 0;
                if (deleteTasks.Any())
                    b = b & business.RecyclingDeleteTask(deleteTasks);
                if (b) ts1.Complete();
            }
            return b;
        }

        /// <summary>
        /// 组件退回任务
        /// </summary>
        /// <param name="currentTask"></param>
        /// <param name="stepId"></param>
        /// <param name="approver"></param>
        /// <returns></returns>
        private Flow_Task GenerateBackStepData(FlowTaskInfo currentTask, string stepId, int approver)
        {
            return new Flow_Task()
            {
                Creatdate = DateTime.Now,
                Creator = currentTask.Reviewer,
                GroupId = currentTask.GroupId,
                InstanceId = currentTask.InstanceId,
                Modifier = currentTask.Reviewer,
                Modifdate = DateTime.Now,
                PrevStepId = currentTask.StepId,
                PrevTaskId = currentTask.TaskId,
                ReceivingTime = DateTime.Now,
                Sender = currentTask.Reviewer,
                Reviewer = approver,
                Sort = currentTask.Sort + 1,
                Type = (int)FlowTaskType.task,
                Status = (int)FlowTaskStatus.audit,
                TaskId = longCommon.GenerateRandomCode(12),
                Title = currentTask.Title,
                StepId = stepId,
                Comment = $"任务退回"
            };
        }
        #endregion
    }
}
