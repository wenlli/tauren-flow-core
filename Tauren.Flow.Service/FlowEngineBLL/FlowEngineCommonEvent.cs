﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tauren.Flow.Entity.Enum;
using Tauren.Flow.Entity.Global;
using Tauren.Flow.Entity.Model;
using Tauren.Flow.Entity.Output;

namespace Tauren.Flow.Service.FlowEngineBLL
{

    /// <summary>
    /// 公共组合
    /// </summary>
    public class FlowEngineCommonEvent
    {
        public static FlowEngineCommonEvent Instacne = new FlowEngineCommonEvent();
        /// <summary>
        /// 将任务发送到下一步骤将任务信息保存
        /// </summary>
        /// <param name="prevTask"></param>
        /// <param name="flow_OutPut"></param>
        /// <param name="NodeInfo"></param>
        /// <returns></returns>
        public List<Flow_Instance_CurrentTask> CurrentInstanceTaskInfoEvent(FlowTaskInfo prevTask, List<Flow_Task> tasks, FlowProcessType flowProcessType = FlowProcessType.Normal)
        {
            List<Flow_Instance_CurrentTask> newTasks = new List<Flow_Instance_CurrentTask>();
            if (tasks != null && tasks.Any())
            {
                foreach (Flow_Task item in tasks)
                {
                    newTasks.Add(new Flow_Instance_CurrentTask()
                    {
                        CurrentStep = item.StepId,
                        CurrentTaskId = item.TaskId,
                        GroupId = prevTask.GroupId,
                        InstanceId = prevTask.InstanceId,
                        PrveStepId = prevTask.StepId,
                        PrveTaskId = prevTask.TaskId,
                        ProcessType = (int)flowProcessType
                    });
                }
            }
            return newTasks;
        }
        /// <summary>
        /// 任务完成和任务终止时将申请业务表中的数据状态更新
        /// </summary>
        /// <param name="instance_Businesses"></param>
        /// <returns></returns>
        public List<ComplatedParameter> GetComplatedParameters(List<Flow_Instance_Business> instance_Businesses, FlowInstanceStatus flowInstanceStatus)
        {
            List<ComplatedParameter> complatedParameters = new List<ComplatedParameter>();
            instance_Businesses.ForEach(i =>
            {
                complatedParameters.Add(new ComplatedParameter() { InstanceStatus = flowInstanceStatus, PrimaryKey = i.PrimaryKey, PrimaryValue = i.PrimaryValue, TableCode = $"{i.TableCode}apply" });
            });
            return complatedParameters;
        }
    }
}
