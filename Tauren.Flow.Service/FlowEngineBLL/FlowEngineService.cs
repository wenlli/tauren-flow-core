﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Tauren.Flow.DLL.Base;
using Tauren.Flow.DLL.FlowCenterDLL;
using Tauren.Flow.DLL.FlowEngine;
using Tauren.Flow.DLL.Resouces;
using Tauren.Flow.Entity.Config;
using Tauren.Flow.Entity.Const;
using Tauren.Flow.Entity.Enum;
using Tauren.Flow.Entity.Global;
using Tauren.Flow.Entity.Model;
using Tauren.Flow.Entity.Output;
using Tauren.Flow.Infrastructure.Common;
using Tauren.Flow.Infrastructure.Globals;
using Tauren.Flow.Service.Base;
using Tauren.Flow.Service.Flow_SrategyBLL;
using Tauren.Flow.Service.ProcessBasicInfoBLL;

namespace Tauren.Flow.Service.FlowEngineBLL
{
    /// <summary>
    /// 流程引擎底层事务
    /// </summary>
    public class FlowEngineService : BaseService, IFlowEngineService
    {
        private IFlowEngineBusiness business;
        private LongCommon longCommon;
        private IProcessBasicInfoService processBasicInfoService;
        public IFlow_Engine_SrategyService _Engine_SrategyService { get; set; }
        private FlowRecyclingService flowRecyclingService;
        private FlowEngineCommonEvent commonEvent = FlowEngineCommonEvent.Instacne;
        private FlowTaskReadService readService;
        private FlowBackService flowBackService;
        public FlowEngineService(IProcessBasicInfoService _processBasicInfoService, IFlowEngineBusiness _business, IConnectionBLLBase connectionBLL, IFlowCenterBusiness _flowCenterBusiness, IMenuResourcesBusiness resourcesBusiness = null) : base(_business, connectionBLL, resourcesBusiness)
        {
            this.processBasicInfoService = _processBasicInfoService;
            this.ResourcesInfo = this.GetResources(GlobalConst.RESOURCES_FLAG_MODULE_FLOWCENTER_KEY);
            this.JudePrmission(GlobalConst.RESOURCES_FLAG_MODULE_FLOWCENTER_KEY.ToLower());
            business = _business;
            longCommon = LongCommon.Default;
            flowRecyclingService = new FlowRecyclingService(processBasicInfoService, business, _flowCenterBusiness, longCommon);
            readService = new FlowTaskReadService(processBasicInfoService, business, longCommon, commonEvent);
            flowBackService = new FlowBackService(processBasicInfoService, business, _flowCenterBusiness, longCommon);
        }

        #region public
        /// <summary>
        /// 执行流程引擎
        /// </summary>
        /// <param name="Parameter"></param>
        /// <returns></returns>
        public IResponseMessage FlowEngineRunEvent(FlowParameter Parameter)
        {
            try
            {
                if (!HavePermission) return ErrorType.BUSINESS_WITHOUT_PERMISSION.Failure(this.CommonText(ErrorType.BUSINESS_WITHOUT_PERMISSION));
                if (!JudeTaskRunepeatSubmit(Parameter)) return GlobalErrorType.GLOBAL_RES_FLOW_CURRENTDATA_REPART_SUMBIT.Failure(CommonText(GlobalErrorType.GLOBAL_RES_FLOW_CURRENTDATA_REPART_SUMBIT));
                FlowTaskInfo currentTask = null;
                ///获取当前任务
                currentTask = processBasicInfoService.QueryCurrentFlowTaskInfo(Parameter.TaskId);
                if (currentTask is null)
                {
                    RemoveRepeatSubmit();
                    return GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_CURRENTTASK_ERROR.Failure(CommonText(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_CURRENTTASK_ERROR));
                }
                if (!string.IsNullOrEmpty(Parameter.Opinion))
                    currentTask.Comment = Parameter.Opinion;
                FlowTaskStatus taskStatus = (FlowTaskStatus)currentTask.Status;
                if (taskStatus != FlowTaskStatus.audit && taskStatus != FlowTaskStatus.momentum)
                {
                    RemoveRepeatSubmit();
                    return GlobalErrorType.GLOBAL_RES_FLOW_NOT_RUN_CURRENT_TASK_ERROR.Failure(CommonText(GlobalErrorType.GLOBAL_RES_FLOW_NOT_RUN_CURRENT_TASK_ERROR));
                }
                ///获取流程信息
                Flow_OutPut flow_OutPut = processBasicInfoService.GetFlowInfo(Parameter.FlowId, Parameter.FlowVersionId);
                ///获取当前节点信息
                NodeInfo currentStep = flow_OutPut.NodeList.FirstOrDefault(o => o.Meta.StepId == currentTask.StepId);
                if (currentStep is null)
                {
                    RemoveRepeatSubmit();
                    return GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_CURRENTSTEP_ERROR.Failure(CommonText(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_CURRENTSTEP_ERROR));
                }
                Result<ExtInstance> result = new Result<ExtInstance>(null);


                ///判断当前节点是否最后一个节点
                switch (currentStep.Meta.Prop)
                {
                    ///当前要处理的节点是结束节点
                    case GlobalConst.NODE_INFO_MEAT_END: result = ComplatedEvent(currentTask, flow_OutPut, currentStep); break;
                    ///当前要处理的节点不是结束节点
                    default:
                        {
                            ///不是分支组中的节点
                            if (string.IsNullOrEmpty(currentStep.Meta.Groups))
                            {
                                result = this.SingleRunEvent(currentTask, flow_OutPut, currentStep, Parameter.NodeInfo);
                            }
                            else
                            {
                                ///是分支组中的节点
                                List<Branchs> groups = JsonConvert.DeserializeObject<List<Branchs>>(currentStep.Meta.Groups);
                                if (groups.Any())
                                {
                                    result = this.BranchRunEvent(currentTask, flow_OutPut, currentStep, Parameter.NodeInfo);
                                }
                                else
                                {
                                    result = this.SingleRunEvent(currentTask, flow_OutPut, currentStep, Parameter.NodeInfo);
                                }
                            }
                        }
                        break;

                }
                if (result.Success) return result.Data.OnSucceed(CommonText(GlobalErrorType.GLOBAL_RES_FLOW_CURRENT_TASK_COMPLATED_SUCCESS));
                else
                {
                    RemoveRepeatSubmit();
                    return result.Code.Failure(CommonText(result.Code));
                }
            }
            catch (Exception ex)
            {
                RemoveRepeatSubmit();
                throw ex;
            }
        }

        /// <summary>
        /// 回收按钮
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        public IResponseMessage FlowRecycling(long taskId)
        {

            if (!HavePermission) return ErrorType.BUSINESS_WITHOUT_PERMISSION.Failure(this.CommonText(ErrorType.BUSINESS_WITHOUT_PERMISSION));
            string key = "recycling";
            if (!JudeTaskRunepeatSubmit(new FlowParameter() { TaskId = taskId }, key)) return GlobalErrorType.GLOBAL_RES_FLOW_CURRENTDATA__RECYCKLINE_REPART_SUMBIT.Failure(CommonText(GlobalErrorType.GLOBAL_RES_FLOW_CURRENTDATA__RECYCKLINE_REPART_SUMBIT));
            Result res = flowRecyclingService.FlowRecyclingEvent(taskId);
            if (res.Success)
            {
                RemoveRepeatSubmit(key);
                return "".OnSucceed(CommonText(GlobalErrorType.GLOBAL_RES_FLOW_CURRENT_TASE_RECYCKLINE_SUCCESS));
            }
            else
            {
                RemoveRepeatSubmit(key);
                return res.Code.Failure(CommonText(res.Code));
            }
        }

        /// <summary>
        /// 任务退回
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        public IResponseMessage FlowTaskBack(RequestParameter Parameter)
        {
            if (!HavePermission) return ErrorType.BUSINESS_WITHOUT_PERMISSION.Failure(this.CommonText(ErrorType.BUSINESS_WITHOUT_PERMISSION));
            string taskKey = "back";
            if (!JudeTaskRunepeatSubmit(new FlowParameter() { TaskId = Parameter.TaskId }, taskKey)) return GlobalErrorType.GLOBAL_RES_FLOW_CURRENTDATA__RECYCKLINE_REPART_SUMBIT.Failure(CommonText(GlobalErrorType.GLOBAL_RES_FLOW_CURRENTDATA__RECYCKLINE_REPART_SUMBIT));
            Result<long> res = flowBackService.FlowTaskBackEvent(Parameter.TaskId, Parameter.Opinion);
            if (res.Success)
            {
                RemoveRepeatSubmit(taskKey);
                return "".OnSucceed(CommonText(GlobalErrorType.GLOBAL_RES_FLOW_CURRENT_TASK_COMPLATED_SUCCESS));
            }
            else
            {
                RemoveRepeatSubmit(taskKey);
                return res.Code.Failure(CommonText(res.Code));
            }
        }

        /// <summary>
        /// 终止流程
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        public IResponseMessage FlowTermination(RequestParameter Parameter)
        {

            if (!HavePermission) return ErrorType.BUSINESS_WITHOUT_PERMISSION.Failure(this.CommonText(ErrorType.BUSINESS_WITHOUT_PERMISSION));
            string key = "end";
            if (!JudeTaskRunepeatSubmit(new FlowParameter() { TaskId = Parameter.TaskId }, key)) return GlobalErrorType.GLOBAL_RES_FLOW_CURRENTDATA__RECYCKLINE_REPART_SUMBIT.Failure(CommonText(GlobalErrorType.GLOBAL_RES_FLOW_CURRENTDATA__RECYCKLINE_REPART_SUMBIT));
            Result res = flowRecyclingService.FlowInstanceEndEvent(Parameter.TaskId, Parameter.Opinion);
            if (res.Success)
            {
                return "".OnSucceed(CommonText(GlobalErrorType.GLOBAL_RES_FLOW_CURRENT_TASK_END_APPROVALED_SUCCESS));
            }
            else
            {
                RemoveRepeatSubmit(key);
                return res.Code.Failure(CommonText(res.Code));
            }
        }

        /// <summary>
        /// 抄送和阅办进行读办
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        public IResponseMessage FlowRead(RequestParameter Parameter)
        {
            if (!HavePermission) return ErrorType.BUSINESS_WITHOUT_PERMISSION.Failure(this.CommonText(ErrorType.BUSINESS_WITHOUT_PERMISSION));
            if (!JudeTaskRunepeatSubmit(new FlowParameter() { TaskId = Parameter.TaskId })) return GlobalErrorType.GLOBAL_RES_FLOW_CURRENTDATA_READ_REPART_SUMBIT.Failure(CommonText(GlobalErrorType.GLOBAL_RES_FLOW_CURRENTDATA_READ_REPART_SUMBIT));
            Result res = readService.FlowTaskReadEvent(Parameter.TaskId, Parameter.Opinion);
            if (res.Success)
            {
                return "".OnSucceed(CommonText(ErrorType.SUCCESS_CODE));
            }
            else
            {
                RemoveRepeatSubmit();
                return res.Code.Failure(CommonText(res.Code));
            }
        }

        /// <summary>
        /// 处理传阅任务
        /// </summary>
        /// <param name="paramter"></param>
        /// <returns></returns>
        public IResponseMessage FlowEngineCirculatedEvent(FlowParameter paramter)
        {
            if (!HavePermission) return ErrorType.BUSINESS_WITHOUT_PERMISSION.Failure(this.CommonText(ErrorType.BUSINESS_WITHOUT_PERMISSION));
            if (!JudeTaskRunepeatSubmit(paramter)) return GlobalErrorType.GLOBAL_RES_FLOW_CURRENTDATA_REPART_SUMBIT.Failure(CommonText(GlobalErrorType.GLOBAL_RES_FLOW_CURRENTDATA_REPART_SUMBIT));
            ///获取当前任务
            FlowTaskInfo currentTask = processBasicInfoService.QueryCurrentFlowTaskInfo(paramter.TaskId);
            if (currentTask is null)
            {
                RemoveRepeatSubmit();
                return GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_CURRENTTASK_ERROR.Failure(CommonText(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_CURRENTTASK_ERROR));
            }
            FlowTaskStatus taskStatus = (FlowTaskStatus)currentTask.Status;
            ///未处理的情况下
            if (taskStatus != FlowTaskStatus.unread && taskStatus != FlowTaskStatus.audit && taskStatus != FlowTaskStatus.momentum)
            {
                RemoveRepeatSubmit();
                return GlobalErrorType.GLOBAL_RES_FLOW_NOT_RUN_CURRENT_TASK_ERROR.Failure(CommonText(GlobalErrorType.GLOBAL_RES_FLOW_NOT_RUN_CURRENT_TASK_ERROR));
            }
            ///获取流程信息
            Flow_OutPut flow_OutPut = processBasicInfoService.GetFlowInfo(paramter.FlowId, paramter.FlowVersionId);
            ///获取当前节点信息
            NodeInfo currentStep = flow_OutPut.NodeList.FirstOrDefault(o => o.Meta.StepId == currentTask.StepId);
            if (currentStep is null)
            {
                RemoveRepeatSubmit();
                return GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_CURRENTSTEP_ERROR.Failure(CommonText(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_CURRENTSTEP_ERROR));
            }
            List<Flow_Branch_Task> flow_Branch_Tasks = new List<Flow_Branch_Task>() { };
            ///创建新的传阅任务
            List<Flow_Task> newTasks = this.CreateNewTaskEvent(currentTask, flow_OutPut, paramter.NodeInfo, FlowTaskStatus.unread, ref flow_Branch_Tasks, FlowTaskType.circulated);
            Flow_Task flow_Task = null;
            if (currentTask.Type == (int)FlowTaskType.circulated)
            {
                flow_Task = business.BasicGet<Flow_Task>(currentTask.TaskId);
                flow_Task.Status = (int)FlowTaskStatus.red;
                flow_Task.ProcessTime = DateTime.Now;
                flow_Task.Modifdate = DateTime.Now;
                flow_Task.Modifier = currentTask.Reviewer;
                flow_Task.Comment = paramter.Opinion;
            }
            using (TransactionScope ts1 = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions() { IsolationLevel = IsolationLevel.ReadCommitted }))
            {
                bool b = true;
                if (flow_Task != null)
                    b = business.UpdateBasicBusiness<Flow_Task>(flow_Task);
                if (newTasks.Any())
                    b = b && business.AddBasicBusiness<List<Flow_Task>>(newTasks) >= 0;
                if (b)
                {
                    ts1.Complete();
                    return "".OnSucceed(CommonText(GlobalErrorType.GLOBAL_RES_FLOW_CURRENT_TASK_COMPLATED_SUCCESS));
                }
                else
                {
                    RemoveRepeatSubmit();
                    return GlobalErrorType.GLOBAL_RES_FLOW_RUN_CURRENT_TASK_COMPLETE_ERROR.Failure(CommonText(GlobalErrorType.GLOBAL_RES_FLOW_RUN_CURRENT_TASK_COMPLETE_ERROR));
                }
            }
        }

        /// <summary>
        /// 删除流程实例
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        public IResponseMessage FlowTaskDelete(long taskId)
        {
            if (!HavePermission) return ErrorType.BUSINESS_WITHOUT_PERMISSION.Failure(this.CommonText(ErrorType.BUSINESS_WITHOUT_PERMISSION));
            ///获取当前任务
            FlowTaskInfo currentTask = processBasicInfoService.QueryCurrentFlowTaskInfo(taskId);
            if (currentTask is null)
            {
                return GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_CURRENTTASK_ERROR.Failure(CommonText(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_CURRENTTASK_ERROR));
            }
            List<Flow_Instance_Business> instance_Businesses = baseDLL.GetBasicListByQuery<Flow_Instance_Business>("instanceid", currentTask.InstanceId + "");
            List<FlowDataAreaParameter> areas = new List<FlowDataAreaParameter>();
            instance_Businesses.ForEach(o =>
            {
                areas.Add(new FlowDataAreaParameter() { FlowTableCode = o.TableCode + "apply", PrimaryKey = o.PrimaryKey, PrimaryValue = o.PrimaryValue });
            });
            using (TransactionScope ts1 = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions() { IsolationLevel = IsolationLevel.ReadCommitted }))
            {
                bool b = business.FlowDeleteInstanceId(currentTask.GroupId, areas);
                if (b)
                {
                    ts1.Complete();
                    return "".OnSucceed(CommonText(GlobalErrorType.GLOBAL_RES_FLOW_DELETE_INSTANCE_ERROR));
                }
                else
                {
                    RemoveRepeatSubmit();
                    return GlobalErrorType.GLOBAL_RES_FLOW_DELETE_INSTANCE_FUILER_ERROR.Failure(CommonText(GlobalErrorType.GLOBAL_RES_FLOW_DELETE_INSTANCE_FUILER_ERROR));
                }
            }
        }

        /// <summary>
        /// 退回至具体步骤
        /// </summary>
        /// <param name="paramter"></param>
        /// <returns></returns>
        public IResponseMessage FlowTaskBackStep(FlowParameter paramter)
        {
            if (!HavePermission) return ErrorType.BUSINESS_WITHOUT_PERMISSION.Failure(this.CommonText(ErrorType.BUSINESS_WITHOUT_PERMISSION));
            string taskKey = "back";
            if (!JudeTaskRunepeatSubmit(new FlowParameter() { TaskId = paramter.TaskId }, taskKey)) return GlobalErrorType.GLOBAL_RES_FLOW_CURRENTDATA__RECYCKLINE_REPART_SUMBIT.Failure(CommonText(GlobalErrorType.GLOBAL_RES_FLOW_CURRENTDATA__RECYCKLINE_REPART_SUMBIT));
            Result<long> res = flowBackService.FlowTaskBackStepEvent(paramter);
            if (res.Success)
            {
                RemoveRepeatSubmit(taskKey);
                return "".OnSucceed(CommonText(GlobalErrorType.GLOBAL_RES_FLOW_CURRENT_TASK_COMPLATED_SUCCESS));
            }
            else
            {
                RemoveRepeatSubmit(taskKey);
                return res.Code.Failure(CommonText(res.Code));
            }
        }

        #endregion

        #region private
        /// <summary>
        /// 完成任务
        /// </summary>
        /// <returns></returns>
        private Result<ExtInstance> ComplatedEvent(FlowTaskInfo currentTask, Flow_OutPut flow_OutPut, NodeInfo currentStep)
        {
            Result<ExtInstance> result = new Result<ExtInstance>(null);
            ////获取业务关联数据
            List<Flow_Instance_Business> instance_Businesses = baseDLL.GetBasicListByQuery<Flow_Instance_Business>("instanceid", currentTask.InstanceId + "");
            ///组装回写状态数据
            List<ComplatedParameter> complatedParameters = commonEvent.GetComplatedParameters(instance_Businesses, FlowInstanceStatus.completed);
            ////回写的表和字段
            List<WriteField> writeFields = business.GetWriteField($"'{(string.Join("','", instance_Businesses.Select(o => o.TableCode)).Distinct())}'", flow_OutPut.FlowProperty.FormVersionId ?? 0);
            List<WriteData> writeDatas = new List<WriteData>();
            instance_Businesses.ForEach(bus =>
            {
                List<WriteField> resultFields = writeFields.Where(o => (o.TableCode + "").ToLower() == (bus.TableCode + "").ToLower()).ToList();
                WriteData writeData = new WriteData()
                {
                    BusinessId = long.Parse(bus.PrimaryValue),
                    InstanceId = currentTask.InstanceId,
                    MainTableCode = bus.MainTable,
                    TableCode = bus.TableCode,
                    WriteFields = resultFields
                };
                ////如果时员工信息表则要区分新增用户还是编辑用户还是重聘
                if (bus.TableCode == GlobalConst.FORM_EMPLOYEEBASICINFO_TABLENAME)
                {
                    writeData.IsAdd = !baseDLL.BasicGetValue<bool>(bus.TableCode + "apply", "autoid", bus.PrimaryValue, "disposetype");
                }
                writeDatas.Add(writeData);
            });
            using (TransactionScope ts1 = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions() { IsolationLevel = IsolationLevel.ReadCommitted }))
            {
                ///完成当前流程及任务
                bool b = this.business.ProcessComplete(currentTask);
                ///回写状态
                b = b && this.business.RunTaskUpdateApplyBusinessStatus(complatedParameters);
                ///回写数据
                b = b & business.FlowComplatedDataProcess(writeDatas);
                ///推送任务到消息队列--todo 
                if (b)
                    ts1.Complete();
                else
                    result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_RUN_CURRENT_TASK_COMPLETE_ERROR);
            }
            return result;
        }

        #region 单个分支进行任务处理任务
        /// <summary>
        /// 单个分支流程处理
        /// </summary>
        /// <param name="currentTask"></param>
        /// <returns></returns>
        private Result<ExtInstance> SingleRunEvent(FlowTaskInfo currentTask, Flow_OutPut flow_OutPut, NodeInfo currentStep, List<NextNodeInfoOutPut> NodeInfo)
        {
            List<Flow_Task> flow_Tasks = this.business.GetTasks(currentTask.GroupId);
            List<Flow_Task> currentStepAllTasks = new List<Flow_Task>();
            ApprovalStrategyType approvalStrategyType = (ApprovalStrategyType)currentStep.Strategy.ApprovalStrategy;
            Flow_Instance _Instance = baseDLL.GetBasicListByQuery<Flow_Instance>("instanceid", currentTask.InstanceId + "").FirstOrDefault();
            List<Flow_Task> newTasks = new List<Flow_Task>();
            List<Flow_Instance_CurrentTask> flow_Instance_CurrentTasks = new List<Flow_Instance_CurrentTask>();
            List<Flow_Branch_Task> flow_Branch_TaskList = new List<Flow_Branch_Task>();

            ///判断当前节点是否多人节点
            switch (approvalStrategyType)
            {
                ///不启用多人审批
                case ApprovalStrategyType.Disable:
                ///一人通过即可
                case ApprovalStrategyType.SingleAgreed:
                    {
                        ///处理当前任务
                        currentStepAllTasks = ProcessCurrentTasksEvent(currentTask, flow_Tasks);
                        //添加新任务
                        newTasks = this.CreateNewTaskEvent(currentTask, flow_OutPut, NodeInfo, FlowTaskStatus.audit, ref flow_Branch_TaskList);
                        ///当前步骤信息
                        flow_Instance_CurrentTasks = this.commonEvent.CurrentInstanceTaskInfoEvent(currentTask, newTasks);
                    }
                    break;
                case ApprovalStrategyType.AllAgree:
                    {
                        ///当前节点上还有其他人未处理的任务
                        if (flow_Tasks.Any(o => o.TaskId != currentTask.TaskId && o.Status == (int)FlowTaskStatus.audit))
                        {
                            var cuTask = flow_Tasks.FirstOrDefault(o => o.TaskId == currentTask.TaskId);
                            cuTask.Status = (int)FlowTaskStatus.approved;
                            cuTask.Read = true;
                            cuTask.ProcessTime = DateTime.Now;
                            cuTask.Modifier = currentTask.Reviewer;
                            cuTask.Modifdate = DateTime.Now;
                            currentStepAllTasks.Add(cuTask);
                            var endNode = flow_OutPut.NodeList.FirstOrDefault(o => o.Meta.Prop == GlobalConst.NODE_INFO_MEAT_END);
                            ///组装临时任务
                            newTasks = this.CreateNewTaskEvent(currentTask, flow_OutPut, NodeInfo, FlowTaskStatus.temp, ref flow_Branch_TaskList).Where(o => o.StepId != endNode.Meta.StepId).ToList();
                        }
                        else
                        {
                            ///处理当前任务
                            currentStepAllTasks = ProcessCurrentTasksEvent(currentTask, flow_Tasks);
                            //添加新任务
                            newTasks = this.CreateNewTaskEvent(currentTask, flow_OutPut, NodeInfo, FlowTaskStatus.audit, ref flow_Branch_TaskList);
                            ///当前步骤信息
                            flow_Instance_CurrentTasks = this.commonEvent.CurrentInstanceTaskInfoEvent(currentTask, newTasks);
                        }
                    }
                    break;
                case ApprovalStrategyType.Scale:
                    {
                        decimal currentTaskCount = flow_Tasks.Count(o => o.Sort == currentTask.Sort && (o.Status.In((int)FlowTaskStatus.audit, (int)FlowTaskStatus.approved, (int)FlowTaskStatus.returned, (int)FlowTaskStatus.terminated)));
                        decimal doingTaskCount = flow_Tasks.Count(o => o.Sort == currentTask.Sort && (o.Status.In((int)FlowTaskStatus.approved, (int)FlowTaskStatus.returned, (int)FlowTaskStatus.terminated)));
                        decimal scale = 0;
                        if (currentTaskCount > 0 && doingTaskCount > 0)
                            scale = (doingTaskCount + 1 / currentTaskCount) * 100;
                        ///当还没有达到比例时处理临时任务
                        if (!string.IsNullOrEmpty(currentStep.Strategy.NodeApprovalScale) && scale < decimal.Parse(currentStep.Strategy.NodeApprovalScale))
                        {
                            var cuTask = flow_Tasks.FirstOrDefault(o => o.TaskId == currentTask.TaskId);
                            cuTask.Status = (int)FlowTaskStatus.approved;
                            cuTask.Read = true;
                            cuTask.ProcessTime = DateTime.Now;
                            cuTask.Modifier = currentTask.Reviewer;
                            cuTask.Modifdate = DateTime.Now;
                            currentStepAllTasks.Add(cuTask);
                            var endNode = flow_OutPut.NodeList.FirstOrDefault(o => o.Meta.Prop == GlobalConst.NODE_INFO_MEAT_END);
                            ///组装临时任务
                            newTasks = this.CreateNewTaskEvent(currentTask, flow_OutPut, NodeInfo, FlowTaskStatus.temp, ref flow_Branch_TaskList).Where(o => o.StepId != endNode.Meta.StepId).ToList();
                        }
                        else
                        {
                            ///处理当前任务
                            currentStepAllTasks = ProcessCurrentTasksEvent(currentTask, flow_Tasks);
                            //添加新任务
                            newTasks = this.CreateNewTaskEvent(currentTask, flow_OutPut, NodeInfo, FlowTaskStatus.audit, ref flow_Branch_TaskList);
                            ///当前步骤信息
                            flow_Instance_CurrentTasks = this.commonEvent.CurrentInstanceTaskInfoEvent(currentTask, newTasks);
                        }
                    }
                    break;
            }
            ///进行所有任务数据操作
            return this.ProcessSaveTaskInfoEvent(new ExtTaskInfoEntity()
            {
                CurrentStep = currentStep,
                CurrentTask = currentTask,
                CurrentStepAllTasks = currentStepAllTasks,
                Flow_Instance_CurrentTasks = flow_Instance_CurrentTasks,
                Flow_OutPut = flow_OutPut,
                NewTasks = newTasks,
                Instance = _Instance,
                Flow_Branch_Tasks = flow_Branch_TaskList
            });
        }

        /// <summary>
        /// 并行分支流程处理
        /// </summary>
        /// <param name="currentTask"></param>
        /// <returns></returns>
        private Result<ExtInstance> BranchRunEvent(FlowTaskInfo currentTask, Flow_OutPut flow_OutPut, NodeInfo currentStep, List<NextNodeInfoOutPut> NodeInfo)
        {

            ///判断是否到分支节点结束
            if (NodeInfo.Count == 1 && NodeInfo.FirstOrDefault().FlowTaskType == FlowTaskType.endbranch)
            {
                return this.EndBranchRunEvent(currentTask, flow_OutPut, currentStep, NodeInfo);
            }
            else
            {
                return this.BrancApprovalRunEvent(currentTask, flow_OutPut, currentStep, NodeInfo);
            }
        }

        /// <summary>
        /// 如果后面节点是审批节点
        /// </summary>
        /// <param name="currentTask"></param>
        /// <returns></returns>
        private Result<ExtInstance> BrancApprovalRunEvent(FlowTaskInfo currentTask, Flow_OutPut flow_OutPut, NodeInfo currentStep, List<NextNodeInfoOutPut> NodeInfo)
        {
            List<Flow_Task> flow_Tasks = this.business.GetTasks(currentTask.GroupId);
            List<Flow_Task> currentStepAllTasks = new List<Flow_Task>();
            ApprovalStrategyType approvalStrategyType = (ApprovalStrategyType)currentStep.Strategy.ApprovalStrategy;
            Flow_Instance _Instance = baseDLL.GetBasicListByQuery<Flow_Instance>("instanceid", currentTask.InstanceId + "").FirstOrDefault();
            List<Flow_Task> newTasks = new List<Flow_Task>();
            List<Flow_Instance_CurrentTask> flow_Instance_CurrentTasks = new List<Flow_Instance_CurrentTask>();
            List<Flow_Branch_Task> flow_Branch_TaskList = new List<Flow_Branch_Task>();
            // List<Flow_Branch_Task> current_Branch_TaskList = baseDLL.GetBasicListByQuery<Flow_Branch_Task>("groupid", currentTask.GroupId + "");
            ///判断当前节点是否多人节点
            switch (approvalStrategyType)
            {
                ///不启用多人审批
                case ApprovalStrategyType.Disable:
                ///一人通过即可
                case ApprovalStrategyType.SingleAgreed:
                    {
                        ///处理当前任务
                        currentStepAllTasks = ProcessBranchCurrentTasksEvent(currentTask, flow_Tasks, currentStep, flow_OutPut.NodeList);
                        //添加新任务
                        newTasks = this.CreateNewTaskEvent(currentTask, flow_OutPut, NodeInfo, FlowTaskStatus.audit, ref flow_Branch_TaskList);
                        ///当前步骤信息
                        flow_Instance_CurrentTasks = this.commonEvent.CurrentInstanceTaskInfoEvent(currentTask, newTasks);
                    }
                    break;
                case ApprovalStrategyType.AllAgree:
                    {
                        ///当前节点上还有其他人未处理的任务
                        if (flow_Tasks.Any(o => o.TaskId != currentTask.TaskId && o.Status == (int)FlowTaskStatus.audit && o.StepId == currentTask.StepId))
                        {
                            var cuTask = flow_Tasks.FirstOrDefault(o => o.TaskId == currentTask.TaskId);
                            cuTask.Status = (int)FlowTaskStatus.approved;
                            cuTask.Read = true;
                            cuTask.ProcessTime = DateTime.Now;
                            cuTask.Modifier = currentTask.Reviewer;
                            cuTask.Modifdate = DateTime.Now;
                            currentStepAllTasks.Add(cuTask);
                            var endNode = flow_OutPut.NodeList.FirstOrDefault(o => o.Meta.Prop == GlobalConst.NODE_INFO_MEAT_END);
                            ///组装临时任务
                            newTasks = this.CreateNewTaskEvent(currentTask, flow_OutPut, NodeInfo, FlowTaskStatus.temp, ref flow_Branch_TaskList).Where(o => o.StepId != endNode.Meta.StepId).ToList();
                        }
                        else
                        {
                            ///处理当前任务
                            currentStepAllTasks = ProcessBranchCurrentTasksEvent(currentTask, flow_Tasks, currentStep, flow_OutPut.NodeList);
                            //添加新任务
                            newTasks = this.CreateNewTaskEvent(currentTask, flow_OutPut, NodeInfo, FlowTaskStatus.audit, ref flow_Branch_TaskList);
                            ///当前步骤信息
                            flow_Instance_CurrentTasks = this.commonEvent.CurrentInstanceTaskInfoEvent(currentTask, newTasks);
                        }
                    }
                    break;
                case ApprovalStrategyType.Scale:
                    {
                        decimal currentTaskCount = flow_Tasks.Count(o => o.Sort == currentTask.Sort && o.StepId == currentTask.StepId && (o.Status.In((int)FlowTaskStatus.audit, (int)FlowTaskStatus.approved, (int)FlowTaskStatus.returned, (int)FlowTaskStatus.terminated)));
                        decimal doingTaskCount = flow_Tasks.Count(o => o.Sort == currentTask.Sort && o.StepId == currentTask.StepId && (o.Status.In((int)FlowTaskStatus.approved, (int)FlowTaskStatus.returned, (int)FlowTaskStatus.terminated)));
                        decimal scale = 0;
                        if (currentTaskCount > 0 && doingTaskCount > 0)
                            scale = (doingTaskCount + 1 / currentTaskCount) * 100;
                        ///当还没有达到比例时处理临时任务
                        if (!string.IsNullOrEmpty(currentStep.Strategy.NodeApprovalScale) && scale < decimal.Parse(currentStep.Strategy.NodeApprovalScale))
                        {
                            var cuTask = flow_Tasks.FirstOrDefault(o => o.TaskId == currentTask.TaskId);
                            cuTask.Status = (int)FlowTaskStatus.approved;
                            cuTask.Read = true;
                            cuTask.ProcessTime = DateTime.Now;
                            cuTask.Modifier = currentTask.Reviewer;
                            cuTask.Modifdate = DateTime.Now;
                            currentStepAllTasks.Add(cuTask);
                            var endNode = flow_OutPut.NodeList.FirstOrDefault(o => o.Meta.Prop == GlobalConst.NODE_INFO_MEAT_END);
                            ///组装临时任务
                            newTasks = this.CreateNewTaskEvent(currentTask, flow_OutPut, NodeInfo, FlowTaskStatus.temp, ref flow_Branch_TaskList).Where(o => o.StepId != endNode.Meta.StepId).ToList();
                        }
                        else
                        {
                            ///处理当前任务
                            currentStepAllTasks = ProcessBranchCurrentTasksEvent(currentTask, flow_Tasks, currentStep, flow_OutPut.NodeList);
                            //添加新任务
                            newTasks = this.CreateNewTaskEvent(currentTask, flow_OutPut, NodeInfo, FlowTaskStatus.audit, ref flow_Branch_TaskList);
                            ///当前步骤信息
                            flow_Instance_CurrentTasks = this.commonEvent.CurrentInstanceTaskInfoEvent(currentTask, newTasks);
                        }
                    }
                    break;
            }
            ///进行所有任务数据操作
            return this.ProcessSaveTaskInfoEvent(new ExtTaskInfoEntity()
            {
                CurrentStep = currentStep,
                CurrentTask = currentTask,
                CurrentStepAllTasks = currentStepAllTasks,
                Flow_Instance_CurrentTasks = flow_Instance_CurrentTasks,
                Flow_OutPut = flow_OutPut,
                NewTasks = newTasks,
                Instance = _Instance,
                Flow_Branch_Tasks = flow_Branch_TaskList
            });
        }


        private Result<ExtInstance> EndBranchRunEvent(FlowTaskInfo currentTask, Flow_OutPut flow_OutPut, NodeInfo currentStep, List<NextNodeInfoOutPut> nodeInfos)
        {
            List<Flow_Task> flow_Tasks = this.business.GetTasks(currentTask.GroupId);
            List<Flow_Task> currentStepAllTasks = new List<Flow_Task>();
            ApprovalStrategyType approvalStrategyType = (ApprovalStrategyType)currentStep.Strategy.ApprovalStrategy;
            Flow_Instance _Instance = baseDLL.GetBasicListByQuery<Flow_Instance>("instanceid", currentTask.InstanceId + "").FirstOrDefault();
            List<Flow_Task> newTasks = new List<Flow_Task>();
            List<Flow_Instance_CurrentTask> flow_Instance_CurrentTasks = new List<Flow_Instance_CurrentTask>();
            List<Flow_Branch_Task> flow_Branch_TaskList = new List<Flow_Branch_Task>();
            List<Flow_Branch_Task> current_Branch_TaskList = baseDLL.GetBasicListByQuery<Flow_Branch_Task>("groupid", currentTask.GroupId + "");

            ///判断当前节点是否多人节点
            switch (approvalStrategyType)
            {
                ///不启用多人审批
                case ApprovalStrategyType.Disable:
                ///一人通过即可
                case ApprovalStrategyType.SingleAgreed:
                    {
                        BuildBranchStrategy(currentTask, flow_OutPut, currentStep, nodeInfos, flow_Tasks, current_Branch_TaskList, ref currentStepAllTasks, ref newTasks, ref flow_Branch_TaskList, ref flow_Instance_CurrentTasks);
                    }
                    break;
                case ApprovalStrategyType.AllAgree:
                    {
                        ///当前节点上还有其他人未处理的任务
                        if (flow_Tasks.Any(o => o.TaskId != currentTask.TaskId && o.Status == (int)FlowTaskStatus.audit && o.StepId == currentTask.StepId))
                        {
                            var cuTask = flow_Tasks.FirstOrDefault(o => o.TaskId == currentTask.TaskId);
                            cuTask.Status = (int)FlowTaskStatus.approved;
                            cuTask.Read = true;
                            cuTask.ProcessTime = DateTime.Now;
                            cuTask.Modifier = currentTask.Reviewer;
                            cuTask.Modifdate = DateTime.Now;
                            currentStepAllTasks.Add(cuTask);
                            var endNode = flow_OutPut.NodeList.FirstOrDefault(o => o.Meta.Prop == GlobalConst.NODE_INFO_MEAT_END);
                            ///组装临时任务
                            newTasks = this.CreateNewTaskEvent(currentTask, flow_OutPut, nodeInfos, FlowTaskStatus.temp, ref flow_Branch_TaskList).Where(o => o.StepId != endNode.Meta.StepId).ToList();
                        }
                        else
                        {
                            BuildBranchStrategy(currentTask, flow_OutPut, currentStep, nodeInfos, flow_Tasks, current_Branch_TaskList, ref currentStepAllTasks, ref newTasks, ref flow_Branch_TaskList, ref flow_Instance_CurrentTasks);
                        }
                    }
                    break;
                case ApprovalStrategyType.Scale:
                    {
                        decimal currentTaskCount = flow_Tasks.Count(o => o.Sort == currentTask.Sort && o.StepId == currentTask.StepId && (o.Status.In((int)FlowTaskStatus.audit, (int)FlowTaskStatus.approved, (int)FlowTaskStatus.returned, (int)FlowTaskStatus.terminated)));
                        decimal doingTaskCount = flow_Tasks.Count(o => o.Sort == currentTask.Sort && o.StepId == currentTask.StepId && (o.Status.In((int)FlowTaskStatus.approved, (int)FlowTaskStatus.returned, (int)FlowTaskStatus.terminated)));
                        decimal scale = 0;
                        if (currentTaskCount > 0 && doingTaskCount > 0)
                            scale = (doingTaskCount + 1 / currentTaskCount) * 100;
                        ///当还没有达到比例时处理临时任务
                        if (!string.IsNullOrEmpty(currentStep.Strategy.NodeApprovalScale) && scale < decimal.Parse(currentStep.Strategy.NodeApprovalScale))
                        {
                            var cuTask = flow_Tasks.FirstOrDefault(o => o.TaskId == currentTask.TaskId);
                            cuTask.Status = (int)FlowTaskStatus.approved;
                            cuTask.Read = true;
                            cuTask.ProcessTime = DateTime.Now;
                            cuTask.Modifier = currentTask.Reviewer;
                            cuTask.Modifdate = DateTime.Now;
                            currentStepAllTasks.Add(cuTask);
                            var endNode = flow_OutPut.NodeList.FirstOrDefault(o => o.Meta.Prop == GlobalConst.NODE_INFO_MEAT_END);
                            ///组装临时任务
                            newTasks = this.CreateNewTaskEvent(currentTask, flow_OutPut, nodeInfos, FlowTaskStatus.temp, ref flow_Branch_TaskList).Where(o => o.StepId != endNode.Meta.StepId).ToList();
                        }
                        else
                        {
                            BuildBranchStrategy(currentTask, flow_OutPut, currentStep, nodeInfos, flow_Tasks, current_Branch_TaskList, ref currentStepAllTasks, ref newTasks, ref flow_Branch_TaskList, ref flow_Instance_CurrentTasks);
                        }
                    }
                    break;
            }
            ///进行所有任务数据操作
            return this.ProcessSaveTaskInfoEvent(new ExtTaskInfoEntity()
            {
                CurrentStep = currentStep,
                CurrentTask = currentTask,
                CurrentStepAllTasks = currentStepAllTasks,
                Flow_Instance_CurrentTasks = flow_Instance_CurrentTasks,
                Flow_OutPut = flow_OutPut,
                NewTasks = newTasks,
                Instance = _Instance,
                Flow_Branch_Tasks = flow_Branch_TaskList
            });
        }

        /// <summary>
        /// 组装分支数据
        /// </summary>
        /// <param name="currentTask"></param>
        /// <param name="flow_OutPut"></param>
        /// <param name="currentStep"></param>
        /// <param name="nodeInfos"></param>
        /// <param name="flow_Tasks"></param>
        /// <param name="current_Branch_TaskList"></param>
        /// <param name="currentStepAllTasks"></param>
        /// <param name="newTasks"></param>
        /// <param name="flow_Branch_TaskList"></param>
        /// <param name="flow_Instance_CurrentTasks"></param>
        private void BuildBranchStrategy(FlowTaskInfo currentTask, Flow_OutPut flow_OutPut, NodeInfo currentStep, List<NextNodeInfoOutPut> nodeInfos, List<Flow_Task> flow_Tasks, List<Flow_Branch_Task> current_Branch_TaskList, ref List<Flow_Task> currentStepAllTasks, ref List<Flow_Task> newTasks, ref List<Flow_Branch_Task> flow_Branch_TaskList, ref List<Flow_Instance_CurrentTask> flow_Instance_CurrentTasks)
        {
            NodeInfo endBranchNode = flow_OutPut.NodeList.FirstOrDefault(o => o.Meta.StepId == nodeInfos.FirstOrDefault().NextStepId);
            NodeInfo startBranchNode = flow_OutPut.NodeList.FirstOrDefault(o => o.Meta.StepId == endBranchNode.Meta.StartGroup);
            BranchStrategyType branchStrategyType = EnumConvertCommon.ConvertType<BranchStrategyType>(startBranchNode.Meta.BranchStrategy);
            List<Flow_Branch_Task> CurrentBranchs = current_Branch_TaskList.Where(o => o.Branch == startBranchNode.Meta.BranchTag).ToList();
            ///处理当前任务
            switch (branchStrategyType)
            {
                case BranchStrategyType.SingleAgreed:
                    {
                        currentStepAllTasks = CurrentBranchs.Join(flow_Tasks.Where(o => !o.Status.In((int)FlowTaskStatus.approved, (int)FlowTaskStatus.otherapproved, (int)FlowTaskStatus.terminated, (int)FlowTaskStatus.otherterminated, (int)FlowTaskStatus.returned, (int)FlowTaskStatus.otherreturned)), a => a.TaskId, b => b.TaskId, (a, b) =>
                        {
                            b.Read = true;
                            b.ProcessTime = DateTime.Now;
                            b.Modifier = currentTask.Reviewer;
                            b.Modifdate = DateTime.Now;
                            b.Status = currentTask.TaskId == b.TaskId ? (int)FlowTaskStatus.approved : (int)FlowTaskStatus.otherapproved;
                            b.Comment = currentTask.TaskId == b.TaskId ? currentTask.Comment : b.Comment;
                            return b;
                        }).ToList();
                        //添加新任务
                        newTasks = this.CreateNewTaskEvent(currentTask, flow_OutPut, nodeInfos, FlowTaskStatus.audit, ref flow_Branch_TaskList);
                        ///当前步骤信息
                        flow_Instance_CurrentTasks = this.commonEvent.CurrentInstanceTaskInfoEvent(currentTask, newTasks);
                    }
                    break;
                case BranchStrategyType.AllAgree:
                    {
                        var res = CurrentBranchs.Where(o => string.IsNullOrEmpty(o.BranchLine)).Join(flow_Tasks.Where(o => o.TaskId != currentTask.TaskId && o.Status.In((int)FlowTaskStatus.audit)), a => a.TaskId, b => b.TaskId, (a, b) => b);
                        if (res.Any())
                        {
                            var cuTask = flow_Tasks.FirstOrDefault(o => o.StepId == currentTask.StepId);
                            cuTask.Status = (int)FlowTaskStatus.approved;
                            cuTask.Read = true;
                            cuTask.ProcessTime = DateTime.Now;
                            cuTask.Modifier = currentTask.Reviewer;
                            cuTask.Modifdate = DateTime.Now;
                            currentStepAllTasks.Add(cuTask);
                            var endNode = flow_OutPut.NodeList.FirstOrDefault(o => o.Meta.Prop == GlobalConst.NODE_INFO_MEAT_END);
                            ///组装临时任务
                            newTasks = this.CreateNewTaskEvent(currentTask, flow_OutPut, nodeInfos, FlowTaskStatus.temp, ref flow_Branch_TaskList).Where(o => o.StepId != endNode.Meta.StepId).ToList();
                        }
                        else
                        {
                            ///处理当前任务
                            currentStepAllTasks = ProcessBranchCurrentTasksEvent(currentTask, flow_Tasks, currentStep, flow_OutPut.NodeList);
                            //添加新任务
                            newTasks = this.CreateNewTaskEvent(currentTask, flow_OutPut, nodeInfos, FlowTaskStatus.audit, ref flow_Branch_TaskList);
                            ///当前步骤信息
                            flow_Instance_CurrentTasks = this.commonEvent.CurrentInstanceTaskInfoEvent(currentTask, newTasks);
                        }
                    }
                    break;
                case BranchStrategyType.Scale:
                    {
                        List<LineInfo> branchLines = flow_OutPut.LinkList.Where(o => o.StartId == startBranchNode.Meta.StepId).ToList();
                        List<Flow_Branch_Task> lineTasks = current_Branch_TaskList.Where(o => !string.IsNullOrEmpty(o.BranchLine) && o.Branch == startBranchNode.Meta.BranchTag).ToList();
                        var groups = lineTasks.GroupBy(o => new { o.Branch, o.BranchLine });
                        decimal cnt = groups.Count();
                        decimal doedCnt = 0;
                        foreach (var group in groups)
                        {
                            var task = group.Join(flow_Tasks.Where(o => o.TaskId != currentTask.TaskId && o.Status == (int)FlowTaskStatus.audit), a => a.TaskId, b => b.TaskId, (a, b) => b);
                            if (!task.Any())
                                doedCnt++;
                        }
                        decimal scale = 0;
                        if (cnt > 0 && doedCnt > 0)
                            scale = (doedCnt / cnt) * 100;
                        if (!string.IsNullOrEmpty(startBranchNode.Meta.ApprovalScale) && scale < decimal.Parse(startBranchNode.Meta.ApprovalScale))
                        {
                            var cuTask = flow_Tasks.FirstOrDefault(o => o.StepId == currentTask.StepId);
                            cuTask.Status = (int)FlowTaskStatus.approved;
                            cuTask.Read = true;
                            cuTask.ProcessTime = DateTime.Now;
                            cuTask.Modifier = currentTask.Reviewer;
                            cuTask.Modifdate = DateTime.Now;
                            currentStepAllTasks.Add(cuTask);
                            var endNode = flow_OutPut.NodeList.FirstOrDefault(o => o.Meta.Prop == GlobalConst.NODE_INFO_MEAT_END);
                            ///组装临时任务
                            newTasks = this.CreateNewTaskEvent(currentTask, flow_OutPut, nodeInfos, FlowTaskStatus.temp, ref flow_Branch_TaskList).Where(o => o.StepId != endNode.Meta.StepId).ToList();
                        }
                        else
                        {
                            ///处理当前任务
                            currentStepAllTasks = ProcessBranchCurrentTasksEvent(currentTask, flow_Tasks, currentStep, flow_OutPut.NodeList);
                            //添加新任务
                            newTasks = this.CreateNewTaskEvent(currentTask, flow_OutPut, nodeInfos, FlowTaskStatus.audit, ref flow_Branch_TaskList);
                            ///当前步骤信息
                            flow_Instance_CurrentTasks = this.commonEvent.CurrentInstanceTaskInfoEvent(currentTask, newTasks);
                        }
                    }
                    break;
            }
        }
        /// <summary>
        /// 组装新的任务 
        /// </summary>
        /// <param name="currentTask"></param>
        /// <param name="flow_OutPut"></param>
        /// <param name="NodeInfo"></param>
        /// <returns></returns>
        private List<Flow_Task> CreateNewTaskEvent(FlowTaskInfo currentTask, Flow_OutPut flow_OutPut, List<NextNodeInfoOutPut> NodeInfo, FlowTaskStatus flowTaskStatus, ref List<Flow_Branch_Task> branch_Tasks, FlowTaskType type = FlowTaskType.task)
        {
            List<Flow_Task> newTasks = new List<Flow_Task>();
            if (NodeInfo != null && NodeInfo.Any())
            {
                foreach (NextNodeInfoOutPut item in NodeInfo)
                {
                    NodeInfo nextStep = flow_OutPut.NodeList.FirstOrDefault(o => o.Meta.StepId == item.NextStepId);
                    List<Branchs> branchs = new List<Branchs>();
                    if (!string.IsNullOrEmpty(nextStep.Meta.Groups))
                    {
                        branchs = JsonConvert.DeserializeObject<List<Branchs>>(nextStep.Meta.Groups);
                    }
                    switch (item.FlowTaskType)
                    {
                        ///正常任务
                        case FlowTaskType.task:
                            {
                                foreach (var aprrove in item.NextStepApproverEmpId)
                                {
                                    long taskId = longCommon.GenerateRandomCode();
                                    newTasks.Add(new Flow_Task()
                                    {
                                        Creatdate = DateTime.Now,
                                        Creator = currentTask.Reviewer,
                                        ReceivingTime = DateTime.Now,
                                        Reviewer = aprrove,
                                        GroupId = currentTask.GroupId,
                                        InstanceId = currentTask.InstanceId,
                                        PrevStepId = currentTask.StepId,
                                        PrevTaskId = currentTask.TaskId,
                                        Sender = currentTask.Reviewer,
                                        Status = (int)flowTaskStatus,
                                        Sort = currentTask.Sort + 1,
                                        StepId = item.NextStepId,
                                        Title = $"{flow_OutPut.FlowProperty.CNName}-{(!EmpDictionary.ContainsKey(currentTask.ApplyEmpId) ? "" : EmpDictionary[currentTask.ApplyEmpId])}",
                                        Urgency = 0,
                                        Modifier = currentTask.Reviewer,
                                        Modifdate = DateTime.Now,
                                        TaskId = taskId,
                                        Type = (int)type
                                    });
                                    if (branchs != null && branchs.Any())
                                        branch_Tasks.AddRange(BuildFlowBranchTask(currentTask, branchs, item, taskId));
                                }
                            }
                            break;
                        case FlowTaskType.endbranch:
                        ///提交到分支的任务
                        case FlowTaskType.branch:
                            {
                                foreach (var branch in item.BranchNodes)
                                {
                                    NodeInfo nodeInfo = flow_OutPut.NodeList.FirstOrDefault(o => o.Meta.StepId == branch.NextStepId);
                                    if (!string.IsNullOrEmpty(nodeInfo.Meta.Groups))
                                    {
                                        branchs = JsonConvert.DeserializeObject<List<Branchs>>(nodeInfo.Meta.Groups);
                                    }
                                    foreach (var aprrove in branch.NextStepApproverEmpId)
                                    {
                                        long taskId = longCommon.GenerateRandomCode();
                                        newTasks.Add(new Flow_Task()
                                        {
                                            Creatdate = DateTime.Now,
                                            Creator = currentTask.Reviewer,
                                            ReceivingTime = DateTime.Now,
                                            Reviewer = aprrove,
                                            GroupId = currentTask.GroupId,
                                            InstanceId = currentTask.InstanceId,
                                            PrevStepId = currentTask.StepId,
                                            PrevTaskId = currentTask.TaskId,
                                            Sender = currentTask.Reviewer,
                                            Status = (int)flowTaskStatus,
                                            Sort = currentTask.Sort + 1,
                                            StepId = branch.NextStepId,
                                            Title = $"{flow_OutPut.FlowProperty.CNName}-{(!EmpDictionary.ContainsKey(currentTask.ApplyEmpId) ? "" : EmpDictionary[currentTask.ApplyEmpId])}",
                                            Urgency = 0,
                                            Modifier = currentTask.Reviewer,
                                            Modifdate = DateTime.Now,
                                            TaskId = taskId,
                                            Type = (int)type
                                        });
                                        branch_Tasks.AddRange(BuildFlowBranchTask(currentTask, branchs, branch, taskId));
                                    }
                                }
                            }
                            break;
                    }

                }
            }
            return newTasks;
        }
        /// <summary>
        /// 组装分支任务关系
        /// </summary>
        /// <returns></returns>
        private List<Flow_Branch_Task> BuildFlowBranchTask(FlowTaskInfo currentTask, List<Branchs> branchs, NextNodeInfoOutPut branch, long taskId)
        {
            List<Flow_Branch_Task> flow_Branch_Tasks = new List<Flow_Branch_Task>();
            foreach (var childBranchs in branchs)
            {
                if (!string.IsNullOrEmpty(childBranchs.Branch) && !string.IsNullOrEmpty(childBranchs.Line))
                {
                    flow_Branch_Tasks.Add(new Flow_Branch_Task()
                    {
                        Branch = childBranchs.Branch,
                        BranchLine = childBranchs.Line,
                        GroupId = currentTask.GroupId,
                        InstanceId = currentTask.InstanceId,
                        StepId = branch.NextStepId,
                        TaskId = taskId,
                        Creatdate = DateTime.Now,
                        Creator = Global.EmpId,
                        Modifdate = DateTime.Now,
                        Modifier = Global.EmpId
                    });
                }
            }
            return flow_Branch_Tasks;
        }
        /// <summary>
        /// 处理当前任务
        /// </summary>
        /// <param name="currentTask"></param>
        /// <param name="flow_Tasks"></param>
        /// <returns></returns>
        private List<Flow_Task> ProcessCurrentTasksEvent(FlowTaskInfo currentTask, List<Flow_Task> flow_Tasks)
        {
            ///处理当前任务包含临时任务
            List<Flow_Task> currentTasks = flow_Tasks.Where(o => o.Sort == currentTask.Sort && (o.Status == (int)FlowTaskStatus.audit || o.Status == (int)FlowTaskStatus.otheraudit || o.Status == (int)FlowTaskStatus.temp || o.Status == (int)FlowTaskStatus.momentum)).ToList();
            currentTasks.ForEach(o =>
            {
                o.Read = true;
                o.ProcessTime = DateTime.Now;
                o.Modifier = currentTask.Reviewer;
                o.Modifdate = DateTime.Now;
                o.Status = currentTask.TaskId == o.TaskId ? (int)FlowTaskStatus.approved : (int)FlowTaskStatus.otherapproved;
                o.Comment = currentTask.TaskId == o.TaskId ? currentTask.Comment : o.Comment;
            });
            ////处理下一节点的临时任务
            var tempTask = flow_Tasks.Where(o => o.Sort != currentTask.Sort && o.Status == (int)FlowTaskStatus.temp).ToList();
            tempTask.ForEach(o =>
            {
                o.Status = (int)FlowTaskStatus.otheraudit;
                o.Modifier = currentTask.Reviewer;
                o.Modifdate = DateTime.Now;
            });
            currentTasks.AddRange(tempTask);
            return currentTasks;
        }


        /// <summary>
        /// 处理分支的当前任务
        /// </summary>
        /// <param name="currentTask"></param>
        /// <param name="flow_Tasks"></param>
        /// <returns></returns>
        private List<Flow_Task> ProcessBranchCurrentTasksEvent(FlowTaskInfo currentTask, List<Flow_Task> flow_Tasks, NodeInfo currentStep, List<NodeInfo> steps)
        {
            List<Flow_Task> currentTasks = new List<Flow_Task>();
            currentTasks = flow_Tasks.Where(o => o.Sort == currentTask.Sort && (o.Status == (int)FlowTaskStatus.audit || o.Status == (int)FlowTaskStatus.otheraudit || o.Status == (int)FlowTaskStatus.temp || o.Status == (int)FlowTaskStatus.momentum)).ToList();
            var currentNodes = steps.Where(p => p.Meta.Groups == currentStep.Meta.Groups).ToList();
            if (currentNodes.Any())
            {
                currentTasks = currentTasks.Join(currentNodes, a => a.StepId, b => b.Meta.StepId, (a, b) => a).ToList();
            }
            ///处理当前任务包含临时任务
            currentTasks.ForEach(o =>
            {
                o.Read = true;
                o.ProcessTime = DateTime.Now;
                o.Modifier = currentTask.Reviewer;
                o.Modifdate = DateTime.Now;
                o.Status = currentTask.TaskId == o.TaskId ? (int)FlowTaskStatus.approved : (int)FlowTaskStatus.otherapproved;
                o.Comment = currentTask.TaskId == o.TaskId ? currentTask.Comment : o.Comment;
            });
            var nextTask = flow_Tasks.FirstOrDefault(o => o.PrevStepId == currentTask.StepId && o.Status == (int)FlowTaskStatus.temp);
            if (nextTask != null)
            {
                var nextStep = steps.FirstOrDefault(o => o.Meta.StepId == nextTask.StepId);
                var nodes = steps.Where(o => o.Meta.Groups == nextStep.Meta.Groups);
                ////处理下一节点的临时任务
                var tempTask = flow_Tasks.Where(o => o.Status == (int)FlowTaskStatus.temp).ToList();
                tempTask = tempTask.Join(nodes, a => a.StepId, b => b.Meta.StepId, (a, b) => a).ToList();
                tempTask.ForEach(o =>
                {
                    o.Status = (int)FlowTaskStatus.otheraudit;
                    o.Modifier = currentTask.Reviewer;
                    o.Modifdate = DateTime.Now;
                });
                currentTasks.AddRange(tempTask);
            }
            return currentTasks;
        }

        /// <summary>
        /// 进行抄送任务
        /// </summary>
        /// <returns></returns>
        private List<Flow_Task> ProcessCCEvent(FlowTaskInfo currentTask, Flow_OutPut flow_OutPut, NodeInfo currentStep, List<Flow_Instance_Business> instance_Businesses = null)
        {
            List<Flow_Task> ccTasks = new List<Flow_Task>();
            if (instance_Businesses == null)
                instance_Businesses = baseDLL.GetBasicListByQuery<Flow_Instance_Business>("instanceid", currentTask.InstanceId + "");
            NodeInfo nodeInfo = null;
            ToViewModel(currentStep, out nodeInfo);
            var ccEmpIds = _Engine_SrategyService.LoadCCNextNodeInfo(new NextNodeInfoRequestParameter()
            {
                CurrentStep = nodeInfo,
                CurrentTask = currentTask,
                Flow_OutPut = flow_OutPut,
                Instance_Businesses = instance_Businesses,
            });
            if (nodeInfo != null)
            {
                if (ccEmpIds.Success && ccEmpIds.Data != null && ccEmpIds.Data.Any())
                {
                    foreach (NextNodeInfoOutPut item in ccEmpIds.Data)
                    {
                        foreach (var aprrove in item.NextStepApproverEmpId)
                        {
                            ccTasks.Add(new Flow_Task()
                            {
                                Creatdate = DateTime.Now,
                                Creator = currentTask.Reviewer,
                                ReceivingTime = DateTime.Now,
                                Reviewer = aprrove,
                                GroupId = currentTask.GroupId,
                                InstanceId = currentTask.InstanceId,
                                PrevStepId = currentTask.PrevStepId,
                                PrevTaskId = currentTask.TaskId,
                                Sender = currentTask.Reviewer,
                                Status = (int)FlowTaskStatus.unread,
                                Sort = currentTask.Sort,
                                StepId = item.NextStepId,
                                Title = $"{flow_OutPut.FlowProperty.CNName}{(!EmpDictionary.ContainsKey(currentTask.ApplyEmpId) ? "" : $"-{EmpDictionary[currentTask.ApplyEmpId]}")}",
                                Urgency = 0,
                                Modifier = currentTask.Reviewer,
                                Modifdate = DateTime.Now,
                                TaskId = longCommon.GenerateRandomCode(),
                                Type = (int)FlowTaskType.cc
                            });
                        }
                    }
                }
            }
            return ccTasks;
        }

        /// <summary>
        /// 保存任务数据
        /// </summary>
        /// <param name="currentTasks"></param>
        /// <param name="newTasks"></param>
        /// <param name="currentTask"></param>
        /// <param name="flow_OutPut"></param>
        /// <param name="currentStep"></param>
        /// <param name="flow_Instance_CurrentTasks"></param>
        /// <param name="_Instance"></param>
        /// <returns></returns>
        private Result<ExtInstance> ProcessSaveTaskInfoEvent(ExtTaskInfoEntity taskData)
        {
            Result<ExtInstance> result = new Result<ExtInstance>(null);
            var ccTask = ProcessCCEvent(taskData.CurrentTask, taskData.Flow_OutPut, taskData.CurrentStep);
            List<ComplatedParameter> complatedParameters = new List<ComplatedParameter>();
            ///当是第一步发起时更新状态未进行中
            if (taskData.CurrentStep.Meta.Prop == GlobalConst.NODE_INFO_MEAT_START)
            {
                List<Flow_Instance_Business> instance_Businesses = business.GetBasicListByQuery<Flow_Instance_Business>("instanceid", taskData.CurrentTask.InstanceId + "");
                complatedParameters = commonEvent.GetComplatedParameters(instance_Businesses, FlowInstanceStatus.ongoing);
            }
            using (TransactionScope ts1 = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions() { IsolationLevel = IsolationLevel.ReadCommitted }))
            {
                ///处理当前任务
                bool b = business.UpdateBasicBusiness(taskData.CurrentStepAllTasks);
                ///创建新任务
                if (taskData.NewTasks.Any())
                    b = b && business.AddBasicBusiness(taskData.NewTasks) > 0;
                ///处理抄送任务              
                if (ccTask.Any())
                    b = b && this.business.AddBasicBusiness(ccTask) >= 0;
                b = b & this.business.DeleteCurrentTask(taskData.CurrentStepAllTasks.Select(o => o.TaskId).ToList());
                ///保存当前任务ID
                if (taskData.Flow_Instance_CurrentTasks.Any())
                {

                    b = b && this.business.AddBasicBusiness(taskData.Flow_Instance_CurrentTasks) >= 0;
                }
                ///判断数据是否存在分支中
                if (taskData.Flow_Branch_Tasks.Any())
                {
                    b = b && this.business.AddBasicBusiness(taskData.Flow_Branch_Tasks) >= 0;
                }
                ///更新流程实例数据
                taskData.Instance.FlowStatus = (int)FlowInstanceStatus.ongoing;
                b = b && business.UpdateBasicBusiness(taskData.Instance);
                b = b && business.RunTaskUpdateApplyBusinessStatus(complatedParameters);
                if (b)
                {
                    ///有任务时并且
                    if (taskData.NewTasks.Any() && taskData.NewTasks.Count(o => o.Status == (int)FlowTaskStatus.audit) == 1)
                    {
                        var nextTask = taskData.NewTasks.FirstOrDefault();
                        if (nextTask != null)
                        {
                            result.Data = new ExtInstance()
                            {
                                CurrentStep = nextTask.StepId,
                                CurrentTaskId = nextTask.TaskId,
                                GroupId = taskData.CurrentTask.GroupId,
                                InstanceId = taskData.CurrentTask.InstanceId
                            };
                        }
                    }
                    ts1.Complete();
                }
                else
                {
                    result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_RUN_CURRENT_TASK_COMPLETE_ERROR);
                }
            }
            return result;
        }

        #endregion

        /// <summary>
        /// 并行分支处理当前任务
        /// </summary>
        /// <param name="currentTask"></param>
        /// <param name="flow_Tasks"></param>
        /// <returns></returns>
        private List<Flow_Task> ParallelBranchProcessCurrentTasksEvent(FlowTaskInfo currentTask, List<Flow_Task> flow_Tasks)
        {
            ///处理当前任务包含临时任务
            List<Flow_Task> currentTasks = flow_Tasks.Where(o => o.Sort == currentTask.Sort && (o.Status == (int)FlowTaskStatus.audit || o.Status == (int)FlowTaskStatus.otheraudit || o.Status == (int)FlowTaskStatus.temp || o.Status == (int)FlowTaskStatus.momentum)).ToList();
            currentTasks.ForEach(o =>
            {
                o.Read = true;
                o.ProcessTime = DateTime.Now;
                o.Modifier = currentTask.Reviewer;
                o.Modifdate = DateTime.Now;
                o.Status = currentTask.TaskId == o.TaskId ? (int)FlowTaskStatus.approved : (int)FlowTaskStatus.otherapproved;
            });
            return currentTasks;
        }

        /// <summary>
        /// 并行分支创建任务
        /// </summary>
        /// <param name="currentTask"></param>
        /// <param name="flow_OutPut"></param>
        /// <param name="NodeInfo"></param>
        /// <param name="flowTaskStatus"></param>
        /// <returns></returns>
        private List<Flow_Task> ParallelBranchCreateNewTaskEvent(FlowTaskInfo currentTask, Flow_OutPut flow_OutPut, List<NextNodeInfoOutPut> NodeInfo)
        {
            List<Flow_Task> newTasks = new List<Flow_Task>();
            if (NodeInfo != null && NodeInfo.Any())
            {
                int i = 0;
                foreach (NextNodeInfoOutPut item in NodeInfo)
                {
                    string sort = $"{currentTask.Sort}{i}";
                    foreach (var aprrove in item.NextStepApproverEmpId)
                    {
                        newTasks.Add(new Flow_Task()
                        {
                            Creatdate = DateTime.Now,
                            Creator = currentTask.Reviewer,
                            ReceivingTime = DateTime.Now,
                            Reviewer = aprrove,
                            GroupId = currentTask.GroupId,
                            InstanceId = currentTask.InstanceId,
                            PrevStepId = currentTask.StepId,
                            PrevTaskId = currentTask.TaskId,
                            Sender = currentTask.Reviewer,
                            Status = (int)FlowTaskStatus.audit,
                            Sort = ulong.Parse(sort),
                            StepId = item.NextStepId,
                            Title = $"{flow_OutPut.FlowProperty.CNName}{(!EmpDictionary.ContainsKey(currentTask.ApplyEmpId) ? "" : $"-{EmpDictionary[currentTask.ApplyEmpId]}")}",
                            Urgency = 0,
                            Modifier = currentTask.Reviewer,
                            Modifdate = DateTime.Now,
                            TaskId = longCommon.GenerateRandomCode(),
                            Type = (int)FlowTaskType.task
                        });
                    }
                    i++;
                }
            }
            return newTasks;
        }
        #endregion

    }
}
