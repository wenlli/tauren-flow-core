﻿using System;
using System.Collections.Generic;
using System.Text;
using Tauren.Flow.DLL.FlowCenterDLL;
using Tauren.Flow.DLL.FlowEngine;
using Tauren.Flow.Entity.Enum;
using Tauren.Flow.Entity.Global;
using Tauren.Flow.Entity.Model;
using Tauren.Flow.Entity.Output;
using Tauren.Flow.Service.ProcessBasicInfoBLL;
using System.Linq;
using System.Transactions;
using Tauren.Flow.Service.Base;
using Tauren.Flow.Entity.Const;
using Tauren.Flow.Infrastructure.Common;
using Google.Protobuf.WellKnownTypes;
using Newtonsoft.Json;
using Tauren.Flow.DLL.Base;

namespace Tauren.Flow.Service.FlowEngineBLL
{

    /// <summary>
    /// 回收业务
    /// </summary>
    public class FlowRecyclingService
    {
        private IProcessBasicInfoService processBasicInfoService;
        private IFlowEngineBusiness business;
        private IFlowCenterBusiness flowCenterBusiness;
        private FlowEngineCommonEvent commonEvent = FlowEngineCommonEvent.Instacne;
        private LongCommon longCommon;
        public FlowRecyclingService(IProcessBasicInfoService _processBasicInfoService, IFlowEngineBusiness _business, IFlowCenterBusiness _flowCenterBusiness, LongCommon _longCommon)
        {
            processBasicInfoService = _processBasicInfoService;
            business = _business;
            flowCenterBusiness = _flowCenterBusiness;
            longCommon = _longCommon;
        }

        /// <summary>
        /// 进行任务回收
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        public Result FlowRecyclingEvent(long taskId)
        {
            Result result = new Result(null);
            ///获取当前任务
            FlowTaskInfo currentTask = processBasicInfoService.QueryCurrentFlowTaskInfo(taskId);
            switch (currentTask)
            {
                case null: result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_CURRENTTASK_ERROR); break;
                default:
                    {
                        FlowTaskStatus taskStatus = (FlowTaskStatus)currentTask.Status;
                        FlowTaskType taskType = (FlowTaskType)currentTask.Type;
                        if (taskStatus != FlowTaskStatus.approved || taskType != FlowTaskType.task)
                            result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_CURRENTTASK_FLOWSTATUS_ERROR);
                        else
                        {
                            Dictionary<long, bool> recycleInfos = flowCenterBusiness.GetTaskRecycleInfo(new long[] { taskId });
                            if (recycleInfos.ContainsKey(taskId) && recycleInfos[taskId])
                            {
                                Flow_OutPut _flow = processBasicInfoService.GetFlowInfo(currentTask.FlowId, currentTask.FlowVersionId);
                                Flow_Instance _Instance = business.GetBasicListByQuery<Flow_Instance>("instanceid", currentTask.InstanceId + "").FirstOrDefault();
                                switch (_flow)
                                {
                                    case null: { result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_FORMINFO_ERROR); } break;
                                    default:
                                        {
                                            ///获取当前节点信息
                                            NodeInfo currentStep = _flow.NodeList.FirstOrDefault(o => o.Meta.StepId == currentTask.StepId);
                                            switch (currentStep)
                                            {
                                                case null: result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_CURRENTSTEP_ERROR); break;
                                                default: result = this.FlowSingeRecyclingEvent(currentTask, currentStep, _Instance); break;
                                            }
                                        }
                                        break;
                                }
                            }
                            else
                                result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NEXT_APPROVAL_ERROR);
                        }
                    }
                    break;
            }
            return result;
        }

        /// <summary>
        /// 将整个流程实例终止
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        public Result FlowInstanceEndEvent(long taskId, string opinion)
        {
            Result result = new Result(null);
            ///获取当前任务
            FlowTaskInfo currentTask = processBasicInfoService.QueryCurrentFlowTaskInfo(taskId);
            switch (currentTask)
            {
                case null: result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_CURRENTTASK_ERROR); break;
                default:
                    {
                        if (GlobalConst.TaskComplatedStatus.Any(o => o == currentTask.Status))
                            result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_CURRENT_TASK_END_APPROVALED_ERROR);
                        else
                        {
                            Flow_OutPut flowInfo = processBasicInfoService.GetFlowInfo(currentTask.FlowId, currentTask.FlowVersionId);
                            NodeInfo currentStep = flowInfo.NodeList.FirstOrDefault(o => o.Meta.StepId == currentTask.StepId);
                            if (!string.IsNullOrEmpty(currentStep.Meta.Groups))
                            {
                                List<Branchs> branchs = JsonConvert.DeserializeObject<List<Branchs>>(currentStep.Meta.Groups);
                                ///当是在分支中的需要拒绝的任务需按照分支策略处理
                                if (branchs.Any())
                                {
                                    this.BranchTterminated(currentTask, currentStep, opinion, branchs, flowInfo);
                                }
                                ///不是分支的终止-则按照节点策略进行处理
                                else
                                {
                                    this.NotBranchTterminated(currentTask, currentStep, opinion);
                                }
                            }
                            ///不是分支的终止-则按照节点策略进行处理
                            else
                            {
                                this.NotBranchTterminated(currentTask, currentStep, opinion);
                            }
                        }
                    }
                    break;
            }
            return result;
        }

        /// <summary>
        /// 不是分支的终止-则按照节点策略进行处理
        /// </summary>
        /// <returns></returns>
        private Result NotBranchTterminated(FlowTaskInfo currentTask, NodeInfo currentStep, string opinion)
        {
            Result result = new Result(null);
            ApprovalStrategyType approvalStrategyType = (ApprovalStrategyType)currentStep.Strategy.ApprovalStrategy;
            Flow_Instance _Instance = business.GetBasicListByQuery<Flow_Instance>("instanceid", currentTask.InstanceId + "").FirstOrDefault();
            List<Flow_Instance_Business> instance_Businesses = business.GetBasicListByQuery<Flow_Instance_Business>("instanceid", currentTask.InstanceId + "");
            List<ComplatedParameter> complatedParameters = new List<ComplatedParameter>();
            List<Flow_Task> flow_Tasks = this.business.GetTasks(currentTask.GroupId).Where(tk => !GlobalConst.TaskComplatedStatus.Any(o => o == tk.Status)).ToList();
            switch (approvalStrategyType)
            {
                case ApprovalStrategyType.Disable:
                case ApprovalStrategyType.SingleAgreed:
                    {
                        _Instance.FlowStatus = (int)FlowInstanceStatus.terminated;
                        _Instance.ComplatedTime = DateTime.Now;
                        complatedParameters = commonEvent.GetComplatedParameters(instance_Businesses, FlowInstanceStatus.terminated);
                    }
                    break;
                case ApprovalStrategyType.AllAgree:
                    {
                        if (flow_Tasks.Any(o => o.TaskId != currentTask.TaskId && currentStep.Meta.StepId == o.StepId && o.Status.In((int)FlowTaskStatus.approved, (int)FlowTaskStatus.returned)))
                        {
                            result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOT_END_ERROR);
                            return result;
                        }
                        else
                        {
                            if (flow_Tasks.Any(o => o.TaskId != currentTask.TaskId && currentStep.Meta.StepId == o.StepId && o.Status == ((int)FlowTaskStatus.audit)))
                            {

                                flow_Tasks = flow_Tasks.Where(p => p.TaskId == currentTask.TaskId).ToList();
                            }
                            else
                            {
                                _Instance.FlowStatus = (int)FlowInstanceStatus.terminated;
                                _Instance.ComplatedTime = DateTime.Now;
                                complatedParameters = commonEvent.GetComplatedParameters(instance_Businesses, FlowInstanceStatus.terminated);
                            }
                        }
                    }
                    break;
                case ApprovalStrategyType.Scale:
                    {
                        decimal currentTaskCount = flow_Tasks.Count(o => o.Sort == currentTask.Sort && (o.Status.In((int)FlowTaskStatus.audit, (int)FlowTaskStatus.approved, (int)FlowTaskStatus.returned, (int)FlowTaskStatus.terminated)));
                        decimal doingTaskCount = flow_Tasks.Count(o => o.Sort == currentTask.Sort && o.Status == (int)FlowTaskStatus.terminated);
                        decimal scale = 0;
                        if (currentTaskCount > 0 && doingTaskCount > 0)
                            scale = (doingTaskCount + 1 / currentTaskCount) * 100;
                        ///当还没有达到比例时处理临时任务
                        if (!string.IsNullOrEmpty(currentStep.Strategy.NodeApprovalScale) && scale < decimal.Parse(currentStep.Strategy.NodeApprovalScale))
                        {
                            if (flow_Tasks.Any(o => o.Sort == currentTask.Sort && o.TaskId != currentTask.TaskId && o.Status == (int)FlowTaskStatus.audit))
                            {
                                flow_Tasks = flow_Tasks.Where(p => p.TaskId == currentTask.TaskId).ToList();
                            }
                            else
                            {
                                result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOT_END_ERROR);
                                return result;
                            }
                        }
                        else
                        {
                            _Instance.FlowStatus = (int)FlowInstanceStatus.terminated;
                            _Instance.ComplatedTime = DateTime.Now;
                            complatedParameters = commonEvent.GetComplatedParameters(instance_Businesses, FlowInstanceStatus.terminated);
                        }
                    }
                    break;
            }
            flow_Tasks.ForEach(cu =>
            {
                FlowTaskStatus flowTaskStatus = (FlowTaskStatus)cu.Status;
                cu.Read = true;
                switch (flowTaskStatus)
                {
                    case FlowTaskStatus.audit:
                        cu.Status = (int)FlowTaskStatus.terminated;
                        break;
                    case FlowTaskStatus.otheraudit:
                        cu.Status = (int)FlowTaskStatus.otherterminated;
                        break;
                    case FlowTaskStatus.temp:
                        cu.Status = (int)FlowTaskStatus.otherterminated;
                        break;
                }
                cu.Comment = cu.TaskId == currentTask.TaskId ? opinion : cu.Comment;
            });
            using (TransactionScope ts1 = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions() { IsolationLevel = IsolationLevel.ReadCommitted }))
            {
                ///处理当前任务
                bool b = business.UpdateBasicBusiness(flow_Tasks);
                b = b && business.UpdateBasicBusiness(_Instance);
                if (complatedParameters.Any())
                    b = b && business.RunTaskUpdateApplyBusinessStatus(complatedParameters);
                ///回写数据--todo
                ///发送邮件--todo
                ///发送提醒--todo
                if (b) ts1.Complete();
                else result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_CURRENT_TASK_END_ERROR);
            }
            return result;
        }

        /// <summary>
        /// 当是在分支中的需要拒绝的任务需按照分支策略处理
        /// </summary>
        /// <returns></returns>
        private Result BranchTterminated(FlowTaskInfo currentTask, NodeInfo currentStep, string opinion, List<Branchs> branchs, Flow_OutPut flowInfo)
        {
            Result result = new Result(null);
            ApprovalStrategyType approvalStrategyType = (ApprovalStrategyType)currentStep.Strategy.ApprovalStrategy;
            Flow_Instance _Instance = business.GetBasicListByQuery<Flow_Instance>("instanceid", currentTask.InstanceId + "").FirstOrDefault();
            List<Flow_Instance_Business> instance_Businesses = business.GetBasicListByQuery<Flow_Instance_Business>("instanceid", currentTask.InstanceId + "");
            List<ComplatedParameter> complatedParameters = new List<ComplatedParameter>();
            List<Flow_Task> flow_Tasks = this.business.GetTasks(currentTask.GroupId).Where(tk => !GlobalConst.TaskComplatedStatus.Any(o => o == tk.Status)).ToList();
            NodeInfo startBranchNode = flowInfo.NodeList.FirstOrDefault(o => o.Meta.StepId == branchs[0].Branch);
            BranchStrategyType endStrategy = EnumConvertCommon.ConvertType<BranchStrategyType>(startBranchNode.Meta.EndStrategy);
            List<Flow_Branch_Task> current_Branch_TaskList = business.GetBasicListByQuery<Flow_Branch_Task>("groupid", currentTask.GroupId + "");

            ///当在并行分支中的节点则不在支持审批策略，只要一个人终止后则节点结束-遵循并行分支终止策略
            this.BranchTterminatedStrategy(endStrategy, ref _Instance, ref complatedParameters, instance_Businesses, current_Branch_TaskList, flow_Tasks, currentTask, startBranchNode, ref result);
            if (!result.Success) return result;
            //switch (approvalStrategyType)
            //{
            //    case ApprovalStrategyType.Disable:
            //    case ApprovalStrategyType.SingleAgreed:
            //        {
            //            this.BranchTterminatedStrategy(endStrategy, _Instance, complatedParameters, instance_Businesses, current_Branch_TaskList, flow_Tasks, currentTask, startBranchNode);
            //        }
            //        break;
            //    case ApprovalStrategyType.AllAgree:
            //        {
            //            if (flow_Tasks.Any(o => o.TaskId != currentTask.TaskId && currentStep.Meta.StepId == o.StepId && o.Status.In((int)FlowTaskStatus.approved, (int)FlowTaskStatus.returned)))
            //            {
            //                result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOT_END_ERROR);
            //            }
            //            else
            //            {
            //                if (flow_Tasks.Any(o => o.TaskId != currentTask.TaskId && currentStep.Meta.StepId == o.StepId && (o.Status.In((int)FlowTaskStatus.audit))))
            //                {
            //                    flow_Tasks = flow_Tasks.Where(p => p.TaskId == currentTask.TaskId).ToList();
            //                }
            //                else
            //                {
            //                    this.BranchTterminatedStrategy(endStrategy, _Instance, complatedParameters, instance_Businesses, current_Branch_TaskList, flow_Tasks, currentTask, startBranchNode);
            //                }
            //            }

            //        }
            //        break;
            //    case ApprovalStrategyType.Scale:
            //        {
            //            decimal currentTaskCount = flow_Tasks.Count(o => o.Sort == currentTask.Sort && (o.Status.In((int)FlowTaskStatus.audit, (int)FlowTaskStatus.approved, (int)FlowTaskStatus.returned, (int)FlowTaskStatus.terminated)));
            //            decimal doingTaskCount = flow_Tasks.Count(o => o.Sort == currentTask.Sort && o.Status == (int)FlowTaskStatus.terminated);
            //            decimal scale = 0;
            //            if (currentTaskCount > 0 && doingTaskCount > 0)
            //                scale = (doingTaskCount + 1 / currentTaskCount) * 100;
            //            ///当还没有达到比例时处理临时任务
            //            if (!string.IsNullOrEmpty(currentStep.Strategy.NodeApprovalScale) && scale < decimal.Parse(currentStep.Strategy.NodeApprovalScale))
            //            {
            //                if (flow_Tasks.Any(o => o.Sort == currentTask.Sort && o.TaskId != currentTask.TaskId && o.Status == (int)FlowTaskStatus.audit))
            //                {
            //                    flow_Tasks = flow_Tasks.Where(p => p.TaskId == currentTask.TaskId).ToList();
            //                }
            //                else
            //                {
            //                    result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOT_END_ERROR);
            //                }
            //            }
            //            else
            //            {
            //                this.BranchTterminatedStrategy(endStrategy, _Instance, complatedParameters, instance_Businesses, current_Branch_TaskList, flow_Tasks, currentTask, startBranchNode);
            //            }
            //        }
            //        break;
            //}
            flow_Tasks.ForEach(cu =>
            {
                FlowTaskStatus flowTaskStatus = (FlowTaskStatus)cu.Status;
                cu.Read = true;
                switch (flowTaskStatus)
                {
                    case FlowTaskStatus.audit:
                        cu.Status = (int)FlowTaskStatus.terminated;
                        break;
                    case FlowTaskStatus.otheraudit:
                        cu.Status = (int)FlowTaskStatus.otherterminated;
                        break;
                    case FlowTaskStatus.temp:
                        cu.Status = (int)FlowTaskStatus.otherterminated;
                        break;
                }
                cu.Comment = cu.TaskId == currentTask.TaskId ? opinion : cu.Comment;
            });
            using (TransactionScope ts1 = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions() { IsolationLevel = IsolationLevel.ReadCommitted }))
            {
                ///处理当前任务
                bool b = business.UpdateBasicBusiness(flow_Tasks);
                b = b && business.UpdateBasicBusiness(_Instance);
                if (complatedParameters.Any())
                    b = b && business.RunTaskUpdateApplyBusinessStatus(complatedParameters);
                ///回写数据--todo
                ///发送邮件--todo
                ///发送提醒--todo
                if (b) ts1.Complete();
                else result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_CURRENT_TASK_END_ERROR);
            }
            return result;
        }
        /// <summary>
        /// 根据分支终止策略执行
        /// </summary>
        /// <param name="endStrategy"></param>
        /// <param name="_Instance"></param>
        /// <param name="complatedParameters"></param>
        /// <param name="instance_Businesses"></param>
        /// <param name="current_Branch_TaskList"></param>
        /// <param name="flow_Tasks"></param>
        /// <param name="currentTask"></param>
        /// <param name="startBranchNode"></param>
        private void BranchTterminatedStrategy(BranchStrategyType endStrategy, ref Flow_Instance _Instance, ref List<ComplatedParameter> complatedParameters, List<Flow_Instance_Business> instance_Businesses, List<Flow_Branch_Task> current_Branch_TaskList, List<Flow_Task> flow_Tasks, FlowTaskInfo currentTask, NodeInfo startBranchNode, ref Result result)
        {
            List<Flow_Branch_Task> lineTasks = current_Branch_TaskList.Where(o => !string.IsNullOrEmpty(o.BranchLine) && o.Branch == startBranchNode.Meta.BranchTag).ToList();
            Flow_Branch_Task branch_Task = lineTasks.FirstOrDefault(o => o.TaskId == currentTask.TaskId && !string.IsNullOrEmpty(o.BranchLine));
            var otherLineTasks = lineTasks.Where(o => o.BranchLine != branch_Task.BranchLine).ToList();
            var otherTasks = otherLineTasks.Join(flow_Tasks, a => a.TaskId, b => b.TaskId, (a, b) => b);
            switch (endStrategy)
            {
                ///一个分支终止则终止
                case BranchStrategyType.SingleAgreed:
                    {
                        _Instance.FlowStatus = (int)FlowInstanceStatus.terminated;
                        _Instance.ComplatedTime = DateTime.Now;
                        complatedParameters = commonEvent.GetComplatedParameters(instance_Businesses, FlowInstanceStatus.terminated);
                    }
                    break;
                ///所有分支终止则终止
                case BranchStrategyType.AllAgree:
                    {
                        if (otherTasks.Any(o => o.Status == (int)FlowTaskStatus.audit))
                        {
                            flow_Tasks = flow_Tasks.Join(lineTasks.Where(o => o.BranchLine == branch_Task.BranchLine), a => a.TaskId, b => b.TaskId, (a, b) => a).ToList();
                        }
                        else
                        {
                            if (otherTasks.Any(o => o.Status.In((int)FlowTaskStatus.approved, (int)FlowTaskStatus.returned)))
                            {
                                result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOT_END_ERROR);
                            }
                            else
                            {
                                _Instance.FlowStatus = (int)FlowInstanceStatus.terminated;
                                _Instance.ComplatedTime = DateTime.Now;
                                complatedParameters = commonEvent.GetComplatedParameters(instance_Businesses, FlowInstanceStatus.terminated);
                            }
                        }
                    }
                    break;
                case BranchStrategyType.Scale:
                    {
                        var groups = lineTasks.GroupBy(o => new { o.Branch, o.BranchLine });
                        decimal cnt = groups.Count();
                        decimal doedCnt = 0;
                        foreach (var item in groups)
                        {
                            var task = item.Join(flow_Tasks, a => a.TaskId, b => b.TaskId, (a, b) => b);
                            if (task.Any(o => o.TaskId == currentTask.TaskId || o.Status == (int)FlowTaskStatus.terminated))
                            {
                                doedCnt++;
                            }
                        }
                        decimal scale = 0;
                        if (cnt > 0 && doedCnt > 0)
                            scale = (doedCnt / cnt) * 100;
                        if (!string.IsNullOrEmpty(startBranchNode.Meta.EndScale) && scale < decimal.Parse(startBranchNode.Meta.EndScale))
                        {
                            if (otherTasks.Any(o => o.Status == (int)FlowTaskStatus.audit))
                            {
                                flow_Tasks = flow_Tasks.Join(lineTasks.Where(o => o.BranchLine == branch_Task.BranchLine), a => a.TaskId, b => b.TaskId, (a, b) => a).ToList();
                            }
                            else
                            {
                                result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOT_END_ERROR);
                            }
                        }
                        else
                        {
                            _Instance.FlowStatus = (int)FlowInstanceStatus.terminated;
                            _Instance.ComplatedTime = DateTime.Now;
                            complatedParameters = commonEvent.GetComplatedParameters(instance_Businesses, FlowInstanceStatus.terminated);
                        }
                    }
                    break;
            }
        }
        /// <summary>
        /// 回收
        /// </summary>
        /// <returns></returns>
        private Result FlowSingeRecyclingEvent(FlowTaskInfo currentTask, NodeInfo currentStep, Flow_Instance _Instance)
        {
            Result result = new Result(null);
            List<Flow_Task> flow_Tasks = this.business.GetTasks(currentTask.GroupId);
            List<Flow_Task> deleteTasks = flow_Tasks.Where(o => o.Sort == (currentTask.Sort + 1)).ToList();
            List<Flow_Task> currentTasks = flow_Tasks.Where(o => o.Sort == currentTask.Sort).ToList();
            List<long> branchTaskIds = deleteTasks.Select(o => o.TaskId).ToList();
            currentTasks.ForEach(cu =>
            {
                cu.Read = false;
                cu.Modifdate = DateTime.Now;
                cu.Modifier = currentTask.Reviewer;
                if (currentStep.Meta.Prop == "start")
                {
                    cu.Status = (int)FlowTaskStatus.momentum;
                    _Instance.FlowStatus = (int)FlowInstanceStatus.momentum;
                    _Instance.ApplyTaskId = currentTask.TaskId;
                }
                else
                {
                    FlowTaskStatus flowTaskStatus = (FlowTaskStatus)cu.Status;
                    switch (flowTaskStatus)
                    {
                        case FlowTaskStatus.approved:
                            cu.Status = (int)FlowTaskStatus.audit;
                            break;
                        case FlowTaskStatus.otherapproved:
                            cu.Status = (int)FlowTaskStatus.otheraudit;
                            break;
                        case FlowTaskStatus.red:
                            cu.Status = (int)FlowTaskStatus.unread;
                            break;
                    }
                }
            });
            FlowTaskInfo prevTask = new FlowTaskInfo() { InstanceId = currentTask.InstanceId, GroupId = currentTask.GroupId, StepId = currentTask.PrevStepId, TaskId = currentTask.PrevTaskId };
            List<Flow_Instance_CurrentTask> flow_Instance_CurrentTasks = this.commonEvent.CurrentInstanceTaskInfoEvent(prevTask, currentTasks.Where(o => (o.Status == (int)FlowTaskStatus.audit || o.Status == (int)FlowTaskStatus.momentum)).ToList(), FlowProcessType.Revocation);
            using (TransactionScope ts1 = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions() { IsolationLevel = IsolationLevel.ReadCommitted }))
            {
                ///处理当前任务
                bool b = business.UpdateBasicBusiness(currentTasks);
                b = b && business.UpdateBasicBusiness(_Instance);
                b = b && business.DeleteCurrentTask(currentTask.TaskId);
                b = b && business.AddBasicBusiness(flow_Instance_CurrentTasks) > 0;
                b = b && business.RecyclingDeleteTask(deleteTasks);
                if (branchTaskIds.Any())
                    b = b && business.DeleteBranchTask(branchTaskIds);
                if (b) ts1.Complete();
                else result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_CURRENT_INSTANCE_UN_RECYCLING_ERROR);
            }
            return result;
        }
    }
}
