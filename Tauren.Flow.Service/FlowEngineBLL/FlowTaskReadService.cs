﻿using System;
using System.Collections.Generic;
using System.Text;
using Tauren.Flow.DLL.FlowEngine;
using Tauren.Flow.Entity.Enum;
using Tauren.Flow.Entity.Global;
using Tauren.Flow.Entity.Model;
using Tauren.Flow.Entity.Output;
using Tauren.Flow.Infrastructure.Common;
using Tauren.Flow.Service.ProcessBasicInfoBLL;

namespace Tauren.Flow.Service.FlowEngineBLL
{
    /// <summary>
    /// 进行阅办的业务数据处理
    /// </summary>
    public class FlowTaskReadService
    {
        private IFlowEngineBusiness business;
        private LongCommon longCommon;
        private IProcessBasicInfoService processBasicInfoService;
        private FlowEngineCommonEvent commonEvent;

        public FlowTaskReadService(IProcessBasicInfoService _processBasicInfoService, IFlowEngineBusiness _business, LongCommon _longCommon, FlowEngineCommonEvent _commonEvent)
        {
            this.business = _business;
            this.processBasicInfoService = _processBasicInfoService;
            this.longCommon = _longCommon;
            this.commonEvent = _commonEvent;
        }

        /// <summary>
        /// 任务读办
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        public Result FlowTaskReadEvent(long taskId, string Opinion)
        {
            Result result = new Result(null);
            ///获取当前任务 
            Flow_Task currentTask = business.Get<Flow_Task>(taskId);
            switch (currentTask)
            {
                case null: result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_CURRENTTASK_ERROR); break;
                default:
                    {
                        FlowTaskStatus taskStatus = (FlowTaskStatus)currentTask.Status;
                        FlowTaskType taskType = (FlowTaskType)currentTask.Type;
                        if (taskStatus != FlowTaskStatus.unread && taskType != FlowTaskType.cc && taskType != FlowTaskType.circulated)
                            result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_CURRENTTASK_UNCC_ERROR);
                        else
                        {
                            currentTask.Read = true;
                            currentTask.Status = (int)FlowTaskStatus.red;
                            currentTask.ProcessTime = DateTime.Now;
                            currentTask.Modifdate = DateTime.Now;
                            currentTask.Modifier = currentTask.Reviewer;
                            currentTask.Comment = Opinion;
                            result.Success = business.UpdateBasicBusiness<Flow_Task>(currentTask);
                        }
                    }
                    break;
            }
            return result;
        }
    }
}
