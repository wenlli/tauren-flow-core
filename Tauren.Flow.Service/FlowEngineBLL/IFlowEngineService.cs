﻿using System;
using System.Collections.Generic;
using System.Text;
using Tauren.Flow.Entity.Config;
using Tauren.Flow.Entity.Global;

namespace Tauren.Flow.Service.FlowEngineBLL
{
    /// <summary>
    /// 流程引擎执行事务
    /// </summary>
    public interface IFlowEngineService
    {
        /// <summary>
        /// 执行流程引擎
        /// </summary>
        /// <param name="Parameter"></param>
        /// <returns></returns>
        IResponseMessage FlowEngineRunEvent(FlowParameter Parameter);

        /// <summary>
        /// 任务回收
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        IResponseMessage FlowRecycling(long taskId);

        /// <summary>
        /// 终止流程
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        IResponseMessage FlowTermination(RequestParameter Paramter);

        /// <summary>
        /// 传阅和抄送任务读办
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        IResponseMessage FlowRead(RequestParameter Paramter);

        /// <summary>
        /// 进行传阅任务处理
        /// </summary>
        /// <param name="paramter"></param>
        /// <returns></returns>
        IResponseMessage FlowEngineCirculatedEvent(FlowParameter paramter);

        /// <summary>
        /// 任务退回
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        IResponseMessage FlowTaskBack(RequestParameter Paramter);

        /// <summary>
        /// 删除流程实例
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        IResponseMessage FlowTaskDelete(long taskId);

        /// <summary>
        /// 退回至具体步骤
        /// </summary>
        /// <param name="paramter"></param>
        /// <returns></returns>
        IResponseMessage FlowTaskBackStep(FlowParameter paramter);
    }
}
