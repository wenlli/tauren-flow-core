﻿
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Tauren.Flow.DLL.Base;
using Tauren.Flow.DLL.Field_FormDLL;
using Tauren.Flow.DLL.FlowInfoDLL;
using Tauren.Flow.DLL.Resouces;
using Tauren.Flow.Entity.Config;
using Tauren.Flow.Entity.Const;
using Tauren.Flow.Entity.Enum;
using Tauren.Flow.Entity.Global;
using Tauren.Flow.Entity.Input;
using Tauren.Flow.Entity.Model;
using Tauren.Flow.Entity.Output;
using Tauren.Flow.Infrastructure.Common;
using Tauren.Flow.Infrastructure.Globals;

namespace Tauren.Flow.Service.FlowInfoBLL
{
    /// <summary>
    /// 流程基本信息
    /// </summary>
    internal class FlowInfoService : Base.BaseService, IFlowInfoService
    {
        private IFlowInfoBusiness business;
        private LongCommon longCommon;
        private ApiVersionsConfig config;
        public IField_FormBusiness formBusiness { get; set; }
        public FlowInfoService(IConfiguration configuration, IFlowInfoBusiness _business, IConnectionBLLBase connectionBLL, IMenuResourcesBusiness resourcesBusiness) : base(_business, connectionBLL, resourcesBusiness)
        {
            this.business = _business;
            this.ResourcesInfo = this.GetResources(GlobalConst.RESOURCES_FLAG_MODULE_FLOW_INFO_KEY);
            this.JudePrmission(GlobalConst.RESOURCES_FLAG_MODULE_FLOW_INFO_KEY.ToLower());
            this.longCommon = new LongCommon();
            config = configuration.Get<ApiVersionsConfig>();
        }

        /// <summary>
        /// 添加流程信息
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public IResponseMessage Add(FlowInfo_InPut data)
        {
            if (!HavePermission) return ErrorType.BUSINESS_WITHOUT_PERMISSION.Failure(this.CommonText(ErrorType.BUSINESS_WITHOUT_PERMISSION));
            if (data is null) return GlobalErrorType.GLOBAL_NOT_FOUND_INPUT_DATA.Failure(this.CommonText(GlobalErrorType.GLOBAL_NOT_FOUND_INPUT_DATA));
            FlowInfo model;
            ToModel(data, out model);
            if (model is null) return GlobalErrorType.GLOBAL_NOT_FOUND_INPUT_DATA.Failure(this.CommonText(GlobalErrorType.GLOBAL_NOT_FOUND_INPUT_DATA));
            model.FlowId = longCommon.GenerateRandomCode(6);
            model.Creator = Global.EmpId;
            model.Modifier = Global.EmpId;
            ///流程管理员
            if (data.Managers.Any())
            {
                model.Manager = string.Join(",", data.Managers.Select(o => o.EmpId));
            }
            ///流程实例管理员
            if (data.InstanceManagers.Any())
            {
                model.InstanceManager = string.Join(",", data.InstanceManagers.Select(o => o.EmpId));
            }
            FlowVersion version = new FlowVersion()
            {
                FlowId = model.FlowId,
                FormId = model.FormId,
                VersionId = longCommon.GenerateRandomCode(6),
                VersionNo = -1,
                Creatdate = DateTime.Now,
                Creator = Global.EmpId,
                Modifdate = DateTime.Now,
                Modifier = Global.EmpId,
                Sequence = 1,
                Name = $"v0.0",
                ParallelTask = data.ParallelTask,
                SumbitPrevStep = data.SumbitPrevStep,
            };
            if (data.Form.Any())
            {
                var form = data.Form.FirstOrDefault();
                model.FormId = form.AutoId;
                version.FormId = form.AutoId;
                version.FormVersionId = form.CurrentVersionId;
            }
            model.CurrentVersion = version.VersionNo;
            model.Status = (int)FlowStatus.design;
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions() { IsolationLevel = IsolationLevel.ReadCommitted }))
            {
                bool b = business.UnRepeatAdd(model) > 0;
                b = b && business.Add(version) >= 0;
                if (b) { ts.Complete(); return "".OnSucceed(this.CommonText(GlobalErrorType.GLOBAL_SAVE_SUCCESSINFO.ToString())); }
                else return GlobalErrorType.GLOBAL_SAVE_FAILURE.OnSucceed(this.CommonText(GlobalErrorType.GLOBAL_SAVE_FAILURE));
            }
        }

        public IResponseMessage Delete(long AutoID)
        {
            if (!HavePermission) return ErrorType.BUSINESS_WITHOUT_PERMISSION.Failure(this.CommonText(ErrorType.BUSINESS_WITHOUT_PERMISSION));
            var data = business.FirstOrDefault(AutoID);
            if (data is null) return GlobalErrorType.GLOBAL_NOT_FOUND_DB_DATA.Failure(this.CommonText(GlobalErrorType.GLOBAL_NOT_FOUND_DB_DATA));
            data.Status = (int)FlowStatus.delete;
            data.Modifier = Global.EmpId;
            data.Modifdate = DateTime.Now;
            bool b = business.Update<FlowInfo>(data);
            if (b)
                return "".OnSucceed(this.Resources(GlobalErrorType.GLOBAL_DELETE_SUCCESSINFO));
            else return GlobalErrorType.GLOBAL_DELETE_FAILURE.OnSucceed(this.CommonText(GlobalErrorType.GLOBAL_DELETE_FAILURE));
        }

        public IResponseMessage Edit(FlowInfo_InPut data)
        {
            throw new NotImplementedException();
        }

        public IResponseMessage FirstOrDefault(long id, long versionId)
        {
            
            if (!HavePermission) return ErrorType.BUSINESS_WITHOUT_PERMISSION.Failure(this.CommonText(ErrorType.BUSINESS_WITHOUT_PERMISSION));
            return BuildViewFlowInfo(id, versionId).Succeed();
        }



        public IResponseMessage Pages(QueryModel queryModel)
        {
            if (!HavePermission && queryModel.CheckPermission) return ErrorType.BUSINESS_WITHOUT_PERMISSION.Failure(this.CommonText(ErrorType.BUSINESS_WITHOUT_PERMISSION));
            TableListModel<PageFlowInfoTable> tableListModel = new TableListModel<PageFlowInfoTable>();
            List<TableListHeaderModel> Headers = new List<TableListHeaderModel>();
            Headers.Add(new TableListHeaderModel() { Key = "rowNo", Name = CommonText("rowNo"), Width = 120, Align = "center", Fixed = true });
            Headers.Add(new TableListHeaderModel() { Key = "flowId", Name = CommonText("autoid"), Width = 70, Hide = true, Type = "primary_key", Align = "center", SorTable = SortType.custom.ToString(), Fixed = true });
            Headers.Add(new TableListHeaderModel() { Key = "cnName", Name = CommonText("thcnname"), Align = "left", IsOperation = true, Fixed = true });
            Headers.Add(new TableListHeaderModel() { Key = "enName", Name = CommonText("thenname"), Align = "left", Fixed = true });
            Headers.Add(new TableListHeaderModel() { Key = "dic_Type", Name = Resources("flowtype"), Width = 100, IsTag = true, Align = "center", Fixed = true, SorTable = SortType.custom.ToString() });
            Headers.Add(new TableListHeaderModel() { Key = "flowIcon", Name = Resources("flowicon"), Width = 100, Align = "center", Fixed = true, SorTable = SortType.custom.ToString(), IsIcon = true, IsSvg = true });
            Headers.Add(new TableListHeaderModel() { Key = "currentVersion", Name = Resources("currentversion"), IsTag = true, Width = 120, Align = "center", SorTable = SortType.custom.ToString() });
            Headers.Add(new TableListHeaderModel() { Key = "versionName", Name = Resources("versionname"), IsTag = true, Width = 120, Align = "center", SorTable = SortType.custom.ToString() });
            Headers.Add(new TableListHeaderModel() { Key = "dic_Status", Name = CommonText("thstate"), IsTag = true, Width = 100, Align = "center", SorTable = SortType.custom.ToString() });
            Headers.Add(new TableListHeaderModel() { Key = "dic_Creator", Name = CommonText("diccreator"), Width = 120, Align = "center" });
            Headers.Add(new TableListHeaderModel() { Key = "dic_Creatdate", Name = CommonText("diccreatdate"), Width = 160, Align = "center" });
            long Total = 0;
            List<PageFlowInfoTable> Data = business.Pages(queryModel, out Total);

            if (Data.Any())
            {
                Data.ForEach(o =>
                {
                    o.Row_Style = ((FlowStatus)o.Status) != FlowStatus.delete && ((FlowStatus)o.Status) != FlowStatus.unloaded ? "" : "danger";
                    o.Enable = ((FlowStatus)o.Status) != FlowStatus.delete && ((FlowStatus)o.Status) != FlowStatus.unloaded;
                    o.Dic_Creator = EmpDictionary.ContainsKey(o.Creator) ? EmpDictionary[o.Creator] : "";
                    o.Dic_Creatdate = o.Creatdate.Value.ToString(GlobalConst.DATE_FORMAT_SECONDS);
                    o.Dic_Status = this.Resources($"formstatus{((FlowStatus)o.Status) + ""}");
                    o.Dic_Type = this.Resources($"flowtype{((FlowType)o.Type) + ""}");
                    o.Operations = new List<SystemButton>();
                    if (Global.IsAdmin && (Buttons is null || !Buttons.Any()))
                    {
                        BuliderButtons(o.Operations, o.CurrentVersionId + "", o.FlowId, ButtomType.set);
                        if (o.Status != (int)FlowStatus.delete)
                            BuliderButtons(o.Operations, o.CurrentVersionId + "", o.FlowId, ButtomType.delete);
                        if (o.Status == (int)FlowStatus.installed)
                        {
                            BuliderButtons(o.Operations, o.CurrentVersionId + "", o.FlowId, ButtomType.unloaded);
                        }
                    }
                    else
                    {
                        if (Buttons.Any(o => o.Location == ButtomLocation.center.ToString() && o.Value == ButtomType.set.ToString()))
                            BuliderButtons(o.Operations, o.CurrentVersionId + "", o.FlowId, ButtomType.set);
                        if (Buttons.Any(o => o.Location == ButtomLocation.center.ToString() && o.Value == ButtomType.delete.ToString()) && o.Status != (int)FlowStatus.delete)
                            BuliderButtons(o.Operations, o.CurrentVersionId + "", o.FlowId, ButtomType.delete);
                        if (o.Status == (int)FlowStatus.installed && Buttons.Any(o => o.Location == ButtomLocation.center.ToString() && o.Value == ButtomType.unloaded.ToString()))
                        {
                            BuliderButtons(o.Operations, o.CurrentVersionId + "", o.FlowId, ButtomType.unloaded);
                        }

                    }
                    if (o.CurrentVersionId != null && o.CurrentVersionId != 0)
                    {
                        //BuliderButtons(o.Operations, o.CurrentVersionId + "", o.FlowId, ButtomType.policy);
                        BuliderButtons(o.Operations, o.CurrentVersionId + "", o.FlowId, ButtomType.quickfield);
                        BuliderButtons(o.Operations, o.CurrentVersionId + "", o.FlowId, ButtomType.instancefield);
                    }
                });
            }
            tableListModel.Body = Data;
            tableListModel.Total = Total;
            tableListModel.Header = Headers;
            if (Global.IsAdmin && (Buttons is null || !Buttons.Any()))
            {
                BuliderButtons(tableListModel.Operations, "", 0, ButtomType.add);
            }
            else
            {
                if (Buttons.Any(o => o.Value == ButtomType.add.ToString()))
                    BuliderButtons(tableListModel.Operations, "", 0, ButtomType.add);
            }
            return tableListModel.Succeed();
        }

        /// <summary>
        /// 保存流程信息
        /// </summary>
        /// <param name="flowInfo"></param>
        /// <returns></returns>
        public IResponseMessage SaveFlow(Flow_OutPut flowInfo)
        {
            if (!HavePermission) return ErrorType.BUSINESS_WITHOUT_PERMISSION.Failure(this.CommonText(ErrorType.BUSINESS_WITHOUT_PERMISSION));
            FlowInfo model = business.FirstOrDefault(flowInfo.FlowProperty.FlowId);
            if (model is null) return GlobalErrorType.GLOBAL_NOT_FOUND_DB_DATA.Failure(this.CommonText(GlobalErrorType.GLOBAL_NOT_FOUND_DB_DATA));
            SettingFlowInfo(model, flowInfo.FlowProperty);
            FlowVersion version = new FlowVersion()
            {
                FlowId = model.FlowId,
                FormId = model.FormId,
                FormVersionId = flowInfo.FlowProperty.Form.FirstOrDefault().CurrentVersionId,
                VersionId = longCommon.GenerateRandomCode(6),
                VersionNo = flowInfo.FlowProperty.VersionNo + 1,
                Creatdate = DateTime.Now,
                Creator = Global.EmpId,
                Modifdate = DateTime.Now,
                Modifier = Global.EmpId,
                Sequence = 1,
                Name = $"v{flowInfo.FlowProperty.VersionNo + 1}.0",
                ParallelTask = flowInfo.FlowProperty.ParallelTask,
                SumbitPrevStep = flowInfo.FlowProperty.SumbitPrevStep
            };
            List<Flow_Step_Info> _Step_Infos = new List<Flow_Step_Info>();
            List<Flow_Step_Strategy> _Step_Strategies = new List<Flow_Step_Strategy>();
            List<Flow_Lines> _Lines = new List<Flow_Lines>();
            List<Flow_Buttons> _Buttons = new List<Flow_Buttons>();
            List<Flow_Step_Field_Area> _Areas = new List<Flow_Step_Field_Area>();
            List<Flow_Step_Area_Fields> _Fields = new List<Flow_Step_Area_Fields>();
            this.SettingStepInfos(flowInfo.NodeList, _Step_Infos, _Step_Strategies, _Buttons, _Areas, _Fields, version, flowInfo.FlowProperty.VersionId);
            this.SettingLineInfos(flowInfo.LinkList, _Lines, version);
            List<Flow_Complated_Info> flow_Complated_Infos = null;
            ToModels(flowInfo.Complateds, out flow_Complated_Infos);
            if (flow_Complated_Infos != null && flow_Complated_Infos.Any())
            {
                flow_Complated_Infos.ForEach(o =>
                {
                    o.FlowId = flowInfo.FlowProperty.FlowId;
                    o.FlowVersionId = version.VersionId.Value;
                    o.Creator = Global.EmpId;
                    o.Creatdate = DateTime.Now;
                    o.Modifdate = DateTime.Now;
                    o.Modifier = Global.EmpId;
                });
            }
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions() { IsolationLevel = IsolationLevel.ReadCommitted }))
            {
                bool b = business.Update<FlowInfo>(model);
                if (b && version != null) b = business.Add(version) >= 0;
                if (b && _Step_Infos.Any()) b = business.Add(_Step_Infos) >= 0;
                if (b && _Step_Strategies.Any()) b = business.Add(_Step_Strategies) >= 0;
                if (b && _Lines.Any()) b = business.Add(_Lines) >= 0;
                if (b && _Buttons.Any()) b = business.Add(_Buttons) >= 0;
                if (b && _Areas.Any()) b = business.Add(_Areas) >= 0;
                if (b && _Fields.Any()) b = business.Add(_Fields) >= 0;
                b = b & business.DeleteFlowComplatedInfo(version.VersionId.Value);
                b = b & business.Add(flow_Complated_Infos) >= 0;
                if (b)
                {
                    ts.Complete();
                    return "".OnSucceed(this.CommonText(GlobalErrorType.GLOBAL_SAVE_SUCCESSINFO.ToString()));
                }
                else return GlobalErrorType.GLOBAL_SAVE_FAILURE.OnSucceed(this.CommonText(GlobalErrorType.GLOBAL_SAVE_FAILURE));
            }
        }

        /// <summary>
        /// 保存流程信息
        /// </summary>
        /// <param name="flowInfo"></param>
        /// <returns></returns>
        public IResponseMessage PublishFlow(Flow_OutPut flowInfo)
        {
            if (!HavePermission) return ErrorType.BUSINESS_WITHOUT_PERMISSION.Failure(this.CommonText(ErrorType.BUSINESS_WITHOUT_PERMISSION));
            FlowInfo model = business.FirstOrDefault(flowInfo.FlowProperty.FlowId);
            if (model is null) return GlobalErrorType.GLOBAL_NOT_FOUND_DB_DATA.Failure(this.CommonText(GlobalErrorType.GLOBAL_NOT_FOUND_DB_DATA));
            SettingFlowInfo(model, flowInfo.FlowProperty);
            FlowVersion version = new FlowVersion()
            {
                FlowId = model.FlowId,
                FormId = model.FormId,
                FormVersionId = flowInfo.FlowProperty.Form.FirstOrDefault().CurrentVersionId,
                VersionId = longCommon.GenerateRandomCode(6),
                VersionNo = flowInfo.FlowProperty.VersionNo + 1,
                Creatdate = DateTime.Now,
                Creator = Global.EmpId,
                Modifdate = DateTime.Now,
                Modifier = Global.EmpId,
                Sequence = 1,
                Name = $"v{flowInfo.FlowProperty.VersionNo + 1}.0",
                ParallelTask = flowInfo.FlowProperty.ParallelTask,
                SumbitPrevStep = flowInfo.FlowProperty.SumbitPrevStep,
            };
            model.CurrentVersion = version.VersionNo;
            model.Status = (int)FlowStatus.installed;
            List<Flow_Step_Info> _Step_Infos = new List<Flow_Step_Info>();
            List<Flow_Step_Strategy> _Step_Strategies = new List<Flow_Step_Strategy>();
            List<Flow_Lines> _Lines = new List<Flow_Lines>();
            List<Flow_Buttons> _Buttons = new List<Flow_Buttons>();
            List<Flow_Step_Field_Area> _Areas = new List<Flow_Step_Field_Area>();
            List<Flow_Step_Area_Fields> _Fields = new List<Flow_Step_Area_Fields>();
            this.SettingStepInfos(flowInfo.NodeList, _Step_Infos, _Step_Strategies, _Buttons, _Areas, _Fields, version, flowInfo.FlowProperty.VersionId);
            this.SettingLineInfos(flowInfo.LinkList, _Lines, version);
            List<Flow_Complated_Info> flow_Complated_Infos = null;
            ToModels(flowInfo.Complateds, out flow_Complated_Infos);
            if (flow_Complated_Infos != null && flow_Complated_Infos.Any())
            {
                flow_Complated_Infos.ForEach(o =>
                {
                    o.FlowId = flowInfo.FlowProperty.FlowId;
                    o.FlowVersionId = version.VersionId.Value;
                    o.Creator = Global.EmpId;
                    o.Creatdate = DateTime.Now;
                    o.Modifdate = DateTime.Now;
                    o.Modifier = Global.EmpId;
                });
            }
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions() { IsolationLevel = IsolationLevel.ReadCommitted }))
            {
                bool b = business.Update<FlowInfo>(model);
                if (b && version != null) b = business.Add(version) >= 0;
                if (b && _Step_Infos.Any()) b = business.Add(_Step_Infos) >= 0;
                if (b && _Step_Strategies.Any()) b = business.Add(_Step_Strategies) >= 0;
                if (b && _Lines.Any()) b = business.Add(_Lines) >= 0;
                if (b && _Buttons.Any()) b = business.Add(_Buttons) >= 0;
                if (b && _Areas.Any()) b = business.Add(_Areas) >= 0;
                if (b && _Fields.Any()) b = business.Add(_Fields) >= 0;
                b = b & business.DeleteFlowComplatedInfo(version.VersionId.Value);
                b = b & business.Add(flow_Complated_Infos) >= 0;
                if (b)
                {
                    ts.Complete();
                    this.AddFlowInfoRedis(flowInfo, version, model);
                    return "".OnSucceed(this.CommonText(GlobalErrorType.GLOBAL_SAVE_SUCCESSINFO.ToString()));
                }
                else return GlobalErrorType.GLOBAL_SAVE_FAILURE.OnSucceed(this.CommonText(GlobalErrorType.GLOBAL_SAVE_FAILURE));
            }
        }

        /// <summary>
        /// 流程卸载
        /// </summary>
        /// <param name="AutoID"></param>
        /// <returns></returns>
        public IResponseMessage Uninstall(long AutoID)
        {
            if (!HavePermission) return ErrorType.BUSINESS_WITHOUT_PERMISSION.Failure(this.CommonText(ErrorType.BUSINESS_WITHOUT_PERMISSION));
            var data = business.Get<FlowInfo>(AutoID);
            if (data is null) return GlobalErrorType.GLOBAL_NOT_FOUND_DB_DATA.Failure(this.CommonText(GlobalErrorType.GLOBAL_NOT_FOUND_DB_DATA));
            data.Status = (int)FlowStatus.unloaded;
            data.Modifier = Global.EmpId;
            data.Modifdate = DateTime.Now;
            bool b = business.Update<FlowInfo>(data);
            if (b) return "".OnSucceed(this.CommonText(GlobalErrorType.GLOBAL_SAVE_SUCCESSINFO.ToString()));
            else return GlobalErrorType.GLOBAL_SAVE_FAILURE.OnSucceed(this.CommonText(GlobalErrorType.GLOBAL_SAVE_FAILURE));
        }

        /// /获取表单最新的字段
        /// </summary>
        /// <param name="FormVersionId">表单版本ID</param>
        /// <param name="FlowVersionId">流程版本ID</param>
        /// <param name="StepId">步骤ID</param>
        /// <returns></returns>
        public IResponseMessage FlowStepFields(long FormVersionId, long FlowVersionId, string StepId)
        {
            return Field_Form_Version_Puts(FormVersionId, FlowVersionId, StepId).OnSucceed();
        }
        /// /获取表单最新的字段
        /// </summary>
        /// <param name="FormVersionId">表单版本ID</param>
        /// <param name="FlowVersionId">流程版本ID</param>
        /// <param name="StepId">步骤ID</param>
        /// <returns></returns>
        public Field_Form_Version_Put Field_Form_Version_Puts(long FormVersionId, long FlowVersionId, string StepId)
        {
            var fields = formBusiness.GetVersionFields(FormVersionId);
            List<Field_Form_VersionExt> _Version_Puts = string.IsNullOrEmpty(StepId) ? fields : fields.Where(o => o.AreaCode.ToLower() != "form_apply_employeebasicinfo").ToList();
            List<Field_Form_VersionExt> Flow_Step_VersionExts = string.IsNullOrEmpty(StepId) ? new List<Field_Form_VersionExt>() : business.StepFormInfo(FlowVersionId).Where(o => o.StepId == StepId).ToList();
            List<Field_Form_VersionExt> _Puts = new List<Field_Form_VersionExt>();
            if (Flow_Step_VersionExts.Any())
            {
                _Puts = _Version_Puts.Join(Flow_Step_VersionExts, a => new { a.AreaCode, a.FieldCode }, b => new { b.AreaCode, b.FieldCode }, (a, b) =>
                {
                    a.Editor = b.Editor;
                    a.Show = b.Show;
                    a.Required = b.Required;
                    a.AreaShow = b.AreaShow;
                    a.AreaEditor = b.AreaEditor;
                    return a;
                }).ToList();
                var _putArea = _Puts.GroupBy(o => new { o.TableCode, o.AreaCNName, o.AreaENName, o.AreaCode, o.AreaUsable, o.MainTable, o.AreaSeq, o.AreaId, o.AreaEditor, o.AreaShow }).ToList();
                ///节点上不包含的新字段-但是在节点上已存在区域
                var exceptAraes = _Version_Puts.Where(o => !_Puts.Any(p => p.AreaCode == o.AreaCode && p.FieldCode == o.FieldCode)).ToList();
                var joinAreas = exceptAraes.Join(_putArea, a => a.AreaCode, b => b.Key.AreaCode, (a, b) =>
                {
                    a.Show = b.Key.AreaShow && a.Show;
                    a.Editor = b.Key.AreaShow && b.Key.AreaEditor && a.Show && a.Editor;
                    a.Required = b.Key.AreaShow && b.Key.AreaEditor && a.Show && a.Editor && a.Required;
                    a.AreaEditor = b.Key.AreaEditor;
                    a.AreaShow = b.Key.AreaShow;
                    return a;
                });
                if (joinAreas.Any()) _Puts.AddRange(joinAreas);
                ///新加入的字段且该区域不在节点上
                var resluts = exceptAraes.Where(o => !joinAreas.Any(p => p.AreaCode == o.AreaCode && p.FieldCode == o.FieldCode)).ToList();
                if (resluts.Any()) _Puts.AddRange(resluts);
            }
            else _Puts = _Version_Puts;
            Field_Form_Version_Put put = null;
            Field_Form_VersionExt ext = _Puts.FirstOrDefault();
            if (ext != null)
            {
                put = new Field_Form_Version_Put();
                put.FormId = ext.FormId;
                put.AutoId = ext.VersionId;
                if (_Puts.Any())
                {
                    string tablecode = $"'{string.Join("','", _Puts.Select(o => o.TableCode).Distinct())}'";
                    var fromtables = formBusiness.GetTableByCode(tablecode);
                    var tables = formBusiness.GetTableFields(tablecode);
                    var joinTables = fromtables.Join(tables, a => a.TableCode, b => b.TableCode, (a, b) => { a.TableName = b.TableName; a.TableENName = b.TableENName; return a; }).ToList();
                    var results = _Puts.Join(tables, a => a.TableCode, b => b.TableCode, (a, b) =>
                    {
                        a.AreaUsable = b.TableEnable && !b.TableHaveDelete;
                        return a;
                    }).Distinct().ToList();
                    results.GroupBy(o => new { o.TableCode, o.AreaCNName, o.AreaENName, o.AreaCode, o.AreaUsable, o.MainTable, o.AreaSeq, o.AreaId, o.AreaEditor, o.AreaShow }).ToList().OrderBy(o => o.Key.AreaSeq).ToList().ForEach(g => put.Areas.Add(BuilderArea(g.Key, results, joinTables, tables)));
                }
            }
            return put;
        }

        /// <summary>
        /// 获取流程人员
        /// </summary>
        /// <param name="empId"></param>
        /// <returns></returns>
        public IResponseMessage GetFlowEmployees(string empId)
        {
            List<Employee> employees = business.GetBasicListByQuery<Employee>("empid", empId);
            List<PersonInfo_OutPut> Results = new List<PersonInfo_OutPut>();
            employees.ForEach(o =>
            {
                Results.Add(new PersonInfo_OutPut() { CNName = o.CNName, ENName = o.ENName, EmpCode = o.EmpCode, EmpId = o.EmpId });
            });
            return Results.Succeed();
        }
        public IResponseMessage GetFlowPositions(string positionId)
        {
            List<Position> Results = business.GetBasicListByQuery<Position>("positionid", positionId);
            return Results.Succeed();
        }

        /// <summary>
        /// 根据流程Id或者流程版本ID获取表单字段
        /// </summary>
        /// <param name="flowId"></param>
        /// <param name="versionId"></param>
        /// <returns></returns>
        public IResponseMessage QuickDisplayField(long flowId, long versionId, string type)
        {
            if (!HavePermission) return ErrorType.BUSINESS_WITHOUT_PERMISSION.Failure(this.CommonText(ErrorType.BUSINESS_WITHOUT_PERMISSION));
            if (flowId == 0 && versionId == 0) return new Field_Form_Version_Put().Succeed();
            ExtFlowInfo _OutPut = business.GetFlowSettingInfo(flowId, versionId);
            if (_OutPut == null) return new Field_Form_Version_Put().Succeed();
            List<Field_Form_VersionExt> _Version_Puts = formBusiness.GetVersionFields(_OutPut.FormVersionId ?? 0).Where(o => !o.Records).ToList();
            List<Field_Form_VersionExt> _Puts = new List<Field_Form_VersionExt>();
            switch (type)
            {
                case "instance":
                    {
                        List<Flow_Instance_Field> instance_Fields = formBusiness.GetFlowInstanceFields(flowId, versionId);
                        if (instance_Fields.Any())
                        {
                            var res = _Version_Puts.Where(o => !instance_Fields.Any(p => p.FieldKey == o.FieldKey)).ToList();
                            if (res.Any())
                            {
                                _Puts.AddRange(res);
                            }
                            var joins = _Version_Puts.Where(o => instance_Fields.Any(p => p.FieldKey == o.FieldKey)).ToList();

                            if (joins.Any())
                            {
                                joins.ForEach(o => o.Selected = true);
                                _Puts.AddRange(joins);
                            }
                        }
                    }
                    break;
                default:
                    {
                        List<Flow_Quick_Field> _Quick_Fields = formBusiness.GetFlowQuickFields(flowId, versionId);
                        if (_Quick_Fields.Any())
                        {
                            var res = _Version_Puts.Where(o => !_Quick_Fields.Any(p => p.FieldKey == o.FieldKey)).ToList();
                            if (res.Any())
                            {
                                _Puts.AddRange(res);
                            }
                            var joins = _Version_Puts.Where(o => _Quick_Fields.Any(p => p.FieldKey == o.FieldKey)).ToList();

                            if (joins.Any())
                            {
                                joins.ForEach(o => o.Selected = true);
                                _Puts.AddRange(joins);
                            }
                        }
                    }
                    break;
            }
            _Puts = _Puts.Any() ? _Puts : _Version_Puts;
            Field_Form_Version_Put put = null;
            Field_Form_VersionExt ext = _Puts.FirstOrDefault();
            if (ext != null)
            {
                put = new Field_Form_Version_Put();
                put.FormId = ext.FormId;
                put.AutoId = ext.VersionId;
                if (_Puts.Any())
                {
                    string tablecode = $"'{string.Join("','", _Puts.Select(o => o.TableCode).Distinct())}'";
                    var fromtables = formBusiness.GetTableByCode(tablecode);
                    var tables = formBusiness.GetTableFields(tablecode);
                    var joinTables = fromtables.Join(tables, a => a.TableCode, b => b.TableCode, (a, b) => { a.TableName = b.TableName; a.TableENName = b.TableENName; return a; }).ToList();
                    var results = _Puts.Join(tables, a => a.TableCode, b => b.TableCode, (a, b) =>
                    {
                        a.AreaUsable = b.TableEnable && !b.TableHaveDelete;
                        return a;
                    }).Distinct().ToList();
                    results.GroupBy(o => new { o.TableCode, o.AreaCNName, o.AreaENName, o.AreaCode, o.AreaUsable, o.MainTable, o.AreaSeq, o.AreaId, o.AreaEditor, o.AreaShow }).ToList().OrderBy(o => o.Key.AreaSeq).ToList().ForEach(g => put.Areas.Add(BuilderArea(g.Key, results, joinTables, tables)));
                }
            }
            return put.OnSucceed();
        }
        public IResponseMessage SaveDisplayField(string type, List<Entity.Model.Flow_Display_Field> Models, long FlowId, long versionId)
        {
            if (!HavePermission) return ErrorType.BUSINESS_WITHOUT_PERMISSION.Failure(this.CommonText(ErrorType.BUSINESS_WITHOUT_PERMISSION));
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions() { IsolationLevel = IsolationLevel.ReadCommitted }))
            {
                bool b = true;

                switch (type)
                {
                    case "instance":
                        {
                            b = formBusiness.DeleteDisplayFields("flow_instance_field", FlowId, versionId);
                            if (Models.Any())
                            {

                                List<Flow_Instance_Field> instance_Fields = new List<Flow_Instance_Field>();
                                Models.ForEach(o => instance_Fields.Add(new Flow_Instance_Field()
                                {
                                    AutoId = longCommon.GenerateRandomCode(12),
                                    Creatdate = DateTime.Now,
                                    Creator = Global.EmpId,
                                    FieldCode = o.FieldCode,
                                    FieldKey = o.FieldKey,
                                    FlowId = o.FlowId,
                                    FlowVersionId = o.FlowVersionId,
                                    Modifdate = DateTime.Now,
                                    Modifier = Global.EmpId,
                                    Table = o.Table,
                                    ControlSouces = o.ControlSouces,
                                    ControlsType = o.ControlsType
                                }));
                                b = b && business.Add(instance_Fields) >= 0;
                            }
                        }
                        break;
                    default:
                        {
                            b = formBusiness.DeleteDisplayFields("flow_quick_field", FlowId, versionId);
                            if (Models.Any())
                            {

                                List<Flow_Quick_Field> instance_Fields = new List<Flow_Quick_Field>();
                                Models.ForEach(o => instance_Fields.Add(new Flow_Quick_Field()
                                {
                                    AutoId = longCommon.GenerateRandomCode(12),
                                    Creatdate = DateTime.Now,
                                    Creator = Global.EmpId,
                                    FieldCode = o.FieldCode,
                                    FieldKey = o.FieldKey,
                                    FlowId = o.FlowId,
                                    FlowVersionId = o.FlowVersionId,
                                    Modifdate = DateTime.Now,
                                    Modifier = Global.EmpId,
                                    Table = o.Table,
                                    ControlSouces = o.ControlSouces,
                                    ControlsType = o.ControlsType
                                }));
                                b = b && business.Add(instance_Fields) >= 0;
                            }
                        }
                        break;
                }
                if (b)
                {
                    ts.Complete();
                    return "".OnSucceed(this.CommonText(GlobalErrorType.GLOBAL_SAVE_SUCCESSINFO.ToString()));
                }
                else return GlobalErrorType.GLOBAL_SAVE_FAILURE.OnSucceed(this.CommonText(GlobalErrorType.GLOBAL_SAVE_FAILURE));
            }
        }
        #region private
        /// <summary>
        /// 将表单的值赋给保存的实体
        /// </summary>
        /// <param name="data"></param>
        /// <param name="data"></param>
        private void SettingFlowInfo(FlowInfo Model, FlowInfo_OutPut Data)
        {
            Model.CNName = Data.CNName;
            Model.ENName = Data.ENName;
            Model.SelectApplyEmp = Data.SelectApplyEmp;
            Model.FlowIcon = Data.FlowIcon;
            Model.FlowStyle = Data.FlowStyle;
            Model.Placeholder = Data.Placeholder;
            Model.SkipRules = Data.SkipRules;
            Model.View_Instance = Data.View_Instance;
            Model.Recycled = Data.Recycled;
            Model.Note = Data.Note;
            ///流程管理员
            if (Data.Managers.Any())
            {
                Model.Manager = string.Join(",", Data.Managers.Select(o => o.EmpId));
            }
            ///流程实例管理员
            if (Data.InstanceManagers.Any())
            {
                Model.InstanceManager = string.Join(",", Data.InstanceManagers.Select(o => o.EmpId));
            }
            if (Data.Form.Any())
            {
                var form = Data.Form.FirstOrDefault();
                Model.FormId = form.AutoId;
            }
            Model.Modifier = Global.EmpId;
            Model.Modifdate = DateTime.Now;
        }

        /// <summary>
        /// 创建节点信息
        /// </summary>
        /// <param name="NodeList"></param>
        /// <param name="_Step_Infos"></param>
        /// <param name="_Step_Strategies"></param>
        private void SettingStepInfos(List<NodeInfo> NodeList, List<Flow_Step_Info> _Step_Infos, List<Flow_Step_Strategy> _Step_Strategies, List<Flow_Buttons> _Buttons, List<Flow_Step_Field_Area> _Areas, List<Flow_Step_Area_Fields> _Fields, FlowVersion version, long oldFlowVersionId)
        {
            ////节点信息
            NodeList.ForEach(node =>
            {
                node.Meta.StepId = node.Id;
                node.Meta.VersionId = version.VersionId;
                node.Meta.Coordinate = JsonEvent.ToJsonString(node.Coordinate);
                node.Meta.Creator = Global.EmpId;
                node.Meta.Modifier = Global.EmpId;
                node.Meta.Creatdate = DateTime.Now;
                node.Meta.Modifdate = DateTime.Now;
                _Step_Infos.Add(node.Meta);
                node.Strategy.AutoId = longCommon.GenerateRandomCode(8);
                node.Strategy.StepId = node.Id;
                node.Strategy.Creator = Global.EmpId;
                node.Strategy.Modifier = Global.EmpId;
                node.Strategy.Creatdate = DateTime.Now;
                node.Strategy.Modifdate = DateTime.Now;
                node.Strategy.VersionId = version.VersionId;
                _Step_Strategies.Add(node.Strategy);
                double i = 0;
                node.Btns.ForEach(btn =>
                {
                    btn.VersionId = version.VersionId;
                    i = double.Parse((i + 1).ToString("f2"));
                    _Buttons.Add(new Flow_Buttons()
                    {
                        BtnCode = btn.BtnCode,
                        CNName = btn.CNName,
                        ENName = btn.ENName,
                        Icon = btn.Icon,
                        StepId = node.Id,
                        Style = btn.Style,
                        ToolTips = btn.ToolTips,
                        VersionId = version.VersionId,
                        Sequence = i,
                        Creator = Global.EmpId,
                        Creatdate = DateTime.Now,
                        Modifier = Global.EmpId,
                        Modifdate = DateTime.Now,
                    });
                });
                if (node.Form != null && node.Form.Areas.Any())
                {
                    SettingArea(_Areas, _Fields, node.Form.Areas, node, version);
                    node.Form = null;
                }
                else
                {
                    Field_Form_Version_Put _Put = Field_Form_Version_Puts(version.FormVersionId.Value, oldFlowVersionId, node.Id);
                    if (_Put != null)
                    {
                        SettingArea(_Areas, _Fields, _Put.Areas, node, version);
                    }
                }
            });
        }
        /// <summary>
        /// 组装节点字段区域
        /// </summary>
        /// <param name="_Areas"></param>
        /// <param name="_Fields"></param>
        /// <param name="Areas"></param>
        private void SettingArea(List<Flow_Step_Field_Area> _Areas, List<Flow_Step_Area_Fields> _Fields, List<Field_Form_Area_Put> _Area_Puts, NodeInfo node, FlowVersion version)
        {
            _Area_Puts.ForEach(area =>
            {
                long AreaId = longCommon.GenerateRandomCode(6);
                _Areas.Add(new Flow_Step_Field_Area()
                {
                    AutoId = AreaId,
                    AreaCode = area.AreaCode,
                    CNName = area.AreaCNName,
                    ENName = area.AreaENName,
                    MainTable = area.MainTable,
                    StepId = node.Id,
                    TableCode = area.TableCode,
                    VersionId = version.VersionId,
                    Creator = Global.EmpId,
                    Creatdate = DateTime.Now,
                    Modifier = Global.EmpId,
                    Modifdate = DateTime.Now,
                    Show = area.Show,
                    Editor = area.Editor,
                });
                area.Fields.ForEach(field =>
                {
                    field.AreaId = AreaId;
                    _Fields.Add(new Flow_Step_Area_Fields()
                    {
                        AreaId = AreaId,
                        AutoId = longCommon.GenerateRandomCode(8),
                        Editor = field.Editor,
                        FieldCode = field.FieldCode,
                        FieldKey = field.FieldKey,
                        Required = field.Required,
                        Show = field.Show,
                        Creator = Global.EmpId,
                        Creatdate = DateTime.Now,
                        Modifier = Global.EmpId,
                        Modifdate = DateTime.Now
                    });
                });
            });
        }

        /// <summary>
        /// 组装连线数据
        /// </summary>
        /// <param name="LineList"></param>
        /// <param name="_Lines"></param>
        /// <param name="version"></param>
        private void SettingLineInfos(List<LineInfo> LineList, List<Flow_Lines> _Lines, FlowVersion version)
        {
            double i = 0;
            ///连线信息
            LineList.ForEach(line =>
           {
               Flow_Lines _Line = new Flow_Lines();
               _Line.LinkId = line.Id;
               _Line.VersionId = version.VersionId;
               _Line.Creatdate = DateTime.Now;
               _Line.Creator = Global.EmpId;
               _Line.Modifdate = DateTime.Now;
               _Line.Modifier = Global.EmpId;
               _Line.EndId = line.EndId;
               _Line.StartId = line.StartId;
               _Line.StartAt = JsonEvent.ToJsonString(line.StartAt);
               _Line.EndAt = JsonEvent.ToJsonString(line.EndAt);
               if (line.Meta != null)
               {
                   _Line.CNName = line.Meta.CNName;
                   _Line.ENName = line.Meta.ENName;
                   _Line.BusinessCondition = line.Meta.BusinessCondition;
                   _Line.OrgCondtion = line.Meta.OrgCondtion;
                   _Line.PersonCondtion = line.Meta.PersonCondtion;
                   _Line.PostionCondtion = line.Meta.PostionCondtion;
                   _Line.BranchTag = line.Meta.BranchTag;
               }
               else
               {
                   _Line.CNName = "节点连线";
                   _Line.ENName = "Node connection";
                   line.Meta = new Flow_Lines_OutPut();
               }

               i = double.Parse((i + 1).ToString("f2"));
               _Line.Sequence = i;
               _Lines.Add(_Line);
               line.Meta.VersionId = version.VersionId;
               line.Meta.EndId = line.EndId;
               line.Meta.StartId = line.StartId;
               line.Meta.LinkId = line.Id;
           });
        }

        /// <summary>
        /// 组装展示节点信息
        /// </summary>
        private void ViewStepInfos(List<Flow_NodeInfos> NodeInfos, List<NodeInfo> Nodes, List<Flow_Buttons_Put> _Buttons)
        {
            NodeInfos.ForEach(node =>
            {
                Nodes.Add(new NodeInfo()
                {
                    Coordinate = JsonEvent.ToEntity<decimal[]>(node.Coordinate),
                    Width = node.Width,
                    Height = node.Height,
                    Id = node.StepId,
                    Meta = new Flow_Step_Info()
                    {
                        StepId = node.StepId,
                        BackOpinion = node.BackOpinion,
                        CCMessage = node.CCMessage,
                        CNName = node.CNName,
                        Coordinate = node.Coordinate,
                        EndOpinion = node.EndOpinion,
                        ENName = node.ENName,
                        HandlerMessage = node.HandlerMessage,
                        Height = node.Height,
                        Note = node.Note,
                        Opinion = node.Opinion,
                        Prop = node.Prop,
                        Required = node.Required,
                        ShowHistory = node.ShowHistory,
                        ShowOpinion = node.ShowOpinion,
                        ShowQuickField = node.ShowQuickField,
                        VersionId = node.VersionId,
                        Width = node.Width,
                        Groups = node.Groups,
                        StartGroup = node.StartGroup,
                        BackScale = node.BackScale,
                        ApprovalScale = node.ApprovalScale,
                        BackStrategy = node.BackStrategy,
                        BranchStrategy = node.BranchStrategy,
                        EndScale = node.EndScale,
                        EndStrategy = node.EndStrategy,
                        BranchTag=node.BranchTag,
                        Line=node.Line,
                    },
                    Strategy = new Flow_Step_Strategy()
                    {
                        AppoverRange = node.AppoverRange,
                        ApprovalStrategy = node.ApprovalStrategy,
                        ApproveParttimePostion = node.ApproveParttimePostion,
                        ApproverType = node.ApproverType,
                        CCApproveParttimePostion = node.CCApproveParttimePostion,
                        CCFormField = node.CCFormField,
                        CCFormFieldApproveType = node.CCFormFieldApproveType,
                        CCOrgLeve = node.CCOrgLeve,
                        CCSendParttimePostion = node.SendParttimePostion,
                        OrgLeve = node.OrgLeve,
                        SendParttimePostion = node.SendParttimePostion,
                        CCStrategyType = node.CCStrategyType,
                        CCType = node.CCType,
                        CCUser = node.CCUser,
                        DefaultApprover = node.DefaultApprover,
                        FormField = node.FormField,
                        FormFieldApproveType = node.FormFieldApproveType,
                        IsAutoSelectStep = node.IsAutoSelectStep,
                        SkipStrategy = node.SkipStrategy,
                        StepId = node.StepId,
                        StrategyType = node.StrategyType,
                        NodeApprovalScale = node.NodeApprovalScale,
                        NodeBackScale = node.NodeBackScale,
                        NodeBackStrategy = node.NodeBackStrategy, 
                    },
                    Btns = _Buttons.Where(o => o.StepId == node.StepId && o.VersionId == node.VersionId).ToList(),
                    Form = null
                });
            });
        }

        /// <summary>
        /// 组装展示连线信息
        /// </summary>
        /// <param name="lines"></param>
        /// <param name="lineInfos"></param>
        private void ViewLinesInfos(List<Flow_Lines_OutPut> Lines, List<LineInfo> LineInfos)
        {
            Lines.ForEach(line =>
            {
                LineInfos.Add(new LineInfo()
                {
                    EndAt = JsonEvent.ToEntity<int[]>(line.EndAt),
                    EndId = line.EndId,
                    Id = line.LinkId,
                    StartAt = JsonEvent.ToEntity<int[]>(line.StartAt),
                    StartId = line.StartId,
                    Meta = line,
                });
            });
        }

        /// <summary>
        /// 组装字段
        /// </summary>
        private Field_Form_Area_Fields_Put BuilderField(dynamic ext, List<Field_Form_VersionExt> fields, Field_Form_Area_Put area)
        {
            if (ext.FieldId <= 0) return null;
            Field_Form_Area_Fields_Put field = new Field_Form_Area_Fields_Put()
            {
                AreaId = ext.AreaId,
                AutoId = ext.FieldId,
                CNName = ext.CNName,
                Editor = ext.Editor,
                ENName = ext.ENName,
                FieldCode = ext.FieldCode,
                FieldKey = ext.FieldKey,
                Required = ext.Required,
                Sequence = ext.Sequence,
                Show = ext.Show,
                Usable = ext.FieldUsable && area.Usable,
                ControlStype = ext.ControlStype,
                ControlSouces = ext.ControlSouces,
                Selected = ext.Selected
            };
            return field;
        }

        /// <summary>
        /// 组装区域
        /// </summary>
        /// <param name="ext"></param>
        /// <returns></returns>
        private Field_Form_Area_Put BuilderArea(dynamic ext, List<Field_Form_VersionExt> fields, List<Form_TableInfoExt> fromtables, List<TableFields> tables)
        {
            Field_Form_Area_Put area = new Field_Form_Area_Put()
            {
                AreaCNName = ext.AreaCNName,
                AreaENName = ext.AreaENName,
                AreaCode = ext.AreaCode,
                AreaSeq = ext.AreaSeq,
                AreaId = ext.AreaId,
                TableCode = ext.TableCode,
                MainTable = ext.MainTable,
                Usable = ext.AreaUsable,
                Editor = ext.AreaEditor,
                Show = ext.AreaShow
            };
            var baseTab = fromtables.FirstOrDefault(o => o.TableCode == ext.MainTable);
            ///主表都被删除和禁用了则子表也要禁用和删除
            if (baseTab != null)
            {
                if (area.Usable)
                {
                    area.Usable = area.Usable && baseTab.Enable && !baseTab.HaveDelete;
                }
                area.MainTableENName = baseTab.TableENName;
                area.MainTableName = baseTab.TableName;
            }
            ///判断子表是否被禁用或则删除
            if (area.Usable)
            {
                var baseTableInfo = fromtables.FirstOrDefault(o => o.TableCode == ext.TableCode);
                if (baseTableInfo != null)
                    area.Usable = area.Usable && baseTableInfo.Enable && !baseTableInfo.HaveDelete;
            }
            var exs = fields.Where(o => o.AreaId == area.AreaId).Join(tables, a => a.FieldCode, b => b.FieldCode, (a, b) => { a.FieldUsable = area.Usable && b.FieldEnable && !b.FieldHaveDelete; return a; }).ToList();
            var exe = fields.Except(exs).Where(o => o.AreaId == area.AreaId);
            if (exe.Any())
            {
                exs.AddRange(exe);
            }
            exs.GroupBy(field => new
            {
                field.AreaId,
                field.FieldId,
                field.CNName,
                field.ENName,
                field.Editor,
                field.FieldCode,
                field.FieldKey,
                field.Sequence,
                field.Show,
                field.FieldUsable,
                field.Required,
                field.ControlStype,
                field.ControlSouces,
                field.Selected
            }).ToList().OrderBy(o => o.Key.Sequence).Where(o => o.Key.FieldId > 0).ToList().ForEach(
                fd => area.Fields.Add(BuilderField(fd.Key, fd.ToList(), area)));
            return area;
        }

        private void AddFlowInfoRedis(Flow_OutPut flowInfo, FlowVersion version, FlowInfo model)
        {
            #region 流程属性缓存存取
            flowInfo.FlowProperty.VersionId = version.VersionId.Value;
            flowInfo.FlowProperty.VersionNo = version.VersionNo.Value;
            flowInfo.FlowProperty.CurrentVersion = version.VersionId.Value;
            flowInfo.FlowProperty.VersionName = version.Name;
            flowInfo.FlowProperty.InstanceManager = model.InstanceManager;
            flowInfo.FlowProperty.Manager = model.Manager;
            flowInfo.FlowProperty.ParallelTask = version.ParallelTask;
            flowInfo.FlowProperty.SumbitPrevStep = version.SumbitPrevStep;
            flowInfo.FlowProperty.FormVersionId = flowInfo.FlowProperty.Form.FirstOrDefault().CurrentVersionId;
            string redisKey = $"{Global.TenementCode.ToUpper()}_{GlobalConst.FLOWINFO_FLAG_REDIS_KEY}_FLOWID_{flowInfo.FlowProperty.FlowId}";
            string redisHashField = $"{redisKey}_FLOWVERSIONID_{flowInfo.FlowProperty.VersionId}";
            RedisClient.HashDelete(redisKey, redisHashField);
            RedisClient.HashSet(redisKey, redisHashField, flowInfo);

            /////连线配置
            //string lineKey = $"{redisHashField}_LINEKEY";
            //RedisClient.HashDelete(redisHashField, lineKey);
            //RedisClient.HashSet(redisHashField, lineKey, flowInfo.LinkList);
            /////节点配置
            //string nodeKey = $"{redisHashField}_NODEKEY";
            //RedisClient.HashDelete(redisHashField, nodeKey);
            //RedisClient.HashSet(redisHashField, nodeKey, flowInfo.NodeList);
            #endregion
        }


        /// <summary>
        /// 组建流程视图
        /// </summary>
        /// <param name="id"></param>
        /// <param name="versionId"></param>
        /// <returns></returns>
        public Flow_OutPut BuildViewFlowInfo(long id, long versionId)
        {
            ExtFlowInfo _OutPut = business.GetFlowSettingInfo(id, versionId);
            if (_OutPut is null) return new Flow_OutPut();
            if (!string.IsNullOrEmpty(_OutPut.Manager))
            {
                List<Employee> employees = business.GetBasicListByQuery<Employee>("empid", _OutPut.Manager);
                employees.ForEach(o =>
                {
                    _OutPut.Managers.Add(new PersonInfo_OutPut() { CNName = o.CNName, ENName = o.ENName, EmpCode = o.EmpCode, EmpId = o.EmpId });
                });

            }
            if (!string.IsNullOrEmpty(_OutPut.InstanceManager))
            {
                List<Employee> employees = business.GetBasicListByQuery<Employee>("empid", _OutPut.InstanceManager);
                employees.ForEach(o =>
                {
                    _OutPut.InstanceManagers.Add(new PersonInfo_OutPut() { CNName = o.CNName, ENName = o.ENName, EmpCode = o.EmpCode, EmpId = o.EmpId });
                });
            }
            if (_OutPut.FlowId > 0)
            {
                _OutPut.Form = business.Forms(_OutPut.FormId.Value);
            }
            ///节点信息
            List<Flow_NodeInfos> NodeInfos = business.StepInfo(_OutPut.FlowId);
            List<Flow_Buttons_Put> _Buttons = business.Buttons(_OutPut.FlowId);
            List<NodeInfo> Nodes = new List<NodeInfo>();
            this.ViewStepInfos(NodeInfos, Nodes, _Buttons);

            ////连线信息
            List<Flow_Lines_OutPut> _Lines = business.FlowLines(_OutPut.FlowId);
            List<LineInfo> lineInfos = new List<LineInfo>();
            this.ViewLinesInfos(_Lines, lineInfos);
            return new Flow_OutPut() { FlowProperty = _OutPut, NodeList = Nodes, LinkList = lineInfos };
        }

        public IResponseMessage EndFlowStepFields(long formVersionId, long flowVersionId, List<Flow_Complated_Info_OutPut> complateField)
        {
            var fields = formBusiness.GetVersionFields(formVersionId);
            List<Field_Form_VersionExt> _Version_Puts = fields.Where(o => o.AreaCode.ToLower() != "form_apply_employeebasicinfo").ToList();
            List<Flow_Complated_Info_OutPut> Flow_Step_VersionExts = business.EndStepFields(flowVersionId).ToList();
            List<Flow_Complated_Info_OutPut> _Puts = complateField;
            List<Flow_Complated_Fields> resutls = new List<Flow_Complated_Fields>();
            if (Flow_Step_VersionExts.Any())
            {
                if (complateField.Any())
                {
                    Flow_Step_VersionExts = Flow_Step_VersionExts.Where(o => !_Puts.Any(p => p.FieldCode == o.FieldCode && p.TableCode == o.TableCode)).ToList();
                    Flow_Step_VersionExts.ForEach(o => o.Choose = false);
                }
                _Version_Puts = _Version_Puts.Where(o => !_Puts.Any(p => p.FieldCode == o.FieldCode && p.TableCode == o.TableCode)).ToList();
                _Puts.AddRange(_Version_Puts.Join(Flow_Step_VersionExts, a => new { a.TableCode, a.FieldCode }, b => new { b.TableCode, b.FieldCode }, (a, b) =>
               {
                   b.CNName = a.AreaCNName;
                   b.ENName = a.AreaENName;
                   b.FieldCNName = a.CNName;
                   b.Choose = complateField.Any() ? b.Choose : true;
                   b.FieldENName = a.ENName;
                   return b;
               }).ToList());
                ///表单上新加的字段，节点上没有字段
                _Version_Puts.Where(o => !_Puts.Any(p => p.TableCode == o.TableCode && p.FieldCode == o.FieldCode)).ToList().ForEach(fd =>
                {
                    GenerateComplateInfo(_Puts, flowVersionId, fd);
                });

                ///新加入的字段且该区域不在节点上
                var resluts = Flow_Step_VersionExts.Where(o => !_Version_Puts.Any(p => p.TableCode == o.TableCode && p.FieldCode == o.FieldCode)).ToList();
                if (resluts.Any()) _Puts.AddRange(resluts);
            }
            else
            {
                _Version_Puts = _Version_Puts.Where(o => !_Puts.Any(p => p.FieldCode == o.FieldCode && p.TableCode == o.TableCode)).ToList();
                ///表单上新加的字段，节点上没有字段
                _Version_Puts.ForEach(fd =>
                {
                    GenerateComplateInfo(_Puts, flowVersionId, fd);
                });
            };
            if (_Puts.Any())
            {
                string tablecode = $"'{string.Join("','", _Puts.Select(o => o.TableCode).Distinct())}'";
                var fromtables = formBusiness.GetTableByCode(tablecode);
                var tables = formBusiness.GetTableFields(tablecode);
                var joinTables = fromtables.Join(tables, a => a.TableCode, b => b.TableCode, (a, b) => { a.TableName = b.TableName; a.TableENName = b.TableENName; return a; }).ToList();
                var results = _Puts.Join(tables, a => a.TableCode, b => b.TableCode, (a, b) =>
                {
                    a.Enable = b.TableEnable && !b.TableHaveDelete;
                    a.FieldEnable = b.TableEnable && !b.TableHaveDelete && b.FieldEnable && !b.FieldHaveDelete;
                    return a;
                }).Distinct().ToList();
                foreach (var item in results.GroupBy(o => new { o.TableCode, o.FlowTableCode, o.Enable, o.CNName, o.ENName }))
                {
                    resutls.Add(new Flow_Complated_Fields()
                    {
                        CNName = item.Key.CNName,
                        ENName = item.Key.ENName,
                        FlowTableCode = item.Key.FlowTableCode,
                        TableCode = item.Key.TableCode,
                        Fields = item.ToList(),
                        Enable = item.Key.Enable
                    });
                }
            }
            return resutls.OnSucceed();
        }
        #endregion

        #region private
        /// <summary>
        /// 生成完成回写字段树
        /// </summary>
        /// <param name="_Puts"></param>
        /// <param name="flowVersionId"></param>
        /// <param name="fd"></param>
        private void GenerateComplateInfo(List<Flow_Complated_Info_OutPut> _Puts, long flowVersionId, Field_Form_VersionExt fd)
        {
            _Puts.Add(new Flow_Complated_Info_OutPut()
            {
                CNName = fd.AreaCNName,
                ENName = fd.AreaENName,
                FieldCNName = fd.CNName,
                FieldCode = fd.FieldCode,
                FieldENName = fd.ENName,
                FlowVersionId = flowVersionId,
                TableCode = fd.TableCode,
                FlowTableCode = $"{fd.TableCode}apply",
            });
        }
        #endregion
    }
}
