﻿

using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Tauren.Flow.DLL.Base;
using Tauren.Flow.DLL.Field_FormDLL;
using Tauren.Flow.DLL.FlowCenterDLL;
using Tauren.Flow.DLL.FlowEngine;
using Tauren.Flow.DLL.FlowInfoDLL;
using Tauren.Flow.DLL.Resouces;
using Tauren.Flow.Entity.Config;
using Tauren.Flow.Entity.Const;
using Tauren.Flow.Entity.Enum;
using Tauren.Flow.Entity.Global;
using Tauren.Flow.Entity.Input;
using Tauren.Flow.Entity.Model;
using Tauren.Flow.Entity.Output;
using Tauren.Flow.Infrastructure.Common;
using Tauren.Flow.Infrastructure.Globals;
namespace Tauren.Flow.Service.FlowInfoBLL
{
    /// <summary>
    /// 流程基本信息
    /// </summary>
    internal class FlowInstanceService : Base.BaseService, IFlowInstanceService
    { 
        public IField_FormBusiness formBusiness { get; set; }
        private IFlowInstanceBusiness instanceBusiness { get; set; }
        public IFlowCenterBusiness flowCenterBusiness { get; set; }
        public FlowInstanceService(IFlowInstanceBusiness _instanceBusiness, IConnectionBLLBase connectionBLL, IMenuResourcesBusiness resourcesBusiness) : base(_instanceBusiness, connectionBLL, resourcesBusiness)
        { 
            this.ResourcesInfo = this.GetResources(GlobalConst.RESOURCES_FLAG_MODULE_FLOW_INSTANCE_KEY);
            this.JudePrmission(GlobalConst.RESOURCES_FLAG_MODULE_FLOW_INSTANCE_KEY.ToLower());
            instanceBusiness = _instanceBusiness;
        }

         
        /// <summary>
        /// 流程树
        /// </summary>
        /// <returns></returns>
        public IResponseMessage FlowTree()
        {
           
            List<GlobalTree> trees = new List<GlobalTree>();
            if (!HavePermission) return ErrorType.BUSINESS_WITHOUT_PERMISSION.Failure(this.CommonText(ErrorType.BUSINESS_WITHOUT_PERMISSION));
            QueryModel query = null;
            if (!Global.IsAdmin)
            {
                query.paramters.Add(new Entity.Output.Options() { Label = "manager", Value = Global.EmpId });
            }
            List<FlowInstanceTree> instanceTrees = instanceBusiness.GetFlowTree(query).OrderByDescending(o => o.FSeq).ToList();
            var groups = instanceTrees.GroupBy(o => o.FlowId);
            foreach (var item in groups)
            {
                var parent = item.FirstOrDefault(o => o.CurVer == o.VerNo);
                GlobalTree tree = new GlobalTree() { CNName = parent.CNName, ENName = parent.ENName, Id = parent.VerId, Hyplink = parent.View_Instance, ParentId = parent.FlowId, Label = Global.IsChinese ? parent.CNName : parent.ENName };
                foreach (var tr in item.OrderBy(o => o.VerNo))
                {
                    tree.Children.Add(new GlobalTree()
                    {
                        CNName = tr.VerName,
                        ENName = tr.VerName,
                        Id = tr.VerId,
                        Hyplink = parent.View_Instance,
                        ParentId = parent.FlowId,
                        Label = tr.VerName
                    });
                }
                trees.Add(tree);

            }
            return trees.OnSucceed();
        }

        public IResponseMessage Instance(QueryModel queryModel)
        {
            if (!HavePermission && queryModel.CheckPermission) return ErrorType.BUSINESS_WITHOUT_PERMISSION.Failure(this.CommonText(ErrorType.BUSINESS_WITHOUT_PERMISSION));
            TableListModel<PageFlowInstanceTable> tableListModel = new TableListModel<PageFlowInstanceTable>();
            List<TableListHeaderModel> Headers = BuildTableListHeaderModel();
            long Total = 0;
            List<PageFlowInstanceTable> Data = instanceBusiness.Instances(queryModel, out Total);
            Dictionary<long, FlowInfo> flowNames = FlowDictionarys();
            Dictionary<string, string> stepNames = flowCenterBusiness.GetStepNames(Data.Select(o => o.StepId).ToArray());
            if (Data.Any())
            {
                Data.ForEach(o =>
                { 
                    o.Operations = new List<SystemButton>();
                    dynamic Args = new { flowId = o.FlowId, flowVersionId = o.FlowVersionId, formId = o.FormId, formVersionId = o.FormVersionId, taskId = o.TaskId, groupId = o.GroupId };
                    BuliderButtons(o.Operations, o.GroupId + "", o.TaskId, ButtomType.detaile, "", Args);
                    o.ProcessType = ApprovalType.view;
                    BuildTaskData(o, stepNames, flowNames);
                });
            }
            tableListModel.Body = Data;
            tableListModel.Total = Total;
            tableListModel.Header = Headers;
            return tableListModel.Succeed();
        }
        #region private
        /// <summary>
        /// 生成完成回写字段树
        /// </summary>
        /// <param name="_Puts"></param>
        /// <param name="flowVersionId"></param>
        /// <param name="fd"></param>
        private void GenerateComplateInfo(List<Flow_Complated_Info_OutPut> _Puts, long flowVersionId, Field_Form_VersionExt fd)
        {
            _Puts.Add(new Flow_Complated_Info_OutPut()
            {
                CNName = fd.AreaCNName,
                ENName = fd.AreaENName,
                FieldCNName = fd.CNName,
                FieldCode = fd.FieldCode,
                FieldENName = fd.ENName,
                FlowVersionId = flowVersionId,
                TableCode = fd.TableCode,
                FlowTableCode = $"{fd.TableCode}apply",
            });
        }
        private List<TableListHeaderModel> BuildTableListHeaderModel()
        {
            List<TableListHeaderModel> headerModels = new List<TableListHeaderModel>();
            headerModels.Add(new TableListHeaderModel() { Key = "rowNo", Name = CommonText("rowNo"), Width = 160, Align = "center", Fixed = true });
            headerModels.Add(new TableListHeaderModel() { Key = "instanceId", Name = Resources("flowinstanceid"), Width = 160, Align = "center", Fixed = true });
            headerModels.Add(new TableListHeaderModel() { Key = "dic_Type", Name = CommonText("flowtasktype"), Width = 120, Align = "center", Fixed = true, IsTag = true, SorTable = SortType.custom.ToString() });
            headerModels.Add(new TableListHeaderModel() { Key = "taskId", Name = Resources("taskid"), Width = 70, Hide = true, Type = "primary_key", Align = "center", SorTable = SortType.custom.ToString(), Fixed = true });
            headerModels.Add(new TableListHeaderModel() { Key = "title", Name = Resources("thtitle"), Align = "left", IsOperation = true, Fixed = true, Width = 260, SorTable = SortType.custom.ToString(), Hyperlink = true });
            headerModels.Add(new TableListHeaderModel() { Key = "dic_FlowId", Name = Resources("thflowname"), Align = "left", Fixed = true, Width = 260, SorTable = SortType.custom.ToString(), });
            headerModels.Add(new TableListHeaderModel() { Key = "dic_StepId", Name = Resources("flowcurrentstep"), Width = 260, Align = "left", SorTable = SortType.custom.ToString() });
            headerModels.Add(new TableListHeaderModel() { Key = "dic_ApplyEmpId", Name = Resources("flowapplyempid"), Width = 160, Align = "center", SorTable = SortType.custom.ToString() });
            headerModels.Add(new TableListHeaderModel() { Key = "dic_InitiatorEmpId", Name = Resources("flowinitiatorempid"), Width = 160, Align = "center", SorTable = SortType.custom.ToString() });
            headerModels.Add(new TableListHeaderModel() { Key = "dic_Sender", Name = Resources("flowsender"), Width = 160, Align = "center", SorTable = SortType.custom.ToString() });
            headerModels.Add(new TableListHeaderModel() { Key = "dic_Reviewer", Name = Resources("flowreviewer"), Width = 160, Align = "center", SorTable = SortType.custom.ToString() });
            headerModels.Add(new TableListHeaderModel() { Key = "dic_ApplyDate", Name = Resources("flowapplydate"), Width = 160, Align = "center", SorTable = SortType.custom.ToString() });
            headerModels.Add(new TableListHeaderModel() { Key = "dic_InitiatorDate", Name = Resources("flowinitiatordate"), Width = 160, Align = "center", SorTable = SortType.custom.ToString() });
            headerModels.Add(new TableListHeaderModel() { Key = "dic_ReceivingTime", Name = Resources("flowreceivingtime"), Width = 160, Align = "center", SorTable = SortType.custom.ToString() });
            headerModels.Add(new TableListHeaderModel() { Key = "dic_ComplatedTime", Name = Resources("flowcomplatedtime"), Width = 160, Align = "center", SorTable = SortType.custom.ToString() });
            headerModels.Add(new TableListHeaderModel() { Key = "dic_Status", Name = Resources("taskstatus"), Width = 160, Align = "center", IsTag = true, SorTable = SortType.custom.ToString() });
            headerModels.Add(new TableListHeaderModel() { Key = "dic_FlowStatus", Name = Resources("flowstatus"), Width = 160, Align = "center", IsTag = true, SorTable = SortType.custom.ToString() });
            return headerModels;
        }

        private void BuildTaskData(PageFlowInstanceTable o, Dictionary<string, string> stepNames, Dictionary<long, FlowInfo> flowNames)
        {
            o.Dic_FlowId = o.FlowId == null ? "" : flowNames.ContainsKey(o.FlowId.Value) ? Global.IsChinese ? flowNames[o.FlowId.Value].CNName : flowNames[o.FlowId.Value].ENName : "";
            string key = $"{o.FlowVersionId}_{o.StepId}";
            o.Dic_StepId = string.IsNullOrWhiteSpace(o.StepId) ? "" : stepNames.ContainsKey(key) ? stepNames[key] : "";
            o.Dic_Reviewer = o.Reviewer == null ? "" : EmpDictionary.ContainsKey(o.Reviewer.Value) ? EmpDictionary[o.Reviewer.Value] : "";
            o.Dic_ApplyDate = o.ApplyDate == null ? "" : o.ApplyDate.Value.ToString(GlobalConst.DATE_FORMAT_SECONDS);
            o.Dic_InitiatorDate = o.InitiatorDate == null ? "" : o.InitiatorDate.Value.ToString(GlobalConst.DATE_FORMAT_SECONDS);
            o.Dic_ReceivingTime = o.ReceivingTime == null ? "" : o.ReceivingTime.Value.ToString(GlobalConst.DATE_FORMAT_SECONDS);
            o.Dic_ComplatedTime = o.ComplatedTime == null ? "" : o.ComplatedTime.Value.ToString(GlobalConst.DATE_FORMAT_SECONDS);
            o.Dic_ProcessTime = o.ProcessTime == null ? "" : o.ProcessTime.Value.ToString(GlobalConst.DATE_FORMAT_SECONDS);
            o.Dic_ApplyEmpId = o.ApplyEmpId == null ? "" : EmpDictionary.ContainsKey(o.ApplyEmpId.Value) ? EmpDictionary[o.ApplyEmpId.Value] : "";
            o.Dic_InitiatorEmpId = o.InitiatorEmpId == null ? "" : EmpDictionary.ContainsKey(o.InitiatorEmpId.Value) ? EmpDictionary[o.InitiatorEmpId.Value] : "";
            o.Dic_Sender = o.Sender == null ? "" : EmpDictionary.ContainsKey(o.Sender.Value) ? EmpDictionary[o.Sender.Value] : "";
            o.Dic_Status = o.Status == null ? "" : Resources($"taskstaus{((FlowTaskStatus)o.Status.Value).ToString()}");
            o.Dic_FlowStatus = o.FlowStatus == null ? "" : Resources($"flowstatus{((FlowInstanceStatus)o.FlowStatus.Value).ToString()}");
            o.Dic_Type = o.Type == null ? "" : Resources($"prcoesstype{((FlowTaskType)o.Type.Value).ToString()}");
            o.Row_Style = "";
            o.Enable = true;
        }
        #endregion
    }
}
