﻿using System;
using System.Collections.Generic;
using System.Text;
using Tauren.Flow.Entity.Config;
using Tauren.Flow.Entity.Output;

namespace Tauren.Flow.Service.FlowInfoBLL
{
    /// <summary>
    /// 流程基本信息
    /// </summary>
    public interface IFlowInfoService
    {
        /// <summary>
        /// 分页获取信息
        /// </summary>
        /// <param name="queryModel"></param>
        /// <param name="Total"></param>
        /// <returns></returns>
        IResponseMessage Pages(Entity.Global.QueryModel queryModel);
        /// <summary>
        /// 获取单个数据信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        IResponseMessage FirstOrDefault(long id, long versionId);

        /// <summary>
        /// 新增数据
        /// </summary>
        /// <returns></returns>
        IResponseMessage Add(Entity.Input.FlowInfo_InPut data);

        /// <summary>
        /// 修改数据
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        IResponseMessage Edit(Entity.Input.FlowInfo_InPut data);

        /// <summary>
        /// 主键数据
        /// </summary>
        /// <param name="AutoID"></param>
        /// <returns></returns>
        IResponseMessage Delete(long AutoID);

        /// <summary>
        /// 卸载流程数据
        /// </summary>
        /// <param name="AutoID"></param>
        /// <returns></returns>
        IResponseMessage Uninstall(long AutoID);

        /// <summary>
        /// 保存数据
        /// </summary>
        /// <param name="flowInfo"></param>
        /// <returns></returns>
        IResponseMessage SaveFlow(Flow_OutPut flowInfo);

        /// <summary>
        /// /获取表单最新的字段
        /// </summary>
        /// <param name="FormVersionId">表单版本ID</param>
        /// <param name="FlowVersionId">流程版本ID</param>
        /// <param name="StepId">步骤ID</param>
        /// <returns></returns>
        IResponseMessage FlowStepFields(long FormVersionId, long FlowVersionId, string StepId);

        /// <summary>
        /// 获取
        /// </summary>
        /// <param name="empId"></param>
        /// <returns></returns>
        IResponseMessage GetFlowEmployees(string empId);

        /// <summary>
        /// 流程人员
        /// </summary>
        /// <param name="flowInfo"></param>
        /// <returns></returns>
        IResponseMessage PublishFlow(Flow_OutPut flowInfo);

        /// <summary>
        /// 流程人员范围
        /// </summary>
        /// <param name="positionId"></param>
        /// <returns></returns>
        IResponseMessage GetFlowPositions(string positionId);
        /// <summary>
        /// 根据流程ID获取快捷字段
        /// </summary>
        /// <param name="flowId"></param>
        /// <param name="versionId"></param>
        /// <returns></returns>
        IResponseMessage QuickDisplayField(long flowId, long versionId, string type);
        IResponseMessage SaveDisplayField(string type, List<Entity.Model.Flow_Display_Field> Models, long FlowId, long versionId);
        /// <summary>
        /// 组建流程视图
        /// </summary>
        /// <param name="id"></param>
        /// <param name="versionId"></param>
        /// <returns></returns>
        Flow_OutPut BuildViewFlowInfo(long id, long versionId);

        /// <summary>
        /// 获取最后节点上要配置回写的字段
        /// </summary>
        /// <param name="formVersionId"></param>
        /// <param name="flowVersionId"></param>
        /// <returns></returns>
        IResponseMessage EndFlowStepFields(long formVersionId, long flowVersionId, List<Flow_Complated_Info_OutPut> complateField);
    }
}
