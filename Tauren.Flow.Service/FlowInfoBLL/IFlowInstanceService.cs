﻿using System;
using System.Collections.Generic;
using System.Text;
using Tauren.Flow.Entity.Config;
using Tauren.Flow.Entity.Output;

namespace Tauren.Flow.Service.FlowInfoBLL
{
    /// <summary>
    /// 流程基本信息
    /// </summary>
    public interface IFlowInstanceService
    { 
        /// <summary>
        /// 流程树
        /// </summary>
        /// <returns></returns>
        IResponseMessage FlowTree();

        /// <summary>
        /// 获取实例数据
        /// </summary>
        /// <param name="queryModel"></param>
        /// <param name="Total"></param>
        /// <returns></returns>
        IResponseMessage Instance(Entity.Global.QueryModel queryModel);
    }
}
