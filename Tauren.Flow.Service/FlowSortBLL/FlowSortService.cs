﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tauren.Flow.DLL.Base;
using Tauren.Flow.DLL.FlowSortDLL;
using Tauren.Flow.DLL.Resouces;
using Tauren.Flow.Entity.Config;
using Tauren.Flow.Entity.Const;
using Tauren.Flow.Entity.Enum;
using Tauren.Flow.Entity.Model;
using Tauren.Flow.Infrastructure.Globals;

namespace Tauren.Flow.Service.FlowSortBLL
{
    public class FlowSortService : Base.BaseService, IFlowSortService
    {
        private IFlowSortBusiness business;
        public FlowSortService(IFlowSortBusiness baseDLL, IConnectionBLLBase connectionBLL, IMenuResourcesBusiness resourcesBusiness) : base(baseDLL, connectionBLL, resourcesBusiness)
        {
            this.business = baseDLL;
            this.ResourcesInfo = this.GetResources(GlobalConst.RESOURCES_FLAG_MODULE_FLOWCENTER_KEY);
            this.JudePrmission(GlobalConst.RESOURCES_FLAG_MODULE_FLOWCENTER_KEY.ToLower());
        }

        public IResponseMessage Add(List<WorkFlowInfoSort> sorts)
        {
            if (!HavePermission) return ErrorType.BUSINESS_WITHOUT_PERMISSION.Failure(this.CommonText(ErrorType.BUSINESS_WITHOUT_PERMISSION));
            if (sorts is null || !sorts.Any()) return GlobalErrorType.GLOBAL_NOT_FOUND_INPUT_DATA.Failure(this.CommonText(GlobalErrorType.GLOBAL_NOT_FOUND_INPUT_DATA));
            List<FlowSort> flowSorts = new List<FlowSort>();
            int i = 1;
            sorts.ForEach(item =>
            {
                flowSorts.Add(new FlowSort()
                {
                    FlowId = item.FlowId,
                    EmpId = Global.EmpId,
                    Sequence = i,
                    Creatdate = DateTime.Now,
                    Creator = Global.EmpId,
                    Modifdate = DateTime.Now,
                    Modifier = Global.EmpId
                });
                i++;
            });
            if (business.AddFlowSort(flowSorts))
                return "".OnSucceed(this.CommonText(GlobalErrorType.GLOBAL_SAVE_SUCCESSINFO.ToString()));
            else return GlobalErrorType.GLOBAL_SAVE_FAILURE.Failure(this.CommonText(GlobalErrorType.GLOBAL_SAVE_FAILURE));
        }

        public IResponseMessage GetFlows()
        {
            List<WorkFlowInfoSort> flowSorts = new List<WorkFlowInfoSort>();
            if (HavePermission)
            {
                flowSorts = business.GetFlows();
                var sort = flowSorts.Where(o => o.Sequence > 0);
                double seq = sort.Any() ? sort.Max(o => o.Sequence) + 1 : 0;
                flowSorts.ForEach(o =>
                {
                    if (o.Sequence == 0)
                    {
                        o.Sequence = seq;
                        seq++;
                    }
                });
            }
            return flowSorts.OrderBy(o => o.Sequence).OnSucceed();
        }
    }
}
