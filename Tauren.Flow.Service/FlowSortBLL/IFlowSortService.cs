﻿using System;
using System.Collections.Generic;
using System.Text;
using Tauren.Flow.Entity.Config;
using Tauren.Flow.Entity.Model;

namespace Tauren.Flow.Service.FlowSortBLL
{
    public interface IFlowSortService
    {
        /// <summary>
        /// 获该员工所有流程
        /// </summary>
        /// <returns></returns>
        IResponseMessage GetFlows();

        /// <summary>
        /// 添加排序
        /// </summary>
        /// <param name="_WorkBenches"></param>
        /// <returns></returns>
        IResponseMessage Add(List<WorkFlowInfoSort> sorts);
    }
}
