﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tauren.Flow.DLL.FlowEngine;
using Tauren.Flow.DLL.Resouces;
using Tauren.Flow.Entity.Const;
using Tauren.Flow.Entity.Enum;
using Tauren.Flow.Entity.Global;
using Tauren.Flow.Entity.Model;
using Tauren.Flow.Entity.Output;
using Tauren.Flow.Infrastructure.Globals;

namespace Tauren.Flow.Service.Flow_SrategyBLL
{
    /// <summary>
    /// 表单人员策略
    /// </summary>
    public class Flow_Engine_Form_OrganizationSrategyService : Base.BaseService
    {
        private Dictionary<Strategy_Form_Organization_Field, Func<SingeNextNodeInfoRequestParameter, Result<NextNodeInfoOutPut>>> Engine_Organization_Srategy = new Dictionary<Strategy_Form_Organization_Field, Func<SingeNextNodeInfoRequestParameter, Result<NextNodeInfoOutPut>>>();

        private IFlow_SrategyDLL flowEngineDLL;
        public Flow_Engine_Form_OrganizationSrategyService(IFlow_SrategyDLL _flowEngineDLL, IConnectionBLLBase connectionBLL, IMenuResourcesBusiness resourcesBusiness) : base(_flowEngineDLL, connectionBLL, resourcesBusiness)
        {
            this.ResourcesInfo = this.GetResources(GlobalConst.RESOURCES_FLAG_MODULE_FLOWCENTER_KEY);
            this.InitFunction();
            this.flowEngineDLL = _flowEngineDLL;
        }

        /// <summary>
        /// 根据表单人员策略获取下一审核信息
        /// </summary>
        /// <param name="requestParameter"></param>
        /// <returns></returns>
        public Result<NextNodeInfoOutPut> LoadNextNodeInfoOnStrategy(SingeNextNodeInfoRequestParameter requestParameter)
        {
            return this.Engine_Organization_Srategy[(Strategy_Form_Organization_Field)requestParameter.NextStep.Strategy.FormFieldApproveType].Invoke(requestParameter);
        }

        /// <summary>
        /// 初始化策略模式
        /// </summary>
        private void InitFunction()
        {
            Engine_Organization_Srategy.Add(Strategy_Form_Organization_Field.Empty, (arg) => { var res = new Result<NextNodeInfoOutPut>(null); res.OnFailure(); return res; });
            Engine_Organization_Srategy.Add(Strategy_Form_Organization_Field.Flow_Form_Field_Current_Department, LoadFlowFormFieldCurrentDepartment);
            Engine_Organization_Srategy.Add(Strategy_Form_Organization_Field.Flow_Form_Field_Current_Department_Director, LoadFlowFormFieldCurrentDepartmentDirector);
            Engine_Organization_Srategy.Add(Strategy_Form_Organization_Field.Flow_Form_Field_Current_Department_Leve_N_Principal, LoadFlowFormFieldCurrentDepartmentLeveNPrincipal);
            Engine_Organization_Srategy.Add(Strategy_Form_Organization_Field.Flow_Form_Field_Current_Department_All_Superior, LoadFlowFormFieldCurrentDepartmentAllSuperior);

        }

        #region 上N级负责人
        /// <summary>
        /// 获取当前部门上N级负责人
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private Result<NextNodeInfoOutPut> LoadFlowFormFieldCurrentDepartmentLeveNPrincipal(SingeNextNodeInfoRequestParameter arg)
        {
            var result = new Result<NextNodeInfoOutPut>(null);
            List<object> ids = GetFormFieldValue(arg);
            if (!ids.Any()) { result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR); return result; }
            List<Vw_Organizationroute> organizationroutes = flowEngineDLL.GetOrgPrincipal(string.Join(",", ids), "organizational");
            var single = organizationroutes.FirstOrDefault(o => o.Id == o.PId);
            if (single != null)
            {
                var parent = organizationroutes.Where(o => o.Level == (single.Level - arg.NextStep.Strategy.OrgLeve));
                if (parent.Any())
                {
                    NextNodeInfoOutPut nodeInfo = BuilderEmpNextNodeInfoOutPut(arg.NextStep);
                    nodeInfo.NextStepApprover = string.Join(",", parent.Select(o => o.EmpId));
                    nodeInfo.NextStepApproverEmpId = parent.Select(o => o.EmpId).ToList();
                    this.BuilderEmp(nodeInfo, arg.NextStep);
                    result.OnSuccess(nodeInfo);
                }
                else result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR);
            }
            else result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR);
            return result;
        }
        #endregion

        #region 所有人
        /// <summary>
        /// 当前部门所有人
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private Result<NextNodeInfoOutPut> LoadFlowFormFieldCurrentDepartmentAllSuperior(SingeNextNodeInfoRequestParameter arg)
        {
            var result = new Result<NextNodeInfoOutPut>(null);
            List<object> ids = GetFormFieldValue(arg);
            if (!ids.Any()) { result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR); return result; }
            var res = flowEngineDLL.GetOrganizationAllUserByOrgid($"'{string.Join("','", ids)}'");
            if (res.Any())
            {
                NextNodeInfoOutPut nodeInfo = BuilderEmpNextNodeInfoOutPut(arg.NextStep);
                nodeInfo.NextStepApprover = string.Join(",", res);
                nodeInfo.NextStepApproverEmpId = res.ToList();
                this.BuilderEmp(nodeInfo, arg.NextStep);
                result.OnSuccess(nodeInfo);
            }
            else result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR);
            return result;
        }

        #endregion

        #region 本级
        /// <summary>
        /// 当前处理人所在部门主管
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private Result<NextNodeInfoOutPut> LoadFlowFormFieldCurrentDepartment(SingeNextNodeInfoRequestParameter arg)
        {
            var result = new Result<NextNodeInfoOutPut>(null);
            List<object> ids = GetFormFieldValue(arg);
            if (!ids.Any()) { result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR); return result; }
            result = BuildApprover(string.Join(",", ids), arg.NextStep);
            return result;
        }
        #endregion

        #region 上级
        /// <summary>
        /// 当前部门上级部门主管
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private Result<NextNodeInfoOutPut> LoadFlowFormFieldCurrentDepartmentDirector(SingeNextNodeInfoRequestParameter arg)
        {
            var result = new Result<NextNodeInfoOutPut>(null);
            List<object> ids = GetFormFieldValue(arg);
            if (!ids.Any()) { result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR); return result; }
            var orgs = baseDLL.GetBasicListByQuery<Organization>("orgid", string.Join(",", ids));
            if (orgs.Any())
            {
                result = BuildApprover(string.Join(",", orgs.Select(o => o.ParentId)), arg.NextStep);
            }
            else result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR);
            return result;
        }

        #endregion
        /// <summary>
        /// 将获取到的人员组装到
        /// </summary>
        /// <param name="orgids"></param>
        /// <param name="NextStep"></param>
        /// <returns></returns>
        private Result<NextNodeInfoOutPut> BuildApprover(string orgids, NodeInfo NextStep)
        {
            var result = new Result<NextNodeInfoOutPut>(null);
            if (string.IsNullOrEmpty(orgids))
                result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR);
            else
            {
                var res = flowEngineDLL.GetOrgSupervisorPositionUsers(orgids, (SendParttimePostion)NextStep.Strategy.SendParttimePostion);
                if (res.Any())
                {
                    NextNodeInfoOutPut nodeInfo = BuilderEmpNextNodeInfoOutPut(NextStep);
                    nodeInfo.NextStepApprover = string.Join(",", res);
                    nodeInfo.NextStepApproverEmpId = res.ToList();
                    this.BuilderEmp(nodeInfo, NextStep);
                    result.OnSuccess(nodeInfo);
                }
                else result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR);
            }
            return result;
        }
    }
}
