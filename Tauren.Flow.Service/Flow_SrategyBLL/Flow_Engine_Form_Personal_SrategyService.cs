﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tauren.Flow.DLL.FlowEngine;
using Tauren.Flow.DLL.Resouces;
using Tauren.Flow.Entity.Const;
using Tauren.Flow.Entity.Enum;
using Tauren.Flow.Entity.Global;
using Tauren.Flow.Entity.Output;
using Tauren.Flow.Infrastructure.Globals;

namespace Tauren.Flow.Service.Flow_SrategyBLL
{
    /// <summary>
    /// 表单人员策略
    /// </summary>
    public class Flow_Engine_Form_Personal_SrategyService : Base.BaseService
    {
        private Dictionary<Strategy_Form_Personal_Field, Func<SingeNextNodeInfoRequestParameter, Result<NextNodeInfoOutPut>>> Engine_Personal_Srategy = new Dictionary<Strategy_Form_Personal_Field, Func<SingeNextNodeInfoRequestParameter, Result<NextNodeInfoOutPut>>>();

        private IFlow_SrategyDLL flowEngineDLL;
        public Flow_Engine_Form_Personal_SrategyService(IFlow_SrategyDLL _flowEngineDLL, IConnectionBLLBase connectionBLL, IMenuResourcesBusiness resourcesBusiness) : base(_flowEngineDLL, connectionBLL, resourcesBusiness)
        {
            this.ResourcesInfo = this.GetResources(GlobalConst.RESOURCES_FLAG_MODULE_FLOWCENTER_KEY);
            this.InitFunction();
            this.flowEngineDLL = _flowEngineDLL;
        }

        /// <summary>
        /// 根据表单人员策略获取下一审核信息
        /// </summary>
        /// <param name="requestParameter"></param>
        /// <returns></returns>
        public Result<NextNodeInfoOutPut> LoadNextNodeInfoOnStrategy(SingeNextNodeInfoRequestParameter requestParameter)
        {
            return this.Engine_Personal_Srategy[(Strategy_Form_Personal_Field)requestParameter.NextStep.Strategy.FormFieldApproveType].Invoke(requestParameter);
        }

        /// <summary>
        /// 初始化策略模式
        /// </summary>
        private void InitFunction()
        {
            Engine_Personal_Srategy.Add(Strategy_Form_Personal_Field.Empty, (arg) => { var res = new Result<NextNodeInfoOutPut>(null); res.OnFailure(); return res; });
            Engine_Personal_Srategy.Add(Strategy_Form_Personal_Field.Flow_Form_Field_Current_Personal, LoadFlowApproverByCurrentPersonal);
            Engine_Personal_Srategy.Add(Strategy_Form_Personal_Field.Flow_Form_Field_Current_Personal_Immediate_Superior, LoadFlowCurrentPersonalImmediateSuperior);
            Engine_Personal_Srategy.Add(Strategy_Form_Personal_Field.Flow_Form_Field_Current_Personal_Indirect_Superior, LoadFlowCurrentPersonalIndirectSuperior);
            Engine_Personal_Srategy.Add(Strategy_Form_Personal_Field.Flow_Form_Field_Current_Personal_All_Superior, LoadFlowCurrentPersonalAllSuperior);
        }

        #region 所有上级 

        /// <summary>
        /// 当前处理人的所有上级
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private Result<NextNodeInfoOutPut> LoadFlowCurrentPersonalAllSuperior(SingeNextNodeInfoRequestParameter arg)
        {
            var result = new Result<NextNodeInfoOutPut>(null);
            List<object> ids = GetFormFieldValue(arg);
            if (!ids.Any()) { result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR); return result; }
            List<int> res = flowEngineDLL.GetPersonalALLuperior(string.Join(",", ids));
            if (res != null && res.Any())
            {
                NextNodeInfoOutPut nodeInfo = BuilderEmpNextNodeInfoOutPut(arg.NextStep);
                nodeInfo.NextStepApprover = string.Join(",", res);
                nodeInfo.NextStepApproverEmpId = res.ToList();
                this.BuilderEmp(nodeInfo, arg.NextStep);
                result.OnSuccess(nodeInfo);
            }
            else
                result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR);
            return result;
        }
        #endregion

        #region 间接上级

        /// <summary>
        /// 当前处理人的间接上级
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private Result<NextNodeInfoOutPut> LoadFlowCurrentPersonalIndirectSuperior(SingeNextNodeInfoRequestParameter arg)
        {
            var result = new Result<NextNodeInfoOutPut>(null);
            List<object> ids = GetFormFieldValue(arg);
            if (!ids.Any()) { result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR); return result; }
            var resData = flowEngineDLL.GetPersonalIndirectSuperior(string.Join(",", ids));
            List<object> list = new List<object>();
            foreach (var item in resData)
            {
                var res = item as IDictionary<string, object>;
                if (res != null && res.Any() && !string.IsNullOrEmpty(res["repoter"] + ""))
                    list.Add(res["repoter"]);
            }
            if (list.Any())
            {
                NextNodeInfoOutPut nodeInfo = BuilderEmpNextNodeInfoOutPut(arg.NextStep);
                nodeInfo.NextStepApprover = string.Join(",", list.Distinct());
                nodeInfo.NextStepApproverEmpId = list.Distinct().Select(o => int.Parse(o + "")).ToList();
                this.BuilderEmp(nodeInfo, arg.NextStep);
                result.OnSuccess(nodeInfo);
            }
            else
                result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR);
            return result;
        }
        #endregion

        #region 直接上级


        /// <summary>
        /// 当前处理人的直接上级
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private Result<NextNodeInfoOutPut> LoadFlowCurrentPersonalImmediateSuperior(SingeNextNodeInfoRequestParameter arg)
        {
            var result = new Result<NextNodeInfoOutPut>(null);
            List<object> ids = GetFormFieldValue(arg);
            if (!ids.Any()){ result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR); return result; }
            var resData = flowEngineDLL.GetPersonalImmediateSuperior(string.Join(",", ids));
            List<object> list = new List<object>();
            foreach (var item in resData)
            {
                var res = item as IDictionary<string, object>;
                if (res != null && res.Any() && !string.IsNullOrEmpty(res["repoter"] + ""))
                    list.Add(res["repoter"]);
            }
            if (list.Any())
            {
                NextNodeInfoOutPut nodeInfo = BuilderEmpNextNodeInfoOutPut(arg.NextStep);
                nodeInfo.NextStepApprover = string.Join(",", list.Distinct());
                nodeInfo.NextStepApproverEmpId = list.Distinct().Select(o => int.Parse(o + "")).ToList();
                this.BuilderEmp(nodeInfo, arg.NextStep);
                result.OnSuccess(nodeInfo);
            }
            else
                result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR);
            return result;
        }
        #endregion

        #region 人员
        /// <summary>
        /// 当前处理人
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private Result<NextNodeInfoOutPut> LoadFlowApproverByCurrentPersonal(SingeNextNodeInfoRequestParameter arg)
        {
            var result = new Result<NextNodeInfoOutPut>(null);
            NextNodeInfoOutPut nodeInfo = BuilderEmpNextNodeInfoOutPut(arg.NextStep);
            List<object> ids = GetFormFieldValue(arg);
            if (!ids.Any()) result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR);
            nodeInfo.NextStepApprover = string.Join(",", ids);
            nodeInfo.NextStepApproverEmpId = ids.Select(o => int.Parse(o+"")).ToList();
            this.BuilderEmp(nodeInfo, arg.NextStep);
            if (!nodeInfo.NextStepApproverEmpId.Any())
                result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR);
            else
                result.OnSuccess(nodeInfo);
            return result;
        }
        #endregion
    }
}
