﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tauren.Flow.DLL.FlowEngine;
using Tauren.Flow.DLL.Resouces;
using Tauren.Flow.Entity.Const;
using Tauren.Flow.Entity.Enum;
using Tauren.Flow.Entity.Global;
using Tauren.Flow.Entity.Model;
using Tauren.Flow.Entity.Output;
using Tauren.Flow.Infrastructure.Globals;

namespace Tauren.Flow.Service.Flow_SrategyBLL
{
    /// <summary>
    /// 表单人员策略
    /// </summary>
    public class Flow_Engine_Form_PositionSrategyService : Base.BaseService
    {
        private Dictionary<Strategy_Form_Position_Field, Func<SingeNextNodeInfoRequestParameter, Result<NextNodeInfoOutPut>>> Engine_Position_Srategy = new Dictionary<Strategy_Form_Position_Field, Func<SingeNextNodeInfoRequestParameter, Result<NextNodeInfoOutPut>>>();

        private IFlow_SrategyDLL flowEngineDLL;
        public Flow_Engine_Form_PositionSrategyService(IFlow_SrategyDLL _flowEngineDLL, IConnectionBLLBase connectionBLL, IMenuResourcesBusiness resourcesBusiness) : base(_flowEngineDLL, connectionBLL, resourcesBusiness)
        {
            this.ResourcesInfo = this.GetResources(GlobalConst.RESOURCES_FLAG_MODULE_FLOWCENTER_KEY);
            this.InitFunction();
            this.flowEngineDLL = _flowEngineDLL;
        }

        /// <summary>
        /// 根据表单人员策略获取下一审核信息
        /// </summary>
        /// <param name="requestParameter"></param>
        /// <returns></returns>
        public Result<NextNodeInfoOutPut> LoadNextNodeInfoOnStrategy(SingeNextNodeInfoRequestParameter requestParameter)
        {
            return this.Engine_Position_Srategy[(Strategy_Form_Position_Field)requestParameter.NextStep.Strategy.FormFieldApproveType].Invoke(requestParameter);
        }

        /// <summary>
        /// 初始化策略模式
        /// </summary>
        private void InitFunction()
        {
            Engine_Position_Srategy.Add(Strategy_Form_Position_Field.Empty, (arg) => { var res = new Result<NextNodeInfoOutPut>(null); res.OnFailure(); return res; });
            Engine_Position_Srategy.Add(Strategy_Form_Position_Field.Flow_Form_Field_Current_Position, Load_Flow_Form_Field_Current_Position);
            Engine_Position_Srategy.Add(Strategy_Form_Position_Field.Flow_Form_Field_Current_Position_Immediate_Superior, Load_Flow_Form_Field_Current_Position_Immediate_Superior);
            Engine_Position_Srategy.Add(Strategy_Form_Position_Field.Flow_Form_Field_Current_Position_Indirect_Superior, Load_Flow_Form_Field_Current_Position_Indirect_Superior);
            Engine_Position_Srategy.Add(Strategy_Form_Position_Field.Flow_Form_Field_Current_Position_All_Superior, Load_Flow_Form_Field_Current_Position_All_Superior);

        }

        #region 间接上级职位
        /// <summary>
        /// 间接上级职位
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private Result<NextNodeInfoOutPut> Load_Flow_Form_Field_Current_Position_Indirect_Superior(SingeNextNodeInfoRequestParameter arg)
        {
            var result = new Result<NextNodeInfoOutPut>(null);
            List<object> ids = GetFormFieldValue(arg);
            if (!ids.Any()) { result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR); return result; }
            string positionIds = flowEngineDLL.GetCurrentPositionParentPositionId(string.Join(",", ids));
            result = BuildApprover(positionIds, arg.NextStep);
            return result;
        }
        #endregion

        #region 所有上级职位
        /// <summary>
        /// 所有上级职位
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private Result<NextNodeInfoOutPut> Load_Flow_Form_Field_Current_Position_All_Superior(SingeNextNodeInfoRequestParameter arg)
        {
            var result = new Result<NextNodeInfoOutPut>(null);
            List<object> ids = GetFormFieldValue(arg);
            if (!ids.Any()) { result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR); return result; }
            string positionIds = flowEngineDLL.GetCurrentPositionAllParentPositionId(string.Join(",", ids));
            result = BuildApprover(positionIds, arg.NextStep);
            return result;
        }

        #endregion

        #region 本级
        /// <summary>
        /// 当前职位人员
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private Result<NextNodeInfoOutPut> Load_Flow_Form_Field_Current_Position(SingeNextNodeInfoRequestParameter arg)
        {
            var result = new Result<NextNodeInfoOutPut>(null);
            List<object> ids = GetFormFieldValue(arg);
            if (!ids.Any()) { result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR); return result; }
            result = BuildApprover($"'{(string.Join("','", ids))}'", arg.NextStep);
            return result;
        }
        #endregion

        #region 上级
        /// <summary>
        /// 当前职位上级职位
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private Result<NextNodeInfoOutPut> Load_Flow_Form_Field_Current_Position_Immediate_Superior(SingeNextNodeInfoRequestParameter arg)
        {
            var result = new Result<NextNodeInfoOutPut>(null);
            List<object> ids = GetFormFieldValue(arg);
            if (!ids.Any()) { result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR); return result; }
            string positionIds = flowEngineDLL.GetCurrentPositionPositionId(string.Join(",", ids));
            result = BuildApprover(positionIds, arg.NextStep);
            return result;
        }

        #endregion
        /// <summary>
        /// 将获取到的人员组装到
        /// </summary>
        /// <param name="orgids"></param>
        /// <param name="NextStep"></param>
        /// <returns></returns>
        private Result<NextNodeInfoOutPut> BuildApprover(string positionIds, NodeInfo NextStep)
        {
            var result = new Result<NextNodeInfoOutPut>(null);
            if (string.IsNullOrEmpty(positionIds))
                result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR);
            else
            {
                var res = flowEngineDLL.GetPositionUsers(positionIds, (SendParttimePostion)NextStep.Strategy.SendParttimePostion);
                if (res.Any())
                {
                    NextNodeInfoOutPut nodeInfo = BuilderEmpNextNodeInfoOutPut(NextStep);
                    nodeInfo.NextStepApprover = string.Join(",", res);
                    nodeInfo.NextStepApproverEmpId = res.ToList();
                    this.BuilderEmp(nodeInfo, NextStep);
                    result.OnSuccess(nodeInfo);
                }
                else result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR);
            }
            return result;
        }
    }
}
