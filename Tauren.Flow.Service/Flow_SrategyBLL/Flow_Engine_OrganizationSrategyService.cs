﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tauren.Flow.DLL.FlowEngine;
using Tauren.Flow.DLL.Resouces;
using Tauren.Flow.Entity.Const;
using Tauren.Flow.Entity.Enum;
using Tauren.Flow.Entity.Global;
using Tauren.Flow.Entity.Model;
using Tauren.Flow.Entity.Output;
using Tauren.Flow.Infrastructure.Globals;

namespace Tauren.Flow.Service.Flow_SrategyBLL
{
    /// <summary>
    /// 表单人员策略
    /// </summary>
    public class Flow_Engine_OrganizationSrategyService : Base.BaseService
    {
        private Dictionary<Strategy_Organiztion, Func<SingeNextNodeInfoRequestParameter, Result<NextNodeInfoOutPut>>> Engine_Organization_Srategy = new Dictionary<Strategy_Organiztion, Func<SingeNextNodeInfoRequestParameter, Result<NextNodeInfoOutPut>>>();

        private IFlow_SrategyDLL flowEngineDLL;
        public Flow_Engine_OrganizationSrategyService(IFlow_SrategyDLL _flowEngineDLL, IConnectionBLLBase connectionBLL, IMenuResourcesBusiness resourcesBusiness) : base(_flowEngineDLL, connectionBLL, resourcesBusiness)
        {
            this.ResourcesInfo = this.GetResources(GlobalConst.RESOURCES_FLAG_MODULE_FLOWCENTER_KEY);
            this.InitFunction();
            this.flowEngineDLL = _flowEngineDLL;
        }

        /// <summary>
        /// 根据表单人员策略获取下一审核信息
        /// </summary>
        /// <param name="requestParameter"></param>
        /// <returns></returns>
        public Result<NextNodeInfoOutPut> LoadNextNodeInfoOnStrategy(SingeNextNodeInfoRequestParameter requestParameter)
        {
            return this.Engine_Organization_Srategy[(Strategy_Organiztion)requestParameter.NextStep.Strategy.ApproverType].Invoke(requestParameter);
        }

        /// <summary>
        /// 初始化策略模式
        /// </summary>
        private void InitFunction()
        {
            Engine_Organization_Srategy.Add(Strategy_Organiztion.Empty, (arg) => { var res = new Result<NextNodeInfoOutPut>(null); res.OnFailure(); return res; });
            Engine_Organization_Srategy.Add(Strategy_Organiztion.Flow_Applicant_At_Department_Director, LoadFlowApproverByApplicantAtDepartmentDirector);
            Engine_Organization_Srategy.Add(Strategy_Organiztion.Flow_Initiator_At_Department_Director, LoadFlowApproverByInitiatorAtDepartmentDirector);
            Engine_Organization_Srategy.Add(Strategy_Organiztion.Flow_Current_Handler_At_Department_Director, LoadFlowApproverByCurrentHandlerAtDepartmentDirector);
            Engine_Organization_Srategy.Add(Strategy_Organiztion.Flow_Applicant_At_Immediate_Department_Director, LoadFlowApproverByApplicantAtImmediateDepartmentDirector);
            Engine_Organization_Srategy.Add(Strategy_Organiztion.Flow_Initiator_At_Immediate_Department_Director, LoadFlowApproverByInitiatorAtImmediateDepartmentDirector);
            Engine_Organization_Srategy.Add(Strategy_Organiztion.Flow_Current_Handler_Immediate_At_Department_Director, LoadFlowApproverByCurrentHandlerAtImmediateDepartmentDirector);
            Engine_Organization_Srategy.Add(Strategy_Organiztion.Flow_Applicant_At_Department_ALL_Personal, LoadFlowOrgALLApproverByApplicant);
            Engine_Organization_Srategy.Add(Strategy_Organiztion.Flow_Initiator_At_Department_ALL_Personal, LoadFlowOrgALLApproverByInitiator);
            Engine_Organization_Srategy.Add(Strategy_Organiztion.Flow_Current_Handler_At_Department_ALL_Personal, LoadFlowOrgALLApproverByCurrentHandler);
            Engine_Organization_Srategy.Add(Strategy_Organiztion.Flow_Applicant_At_Department_Leve_N_Principal, LoadFlowOrgLevelNPrincipalByApplicant);
            Engine_Organization_Srategy.Add(Strategy_Organiztion.Flow_Initiator_At_Department_Leve_N_Principal, LoadFlowOrgLevelNPrincipalByInitiator);
            Engine_Organization_Srategy.Add(Strategy_Organiztion.Flow_Current_Handler_At_Department_Leve_N_Principal, LoadFlowOrgLevelNPrincipalByCurrentHandler);

        }

        #region 上N级负责人
        /// <summary>
        /// 获取申请人所在部门上N级负责人
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private Result<NextNodeInfoOutPut> LoadFlowOrgLevelNPrincipalByApplicant(SingeNextNodeInfoRequestParameter arg)
        {
            var result = new Result<NextNodeInfoOutPut>(null);
            Employee employee = baseDLL.GetBasicListByQuery<Employee>("empid", arg.CurrentTask.ApplyEmpId + "").FirstOrDefault();
            List<Vw_Organizationroute> organizationroutes = flowEngineDLL.GetOrgPrincipal(employee.OrgId, employee.UnitInfoId > 0 ? "unit" : "organizational");
            var single = organizationroutes.FirstOrDefault(o => o.Id == o.PId);
            if (single != null)
            {
                var parent = organizationroutes.Where(o => o.Level == (single.Level - arg.NextStep.Strategy.OrgLeve));
                if (parent.Any())
                {
                    NextNodeInfoOutPut nodeInfo = BuilderEmpNextNodeInfoOutPut(arg.NextStep);
                    nodeInfo.NextStepApprover = string.Join(",", parent.Select(o => o.EmpId));
                    nodeInfo.NextStepApproverEmpId = parent.Select(o => o.EmpId).ToList();
                    this.BuilderEmp(nodeInfo, arg.NextStep);
                    result.OnSuccess(nodeInfo);
                }
                else result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR);
            }
            else result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR);
            return result;
        }

        /// <summary>
        /// 获取发起人所在部门上N级负责人
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private Result<NextNodeInfoOutPut> LoadFlowOrgLevelNPrincipalByInitiator(SingeNextNodeInfoRequestParameter arg)
        {
            var result = new Result<NextNodeInfoOutPut>(null);
            Employee employee = baseDLL.GetBasicListByQuery<Employee>("empid", arg.CurrentTask.InitiatorEmpId + "").FirstOrDefault();
            List<Vw_Organizationroute> organizationroutes = flowEngineDLL.GetOrgPrincipal(employee.OrgId, employee.UnitInfoId > 0 ? "unit" : "organizational");
            var single = organizationroutes.FirstOrDefault(o => o.Id == o.PId);
            if (single != null)
            {
                var parent = organizationroutes.Where(o => o.Level == (single.Level - arg.NextStep.Strategy.OrgLeve));
                if (parent.Any())
                {
                    NextNodeInfoOutPut nodeInfo = BuilderEmpNextNodeInfoOutPut(arg.NextStep);
                    nodeInfo.NextStepApprover = string.Join(",", parent.Select(o => o.EmpId));
                    nodeInfo.NextStepApproverEmpId = parent.Select(o => o.EmpId).ToList();
                    this.BuilderEmp(nodeInfo, arg.NextStep);
                    result.OnSuccess(nodeInfo);
                }
                else result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR);
            }
            else result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR);
            return result;
        }

        /// <summary>
        /// 获取当前审批人所在部门上N级负责人
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private Result<NextNodeInfoOutPut> LoadFlowOrgLevelNPrincipalByCurrentHandler(SingeNextNodeInfoRequestParameter arg)
        {
            var result = new Result<NextNodeInfoOutPut>(null);
            Employee employee = baseDLL.GetBasicListByQuery<Employee>("empid", arg.CurrentTask.Reviewer + "").FirstOrDefault();
            List<Vw_Organizationroute> organizationroutes = flowEngineDLL.GetOrgPrincipal(employee.OrgId, employee.UnitInfoId > 0 ? "unit" : "organizational");
            var single = organizationroutes.FirstOrDefault(o => o.Id == o.PId);
            if (single != null)
            {
                var parent = organizationroutes.Where(o => o.Level == (single.Level - arg.NextStep.Strategy.OrgLeve));
                if (parent.Any())
                {
                    NextNodeInfoOutPut nodeInfo = BuilderEmpNextNodeInfoOutPut(arg.NextStep);
                    nodeInfo.NextStepApprover = string.Join(",", parent.Select(o => o.EmpId));
                    nodeInfo.NextStepApproverEmpId = parent.Select(o => o.EmpId).ToList();
                    this.BuilderEmp(nodeInfo, arg.NextStep);
                    result.OnSuccess(nodeInfo);
                }
                else result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR);
            }
            else result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR);
            return result;
        }
        #endregion

        #region 所有人
        /// <summary>
        /// 当前任务处理人所在部门所有人
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private Result<NextNodeInfoOutPut> LoadFlowOrgALLApproverByCurrentHandler(SingeNextNodeInfoRequestParameter arg)
        {
            var result = new Result<NextNodeInfoOutPut>(null);
            var res = flowEngineDLL.GetOrganizationAllUser(arg.CurrentTask.Reviewer + "");
            if (res.Any())
            {
                NextNodeInfoOutPut nodeInfo = BuilderEmpNextNodeInfoOutPut(arg.NextStep);
                nodeInfo.NextStepApprover = string.Join(",", res);
                nodeInfo.NextStepApproverEmpId = res.ToList();
                this.BuilderEmp(nodeInfo, arg.NextStep);
                result.OnSuccess(nodeInfo);
            }
            else result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR);
            return result;
        }
        /// <summary>
        /// 发起人所在部门所有人
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private Result<NextNodeInfoOutPut> LoadFlowOrgALLApproverByInitiator(SingeNextNodeInfoRequestParameter arg)
        {
            var result = new Result<NextNodeInfoOutPut>(null);
            var res = flowEngineDLL.GetOrganizationAllUser(arg.CurrentTask.InitiatorEmpId + "");
            if (res.Any())
            {
                NextNodeInfoOutPut nodeInfo = BuilderEmpNextNodeInfoOutPut(arg.NextStep);
                nodeInfo.NextStepApprover = string.Join(",", res);
                nodeInfo.NextStepApproverEmpId = res.ToList();
                this.BuilderEmp(nodeInfo, arg.NextStep);
                result.OnSuccess(nodeInfo);
            }
            else result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR);
            return result;
        }

        /// <summary>
        /// 申请人所在部门所有人
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private Result<NextNodeInfoOutPut> LoadFlowOrgALLApproverByApplicant(SingeNextNodeInfoRequestParameter arg)
        {
            var result = new Result<NextNodeInfoOutPut>(null);
            var res = flowEngineDLL.GetOrganizationAllUser(arg.CurrentTask.ApplyEmpId + "");
            if (res.Any())
            {
                NextNodeInfoOutPut nodeInfo = BuilderEmpNextNodeInfoOutPut(arg.NextStep);
                nodeInfo.NextStepApprover = string.Join(",", res);
                nodeInfo.NextStepApproverEmpId = res.ToList();
                this.BuilderEmp(nodeInfo, arg.NextStep);
                result.OnSuccess(nodeInfo);
            }
            else result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR);
            return result;
        }
        #endregion

        #region 本级
        /// <summary>
        /// 当前处理人所在部门主管
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private Result<NextNodeInfoOutPut> LoadFlowApproverByCurrentHandlerAtDepartmentDirector(SingeNextNodeInfoRequestParameter arg)
        {
            var result = new Result<NextNodeInfoOutPut>(null);
            string orgids = flowEngineDLL.GetCurrentUserOrgid(arg.CurrentTask.Reviewer + "", (ApproveParttimePostion)arg.NextStep.Strategy.ApproveParttimePostion);
            result = BuildApprover(orgids, arg.NextStep);
            return result;
        }

        /// <summary>
        /// 发起人所在部门主管
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private Result<NextNodeInfoOutPut> LoadFlowApproverByInitiatorAtDepartmentDirector(SingeNextNodeInfoRequestParameter arg)
        {
            var result = new Result<NextNodeInfoOutPut>(null);
            string orgids = flowEngineDLL.GetCurrentUserOrgid(arg.CurrentTask.InitiatorEmpId + "", (ApproveParttimePostion)arg.NextStep.Strategy.ApproveParttimePostion);
            result = BuildApprover(orgids, arg.NextStep);
            return result;
        }

        /// <summary>
        /// 申请人所在部门主管
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private Result<NextNodeInfoOutPut> LoadFlowApproverByApplicantAtDepartmentDirector(SingeNextNodeInfoRequestParameter arg)
        {
            var result = new Result<NextNodeInfoOutPut>(null);
            string orgids = flowEngineDLL.GetCurrentUserOrgid(arg.CurrentTask.ApplyEmpId + "", (ApproveParttimePostion)arg.NextStep.Strategy.ApproveParttimePostion);
            result = BuildApprover(orgids, arg.NextStep);
            return result;
        }
        #endregion

        #region 上级
        /// <summary>
        /// 当前处理人所在部门上级部门主管
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private Result<NextNodeInfoOutPut> LoadFlowApproverByCurrentHandlerAtImmediateDepartmentDirector(SingeNextNodeInfoRequestParameter arg)
        {
            var result = new Result<NextNodeInfoOutPut>(null);
            string orgids = flowEngineDLL.GetCurrentUserParentOrgid(arg.CurrentTask.Reviewer + "", (ApproveParttimePostion)arg.NextStep.Strategy.ApproveParttimePostion);
            result = BuildApprover(orgids, arg.NextStep);
            return result;
        }

        /// <summary>
        /// 发起人所在部门上级部门主管
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private Result<NextNodeInfoOutPut> LoadFlowApproverByInitiatorAtImmediateDepartmentDirector(SingeNextNodeInfoRequestParameter arg)
        {
            var result = new Result<NextNodeInfoOutPut>(null);
            string orgids = flowEngineDLL.GetCurrentUserParentOrgid(arg.CurrentTask.InitiatorEmpId + "", (ApproveParttimePostion)arg.NextStep.Strategy.ApproveParttimePostion);
            result = BuildApprover(orgids, arg.NextStep);
            return result;
        }

        /// <summary>
        /// 申请人所在部门上级部门主管
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private Result<NextNodeInfoOutPut> LoadFlowApproverByApplicantAtImmediateDepartmentDirector(SingeNextNodeInfoRequestParameter arg)
        {
            var result = new Result<NextNodeInfoOutPut>(null);
            string orgids = flowEngineDLL.GetCurrentUserParentOrgid(arg.CurrentTask.ApplyEmpId + "", (ApproveParttimePostion)arg.NextStep.Strategy.ApproveParttimePostion);
            result = BuildApprover(orgids, arg.NextStep);
            return result;
        }
        #endregion
        /// <summary>
        /// 将获取到的人员组装到
        /// </summary>
        /// <param name="orgids"></param>
        /// <param name="NextStep"></param>
        /// <returns></returns>
        private Result<NextNodeInfoOutPut> BuildApprover(string orgids, NodeInfo NextStep)
        {
            var result = new Result<NextNodeInfoOutPut>(null);
            if (string.IsNullOrEmpty(orgids))
                result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR);
            else
            {
                var res = flowEngineDLL.GetOrgSupervisorPositionUsers(orgids, (SendParttimePostion)NextStep.Strategy.SendParttimePostion);
                if (res.Any())
                {
                    NextNodeInfoOutPut nodeInfo = BuilderEmpNextNodeInfoOutPut(NextStep);
                    nodeInfo.NextStepApprover = string.Join(",", res);
                    nodeInfo.NextStepApproverEmpId = res.ToList();
                    this.BuilderEmp(nodeInfo, NextStep);
                    result.OnSuccess(nodeInfo);
                }
                else result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR);
            }
            return result;
        }
    }
}
