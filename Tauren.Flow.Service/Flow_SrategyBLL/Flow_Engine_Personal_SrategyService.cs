﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tauren.Flow.DLL.FlowEngine;
using Tauren.Flow.DLL.Resouces;
using Tauren.Flow.Entity.Const;
using Tauren.Flow.Entity.Enum;
using Tauren.Flow.Entity.Global;
using Tauren.Flow.Entity.Output;
using Tauren.Flow.Infrastructure.Globals;

namespace Tauren.Flow.Service.Flow_SrategyBLL
{
    public class Flow_Engine_Personal_SrategyService : Base.BaseService, IFlow_Engine_Personal_SrategyService
    {
        private Dictionary<Strategy_Personal, Func<SingeNextNodeInfoRequestParameter, Result<NextNodeInfoOutPut>>> Engine_Personal_Srategy = new Dictionary<Strategy_Personal, Func<SingeNextNodeInfoRequestParameter, Result<NextNodeInfoOutPut>>>();

        private IFlow_SrategyDLL flowEngineDLL;
        public Flow_Engine_Personal_SrategyService(IFlow_SrategyDLL _flowEngineDLL, IConnectionBLLBase connectionBLL, IMenuResourcesBusiness resourcesBusiness) : base(_flowEngineDLL, connectionBLL, resourcesBusiness)
        {
            this.ResourcesInfo = this.GetResources(GlobalConst.RESOURCES_FLAG_MODULE_FLOWCENTER_KEY);
            this.InitFunction();
            this.flowEngineDLL = _flowEngineDLL;
        }

        /// <summary>
        /// 根据人员策略获取下一审核信息
        /// </summary>
        /// <param name="requestParameter"></param>
        /// <returns></returns>
        public Result<NextNodeInfoOutPut> LoadNextNodeInfoOnStrategy(SingeNextNodeInfoRequestParameter requestParameter)
        {
            return this.Engine_Personal_Srategy[(Strategy_Personal)requestParameter.NextStep.Strategy.ApproverType].Invoke(requestParameter);
        }

        /// <summary>
        /// 初始化策略模式
        /// </summary>
        private void InitFunction()
        {
            Engine_Personal_Srategy.Add(Strategy_Personal.Empty, (arg) => { var res = new Result<NextNodeInfoOutPut>(null); res.OnFailure(); return res; });
            Engine_Personal_Srategy.Add(Strategy_Personal.Flow_Applicant, LoadFlowApproverByApplicant);
            Engine_Personal_Srategy.Add(Strategy_Personal.Flow_Initiator, LoadFlowApproverByInitiator);
            Engine_Personal_Srategy.Add(Strategy_Personal.Flow_Current_Handler, LoadFlowApproverByCurrentHandler);
            Engine_Personal_Srategy.Add(Strategy_Personal.Flow_Applicant_Immediate_Superior, LoadFlowApplicantImmediateSuperior);
            Engine_Personal_Srategy.Add(Strategy_Personal.Flow_Initiator_Immediate_Superior, LoadFlowInitiatorImmediateSuperior);
            Engine_Personal_Srategy.Add(Strategy_Personal.Flow_Current_Handler_Immediate_Superior, LoadFlowCurrentHandlerImmediateSuperior);
            Engine_Personal_Srategy.Add(Strategy_Personal.Flow_Applicant_Indirect_Superior, LoadFlowApplicantIndirectSuperior);
            Engine_Personal_Srategy.Add(Strategy_Personal.Flow_Initiator_Indirect_Superior, LoadFlowInitiatorIndirectSuperior);
            Engine_Personal_Srategy.Add(Strategy_Personal.Flow_Current_Handler_Indirect_Superior, LoadFlowCurrentHandlerIndirectSuperior);
            Engine_Personal_Srategy.Add(Strategy_Personal.Flow_Applicant_All_Superior, LoadFlowApplicantAllSuperior);
            Engine_Personal_Srategy.Add(Strategy_Personal.Flow_Initiator_All_Superior, LoadFlowInitiatorAllSuperior);
            Engine_Personal_Srategy.Add(Strategy_Personal.Flow_Current_Handler_All_Superior, LoadFlowCurrentHandlerAllSuperior);
        }

        #region 所有上级
        /// <summary>
        /// 申请人的所有上级
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private Result<NextNodeInfoOutPut> LoadFlowApplicantAllSuperior(SingeNextNodeInfoRequestParameter arg)
        {
            var result = new Result<NextNodeInfoOutPut>(null);
            List<int> res = flowEngineDLL.GetPersonalALLuperior(arg.CurrentTask.ApplyEmpId + "");
            if (res != null && res.Any())
            {
                NextNodeInfoOutPut nodeInfo = BuilderEmpNextNodeInfoOutPut(arg.NextStep);
                nodeInfo.NextStepApprover = string.Join(",", res);
                nodeInfo.NextStepApproverEmpId = res.ToList();
                this.BuilderEmp(nodeInfo, arg.NextStep);
                result.OnSuccess(nodeInfo);
            }
            else
                result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR);
            return result;
        }
        /// <summary>
        /// 发起人的所有上级
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private Result<NextNodeInfoOutPut> LoadFlowInitiatorAllSuperior(SingeNextNodeInfoRequestParameter arg)
        {
            var result = new Result<NextNodeInfoOutPut>(null);
            List<int> res = flowEngineDLL.GetPersonalALLuperior(arg.CurrentTask.InitiatorEmpId + "");
            if (res != null && res.Any())
            {
                NextNodeInfoOutPut nodeInfo = BuilderEmpNextNodeInfoOutPut(arg.NextStep);
                nodeInfo.NextStepApprover = string.Join(",", res);
                nodeInfo.NextStepApproverEmpId = res.ToList();
                this.BuilderEmp(nodeInfo, arg.NextStep);
                result.OnSuccess(nodeInfo);
            }
            else
                result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR);
            return result;
        }

        /// <summary>
        /// 当前处理人的所有上级
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private Result<NextNodeInfoOutPut> LoadFlowCurrentHandlerAllSuperior(SingeNextNodeInfoRequestParameter arg)
        {
            var result = new Result<NextNodeInfoOutPut>(null);
            List<int> res = flowEngineDLL.GetPersonalALLuperior(arg.CurrentTask.Reviewer + "");
            if (res != null && res.Any())
            {
                NextNodeInfoOutPut nodeInfo = BuilderEmpNextNodeInfoOutPut(arg.NextStep);
                nodeInfo.NextStepApprover = string.Join(",", res);
                nodeInfo.NextStepApproverEmpId = res.ToList();
                this.BuilderEmp(nodeInfo, arg.NextStep);
                result.OnSuccess(nodeInfo);
            }
            else
                result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR);
            return result;
        }
        #endregion

        #region 间接上级
        /// <summary>
        /// 申请人的间接上级
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private Result<NextNodeInfoOutPut> LoadFlowApplicantIndirectSuperior(SingeNextNodeInfoRequestParameter arg)
        {
            var result = new Result<NextNodeInfoOutPut>(null);
            var resData = flowEngineDLL.GetPersonalIndirectSuperior(arg.CurrentTask.ApplyEmpId + "");
            IDictionary<string, object> res = null;
            if (resData.Any())
                res = resData.FirstOrDefault() as IDictionary<string, object>;
            if (res != null && !res.Any() && !string.IsNullOrEmpty(res["repoter"] + ""))
            {
                NextNodeInfoOutPut nodeInfo = BuilderEmpNextNodeInfoOutPut(arg.NextStep);
                nodeInfo.NextStepApprover = res["repoter"] + "";
                nodeInfo.NextStepApproverEmpId = new List<int> { int.Parse(res["repoter"] + "") };
                this.BuilderEmp(nodeInfo, arg.NextStep);
                result.OnSuccess(nodeInfo);
            }
            else
                result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR);
            return result;
        }
        /// <summary>
        /// 发起人的间接上级
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private Result<NextNodeInfoOutPut> LoadFlowInitiatorIndirectSuperior(SingeNextNodeInfoRequestParameter arg)
        {
            var result = new Result<NextNodeInfoOutPut>(null);
            var resData = flowEngineDLL.GetPersonalIndirectSuperior(arg.CurrentTask.InitiatorEmpId + "");
            IDictionary<string, object> res = null;
            if (resData.Any())
                res = resData.FirstOrDefault() as IDictionary<string, object>;
            if (res != null && !res.Any() && !string.IsNullOrEmpty(res["repoter"] + ""))
            {
                NextNodeInfoOutPut nodeInfo = BuilderEmpNextNodeInfoOutPut(arg.NextStep);
                nodeInfo.NextStepApprover = res["repoter"] + "";
                nodeInfo.NextStepApproverEmpId = new List<int>{ int.Parse(res["repoter"] + "") };
                this.BuilderEmp(nodeInfo, arg.NextStep);
                result.OnSuccess(nodeInfo);
            }
            else
                result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR);
            return result;
        }

        /// <summary>
        /// 当前处理人的间接上级
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private Result<NextNodeInfoOutPut> LoadFlowCurrentHandlerIndirectSuperior(SingeNextNodeInfoRequestParameter arg)
        {
            var result = new Result<NextNodeInfoOutPut>(null);
            var resData = flowEngineDLL.GetPersonalIndirectSuperior(arg.CurrentTask.Reviewer + "");
            IDictionary<string, object> res = null;
            if (resData.Any())
                res = resData.FirstOrDefault() as IDictionary<string, object>;
            if (res != null && !res.Any() && !string.IsNullOrEmpty(res["repoter"] + ""))
            {
                NextNodeInfoOutPut nodeInfo = BuilderEmpNextNodeInfoOutPut(arg.NextStep);
                nodeInfo.NextStepApprover = res["repoter"] + "";
                nodeInfo.NextStepApproverEmpId = new List<int> { int.Parse(res["repoter"] + "") };
                this.BuilderEmp(nodeInfo, arg.NextStep);
                result.OnSuccess(nodeInfo);
            }
            else
                result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR);
            return result;
        }
        #endregion

        #region 直接上级
        /// <summary>
        /// 申请人的直接上级
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private Result<NextNodeInfoOutPut> LoadFlowApplicantImmediateSuperior(SingeNextNodeInfoRequestParameter arg)
        {
            var result = new Result<NextNodeInfoOutPut>(null);
            var resData = flowEngineDLL.GetPersonalImmediateSuperior(arg.CurrentTask.ApplyEmpId + "");
            IDictionary<string, object> res = null;
            if (resData.Any())
                res = resData.FirstOrDefault() as IDictionary<string, object>;
            if (res != null && res.Any() && !string.IsNullOrEmpty(res["repoter"] + ""))
            {
                NextNodeInfoOutPut nodeInfo = BuilderEmpNextNodeInfoOutPut(arg.NextStep);
                nodeInfo.NextStepApprover = res["repoter"] + "";
                nodeInfo.NextStepApproverEmpId = new List<int> { int.Parse(res["repoter"] + "") };
                this.BuilderEmp(nodeInfo, arg.NextStep);
                result.OnSuccess(nodeInfo);
            }
            else
                result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR);
            return result;
        }
        /// <summary>
        /// 发起人的直接上级
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private Result<NextNodeInfoOutPut> LoadFlowInitiatorImmediateSuperior(SingeNextNodeInfoRequestParameter arg)
        {
            var result = new Result<NextNodeInfoOutPut>(null);
            var resData = flowEngineDLL.GetPersonalImmediateSuperior(arg.CurrentTask.InitiatorEmpId + "");
            IDictionary<string, object> res = null;
            if (resData.Any())
                res = resData.FirstOrDefault() as IDictionary<string, object>;
            if (res != null && res.Any() && !string.IsNullOrEmpty(res["repoter"] + ""))
            {
                NextNodeInfoOutPut nodeInfo = BuilderEmpNextNodeInfoOutPut(arg.NextStep);
                nodeInfo.NextStepApprover = res["repoter"] + "";
                nodeInfo.NextStepApproverEmpId = new List<int> { int.Parse(res["repoter"] + "") };
                this.BuilderEmp(nodeInfo, arg.NextStep);
                result.OnSuccess(nodeInfo);
            }
            else
                result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR);
            return result;
        }

        /// <summary>
        /// 当前处理人的直接上级
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private Result<NextNodeInfoOutPut> LoadFlowCurrentHandlerImmediateSuperior(SingeNextNodeInfoRequestParameter arg)
        {
            var result = new Result<NextNodeInfoOutPut>(null);
            var resData = flowEngineDLL.GetPersonalImmediateSuperior(arg.CurrentTask.Reviewer + "");
            IDictionary<string, object> res = null;
            if (resData.Any())
                res = resData.FirstOrDefault() as IDictionary<string, object>;
            if (res != null && res.Any() && !string.IsNullOrEmpty(res["repoter"] + ""))
            {
                NextNodeInfoOutPut nodeInfo = BuilderEmpNextNodeInfoOutPut(arg.NextStep);
                nodeInfo.NextStepApprover = res["repoter"] + "";
                nodeInfo.NextStepApproverEmpId = new List<int> { int.Parse(res["repoter"] + "") };
                this.BuilderEmp(nodeInfo, arg.NextStep);
                result.OnSuccess(nodeInfo);
            }
            else
                result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR);
            return result;
        }
        #endregion

        #region 人员
        /// <summary>
        /// 当前处理人
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private Result<NextNodeInfoOutPut> LoadFlowApproverByCurrentHandler(SingeNextNodeInfoRequestParameter arg)
        {
            var result = new Result<NextNodeInfoOutPut>(null);
            NextNodeInfoOutPut nodeInfo = BuilderEmpNextNodeInfoOutPut(arg.NextStep);
            nodeInfo.NextStepApprover = arg.CurrentTask.Reviewer + "";
            nodeInfo.NextStepApproverEmpId = new List<int> { arg.CurrentTask.Reviewer };
            this.BuilderEmp(nodeInfo, arg.NextStep);
            if (!nodeInfo.NextStepApproverEmpId.Any())
                result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR);
            else
                result.OnSuccess(nodeInfo);
            return result;
        }

        /// <summary>
        /// 申请人
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private Result<NextNodeInfoOutPut> LoadFlowApproverByApplicant(SingeNextNodeInfoRequestParameter arg)
        {
            var result = new Result<NextNodeInfoOutPut>(null);
            NextNodeInfoOutPut nodeInfo = BuilderEmpNextNodeInfoOutPut(arg.NextStep);
            nodeInfo.NextStepApprover = arg.CurrentTask.ApplyEmpId + "";
            nodeInfo.NextStepApproverEmpId = new List<int> { arg.CurrentTask.ApplyEmpId }; ;
            this.BuilderEmp(nodeInfo, arg.NextStep);
            if (!nodeInfo.NextStepApproverEmpId.Any())
                result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR);
            else
                result.OnSuccess(nodeInfo);
            return result;
        }

        /// <summary>
        /// 发起人
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private Result<NextNodeInfoOutPut> LoadFlowApproverByInitiator(SingeNextNodeInfoRequestParameter arg)
        {
            var result = new Result<NextNodeInfoOutPut>(null);
            NextNodeInfoOutPut nodeInfo = BuilderEmpNextNodeInfoOutPut(arg.NextStep);
            nodeInfo.NextStepApprover = arg.CurrentTask.InitiatorEmpId + "";
            nodeInfo.NextStepApproverEmpId = new List<int> { arg.CurrentTask.InitiatorEmpId };
            this.BuilderEmp(nodeInfo, arg.NextStep);
            if (!nodeInfo.NextStepApproverEmpId.Any())
                result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR);
            else
                result.OnSuccess(nodeInfo);
            return result;
        }
        #endregion         
    }
}
