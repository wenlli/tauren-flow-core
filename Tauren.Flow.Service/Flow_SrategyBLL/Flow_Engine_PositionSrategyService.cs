﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tauren.Flow.DLL.FlowEngine;
using Tauren.Flow.DLL.Resouces;
using Tauren.Flow.Entity.Const;
using Tauren.Flow.Entity.Enum;
using Tauren.Flow.Entity.Global;
using Tauren.Flow.Entity.Model;
using Tauren.Flow.Entity.Output;
using Tauren.Flow.Infrastructure.Globals;

namespace Tauren.Flow.Service.Flow_SrategyBLL
{
    /// <summary>
    /// 表单人员策略
    /// </summary>
    public class Flow_Engine_PositionSrategyService : Base.BaseService
    {
        private Dictionary<Strategy_Position, Func<SingeNextNodeInfoRequestParameter, Result<NextNodeInfoOutPut>>> Engine_Position_Srategy = new Dictionary<Strategy_Position, Func<SingeNextNodeInfoRequestParameter, Result<NextNodeInfoOutPut>>>();

        private IFlow_SrategyDLL flowEngineDLL;
        public Flow_Engine_PositionSrategyService(IFlow_SrategyDLL _flowEngineDLL, IConnectionBLLBase connectionBLL, IMenuResourcesBusiness resourcesBusiness) : base(_flowEngineDLL, connectionBLL, resourcesBusiness)
        {
            this.ResourcesInfo = this.GetResources(GlobalConst.RESOURCES_FLAG_MODULE_FLOWCENTER_KEY);
            this.InitFunction();
            this.flowEngineDLL = _flowEngineDLL;
        }

        /// <summary>
        /// 根据表单人员策略获取下一审核信息
        /// </summary>
        /// <param name="requestParameter"></param>
        /// <returns></returns>
        public Result<NextNodeInfoOutPut> LoadNextNodeInfoOnStrategy(SingeNextNodeInfoRequestParameter requestParameter)
        {
            return this.Engine_Position_Srategy[(Strategy_Position)requestParameter.NextStep.Strategy.ApproverType].Invoke(requestParameter);
        }

        /// <summary>
        /// 初始化策略模式
        /// </summary>
        private void InitFunction()
        {
            Engine_Position_Srategy.Add(Strategy_Position.Empty, (arg) => { var res = new Result<NextNodeInfoOutPut>(null); res.OnFailure(); return res; });
            Engine_Position_Srategy.Add(Strategy_Position.Flow_Applicant_Immediate_Superior, LoadFlowApplicantImmediateSuperior);
            Engine_Position_Srategy.Add(Strategy_Position.Flow_Initiator_Immediate_Superior, LoadFlow_Initiator_Immediate_Superior);
            Engine_Position_Srategy.Add(Strategy_Position.Flow_Current_Handler_Immediate_Superior, LoadFlow_Current_Handler_Immediate_Superior);
            Engine_Position_Srategy.Add(Strategy_Position.Flow_Applicant_Indirect_Superior, LoadFlowApplicantIndirectSuperior);
            Engine_Position_Srategy.Add(Strategy_Position.Flow_Initiator_Indirect_Superior, LoadFlow_Initiator_Indirect_Superior);
            Engine_Position_Srategy.Add(Strategy_Position.Flow_Current_Handler_Indirect_Superior, LoadFlow_Current_Handler_Indirect_Superior);
            Engine_Position_Srategy.Add(Strategy_Position.Flow_Current_Handler_All_Superior, LoadFlow_Current_Handler_ALL_Superior);
            Engine_Position_Srategy.Add(Strategy_Position.Flow_Applicant_All_Superior, LoadFlowApplicantALLSuperior);
            Engine_Position_Srategy.Add(Strategy_Position.Flow_Initiator_All_Superior, LoadFlow_Initiator_ALL_Superior);
        }

        #region 直接上级
        /// <summary>
        /// 申请人所在职位的直接上级职位
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private Result<NextNodeInfoOutPut> LoadFlowApplicantImmediateSuperior(SingeNextNodeInfoRequestParameter arg)
        {
            var result = new Result<NextNodeInfoOutPut>(null);
            string positionIds = flowEngineDLL.GetCurrentUserPositionId(arg.CurrentTask.ApplyEmpId + "", (ApproveParttimePostion)arg.NextStep.Strategy.ApproveParttimePostion);
            result = BuildApprover(positionIds, arg.NextStep);
            return result;
        }
        /// <summary>
        /// 发起人所在职位的直接上级职位
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private Result<NextNodeInfoOutPut> LoadFlow_Initiator_Immediate_Superior(SingeNextNodeInfoRequestParameter arg)
        {
            var result = new Result<NextNodeInfoOutPut>(null);
            string positionIds = flowEngineDLL.GetCurrentUserPositionId(arg.CurrentTask.InitiatorEmpId + "", (ApproveParttimePostion)arg.NextStep.Strategy.ApproveParttimePostion);
            result = BuildApprover(positionIds, arg.NextStep);
            return result;
        }
        /// <summary>
        /// 当前任务处理人所在职位的直接上级职位
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private Result<NextNodeInfoOutPut> LoadFlow_Current_Handler_Immediate_Superior(SingeNextNodeInfoRequestParameter arg)
        {
            var result = new Result<NextNodeInfoOutPut>(null);
            string positionIds = flowEngineDLL.GetCurrentUserPositionId(arg.CurrentTask.Reviewer + "", (ApproveParttimePostion)arg.NextStep.Strategy.ApproveParttimePostion);
            result = BuildApprover(positionIds, arg.NextStep);
            return result;
        }
        #endregion

        #region 间接上级
        /// <summary>
        /// 申请人所在职位的间接上级职位
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private Result<NextNodeInfoOutPut> LoadFlowApplicantIndirectSuperior(SingeNextNodeInfoRequestParameter arg)
        {
            var result = new Result<NextNodeInfoOutPut>(null);
            string positionIds = flowEngineDLL.GetCurrentUserParentPositionId(arg.CurrentTask.ApplyEmpId + "", (ApproveParttimePostion)arg.NextStep.Strategy.ApproveParttimePostion);
            result = BuildApprover(positionIds, arg.NextStep);
            return result;
        }
        /// <summary>
        /// 发起人所在职位的间接上级职位
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private Result<NextNodeInfoOutPut> LoadFlow_Initiator_Indirect_Superior(SingeNextNodeInfoRequestParameter arg)
        {
            var result = new Result<NextNodeInfoOutPut>(null);
            string positionIds = flowEngineDLL.GetCurrentUserParentPositionId(arg.CurrentTask.InitiatorEmpId + "", (ApproveParttimePostion)arg.NextStep.Strategy.ApproveParttimePostion);
            result = BuildApprover(positionIds, arg.NextStep);
            return result;
        }
        /// <summary>
        /// 当前任务处理人所在职位的间接上级职位
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private Result<NextNodeInfoOutPut> LoadFlow_Current_Handler_Indirect_Superior(SingeNextNodeInfoRequestParameter arg)
        {
            var result = new Result<NextNodeInfoOutPut>(null);
            string positionIds = flowEngineDLL.GetCurrentUserParentPositionId(arg.CurrentTask.Reviewer + "", (ApproveParttimePostion)arg.NextStep.Strategy.ApproveParttimePostion);
            result = BuildApprover(positionIds, arg.NextStep);
            return result;
        }
        #endregion

        #region 所有上级
        /// <summary>
        /// 申请人所在职位的所有上级职位
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private Result<NextNodeInfoOutPut> LoadFlowApplicantALLSuperior(SingeNextNodeInfoRequestParameter arg)
        {
            var result = new Result<NextNodeInfoOutPut>(null);
            string positionIds = flowEngineDLL.GetCurrentUserAllParentPositionId(arg.CurrentTask.ApplyEmpId + "", (ApproveParttimePostion)arg.NextStep.Strategy.ApproveParttimePostion);
            result = BuildApprover(positionIds, arg.NextStep);
            return result;
        }
        /// <summary>
        /// 发起人所在职位的所有上级职位
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private Result<NextNodeInfoOutPut> LoadFlow_Initiator_ALL_Superior(SingeNextNodeInfoRequestParameter arg)
        {
            var result = new Result<NextNodeInfoOutPut>(null);
            string positionIds = flowEngineDLL.GetCurrentUserAllParentPositionId(arg.CurrentTask.InitiatorEmpId + "", (ApproveParttimePostion)arg.NextStep.Strategy.ApproveParttimePostion);
            result = BuildApprover(positionIds, arg.NextStep);
            return result;
        }
        /// <summary>
        /// 当前任务处理人所在职位的所有上级职位
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private Result<NextNodeInfoOutPut> LoadFlow_Current_Handler_ALL_Superior(SingeNextNodeInfoRequestParameter arg)
        {
            var result = new Result<NextNodeInfoOutPut>(null);
            string positionIds = flowEngineDLL.GetCurrentUserAllParentPositionId(arg.CurrentTask.Reviewer + "", (ApproveParttimePostion)arg.NextStep.Strategy.ApproveParttimePostion);
            result = BuildApprover(positionIds, arg.NextStep);
            return result;
        }
        #endregion

        /// <summary>
        /// 将获取到的人员组装到
        /// </summary>
        /// <param name="orgids"></param>
        /// <param name="NextStep"></param>
        /// <returns></returns>
        private Result<NextNodeInfoOutPut> BuildApprover(string positionIds, NodeInfo NextStep)
        {
            var result = new Result<NextNodeInfoOutPut>(null);
            if (string.IsNullOrEmpty(positionIds))
                result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR);
            else
            {
                var res = flowEngineDLL.GetPositionUsers(positionIds, (SendParttimePostion)NextStep.Strategy.SendParttimePostion);
                if (res.Any())
                {
                    NextNodeInfoOutPut nodeInfo = BuilderEmpNextNodeInfoOutPut(NextStep);
                    nodeInfo.NextStepApprover = string.Join(",", res);
                    nodeInfo.NextStepApproverEmpId = res.ToList();
                    this.BuilderEmp(nodeInfo, NextStep);
                    result.OnSuccess(nodeInfo);
                }
                else result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR);
            }
            return result;
        }
    }
}
