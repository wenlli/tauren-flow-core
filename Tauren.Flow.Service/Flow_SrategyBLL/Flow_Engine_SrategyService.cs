﻿using System;
using System.Collections.Generic;
using System.Text;
using Tauren.Flow.DLL.Base;
using Tauren.Flow.DLL.FlowEngine;
using Tauren.Flow.DLL.Resouces;
using Tauren.Flow.Entity.Enum;
using Tauren.Flow.Entity.Global;
using Tauren.Flow.Entity.Output;
using Tauren.Flow.Infrastructure.Globals;
using System.Linq;
using Tauren.Flow.Entity.Const;

namespace Tauren.Flow.Service.Flow_SrategyBLL
{
    /// <summary>
    /// 策略信息
    /// </summary>
    public class Flow_Engine_SrategyService : Base.BaseService, IFlow_Engine_SrategyService
    {
        private Dictionary<StrategyType, Func<SingeNextNodeInfoRequestParameter, Result<NextNodeInfoOutPut>>> Engine_Srategy = new Dictionary<StrategyType, Func<SingeNextNodeInfoRequestParameter, Result<NextNodeInfoOutPut>>>();

        private IFlow_Engine_Personal_SrategyService flow_Engine_Personal_SrategyService;
        private Flow_Engine_Form_Personal_SrategyService flow_Engine_Form_Personal_SrategyService;
        private Flow_Engine_OrganizationSrategyService flow_Engine_OrganizationSrategyService;
        private Flow_Engine_Form_OrganizationSrategyService flow_Engine_Form_OrganizationSrategyService;
        private Flow_Engine_Form_PositionSrategyService flow_Engine_Form_PositionSrategyService;
        private Flow_Engine_PositionSrategyService flow_Engine_PositionSrategyService;
        public Flow_Engine_SrategyService(IFlow_SrategyDLL flowEngineDLL, IConnectionBLLBase connectionBLL, IMenuResourcesBusiness resourcesBusiness, IFlow_Engine_Personal_SrategyService _flow_Engine_Personal_SrategyService) : base(flowEngineDLL, connectionBLL, resourcesBusiness)
        {
            this.ResourcesInfo = this.GetResources(GlobalConst.RESOURCES_FLAG_MODULE_FLOWCENTER_KEY);
            flow_Engine_Personal_SrategyService = _flow_Engine_Personal_SrategyService;
            flow_Engine_Form_Personal_SrategyService = new Flow_Engine_Form_Personal_SrategyService(flowEngineDLL, connectionBLL, resourcesBusiness);
            flow_Engine_OrganizationSrategyService = new Flow_Engine_OrganizationSrategyService(flowEngineDLL, connectionBLL, resourcesBusiness);
            flow_Engine_Form_OrganizationSrategyService = new Flow_Engine_Form_OrganizationSrategyService(flowEngineDLL, connectionBLL, resourcesBusiness);
            flow_Engine_Form_PositionSrategyService = new Flow_Engine_Form_PositionSrategyService(flowEngineDLL, connectionBLL, resourcesBusiness);
            flow_Engine_PositionSrategyService = new Flow_Engine_PositionSrategyService(flowEngineDLL, connectionBLL, resourcesBusiness);
            this.InitFunction();
        }

        public Result<List<NextNodeInfoOutPut>> LoadNextNodeInfo(NextNodeInfoRequestParameter requestParameter)
        {
            Result<List<NextNodeInfoOutPut>> result = new Result<List<NextNodeInfoOutPut>>(new List<NextNodeInfoOutPut>());

            foreach (var line in requestParameter.NextLineInfos)
            {
                var nextStep = requestParameter.Flow_OutPut.NodeList.FirstOrDefault(o => o.Meta.StepId == line.Meta.EndId);
                if (nextStep != null)
                {
                    ///判断是否结束节点
                    if (nextStep.Meta.Prop == GlobalConst.NODE_INFO_MEAT_END)
                    {
                        result.OnSuccess(new List<NextNodeInfoOutPut>(){
                                    new NextNodeInfoOutPut() {
                                        ApprovalRangeType=ApprovalRangeType.UnChoosable,
                                        NextStepApprover=GlobalConst.SystemFlowPersonnel.ToString(),
                                        NextStepApproverEmpId=new List<int>{ GlobalConst.SystemFlowPersonnel},
                                        NextStepApproverEmps=new List<ApproverModel>(){
                                            new ApproverModel() {
                                                CNName="系统指定人员",
                                                ENName="System designee",
                                                EmpId=GlobalConst.SystemFlowPersonnel
                                            }
                                        },
                                        NextStepId=nextStep.Meta.StepId,
                                        NextStepCNName=nextStep.Meta.CNName,
                                        NextStepENName=nextStep.Meta.ENName,
                                        FlowTaskType=FlowTaskType.task
                                      }
                      });
                    }
                    else
                    {
                        if (nextStep.Meta.Prop == GlobalConst.NODE_INFO_MEAT_BRANCH || nextStep.Meta.Prop == GlobalConst.NODE_INFO_MEAT_ENBRANCH)
                        {
                            result.Data.Add(new NextNodeInfoOutPut()
                            {
                                NextStepId = nextStep.Meta.StepId
                            });
                        }
                        else
                        {
                            var res = this.Engine_Srategy[(StrategyType)nextStep.Strategy.StrategyType].Invoke(new SingeNextNodeInfoRequestParameter()
                            {
                                CurrentStep = requestParameter.CurrentStep,
                                CurrentTask = requestParameter.CurrentTask,
                                Flow_OutPut = requestParameter.Flow_OutPut,
                                Instance_Businesses = requestParameter.Instance_Businesses,
                                NextStep = nextStep
                            });
                            if (res.Success)
                            {
                                res.Data.IsMultiSelect = (ApprovalStrategyType)nextStep.Strategy.ApprovalStrategy != ApprovalStrategyType.Disable;
                                result.Data.Add(res.Data);
                            }
                        }
                    }
                }
            }
            return result;
        }
        #region private
        /// <summary>
        /// 初始化策略
        /// </summary>
        private void InitFunction()
        {

            ////没有数据的情况下
            Engine_Srategy.Add(StrategyType.UnEmpty, (arg) => { var res = new Result<NextNodeInfoOutPut>(null); res.OnFailure(); return res; });
            ///所有用户
            Engine_Srategy.Add(StrategyType.Allperson, Load_Strategy_All_Personal);
            ///审批人范围
            Engine_Srategy.Add(StrategyType.Range, Load_Strategy_Range);
            ///默认人
            Engine_Srategy.Add(StrategyType.DefaultApprover, Load_Strategy_DefaultValue);
            ///按人员
            Engine_Srategy.Add(StrategyType.Person, flow_Engine_Personal_SrategyService.LoadNextNodeInfoOnStrategy);
            ///按部门
            Engine_Srategy.Add(StrategyType.Organization, flow_Engine_OrganizationSrategyService.LoadNextNodeInfoOnStrategy);
            ///按职位
            Engine_Srategy.Add(StrategyType.Position, flow_Engine_PositionSrategyService.LoadNextNodeInfoOnStrategy);
            ///按表单人员
            Engine_Srategy.Add(StrategyType.Form_Person, flow_Engine_Form_Personal_SrategyService.LoadNextNodeInfoOnStrategy);
            ///按表单部门
            Engine_Srategy.Add(StrategyType.Form_Organization, flow_Engine_Form_OrganizationSrategyService.LoadNextNodeInfoOnStrategy);
            ///按表单职位
            Engine_Srategy.Add(StrategyType.Form_Position, flow_Engine_Form_PositionSrategyService.LoadNextNodeInfoOnStrategy);
        }
        #region 根据策略获取信息

        /// <summary>
        /// 默认审批人
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private Result<NextNodeInfoOutPut> Load_Strategy_DefaultValue(SingeNextNodeInfoRequestParameter arg)
        {
            var result = new Result<NextNodeInfoOutPut>(null);
            var nodeInfo = new NextNodeInfoOutPut() { ApprovalRangeType = ApprovalRangeType.UnChoosable, NextStepId = arg.NextStep.Meta.StepId, NextStepCNName = arg.NextStep.Meta.CNName, NextStepENName = arg.NextStep.Meta.ENName, NextStepApprover = arg.NextStep.Strategy.DefaultApprover };
            this.BuilderDefaultEmp(nodeInfo, arg.NextStep);
            if (nodeInfo.NextStepApproverEmpId == null || !nodeInfo.NextStepApproverEmpId.Any())
                result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR);
            else
                result.OnSuccess(nodeInfo);
            return result;
        }

        /// <summary>
        /// 所有人
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private Result<NextNodeInfoOutPut> Load_Strategy_All_Personal(SingeNextNodeInfoRequestParameter arg)
        {
            var result = new Result<NextNodeInfoOutPut>(null);
            var nextStep = arg.NextStep;
            result.Data = new NextNodeInfoOutPut() { ApprovalRangeType = ApprovalRangeType.Owner, NextStepId = nextStep.Meta.StepId, NextStepCNName = nextStep.Meta.CNName, NextStepENName = nextStep.Meta.ENName };
            return result;
        }

        /// <summary>
        /// 范围
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private Result<NextNodeInfoOutPut> Load_Strategy_Range(SingeNextNodeInfoRequestParameter arg)
        {
            var result = new Result<NextNodeInfoOutPut>(null);
            var nextStep = arg.NextStep;
            if (string.IsNullOrEmpty(nextStep.Strategy.AppoverRange))
                result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NEXTSTEP_HANLDER_ERROR);
            else
                result.Data = new NextNodeInfoOutPut() { ApprovalRangeType = ApprovalRangeType.Position, NextStepId = nextStep.Meta.StepId, NextStepCNName = nextStep.Meta.CNName, NextStepENName = nextStep.Meta.ENName, NextStepApprover = nextStep.Strategy.AppoverRange };
            return result;
        }
        #endregion

        #endregion
        /// <summary>
        /// 组装人员
        /// </summary>
        /// <param name="outPut"></param>  
        private void BuilderDefaultEmp(NextNodeInfoOutPut outPut, NodeInfo nextNodeInfo)
        {
            var empids = new string[] { };
            var Values = new List<FieldValues>();
            string DefaultValue = !string.IsNullOrEmpty(outPut.NextStepApprover) ? outPut.NextStepApprover : "";

            if (!string.IsNullOrEmpty(DefaultValue) && DefaultValue != "0")
            {
                empids = DefaultValue.Split(new string[] { GlobalConst.SEPARATOR_COMMA }, StringSplitOptions.RemoveEmptyEntries);
                Values = empids.Join(this.EmployeesList, a => a, b => b.EmpId + "", (a, b) =>
                {
                    FieldValues res = new FieldValues { Value = a, CNName = b.CNName, ENName = b.ENName };
                    return res;
                }).ToList();
            }
            if (Values.Any())
            {
                if (Values.Count > 1 && (ApprovalStrategyType)nextNodeInfo.Strategy.ApprovalStrategy == ApprovalStrategyType.Disable)
                    outPut.ApprovalRangeType = ApprovalRangeType.Person;
                Values.ForEach(o =>
                {
                    if (!string.IsNullOrEmpty(o.Value))
                    {
                        outPut.NextStepApproverEmpId.Add(int.Parse(o.Value));
                        outPut.NextStepApproverEmps.Add(new ApproverModel() { CNName = o.CNName, ENName = o.ENName, EmpId = int.Parse(o.Value) });
                    }
                });
            }
        }

        /// <summary>
        /// 组装抄送人
        /// </summary>
        /// <param name="requestParameter"></param>
        /// <returns></returns>
        public Result<List<NextNodeInfoOutPut>> LoadCCNextNodeInfo(NextNodeInfoRequestParameter requestParameter)
        {
            Result<List<NextNodeInfoOutPut>> result = new Result<List<NextNodeInfoOutPut>>(new List<NextNodeInfoOutPut>());

            var res = this.Engine_Srategy[(StrategyType)requestParameter.CurrentStep.Strategy.StrategyType].Invoke(new SingeNextNodeInfoRequestParameter()
            {
                CurrentStep = requestParameter.CurrentStep,
                CurrentTask = requestParameter.CurrentTask,
                Flow_OutPut = requestParameter.Flow_OutPut,
                Instance_Businesses = requestParameter.Instance_Businesses,
                NextStep = requestParameter.CurrentStep
            });
            if (res.Success)
            {
                result.Data.Add(res.Data);
            }
            return result;
        }
    }
}
