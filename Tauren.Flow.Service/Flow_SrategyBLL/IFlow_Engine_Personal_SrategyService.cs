﻿using System;
using System.Collections.Generic;
using System.Text;
using Tauren.Flow.Entity.Global;
using Tauren.Flow.Entity.Output;

namespace Tauren.Flow.Service.Flow_SrategyBLL
{
    /// <summary>
    /// 人员相关策略
    /// </summary>
    public interface IFlow_Engine_Personal_SrategyService
    {
        /// <summary>
        /// 根据人员策略获取下一审核信息
        /// </summary>
        /// <param name="requestParameter"></param>
        /// <returns></returns>
        Result<NextNodeInfoOutPut> LoadNextNodeInfoOnStrategy(SingeNextNodeInfoRequestParameter requestParameter);
    }
}
