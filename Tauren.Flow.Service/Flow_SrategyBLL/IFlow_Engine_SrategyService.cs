﻿using System;
using System.Collections.Generic;
using System.Text;
using Tauren.Flow.Entity.Global;
using Tauren.Flow.Entity.Output;

namespace Tauren.Flow.Service.Flow_SrategyBLL
{
    public interface IFlow_Engine_SrategyService
    {
        /// <summary>
        /// 获取下一步节点信息
        /// </summary>
        /// <param name="requestParameter"></param>
        /// <returns></returns>
        Result<List<NextNodeInfoOutPut>> LoadNextNodeInfo(NextNodeInfoRequestParameter requestParameter);
        /// <summary>
        /// 获取当前节点抄送人
        /// </summary>
        /// <param name="requestParameter"></param>
        /// <returns></returns>
        Result<List<NextNodeInfoOutPut>> LoadCCNextNodeInfo(NextNodeInfoRequestParameter requestParameter);
    }
}
