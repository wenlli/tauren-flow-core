﻿using System;
using System.Collections.Generic;
using System.Text;
using Tauren.Flow.Entity.Global;
using Tauren.Flow.Entity.Model;
using Tauren.Flow.Entity.Output;

namespace Tauren.Flow.Service.ProcessBasicInfoBLL
{
    public interface IProcessBasicInfoService
    {
        /// <summary>
        /// 获取流程基本信息
        /// </summary>
        /// <param name="flowId"></param>
        /// <param name="versionId"></param>
        /// <returns></returns>
        Flow_OutPut GetFlowInfo(long flowId, long versionId);
        /// <summary>
        /// 根据任务ID获取步骤信息
        /// </summary>
        /// <returns></returns>
        Result<FormConfig> GetFormFieldInfo(FlowParameter paramter, Flow_OutPut flow_Out);

        /// <summary>
        /// 根据任务ID获取当前任务
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        FlowTaskInfo QueryCurrentFlowTaskInfo(long taskId);
        /// <summary>
        /// 获取所有任务
        /// </summary>
        /// <param name="GroupId"></param>
        /// <returns></returns>
        List<FlowTaskInfo> QueryALLFlowTasks(long groupId);

        /// <summary>
        /// 获取当前任务的上游任务
        /// </summary>
        /// <param name="currentTaskId"></param>
        /// <returns></returns>
        List<ExtFlow_Instance_CurrentTask> QueryPrevTask(long currentTaskId);
    }
}
