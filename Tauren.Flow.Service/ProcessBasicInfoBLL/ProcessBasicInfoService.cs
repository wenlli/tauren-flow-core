﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tauren.Flow.DLL.Base;
using Tauren.Flow.DLL.Field_FormDLL;
using Tauren.Flow.DLL.FlowInfoDLL;
using Tauren.Flow.DLL.Resouces;
using Tauren.Flow.Entity.Const;
using Tauren.Flow.Entity.Enum;
using Tauren.Flow.Entity.Global;
using Tauren.Flow.Entity.Model;
using Tauren.Flow.Entity.Output;
using Tauren.Flow.Infrastructure.Common;
using Tauren.Flow.Infrastructure.Globals;

namespace Tauren.Flow.Service.ProcessBasicInfoBLL
{
    /// <summary>
    /// 流程全局
    /// </summary>
    public class ProcessBasicInfoService : Base.BaseService, IProcessBasicInfoService
    {
        private IFlowInfoBusiness business;
        public IField_FormBusiness formBusiness { get; set; }

        public ProcessBasicInfoService(IFlowInfoBusiness _business, IConnectionBLLBase connectionBLL, IMenuResourcesBusiness resourcesBusiness) : base(_business, connectionBLL, resourcesBusiness)
        {
            this.business = _business;
        }

        /// <summary>
        /// 获取任务
        /// </summary>
        /// <param name="flowId"></param>
        /// <param name="versionId"></param>
        /// <returns></returns>
        public Flow_OutPut GetFlowInfo(long flowId, long versionId)
        {
            string redisKey = $"{Global.TenementCode.ToUpper()}_{GlobalConst.FLOWINFO_FLAG_REDIS_KEY}_FLOWID_{flowId}";
            string redisHashField = $"{redisKey}_FLOWVERSIONID_{versionId}";
            Flow_OutPut flow_OutPut = RedisClient.HashGet<Flow_OutPut>(redisKey, redisHashField);
            if (flow_OutPut == null)
            {
                ExtFlowInfo Info = business.GetFlowSettingInfo(flowId, versionId);
                if (Info is null) return null;
                if (!string.IsNullOrEmpty(Info.Manager))
                {
                    List<Employee> employees = business.GetBasicListByQuery<Employee>("empid", Info.Manager);
                    employees.ForEach(o =>
                    {
                        Info.Managers.Add(new PersonInfo_OutPut() { CNName = o.CNName, ENName = o.ENName, EmpCode = o.EmpCode, EmpId = o.EmpId });
                    });

                }
                if (!string.IsNullOrEmpty(Info.InstanceManager))
                {
                    List<Employee> employees = business.GetBasicListByQuery<Employee>("empid", Info.InstanceManager);
                    employees.ForEach(o =>
                    {
                        Info.InstanceManagers.Add(new PersonInfo_OutPut() { CNName = o.CNName, ENName = o.ENName, EmpCode = o.EmpCode, EmpId = o.EmpId });
                    });
                }
                if (Info.FlowId > 0)
                {
                    Info.Form = business.Forms(Info.FormId.Value);
                }
                ///节点信息
                List<Flow_NodeInfos> NodeInfos = business.StepInfo(Info.FlowId);
                List<Flow_Buttons_Put> _Buttons = business.Buttons(Info.FlowId);
                List<NodeInfo> Nodes = new List<NodeInfo>();
                this.ViewStepInfos(NodeInfos, Nodes, _Buttons);

                ////连线信息
                List<Flow_Lines_OutPut> _Lines = business.FlowLines(Info.FlowId);
                List<LineInfo> lineInfos = new List<LineInfo>();
                this.ViewLinesInfos(_Lines, lineInfos);
                flow_OutPut = new Flow_OutPut() { FlowProperty = Info, NodeList = Nodes, LinkList = lineInfos };
                AddFlowInfoRedis(flow_OutPut);
            }
            return flow_OutPut;
        }
        /// <summary>
        /// 获取表但字段配置
        /// </summary>
        /// <param name="paramter"></param>
        /// <returns></returns>
        public Result<FormConfig> GetFormFieldInfo(FlowParameter paramter, Flow_OutPut flow_Out)
        {
            Result<FormConfig> result = new Result<FormConfig>(null);
            List<FieldConfig> fieldConfigs = formBusiness.GetGlobalFields(paramter.FormVersionId);
            if (!fieldConfigs.Any()) result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_FLOWINFO_ERROR);

            if (flow_Out is null) result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_FORMINFO_ERROR);
            if (result.Success)
            {
                NodeInfo nodeInfo = null;
                //当有任务时步骤id
                if (!string.IsNullOrEmpty(paramter.StepId))
                {
                    nodeInfo = flow_Out.NodeList.FirstOrDefault(o => o.Meta.StepId == paramter.StepId);
                }
                else
                {
                    nodeInfo = flow_Out.NodeList.FirstOrDefault(o => o.Meta.Prop == GlobalConst.NODE_INFO_MEAT_START);
                }
                if (nodeInfo is null) result.OnFailure(GlobalErrorType.GLOBAL_RES_FLOW_NOTFOUNT_NODEINFO_ERROR);
                if (result.Success)
                {
                    FormConfig config = new FormConfig();
                    List<Field_Form_VersionExt> Flow_Step_VersionExts = business.StepFormInfo(paramter.FlowVersionId).Where(o => o.StepId == nodeInfo.Id).ToList();
                    config.Buttons = nodeInfo.Btns;
                    List<FieldConfig> _Puts = new List<FieldConfig>();
                    if (Flow_Step_VersionExts.Any())
                    {
                        _Puts = fieldConfigs.Join(Flow_Step_VersionExts, a => new { a.AreaCode, a.FieldCode }, b => new { b.AreaCode, b.FieldCode }, (a, b) =>
                        {
                            a.Editor = paramter.ApprovalType == ApprovalType.view || paramter.ApprovalType == ApprovalType.circulated ? false : b.Editor;
                            a.Show = b.Show;
                            a.Required = paramter.ApprovalType == ApprovalType.view || paramter.ApprovalType == ApprovalType.circulated ? false : b.Required;
                            a.AreaShow = b.AreaShow;
                            a.AreaEditor = paramter.ApprovalType == ApprovalType.view || paramter.ApprovalType == ApprovalType.circulated ? false : b.AreaEditor;
                            return a;
                        }).ToList();
                    }
                    else
                    {
                        _Puts = fieldConfigs;
                        _Puts.ForEach(a =>
                        {
                            a.Editor = paramter.ApprovalType == ApprovalType.view || paramter.ApprovalType == ApprovalType.circulated ? false : a.Editor;
                            a.Required = a.Required;
                            a.AreaShow = true;
                            a.AreaEditor = paramter.ApprovalType == ApprovalType.view || paramter.ApprovalType == ApprovalType.circulated ? false : true;
                        });
                    }
                    var _putArea = _Puts.GroupBy(o => new { o.TableCode, o.AreaCNName, o.AreaENName, o.AreaCode, o.AreaUsable, o.MainTable, o.AreaSeq, o.AreaId, o.AreaEditor, o.AreaShow }).ToList();
                    ///节点上不包含的新字段-但是在节点上已存在区域
                    var exceptAraes = fieldConfigs.Where(o => !_Puts.Any(p => p.AreaCode == o.AreaCode && p.FieldCode == o.FieldCode)).ToList();
                    var joinAreas = exceptAraes.Join(_putArea, a => a.AreaCode, b => b.Key.AreaCode, (a, b) =>
                    {
                        a.Show = b.Key.AreaShow && a.Show;
                        a.Editor = b.Key.AreaShow && b.Key.AreaEditor && a.Show && a.Editor;
                        a.Required = b.Key.AreaShow && b.Key.AreaEditor && a.Show && a.Editor && a.Required;
                        a.AreaEditor = b.Key.AreaEditor;
                        a.AreaShow = b.Key.AreaShow;
                        return a;
                    });
                    if (joinAreas.Any()) _Puts.AddRange(joinAreas);
                    ///新加入的字段且该区域不在节点上
                    var resluts = exceptAraes.Where(o => !joinAreas.Any(p => p.AreaCode == o.AreaCode && p.FieldCode == o.FieldCode)).ToList();
                    if (resluts.Any()) _Puts.AddRange(resluts);
                    this.BuilderApplyinfos(_Puts, flow_Out, paramter.ApprovalType);
                    this.BuilderOtherAreaInfos(_Puts, flow_Out, config, paramter.ApprovalType);
                    this.BuilderOpinionAreas(config, paramter.ApprovalType, nodeInfo);
                    result.Data = config;
                }
            }
            return result;
        }


        /// <summary>
        /// 根据任务ID获取当前数据
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        public FlowTaskInfo QueryCurrentFlowTaskInfo(long taskId)
        {
            var currentTask = business.QueryCurrentFlowTaskInfo(taskId);
            return currentTask;
        }
        #region private

        /// <summary>
        /// 组装申请人信息区域-申请人中文名必须显示作为申请人选择
        /// </summary>
        /// <param name=""></param>
        private void BuilderApplyinfos(List<FieldConfig> _Puts, Flow_OutPut flow_Out, ApprovalType approvalType)
        {
            var applyinfos = _Puts.Where(o => o.AreaCode == GlobalConst.FORM_APPLY_EMPLOYEEBASICINFO);
            foreach (var item in applyinfos)
            {
                item.AreaShow = true;
                item.AreaEditor = false;
                item.Editor = false;
                item.AreaEditor = false;
                item.AreaShow = true;
                if (item.FieldCode == "cnname")
                {
                    item.Show = true;
                    ///判断流程是否可以代他人审核
                    if (flow_Out.FlowProperty.SelectApplyEmp != null && flow_Out.FlowProperty.SelectApplyEmp.Value && !GlobalConst.ViewApprovalTypes.Any(o => o == approvalType))
                    {
                        item.Editor = true;
                        item.Required = true;
                    }
                }
            }
        }

        /// <summary>
        /// 组装其他区域模块信息
        /// </summary>
        /// <param name=""></param>
        private void BuilderOtherAreaInfos(List<FieldConfig> _Puts, Flow_OutPut flow_Out, FormConfig config, ApprovalType approvalType)
        {
            FieldConfig ext = _Puts.FirstOrDefault();
            config.FormId = ext.FormId;
            if (ext != null && ext.VersionId != 0)
            {
                config.FlowCNName = flow_Out.FlowProperty.CNName;
                config.FlowENName = flow_Out.FlowProperty.ENName;
                config.VersionNo = ext.VersionNo;
                config.UseApplicantEmpId = ext.UseApplicantEmpId;
                config.FormCNName = ext.FormCNName;
                config.FormENName = ext.FormENName;
                if (_Puts.Any())
                {
                    string tablecode = $"'{string.Join("','", _Puts.Select(o => o.TableCode).Distinct())}'";
                    var fromtables = formBusiness.GetTableByCode(tablecode);
                    var tables = formBusiness.GetTableFields(tablecode);
                    var joinTables = fromtables.Join(tables, a => a.TableCode, b => b.TableCode, (a, b) => { a.TableName = b.TableName; a.TableENName = b.TableENName; return a; }).ToList();
                    var results = _Puts.Join(tables, a => a.TableCode, b => b.TableCode, (a, b) =>
                    {
                        a.AreaUsable = b.TableEnable && !b.TableHaveDelete;
                        return a;
                    }).Distinct().ToList();
                    results.GroupBy(o => new { o.TableCode, o.AreaCNName, o.AreaENName, o.AreaCode, o.AreaUsable, o.MainTable, o.AreaSeq, o.AreaId, o.Presentation, o.Records, o.Rowes, o.Columnes, o.AreaEditor, o.AreaShow }).ToList().OrderBy(o => o.Key.AreaSeq).ToList().ForEach(g => config.Areas.Add(BuilderArea(g.Key, results, joinTables, tables)));
                }
                this.BuliderEmployeeInfo(config, approvalType);

            }
        }

        /// <summary>
        /// 当表单中没有申请人信息但是有员工信息表时员工表单可选择
        /// </summary>
        /// <param name="config"></param>
        private void BuliderEmployeeInfo(FormConfig config, ApprovalType approval)
        {
            if (!config.Areas.Any(o => o.AreaCode == GlobalConst.FORM_APPLY_EMPLOYEEBASICINFO) && config.Areas.Any(o => o.TableCode == "employee") && (approval == ApprovalType.apply || approval == ApprovalType.add))
            {
                var cnname = config.Areas.FirstOrDefault(o => o.TableCode == "employee").Fields.FirstOrDefault(o => o.FieldCode.ToLower() == "cnnanme");
                if (cnname != null)
                {
                    cnname.ControlsType = ControlType.employee.ToString();
                    cnname.Show = true;
                    cnname.Editor = true;
                    cnname.Required = true;
                    var rule = new Dictionary<object, object>();
                    rule.Add("required", true);
                    rule.Add("message", this.CommonText("fieldsrequired"));
                    rule.Add("trigger", "change");
                    cnname.Rules.Add(rule);
                }
            }
        }
        /// <summary>
        /// 组装展示节点信息
        /// </summary>
        private void ViewStepInfos(List<Flow_NodeInfos> NodeInfos, List<NodeInfo> Nodes, List<Flow_Buttons_Put> _Buttons)
        {
            NodeInfos.ForEach(node =>
            {
                Nodes.Add(new NodeInfo()
                {
                    Coordinate = JsonEvent.ToEntity<decimal[]>(node.Coordinate),
                    Width = node.Width,
                    Height = node.Height,
                    Id = node.StepId,
                    Meta = new Flow_Step_Info()
                    {
                        StepId = node.StepId,
                        BackOpinion = node.BackOpinion,
                        CCMessage = node.CCMessage,
                        CNName = node.CNName,
                        Coordinate = node.Coordinate,
                        EndOpinion = node.EndOpinion,
                        ENName = node.ENName,
                        HandlerMessage = node.HandlerMessage,
                        Height = node.Height,
                        Note = node.Note,
                        Opinion = node.Opinion,
                        Prop = node.Prop,
                        Required = node.Required,
                        ShowHistory = node.ShowHistory,
                        ShowOpinion = node.ShowOpinion,
                        ShowQuickField = node.ShowQuickField,
                        VersionId = node.VersionId,
                        Width = node.Width,
                        Groups = node.Groups,
                        StartGroup = node.StartGroup,
                        BackScale = node.BackScale,
                        ApprovalScale = node.ApprovalScale,
                        BackStrategy = node.BackStrategy,
                        BranchStrategy = node.BranchStrategy,
                        EndScale = node.EndScale,
                        EndStrategy = node.EndStrategy,
                        BranchTag = node.BranchTag,
                        Line = node.Line,
                    },
                    Strategy = new Flow_Step_Strategy()
                    {
                        AppoverRange = node.AppoverRange,
                        ApprovalStrategy = node.ApprovalStrategy,
                        ApproveParttimePostion = node.ApproveParttimePostion,
                        ApproverType = node.ApproverType,
                        CCApproveParttimePostion = node.CCApproveParttimePostion,
                        CCFormField = node.CCFormField,
                        CCFormFieldApproveType = node.CCFormFieldApproveType,
                        CCOrgLeve = node.CCOrgLeve,
                        CCSendParttimePostion = node.SendParttimePostion,
                        OrgLeve = node.OrgLeve,
                        SendParttimePostion = node.SendParttimePostion,
                        CCStrategyType = node.CCStrategyType,
                        CCType = node.CCType,
                        CCUser = node.CCUser,
                        DefaultApprover = node.DefaultApprover,
                        FormField = node.FormField,
                        FormFieldApproveType = node.FormFieldApproveType,
                        IsAutoSelectStep = node.IsAutoSelectStep,
                        SkipStrategy = node.SkipStrategy,
                        StepId = node.StepId,
                        StrategyType = node.StrategyType,
                        NodeApprovalScale = node.NodeApprovalScale,
                        NodeBackScale = node.NodeBackScale,
                        NodeBackStrategy = node.NodeBackStrategy, 
                    },
                    Btns = _Buttons.Where(o => o.StepId == node.StepId && o.VersionId == node.VersionId).ToList(),
                    Form = null
                });
            });
        }

        /// <summary>
        /// 组装展示连线信息
        /// </summary>
        /// <param name="lines"></param>
        /// <param name="lineInfos"></param>
        private void ViewLinesInfos(List<Flow_Lines_OutPut> Lines, List<LineInfo> LineInfos)
        {
            Lines.ForEach(line =>
            {
                LineInfos.Add(new LineInfo()
                {
                    EndAt = JsonEvent.ToEntity<int[]>(line.EndAt),
                    EndId = line.EndId,
                    Id = line.LinkId,
                    StartAt = JsonEvent.ToEntity<int[]>(line.StartAt),
                    StartId = line.StartId,
                    Meta = line,
                });
            });
        }

        /// <summary>
        /// 添加缓存
        /// </summary>
        /// <param name="flowInfo"></param>
        private void AddFlowInfoRedis(Flow_OutPut flowInfo)
        {
            #region 流程属性缓存存取
            string redisKey = $"{Global.TenementCode.ToUpper()}_{GlobalConst.FLOWINFO_FLAG_REDIS_KEY}_FLOWID_{flowInfo.FlowProperty.FlowId}";
            string redisHashField = $"{redisKey}_FLOWVERSIONID_{flowInfo.FlowProperty.VersionId}";
            RedisClient.HashDelete(redisKey, redisHashField);
            RedisClient.HashSet(redisKey, redisHashField, flowInfo);

            ///连线配置
            string lineKey = $"{redisHashField}_LINEKEY";
            RedisClient.HashDelete(redisHashField, lineKey);
            RedisClient.HashSet(redisHashField, lineKey, flowInfo.LinkList);
            ///节点配置
            string nodeKey = $"{redisHashField}_NODEKEY";
            RedisClient.HashDelete(redisHashField, nodeKey);
            RedisClient.HashSet(redisHashField, nodeKey, flowInfo.NodeList);
            #endregion
        }

        /// <summary>
        /// 组装区域
        /// </summary>
        /// <param name="ext"></param>
        /// <returns></returns>
        private Form_Area BuilderArea(dynamic ext, List<FieldConfig> fields, List<Form_TableInfoExt> fromtables, List<TableFields> tables)
        {
            Form_Area area = new Form_Area()
            {
                AreaCNName = ext.AreaCNName,
                AreaENName = ext.AreaENName,
                AreaCode = ext.AreaCode,
                AreaSeq = ext.AreaSeq,
                AreaId = ext.AreaId,
                TableCode = ext.TableCode,
                Columnes = ext.Columnes,
                MainTable = ext.MainTable,
                Presentation = ext.Presentation,
                Records = ext.Records,
                Rowes = ext.Rowes,
                Usable = ext.AreaUsable,
                Show = ext.AreaShow,
                Editor = ext.AreaEditor
            };
            var baseTab = fromtables.FirstOrDefault(o => o.TableCode == ext.MainTable);
            ///主表都被删除和禁用了则子表也要禁用和删除
            if (baseTab != null)
            {
                if (area.Usable)
                {
                    area.Usable = area.Usable && baseTab.Enable && !baseTab.HaveDelete;
                }
                area.MainTableENName = baseTab.TableENName;
                area.MainTableName = baseTab.TableName;
            }
            ///判断子表是否被禁用或则删除
            if (area.Usable)
            {
                var baseTableInfo = fromtables.FirstOrDefault(o => o.TableCode == ext.TableCode);
                if (baseTableInfo != null)
                    area.Usable = area.Usable && baseTableInfo.Enable && !baseTableInfo.HaveDelete;
            }
            var exs = fields.Where(o => o.AreaId == area.AreaId).Join(tables, a => a.FieldCode, b => b.FieldCode, (a, b) => { a.FieldUsable = area.Usable && b.FieldEnable && !b.FieldHaveDelete; return a; }).ToList();
            var exe = fields.Except(exs).Where(o => o.AreaId == area.AreaId);
            if (exe.Any())
            {
                exs.AddRange(exe);
            }
            exs.GroupBy(field => new
            {
                field.AreaId,
                field.FieldId,
                field.CNName,
                field.ENName,
                field.ControlsType,
                field.ControlSouces,
                field.DefaultValue,
                field.Editor,
                field.FieldCode,
                field.FieldKey,
                field.FLength,
                field.Max,
                field.Min,
                field.Precisions,
                field.Required,
                field.SelectType,
                field.Sequence,
                field.Show,
                field.Type,
                field.FieldUsable
            }).ToList().OrderBy(o => o.Key.Sequence).Where(o => o.Key.FieldId > 0).ToList().ForEach(
                fd => area.Fields.Add(BuilderField(fd.Key, fd.ToList(), area)));
            return area;
        }
        /// <summary>
        /// 组装字段
        /// </summary>
        private Form_Area_Fields BuilderField(dynamic ext, List<FieldConfig> fields, Form_Area area)
        {
            if (ext.FieldId <= 0) return null;
            Form_Area_Fields field = new Form_Area_Fields()
            {
                AreaId = ext.AreaId,
                AutoId = ext.FieldId,
                CNName = ext.CNName,
                ControlSouces = ext.ControlSouces,
                ControlsType = ext.ControlsType,
                DefaultValue = ext.DefaultValue,
                Editor = ext.Editor,
                ENName = ext.ENName,
                FieldCode = ext.FieldCode,
                FieldKey = ext.FieldKey,
                FLength = ext.FLength,
                Max = ext.Max,
                Min = ext.Min,
                Precisions = ext.Precisions,
                Required = ext.Required,
                SelectType = ext.SelectType,
                Sequence = ext.Sequence,
                Show = ext.Show,
                Type = ext.Type,
                Usable = ext.FieldUsable && area.Usable,
            };
            fields.Where(o => o.FieldId == field.AutoId).ToList().ForEach(o =>
            {
                var condition = BuilderCondition(o);
                if (!(condition is null))
                    field.Conditions.Add(condition);
                Dictionary<object, object> rule;
                var regular = BuilderRegular(o, out rule);
                if (!(regular is null))
                {
                    field.Regulars.Add(regular);
                }
                if (rule != null && rule.Any())
                {
                    field.Rules.Add(rule);
                }

            });
            if (field.Required && field.Show && field.Editor)
            {
                var rule = new Dictionary<object, object>();
                rule.Add("required", true);
                rule.Add("message", this.CommonText("fieldsrequired"));
                if (field.ControlsType == ControlType.text.ToString() || field.ControlsType == ControlType.textarea.ToString())
                    rule.Add("trigger", "blur");
                else rule.Add("trigger", "change");
                field.Rules.Add(rule);
            }
            return field;
        }

        /// <summary>
        /// 组装字段规则
        /// </summary>
        /// <param name="fc"></param>
        /// <returns></returns>
        private Field_Form_Area_Fields_Regular BuilderRegular(FieldConfig fc, out Dictionary<object, object> rule)
        {
            rule = new Dictionary<object, object>();
            if (!string.IsNullOrEmpty(fc.Regulars) && !string.IsNullOrEmpty(fc.Regulars))
            {
                Fields_Regular dys = fc.Regulars.ToEntity<Fields_Regular>();
                if (!string.IsNullOrEmpty(dys.trigger + "") && !string.IsNullOrEmpty(dys.pattern + "") && !string.IsNullOrEmpty(dys.message + ""))
                {
                    rule = new Dictionary<object, object>();
                    rule.Add("min", dys.min);
                    rule.Add("pattern", dys.pattern);
                    rule.Add("message", dys.message);
                    rule.Add("trigger", dys.trigger);
                }
                if (!string.IsNullOrEmpty(dys.trigger + "") && !string.IsNullOrEmpty(dys.min + "") && !string.IsNullOrEmpty(dys.max + "") && !string.IsNullOrEmpty(dys.message + ""))
                {
                    rule = new Dictionary<object, object>();
                    rule.Add("min", dys.min);
                    rule.Add("max", dys.max);
                    rule.Add("message", dys.message);
                    rule.Add("trigger", dys.trigger);
                }
                return new Field_Form_Area_Fields_Regular()
                {
                    FieldKey = fc.FieldKey,
                    Regulars = fc.Regulars
                };

            }
            else return null;
        }

        /// <summary>
        /// 组装字段条件
        /// </summary>
        /// <param name="fc"></param>
        /// <returns></returns>
        private Field_Form_Area_Fields_Condition BuilderCondition(FieldConfig fc)
        {
            if (!string.IsNullOrEmpty(fc.CascadeField) && !string.IsNullOrEmpty(fc.CascadeFieldValue))
                return new Field_Form_Area_Fields_Condition()
                {
                    Associated = fc.Associated,
                    CascadeField = fc.CascadeField,
                    CascadeFieldValue = fc.CascadeFieldValue,
                    Condition = fc.Condition,
                    FieldKey = fc.FieldKey,
                    LeftBracket = fc.LeftBracket,
                    RightBracket = fc.RightBracket
                };
            else return null;
        }

        public List<FlowTaskInfo> QueryALLFlowTasks(long groupId)
        {
            return this.business.QueryALLFlowTasks(groupId);
        }

        public List<ExtFlow_Instance_CurrentTask> QueryPrevTask(long currentTaskId) => business.GetCurrentTaskPrevTask(currentTaskId);
        #endregion

        /// <summary>
        /// 组装意见框
        /// </summary>
        /// <param name="_Puts"></param>
        /// <param name="flow_Out"></param>
        /// <param name="config"></param>
        /// <param name="approvalType"></param>
        private void BuilderOpinionAreas(FormConfig config, ApprovalType approvalType, NodeInfo nodeInfo)
        {
            if (approvalType == ApprovalType.approval && nodeInfo.Meta.ShowOpinion)
            {
                var areaId = LongCommon.Default.GenerateRandomCode();
                Form_Area_Fields field = new Form_Area_Fields()
                {
                    AreaId = areaId,
                    CNName = "审批意见",
                    ControlsType = ControlType.textarea.ToString(),
                    Editor = true,
                    FieldCode = "opinion",
                    FieldKey = "flow_task$opinion",
                    Show = true,
                    Usable = true,
                    Type = FieldType.varchar.ToString(),
                    FLength = 500,
                    DefaultValue = nodeInfo.Meta.Opinion + "",
                    ENName = "Approval Opinion"
                };
                if (nodeInfo.Meta.Required)
                {
                    var rule = new Dictionary<object, object>();
                    rule.Add("required", true);
                    rule.Add("message", this.CommonText("fieldsrequired"));
                    rule.Add("trigger", "blur");
                    field.Rules.Add(rule);
                }
                config.Opinion = field;
            }
        }

    }
}
