﻿using Tauren.Flow.Entity.Config;

namespace Tauren.Flow.Service.System_Log
{
    /// <summary>
    /// 日志业务逻辑处理
    /// </summary>
    public interface ISystemLogService
    {
        /// <summary>
        /// 新增租户
        /// </summary>
        /// <param name="tenant"></param>
        /// <returns></returns>
        IResponseMessage Add(Entity.Model.SystemLog Model); 
    }
}
