﻿using Tauren.Flow.DLL.System_Log;
using Tauren.Flow.Entity.Config;
using Tauren.Flow.Entity.Model;

namespace Tauren.Flow.Service.System_Log
{
    /// <summary>
    /// 日志业务逻辑处理
    /// </summary>
    public class SystemLogService : ISystemLogService
    {
        public SystemLogService(ISystemLogBusiness _Business)
        {
            this.Business = _Business;
        }

        public ISystemLogBusiness Business { get; set; }

        public IResponseMessage Add(SystemLog Model) => Business.Add(Model).Succeed();         
    }
}
